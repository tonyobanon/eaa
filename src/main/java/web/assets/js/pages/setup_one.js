
function load() {
	
	$('.ui.basic.modal')
	  .modal('show');

	getCountries().then(countries => {
		
		var countriesMenu = $('input[name="company_country"] ~ .menu');
		
		for (var i in countries) {
			countriesMenu.append("<div class=\"item\" data-value=\"" + i + "\"><i class=\"" + i.toLowerCase() + " flag\"></i>" +  countries[i] + "</div>");
		}
			
		$('input[name="company_country"]').parent().dropdown({
			onChange: (value, text, choice) => {
				
				getSpokenLanguages(value).then(languages => {
					
					var languagesMenu = $('input[name="company_language"] ~ .menu').html("");
					
					for(var i in languages){
						languagesMenu.append("<div class=\"item\" data-value=\"" + i + "\">" +  languages[i] + "</div>");
					}
					
					$('input[name="company_language"]').parent().removeClass("disabled").dropdown();
					
				});
			}
		});
		
		var audiencesMenu = $('input[name="company_audience"] ~ .menu');
		for (var i in countries) {
			audiencesMenu.append("<div class=\"item\" data-value=\"" + i + "\"><i class=\"" + i.toLowerCase() + " flag\"></i>" +  countries[i] + "</div>");
		}	
		$('input[name="company_audience"]').parent().dropdown();
		
	});
	
	$('input[name="company_audience"]').parent().dropdown();
	$('input[name="terms_of_service"]').parent().checkbox();
	
	
	 $('.ui .form').form({
         fields: {
        	 company_name: {
        		 identifier: 'company_name',
        		 rules: [{
        			 type: 'empty',
        			 prompt: 'Please enter your company name'
        		 }]
        	 },
        	 company_country: {
        		 identifier: 'company_country',
        		 rules: [{
        			 type: 'empty',
        			 prompt: 'Please select the country you currently operate'
        		 }]
        	 },
        	 company_languge: {
        		 identifier: 'company_language',
        		 rules: [{
        			 type: 'empty',
        			 prompt: 'Please select your language'
        		 }]
        	 },
        	 company_audience: {
        		 identifier: 'company_audience',
        		 rules: [{
        			 type: 'empty',
        			 prompt: 'Please select your audience'
        		 }]
        	 },
        	 
        	 terms_of_service: {
        		 identifier: 'terms_of_service',
        		 rules: [{
        			 type: 'checked',
        			 prompt: 'You must agree the terms and conditions'
        		 }]
        	 }},
        	 
        	 inline: true,
        	 on: 'blur',
        	 keyboardShortcuts: false,
        	 onSuccess: function () {
        		 
        		 $(".ui .form .button").addClass("disabled");
        		 
        		 // Save to Local Storage, and continue to next step
        		 window.localStorage.setItem("company_name", $('input[name="company_name"]').val());
        		 window.localStorage.setItem("company_logo", $('input[name="company_logo"]').val()  === undefined ? " " : $('input[name="company_logo"]').val());
        		 window.localStorage.setItem("company_country", $('input[name="company_country"]').val());
        		 window.localStorage.setItem("company_language", $('input[name="company_language"]').val());
        		 window.localStorage.setItem("company_audience", $('input[name="company_audience"]').val());
        	 
        		 window.location = "/setup/two";
        	 },
        	 onFailure: function () {
        	
        	 }
     });
	
	 $(".ui .form .button").on('click', function (e) {
		 $('.ui .form').form('validate form');
		// e.stopPropagation();
	 });
	
}


function finishWithDefaults(){
	
	var payload = {};
	payload["companyName"]	  = "Kylantis, Inc";
	payload["companyLogoUrl"] = " ";
	payload["country"] 		  = "NG";
	payload["language"]		  = "NG.05";
	payload["audience"] 	  = "1";
	
	var profileList = [];
	profileList[0] = {
			email:"support@kylantis.com",
			password:"admin",
			fname:"Kylantis",
			lname:"HMO",
			dob:"1/1/2017",
			address:"36, Victoria Street",
			phone:"08165414256",
			country:"NG",
			territory:"NG.05",
			city:"2566680"
	}
	
	payload["admins"] = profileList;
		
	showSpinner();
	doSetup(payload);

	// @TODO: Clear local storage entries
}
