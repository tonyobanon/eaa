package com.kylantis.eaa.core.fusion;

import javax.servlet.http.HttpServletResponse;
import io.vertx.ext.web.RoutingContext;

public class Handlers {

	public static void APIAuthHandler(RoutingContext ctx) {

		String serviceUri = ctx.request().path().replace("/api", "").replaceAll("/[a-zA-Z]+\\z", "");

		Integer functionalityId = WebRoutes.routesMappings.get(ctx.request().path().replace("/api", ""));

		if (functionalityId == null) {
			functionalityId = WebRoutes.routesMappings.get(serviceUri);
		}

		if (functionalityId == null) {
			functionalityId = -1;
		}

		if (functionalityId < 0) {
			ctx.next();
			return;
		}

		boolean hasAccess = false;

		String sessionToken = ctx.request().getHeader(FusionHelper.sessionTokenName());
		Integer userId = FusionHelper.getUserIdFromToken(sessionToken);

		if (userId != null) {
			hasAccess = true;
		}

		if (functionalityId > 0 && hasAccess == true) {
			hasAccess = false;
			for (String roleName : FusionHelper.getRoles(userId)) {

				// Check that this role has the right to view this page
				if(FusionHelper.isRoleAllowed(roleName, serviceUri)){
					hasAccess = true;
					break;
				}
			}
		}

		if (hasAccess) {
			FusionHelper.setUserId(ctx.request(), userId);
			ctx.next();
		} else {
			ctx.response().setStatusCode(HttpServletResponse.SC_UNAUTHORIZED).end();
		}

	}

}
