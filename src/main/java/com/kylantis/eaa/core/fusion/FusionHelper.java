package com.kylantis.eaa.core.fusion;

import java.util.List;

import com.kylantis.eaa.core.base.Redis;
import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.keys.CacheKeys;
import com.kylantis.eaa.core.users.RolesModel;
import com.kylantis.eaa.core.users.UserProfileModel;

import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonArray;

public class FusionHelper {

	public static Integer getUserIdFromToken(String sessionToken) {
		return Redis.getInt(CacheKeys.SESSION_TOKEN_TO_USER_ID_$TOKEN.replace("$TOKEN", sessionToken));
	}

	public static Integer getUserId(HttpServerRequest req) {
		return Integer.parseInt(req.getParam(WebRoutes.USER_ID_PARAM_NAME));
	}
	
	public static void setUserId(HttpServerRequest req, Integer userId) {
		req.params().add(WebRoutes.USER_ID_PARAM_NAME, userId.toString());
	}

	public static List<String> getRoles(Integer userId) {
		return UserProfileModel.getRoles(userId);
	}

	public static final String sessionTokenName() {
		return "X-Session-Token";
	}

	public static String baseFolder() {
		return "web";
	}

	public static boolean isRoleAllowed(String roleName, String uri) {

		// Check that this role has the right to view this page

		List<String> functionalities = FusionHelper.getCachedFunctionalities(roleName);

		if (functionalities == null) {

			functionalities = FusionHelper.getFunctionalities(roleName);

			// Cache role functionalities
			FusionHelper.cacheFunctionalities(roleName, functionalities);
		}

		if (functionalities.contains(WebRoutes.routesMappings.get(uri))) {
			// Do extra stuff
			return true;
		}
		return false;
	}

	public static void cacheFunctionalities(String roleName, List<String> functionalities) {
		Redis.put(CacheKeys.ROLE_FUNCTIONALITIES_$ROLE.replace("$ROLE", roleName),
				new JsonArray(functionalities).toString());
	}

	public static List<String> getCachedFunctionalities(String roleName) {
		String o = Redis.get(CacheKeys.ROLE_FUNCTIONALITIES_$ROLE.replace("$ROLE", roleName));
		if (o != null) {
			return new JsonArray(o).getList();
		}
		return null;
	}

	public static List<Id> getFunctionalities(String roleName) {
		return RolesModel.getFunctionalities(roleName);
	}
}
