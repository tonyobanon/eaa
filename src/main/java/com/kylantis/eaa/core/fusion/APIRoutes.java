package com.kylantis.eaa.core.fusion;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Path;

import javax.servlet.http.HttpServletResponse;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.ClassIdentityType;
import com.kylantis.eaa.core.base.ClasspathScanner;
import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.base.Logger;
import com.kylantis.eaa.core.keys.AppConfigKey;
import com.kylantis.eaa.core.utils.Utils;

import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class APIRoutes {

	protected static Router get(final RouteSet routeSet) {

		final Router router = Router.router(WebServer.vertX);

		// Auth Handler
		router.route().handler(Handlers::APIAuthHandler);

		// Discover Services
		Logger.info("Scanning for services");

		String ext = Application.getConfig(AppConfigKey.CLASSES_SERVICES_EXT);

		for (Class<? extends BaseService> service : new ClasspathScanner<>(ext, BaseService.class,
				ClassIdentityType.SUPER_CLASS).scanClasses()) {

			
			BaseService<?> _serviceInstance = null;
			try {
				_serviceInstance = service.newInstance();
			} catch (InstantiationException | IllegalAccessException ex) {
				Exceptions.throwRuntime(ex);
			}
			final BaseService<?> serviceInstance = _serviceInstance;

			
			// Silently ignore
			if (!service.isAnnotationPresent(EndpointClass.class)) {
				continue;
			}

			EndpointClass classAnnotation = service.getAnnotation(EndpointClass.class);

			if (!WebServer.endpointClassUriPattern.matcher(classAnnotation.uri()).matches()) {
				throw new RuntimeException("Improper URI format for " + service.getName());
			}

			StringBuilder clientFunctions = new StringBuilder();

			for (Method method : service.getMethods()) {

				if (!method.isAnnotationPresent(EndpointMethod.class)) {
					// Silently ignore
					continue;
				}

				EndpointMethod methodAnnotation = method.getAnnotation(EndpointMethod.class);

				if (!WebServer.endpointMethodUriPattern.matcher(methodAnnotation.uri()).matches()) {
					throw new RuntimeException("Improper URI format for " + service.getName() + "/" + method.getName());
				}

				if (methodAnnotation.createXhrClient()) {

					// Generate XHR clients
					clientFunctions.append(RPCFactory.generateXHRClient(classAnnotation, method, methodAnnotation));

				}

				// Add Handler

				if (methodAnnotation.isBlocking()) {
					router.route(methodAnnotation.method(), classAnnotation.uri() + methodAnnotation.uri())
							.blockingHandler(ctx -> {
								handler(serviceInstance, method, methodAnnotation, ctx);
								if (!ctx.response().ended()) {
									ctx.response().end();
								}
							});
				} else {
					router.route(methodAnnotation.method(), classAnnotation.uri() + methodAnnotation.uri())
							.handler(ctx -> {
								handler(serviceInstance, method, methodAnnotation, ctx);
								if (!ctx.response().ended()) {
									ctx.response().end();
								}
							});
				}

			}

			// System.out.println(WebRoutes.webFolderURI.toAbsolutePath());

			Path clientStub = WebRoutes.webFolderURI.resolve(WebServer.XHR_CLIENTS_STUBS_PATH + "/"
					+ service.getSimpleName().replace("Service", "").toLowerCase() + ".js");

			File clientStubFile = clientStub.toFile();

			try {
				if (clientStubFile.exists()) {
					clientStubFile.delete();
				}

				clientStubFile.createNewFile();

				Utils.saveString(clientFunctions.toString(),
						java.nio.file.Files.newOutputStream(clientStubFile.toPath()));
			} catch (IOException e) {
				Exceptions.throwRuntime(e);
			}

		}

		if (routeSet.equals(RouteSet.ALL)) {
			// Modify Router
			router.route("/platform/tools/setup").disable();
		}

		return router;
	}

	private static void handler(BaseService<?> instance, Method method, EndpointMethod methodAnnotation, RoutingContext ctx) {
		// Verify Scheme
		if (methodAnnotation.requireSSL()) {
			if (!ctx.request().isSSL()) {
				ctx.response().setStatusCode(HttpServletResponse.SC_NOT_ACCEPTABLE).end();
			}
		}

		ctx.response()
				// do not allow proxies to cache the data
				.putHeader("Cache-Control", "no-store, no-cache");

		try {
			method.invoke(instance, ctx);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			Exceptions.throwRuntime(e);
		}

	}

}
