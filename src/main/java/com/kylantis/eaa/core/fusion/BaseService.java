package com.kylantis.eaa.core.fusion;

import com.kylantis.eaa.core.base.BaseModel;
import com.kylantis.eaa.core.ml.IdentityType;
import com.kylantis.eaa.core.ml.ModelAdapter;

public abstract class BaseService<T extends BaseModel> extends ModelAdapter<T> {

	@Override
	protected final IdentityType identity() {
		return IdentityType.SERVICE;
	}
	
	@Override
	protected String title() {
		return null;
	}
}
