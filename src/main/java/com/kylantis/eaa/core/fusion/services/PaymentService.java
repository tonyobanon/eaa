package com.kylantis.eaa.core.fusion.services;

import com.kylantis.eaa.core.base.GsonFactory;
import com.kylantis.eaa.core.fusion.BaseService;
import com.kylantis.eaa.core.fusion.EndpointClass;
import com.kylantis.eaa.core.fusion.EndpointMethod;
import com.kylantis.eaa.core.ml.IPaymentModel;

import io.vertx.ext.web.RoutingContext;

@EndpointClass(uri = "/payments")
public class PaymentService extends BaseService<IPaymentModel>{

	@EndpointMethod(uri = "/providerNames")
	public void getPaymentModuleNames(RoutingContext ctx) {
		String json = GsonFactory.newInstance().toJson(getPaymentModel().getProviderNames());
		ctx.response().setChunked(true).write(json).end();
	}

	@EndpointMethod(uri = "/providerParams", requestParams={"moduleName"})
	public void getPaymentModuleParams(RoutingContext ctx) {
		String name = ctx.request().getParam("moduleName");
		String json = GsonFactory.newInstance().toJson(getPaymentModel().getModuleParams(name));
		ctx.response().setChunked(true).write(json).end();
	}
}
