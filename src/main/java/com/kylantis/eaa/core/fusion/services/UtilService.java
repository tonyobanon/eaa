package com.kylantis.eaa.core.fusion.services;

import com.kylantis.eaa.core.fusion.BaseService;
import com.kylantis.eaa.core.fusion.EndpointClass;
import com.kylantis.eaa.core.fusion.EndpointMethod;
import com.kylantis.eaa.core.ml.IPlatformModel;

import io.vertx.ext.web.RoutingContext;

@EndpointClass(uri = "/util/")
public class UtilService extends BaseService<IPlatformModel> {

	@EndpointMethod(uri = "/getAddress")
	public static void getIpAddress(RoutingContext ctx) {
		ctx.response().end(ctx.request().remoteAddress().host());
	}

}
