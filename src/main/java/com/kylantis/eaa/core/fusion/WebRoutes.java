package com.kylantis.eaa.core.fusion;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.tika.Tika;

import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.utils.Utils;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class WebRoutes {

	protected static Path webFolderURI = null;

	public static String DEFAULT_INDEX_URI = "/index";
	public static String DEFAULT_SETUP_URI = "/setup/one";
	
	public static String DEFAULT_CONSOLE_URI = "/console";
	
	public static String DEFAULT_LOGIN_URI = "/login";
	public static String DEFAULT_REGISTER_URI = "/register";

	private static Pattern webpagePattern = Pattern
			.compile("\\A[a-zA-Z]+(\\Q_\\E[a-zA-Z]+)*\\Q.\\E(html|htm|xhtml){1}\\z");
	// Let's be lenient a little with the fileName
	// .compile("\\A[a-zA-Z]+(\\Q_\\E[a-zA-Z]+)*\\Q.\\E[a-zA-Z]+\\z");

	public static Tika TIKA_INSTANCE = new Tika();

	protected static Map<String, Integer> routesMappings = new HashMap<>();

	public static final String USER_ID_PARAM_NAME = "x_uid";
	public static final String ENTITY_ID_PARAM_NAME = "x_eId";

	public static Router get(final RouteSet routeSet) {

		final Router router = Router.router(WebServer.vertX);

		try {

			router.route().handler(ctx -> {
				// Improve security
				ctx.response()
						// allow proxies to cache the data
						// .putHeader("Cache-Control", "public,
						// max-age=31536000")

						// @DEV
						.putHeader("Cache-Control", "no-cache")
						// prevents Internet Explorer from MIME -
						// sniffing a
						// response away from the declared content-type
						.putHeader("X-Content-Type-Options", "nosniff")
						// Strict HTTPS (for about ~6Months)
						.putHeader("Strict-Transport-Security", "max-age=" + 15768000)
						// IE8+ do not allow opening of attachments in
						// the context
						// of this resource
						.putHeader("X-Download-Options", "noopen")
						// enable XSS for IE
						.putHeader("X-XSS-Protection", "1; mode=block")
						// deny frames
						.putHeader("X-FRAME-OPTIONS", "DENY");
				ctx.next();
			});

			get(router);

			router.get("/").handler(ctx -> {
				ctx.reroute(routeSet.equals(RouteSet.SETUP_ONLY) ? DEFAULT_SETUP_URI : DEFAULT_INDEX_URI);
			});

			if (routeSet.equals(RouteSet.ALL)) {
				// Modify Router
				router.get("/setup/*").disable();
			}

			return router;

		} catch (IOException e) {
			Exceptions.throwRuntime(e);
			return null;
		}
	}

	private static void get(Router router) throws IOException {

		final Path webFolder = webFolderURI;
		final Path protectedFolder = webFolder.resolve("protected");

		if (protectedFolder != null) {
			JsonObject o = new JsonObject(
					Utils.getString(Files.newInputStream(protectedFolder.resolve("mapping.json"))));
			o.forEach(e -> {
				routesMappings.put(e.getKey(), (Integer) e.getValue());
			});
		}

		// Walk tree inorder to expose all files in the web folder

		Files.walkFileTree(webFolder, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

				Path path = webFolder.relativize(file);

				if (path.getParent() == null) {
					// root files

					String uri = path.toString();

					if (!webpagePattern.matcher(uri).matches()) {
						// Expose as a normal file
						uri = "/" + path.toString().replaceAll("\\\\", "/");
						registerURI(router, uri, file, -1);
						return FileVisitResult.CONTINUE;
					}

					uri = toCanonicalURI(uri);

					registerURI(router, uri, file, -1);

				} else if (path.getParent().toString().equals("protected")) {
					// protected files

					path = protectedFolder.relativize(file);

					String uri = path.toString();

					Integer functionalityId = routesMappings.get(uri);

					if (functionalityId == null) {
						// Since this file is in the protected folder, require
						// the user to log in at the barest minimum
						functionalityId = 0;
					}

					if (!webpagePattern.matcher(uri).matches()) {
						// Expose as a normal file
						uri = "/" + path.toString().replaceAll("\\\\", "/");
						registerURI(router, uri, file, functionalityId);
						return FileVisitResult.CONTINUE;
					}

					uri = toCanonicalURI(uri);

					registerURI(router, uri, file, functionalityId);

				} else {

					String uri = "/" + path.toString().replaceAll("\\\\", "/");
					registerURI(router, uri, file, -1);
				}

				// Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path file, IOException e) throws IOException {
				if (e == null) {
					return FileVisitResult.CONTINUE;
				} else {
					// directory iteration failed
					throw e;
				}
			}
		});

		// watchForChanges();

	}

	private static String toCanonicalURI(String fileName) {
		return "/"
				// replace underscores
				+ fileName.replaceAll("_", "/")
						// remove file extensions
						.split(Pattern.quote("."))[0];
	}

	private static boolean handleAuth(String uri, RoutingContext ctx, int functionalityId) {

		if (functionalityId < 0) {
			return true;
		}

		// Login is required, at the barest minimum
		// Get sessionToken from either a cookie or request header
		String sessionToken;
		try {
			sessionToken = ctx.getCookie(FusionHelper.sessionTokenName()).getValue();
		} catch (NullPointerException e) {
			sessionToken = ctx.request().getHeader(FusionHelper.sessionTokenName());
		}

		if (sessionToken == null) {
			return false;
		}

		// Verify that sessionToken is valid
		Integer userId = FusionHelper.getUserIdFromToken(sessionToken);

		if (userId == null) {
			return false;
		} else if (functionalityId == 0) {
			return true;
		}

		if (functionalityId > 0) {

			for (String roleName : FusionHelper.getRoles(userId)) {
				if(FusionHelper.isRoleAllowed(roleName, uri)){
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * 
	 * 
	 * 
	 * @param secure
	 *            This indicates that the resource should be served only if a
	 *            user is logged in
	 * @param functionalityId
	 *            If this is specified then the currently logged in user must
	 *            have access to this functionality before this resource is
	 *            rendered
	 */
	private static void registerURI(Router router, String uri, Path file, int functionalityId) {

		router.get(uri).handler((ctx) -> {

			// String path = ctx.currentRoute().getPath();

			if (!handleAuth(uri, ctx, functionalityId)) {
				ctx.response().setStatusCode(HttpServletResponse.SC_FOUND)
						.putHeader("Location", DEFAULT_LOGIN_URI + "?returnUrl=" + uri).end();
				return;
			}

			if ((uri.equals(DEFAULT_LOGIN_URI) || uri.equals(DEFAULT_REGISTER_URI))
					&& handleAuth(DEFAULT_LOGIN_URI, ctx, 0)) {
				String returnUrl = ctx.request().getParam("returnUrl");
				if (returnUrl == null) {
					returnUrl = DEFAULT_INDEX_URI;
				}
				ctx.response().setStatusCode(HttpServletResponse.SC_FOUND).putHeader("Location", returnUrl).end();
				return;
			}

			ctx.request().params().forEach(e -> {
				ctx.addCookie(Cookie.cookie(e.getKey(), e.getValue()).setPath(uri));
			});

			try {
				ctx.response().putHeader("content-type", TIKA_INSTANCE.detect(file.toFile())).sendFile(file.toString());
			} catch (IOException e) {
				Exceptions.throwRuntime(e);
			}
		});
	}
	
	public static Integer getRouteMapping(String route){
		return WebRoutes.routesMappings.get(route);
	}

	static {
		try {
			webFolderURI = Paths.get(ClassLoader.getSystemClassLoader().getResource(FusionHelper.baseFolder()).toURI());
		} catch (URISyntaxException ex) {
			Exceptions.throwRuntime(ex);
		}
	}

}
