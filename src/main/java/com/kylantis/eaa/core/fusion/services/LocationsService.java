package com.kylantis.eaa.core.fusion.services;

import javax.servlet.http.HttpServletResponse;

import com.kylantis.eaa.core.base.GsonFactory;
import com.kylantis.eaa.core.base.Redis;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.fusion.BaseService;
import com.kylantis.eaa.core.fusion.EndpointClass;
import com.kylantis.eaa.core.fusion.EndpointMethod;
import com.kylantis.eaa.core.keys.CacheKeys;
import com.kylantis.eaa.core.ml.ILocationModel;

import io.vertx.ext.web.RoutingContext;

@EndpointClass(uri = "/locations/service")
public class LocationsService extends BaseService<ILocationModel> {

	@EndpointMethod(uri = "/countryList")
	public void getCountries(RoutingContext ctx) {
		String json = Redis.get(CacheKeys.COUNTRY_NAMES);
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = GsonFactory.newInstance().toJson(getLocationModel().getCountryNames());
			Redis.put(CacheKeys.COUNTRY_NAMES, json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/currencyList")
	public void getCurrencies(RoutingContext ctx) {
		String json = Redis.get(CacheKeys.CURRENCY_NAMES);
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = GsonFactory.newInstance().toJson(getLocationModel().getCurrencyNames());
			Redis.put(CacheKeys.CURRENCY_NAMES, json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/localeList", requestParams = "countryCode")
	public void getLocales(RoutingContext ctx) {

		String countryCode = ctx.request().getParam("countryCode");

		if (countryCode != null && !countryCode.equals("null")) {
			String json = Redis.get(CacheKeys.LOCALES_$COUNTRY.replace("$COUNTRY", countryCode));
			if (json != null) {
				ctx.response().setChunked(true).write(json);
			} else {
				json = GsonFactory.newInstance().toJson(getLocationModel().getAvailableLocales(countryCode));
				Redis.put(CacheKeys.LOCALES_$COUNTRY.replace("$COUNTRY", countryCode), json);
				ctx.response().setChunked(true).write(json);
			}
		} else {
			String json = Redis.get(CacheKeys.LOCALES);
			if (json != null) {
				ctx.response().setChunked(true).write(json);
			} else {
				json = GsonFactory.newInstance().toJson(getLocationModel().getAllLocales());
				Redis.put(CacheKeys.LOCALES, json);
				ctx.response().setChunked(true).write(json);
			}
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/timezoneList")
	public void getTimezones(RoutingContext ctx) {
		String json = Redis.get(CacheKeys.TIMEZONES);
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = GsonFactory.newInstance().toJson(getLocationModel().getAllTimezones());
			Redis.put(CacheKeys.TIMEZONES, json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/address", requestParams = "addressId")
	public void getAddress(RoutingContext ctx) {
		String addressId = ctx.request().getParam("addressId");
		String json = Redis.get(CacheKeys.ADDRESS_$ADDRESS.replace("$ADDRESS", addressId));
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			try {
				json = GsonFactory.newInstance().toJson(getLocationModel().getAddress(addressId));
				Redis.put(CacheKeys.ADDRESS_$ADDRESS.replace("$ADDRESS", addressId), json);
				ctx.response().setChunked(true).write(json);
			} catch (ResourceException e) {
				ctx.response().setStatusCode(HttpServletResponse.SC_NOT_FOUND);
			}
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/territoryList", requestParams = "countryCode")
	public void getTerritories(RoutingContext ctx) {
		String countryCode = ctx.request().getParam("countryCode");
		String json = Redis.get(CacheKeys.TERRITORIES_$COUNTRY.replace("$COUNTRY", countryCode));
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = GsonFactory.newInstance().toJson(getLocationModel().getTerritoryNames(countryCode));
			Redis.put(CacheKeys.TERRITORIES_$COUNTRY.replace("$COUNTRY", countryCode), json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/cityList", requestParams = "territoryCode")
	public void getCities(RoutingContext ctx) {
		String territoryCode = ctx.request().getParam("territoryCode");
		String json = Redis.get(CacheKeys.CITIES_$TERRITORY.replace("$TERRITORY", territoryCode));
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = GsonFactory.newInstance().toJson(getLocationModel().getCityNames(territoryCode));
			Redis.put(CacheKeys.CITIES_$TERRITORY.replace("$TERRITORY", territoryCode), json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/cityCoordinates", requestParams = "cityCode")
	public void getCityCoordinates(RoutingContext ctx) {
		String cityCode = ctx.request().getParam("cityCode");
		String json = Redis.get(CacheKeys.COORDINATES_$CITY.replace("$CITY", cityCode));
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = GsonFactory.newInstance().toJson(getLocationModel().getCoordinates(Integer.parseInt(cityCode)));
			Redis.put(CacheKeys.COORDINATES_$CITY.replace("$CITY", cityCode), json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/cityTimezone", requestParams = "cityCode")
	public void getCityTimezone(RoutingContext ctx) {
		String cityCode = ctx.request().getParam("cityCode");
		String json = Redis.get(CacheKeys.TIMEZONE_$CITY.replace("$CITY", cityCode));
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = GsonFactory.newInstance().toJson(getLocationModel().getTimezone(Integer.parseInt(cityCode)));
			Redis.put(CacheKeys.TIMEZONE_$CITY.replace("$CITY", cityCode), json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/countryLanguages", requestParams = "countryCode")
	public void getSpokenLanguages(RoutingContext ctx) {
		String countryCode = ctx.request().getParam("countryCode");
		String json = Redis.get(CacheKeys.SPOKEN_LANGUAGES_$COUNTRY.replace("$COUNTRY", countryCode));
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = GsonFactory.newInstance().toJson(getLocationModel().getSpokenLanguages(countryCode));
			Redis.put(CacheKeys.SPOKEN_LANGUAGES_$COUNTRY.replace("$COUNTRY", countryCode), json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/currencyCode", requestParams = "countryCode")
	public void getCurrencyCode(RoutingContext ctx) {
		String countryCode = ctx.request().getParam("countryCode");
		String json = Redis.get(CacheKeys.CURRENCY_CODE_$COUNTRY.replace("$COUNTRY", countryCode));
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = "[\"" + getLocationModel().getCurrencyCode(countryCode) + "\"]";
			Redis.put(CacheKeys.CURRENCY_CODE_$COUNTRY.replace("$COUNTRY", countryCode), json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/currencyName", requestParams = "countryCode")
	public void getCurrencyName(RoutingContext ctx) {
		String countryCode = ctx.request().getParam("countryCode");
		String json = Redis.get(CacheKeys.CURRENCY_NAME_$COUNTRY.replace("$COUNTRY", countryCode));
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = "[\"" + getLocationModel().getCurrencyName(countryCode) + "\"]";
			Redis.put(CacheKeys.CURRENCY_NAME_$COUNTRY.replace("$COUNTRY", countryCode), json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/addressOwner", requestParams = "addressId")
	public void getAddressOwner(RoutingContext ctx) {
		String addressId = ctx.request().getParam("addressId");
		String json = Redis.get(CacheKeys.ADDRESS_OWNER_$ADDRESS.replace("$ADDRESS", addressId));
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = "[\"" + getLocationModel().getAddressOwner(addressId) + "\"]";
			Redis.put(CacheKeys.ADDRESS_OWNER_$ADDRESS.replace("$ADDRESS", addressId), json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

}
