package com.kylantis.eaa.core.fusion.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.kylantis.eaa.core.base.GsonFactory;
import com.kylantis.eaa.core.base.Redis;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.fusion.BaseService;
import com.kylantis.eaa.core.fusion.EndpointClass;
import com.kylantis.eaa.core.fusion.EndpointMethod;
import com.kylantis.eaa.core.keys.CacheKeys;
import com.kylantis.eaa.core.ml.IRolesModel;
import com.kylantis.eaa.core.users.RoleRealm;
import com.kylantis.eaa.core.users.RoleUpdateAction;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

@EndpointClass(uri = "/roles/management")
public class RolesService extends BaseService<IRolesModel> {

	/**
	 * This retrieves all the functionalities applicable to this role realm
	 * */
	@EndpointMethod(uri = "/realm-functionalities", requestParams = { "realm" })
	public void getRealmFunctionalities(RoutingContext ctx) {
		RoleRealm roleRealm = RoleRealm.from(Integer.parseInt(ctx.request().getParam("realm")));
		String json = GsonFactory.newInstance().toJson(getRolesModel().getAvailableFunctionalities(roleRealm));
		ctx.response().setChunked(true).write(json);
		ctx.response().end();
	}

	/**
	 * This retrieves all the functionalities for this role
	 * */
	@EndpointMethod(uri = "/functionalities", requestParams = { "roleName" })
	public void getRoleFunctionalities(RoutingContext ctx) {
		
		String roleName = ctx.request().getParam("roleName");
		List<String> e = getRolesModel().getFunctionalities(roleName);

		Redis.put(CacheKeys.ROLE_FUNCTIONALITIES_$ROLE.replace("$ROLE", roleName),
				new JsonArray(e).toString());

		String json = GsonFactory.newInstance().toJson(e);
		ctx.response().setChunked(true).write(json);
		ctx.response().end();
	}

	@EndpointMethod(uri = "/new-role", bodyParams = { "roleName", "realm"}, method = HttpMethod.PUT)
	public void newRole(RoutingContext ctx) {

		JsonObject body = ctx.getBodyAsJson();

		String roleName = body.getString("roleName");
		RoleRealm roleRealm = RoleRealm.from(body.getInteger("realm"));
		getRolesModel().newRole(roleName, roleRealm);
		ctx.response().end();
	}

	@EndpointMethod(uri = "/update-role", bodyParams = { "roleName", "functionalities", "action" }, method = HttpMethod.POST)
	public void updateRole(RoutingContext ctx) {

		JsonObject body = ctx.getBodyAsJson();

		String roleName = body.getString("roleName");
		String functionality = body.getString("functionality");
		RoleUpdateAction updateAction = RoleUpdateAction.from(body.getInteger("action"));

		getRolesModel().updateFunctionality(roleName, updateAction, functionality);

		Redis.del(CacheKeys.ROLE_FUNCTIONALITIES_$ROLE.replace("$ROLE", roleName));

		ctx.response().end();
	}
	
	@EndpointMethod(uri = "/add-user-role", requestParams = { "user", "role" }, method = HttpMethod.PUT)
	public void addUserRole(RoutingContext ctx) {

		Integer userToUpdate = Integer.parseInt(ctx.request().getParam("user"));
		String role = ctx.request().getParam("role");

		try {
			getRolesModel().addRole(userToUpdate, role);
		} catch (ResourceException e) {
			ctx.response().setStatusCode(HttpServletResponse.SC_FORBIDDEN);
		}
		ctx.response().end();
	}
	
	@EndpointMethod(uri = "/remove-user-role", requestParams = {"user", "role" }, method = HttpMethod.DELETE)
	public void removeUserRole(RoutingContext ctx) {

		Integer userToUpdate = Integer.parseInt(ctx.request().getParam("user"));
		String role = ctx.request().getParam("role");

		try {
			getRolesModel().removeRole(userToUpdate, role);
		} catch (ResourceException e) {
			ctx.response().setStatusCode(HttpServletResponse.SC_FORBIDDEN);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/role", requestParams = { "roleName" }, method = HttpMethod.DELETE)
	public void deleteRole(RoutingContext ctx) {
		String roleName = ctx.request().getParam("roleName");
		getRolesModel().deleteRole(roleName);
		ctx.response().end();
	}

	@EndpointMethod(uri = "/list", method = HttpMethod.GET)
	public void listRoles(RoutingContext ctx) {
		Map<String, RoleRealm> roles = getRolesModel().listRoles();
		ctx.response().write(GsonFactory.newInstance().toJson(roles)).setChunked(true).end();
	}
}
