package com.kylantis.eaa.core.fusion.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.kylantis.eaa.core.base.GsonFactory;
import com.kylantis.eaa.core.content.Blob;
import com.kylantis.eaa.core.fusion.BaseService;
import com.kylantis.eaa.core.fusion.EndpointClass;
import com.kylantis.eaa.core.fusion.EndpointMethod;
import com.kylantis.eaa.core.ml.IBlobStoreModel;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;

@EndpointClass(uri = "/services/blob")
public class BlobStoreService extends BaseService<IBlobStoreModel> {

	@EndpointMethod(uri = "/save", method = HttpMethod.PUT, isBlocking = true, enableMultipart = true)
	public void newBlobEntry(RoutingContext ctx) {
		List<String> blobIds = new ArrayList<>();
		ctx.fileUploads().forEach(f -> {
			try {
				String blobId = getBlobStoreModel().save(null, Files.newInputStream(Paths.get(f.uploadedFileName())));
				blobIds.add(blobId);
			} catch (IOException e) {
				// Silently fail
			}
		});
		ctx.response().write(GsonFactory.newInstance().toJson(blobIds)).setChunked(true);
		ctx.response().end();
	}

	@EndpointMethod(uri = "/get", requestParams = { "blobId" }, createXhrClient = false)
	public void getBlob(RoutingContext ctx) {
		String blobId = ctx.request().getParam("blobId");
		Blob blob = getBlobStoreModel().get(blobId);
		ctx.response().putHeader("Content-Type", blob.getMimeType()).bodyEndHandler(v -> {
			ctx.response().end();
		}).write(Buffer.buffer(blob.getData()));
	}

	@EndpointMethod(uri = "/delete", requestParams = { "blobId" })
	public void deleteBlobEntry(RoutingContext ctx) {
		String blobId = ctx.request().getParam("blobId");
		getBlobStoreModel().delete(blobId);
		ctx.response().end();
	}

}
