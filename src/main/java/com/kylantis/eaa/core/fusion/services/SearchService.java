package com.kylantis.eaa.core.fusion.services;

import javax.servlet.http.HttpServletResponse;

import com.kylantis.eaa.core.base.Redis;
import com.kylantis.eaa.core.fusion.BaseService;
import com.kylantis.eaa.core.ml.ISearchGraphModel;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class SearchService extends BaseService<ISearchGraphModel> {
	
	public void search(RoutingContext ctx) {

		String sessionToken = ctx.request().getParam("token");
		
		JsonObject body = ctx.getBodyAsJson();
		String query = body.getString("query");
		JsonArray entityTypes = body.getJsonArray("entityTypes");
		
		Integer userId = Redis.getInt(sessionToken);
		
		if (userId == null) {
			ctx.response().setStatusCode(HttpServletResponse.SC_UNAUTHORIZED);
		} else {
			
			
			
		}
	}
	
}
