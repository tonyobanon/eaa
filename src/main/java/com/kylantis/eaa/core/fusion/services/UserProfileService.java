package com.kylantis.eaa.core.fusion.services;

import javax.servlet.http.HttpServletResponse;

import com.kylantis.eaa.core.base.GsonFactory;
import com.kylantis.eaa.core.base.Redis;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.base.Todo;
import com.kylantis.eaa.core.fusion.BaseService;
import com.kylantis.eaa.core.fusion.EndpointClass;
import com.kylantis.eaa.core.fusion.EndpointMethod;
import com.kylantis.eaa.core.fusion.FusionHelper;
import com.kylantis.eaa.core.fusion.WebRoutes;
import com.kylantis.eaa.core.keys.CacheKeys;
import com.kylantis.eaa.core.ml.IUserProfileModel;
import com.kylantis.eaa.core.users.AddressSpec;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

@EndpointClass(uri = "/users/profile")
public class UserProfileService extends BaseService<IUserProfileModel> {

	@EndpointMethod(uri = "/role", requestParams = {
			"role" }, method = HttpMethod.POST)
	public void setDefaultRole(RoutingContext ctx) {

		Integer userId = FusionHelper.getUserId(ctx.request());
		String role = ctx.request().getParam("role");

		getUserProfileModel().updateDefaultRole(userId, role);
		ctx.response().end();
	}
	
	@EndpointMethod(uri = "/role")
	public void getDefaultRole(RoutingContext ctx) {
		Integer userId = FusionHelper.getUserId(ctx.request());
		String roleId = getUserProfileModel().getDefaultRole(userId);
		ctx.response().setChunked(true).write(roleId).end();
		ctx.response().end();
	}

	@EndpointMethod(uri = "/roles")
	public void getRoles(RoutingContext ctx) {
		Integer userId = FusionHelper.getUserId(ctx.request());
		String json = GsonFactory.newInstance().toJson(getUserProfileModel().getRoles(userId));
		ctx.response().setChunked(true).write(json).end();
	}
	
	@EndpointMethod(uri = "/id")
	public void getId(RoutingContext ctx) {
		Integer userId = FusionHelper.getUserId(ctx.request());
		ctx.response().setChunked(true).write(userId.toString()).end();
	}
	
	@EndpointMethod(uri = "/get-profile")
	public void getUserProfile(RoutingContext ctx) {
		Integer userId = FusionHelper.getUserId(ctx.request());
		String json = Redis.get(CacheKeys.USER_PROFILE_$USER.replace("$USER", userId.toString()));
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = GsonFactory.newInstance().toJson(getUserProfileModel().getProfile(userId));
			Redis.put(CacheKeys.USER_PROFILE_$USER.replace("$USER", userId.toString()), json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/addresses")
	public void getUserAddresses(RoutingContext ctx) {

		Integer userId = FusionHelper.getUserId(ctx.request());
		String json = Redis.get(CacheKeys.USER_ADDRESSES_$OWNER.replace("$OWNER", "u" + userId.toString()));
		if (json != null) {
			ctx.response().setChunked(true).write(json);
		} else {
			json = GsonFactory.newInstance().toJson(getUserProfileModel().getAddresses(userId));
			Redis.put(CacheKeys.USER_ADDRESSES_$OWNER.replace("$OWNER", "u" + userId.toString()), json);
			ctx.response().setChunked(true).write(json);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/addresses", method = HttpMethod.PUT, bodyParams = { "address" })
	public void addUserAddress(RoutingContext ctx) {

		JsonObject body = ctx.getBodyAsJson();
		String addresss = body.getJsonObject("address").encode();

		Integer userId = FusionHelper.getUserId(ctx.request());

		AddressSpec spec = GsonFactory.newInstance().fromJson(addresss, AddressSpec.class);
		String addressId = getUserProfileModel().addAddress(userId, spec);

		Redis.put(CacheKeys.ADDRESS_$ADDRESS.replace("$ADDRESS", addressId), addresss);
		Redis.put(CacheKeys.ADDRESS_OWNER_$ADDRESS.replace("$ADDRESS", addressId), "u" + userId);

		ctx.response().write("[" + addressId + "]");
		ctx.response().end();
	}

	@EndpointMethod(uri = "/addresses", requestParams = {
			"addressId" }, method = HttpMethod.DELETE)
	public void removeUserAddresss(RoutingContext ctx) {

		Integer userId = FusionHelper.getUserId(ctx.request());
		String addressId = ctx.request().getParam("addressId");

		try {
			getUserProfileModel().removeAddress(userId, addressId);

			Redis.del(CacheKeys.ADDRESS_$ADDRESS.replace("$ADDRESS", addressId));
			Redis.del(CacheKeys.ADDRESS_OWNER_$ADDRESS.replace("$ADDRESS", addressId));

		} catch (ResourceException e) {
			ctx.response().setStatusCode(HttpServletResponse.SC_FORBIDDEN);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/name", requestParams = {
			"name" }, method = HttpMethod.POST)
	public void updateUserLastName(RoutingContext ctx) {

		Integer userId = FusionHelper.getUserId(ctx.request());
		String name = ctx.request().getParam("name");

		try {
			getUserProfileModel().updateName(userId, name);
			Redis.del(CacheKeys.USER_PROFILE_$USER.replace("$USER", userId.toString()));
		} catch (ResourceException e) {
			ctx.response().setStatusCode(HttpServletResponse.SC_NOT_ACCEPTABLE);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/phone", requestParams = {
			"phone" }, method = HttpMethod.POST)
	public void updateUserPhone(RoutingContext ctx) {

		Integer userId = FusionHelper.getUserId(ctx.request());
		Long phone = Long.parseLong(ctx.request().getParam("phone"));

		try {
			getUserProfileModel().updatePhone(userId, phone);
			Redis.del(CacheKeys.USER_PROFILE_$USER.replace("$USER", userId.toString()));
		} catch (ResourceException e) {
			ctx.response().setStatusCode(HttpServletResponse.SC_CONFLICT);
		}
		ctx.response().end();
	}

	@Todo("Prototype alert!! In production, a user's email is updated after clicking on a "
			+ "verification link in his inbox")
	@EndpointMethod(uri = "/email", requestParams = {
			"email" }, method = HttpMethod.POST)
	public void updateUserEmail(RoutingContext ctx) {

		Integer userId = FusionHelper.getUserId(ctx.request());
		String email = ctx.request().getParam("email");

		try {
			getUserProfileModel().updateEmail(userId, email);
			Redis.del(CacheKeys.USER_PROFILE_$USER.replace("$USER", userId.toString()));
		} catch (ResourceException e) {
			ctx.response().setStatusCode(HttpServletResponse.SC_CONFLICT);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/image", requestParams = {
			"blobId" }, method = HttpMethod.POST)
	public void updateUserImage(RoutingContext ctx) {

		Integer userId = FusionHelper.getUserId(ctx.request());
		String blobId = ctx.request().getParam("blobId");

		getUserProfileModel().updateImage(userId, blobId);
		Redis.del(CacheKeys.USER_PROFILE_$USER.replace("$USER", userId.toString()));

		ctx.response().end();
	}

	@EndpointMethod(uri = "/address", requestParams = {
			"addressId" }, method = HttpMethod.POST)
	public void setDefaultAddress(RoutingContext ctx) {

		Integer userId = FusionHelper.getUserId(ctx.request());
		String addressId = ctx.request().getParam("addressId");

		getUserProfileModel().updateDefaultAddress(userId, addressId);
		ctx.response().end();
	}

	@EndpointMethod(uri = "/address")
	public void getDefaultAddress(RoutingContext ctx) {
		Integer userId = FusionHelper.getUserId(ctx.request());
		String addressId = getUserProfileModel().getDefaultAddress(userId);
		ctx.response().setChunked(true).write(addressId).end();
		ctx.response().end();
	}

	@EndpointMethod(uri = "/isNameUpdatable", requestParams = { WebRoutes.USER_ID_PARAM_NAME })
	public void isUserLastNameUpdatable(RoutingContext ctx) {

		Integer userId = FusionHelper.getUserId(ctx.request());
		ctx.response().write("[" + Boolean.toString(getUserProfileModel().isNameChangeAllowed(userId)) + "]");
		ctx.response().end();
	}

}
