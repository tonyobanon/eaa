package com.kylantis.eaa.core.fusion;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import io.vertx.core.http.HttpMethod;

@Retention(RetentionPolicy.RUNTIME)
public @interface EndpointMethod {
	
	String uri();

	boolean createXhrClient() default true;
	
	boolean requireSSL() default false;
	
	boolean isBlocking() default false;
	
	boolean isAsync() default true;
	
	String[] headerParams() default {};

	String[] requestParams() default {};

	String[] bodyParams() default {};
	
	boolean enableMultipart() default false;
	
	HttpMethod method() default HttpMethod.GET;

	int functionId() default -2;
}
