package com.kylantis.eaa.core.fusion;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.kylantis.eaa.core.base.Redis;
import com.kylantis.eaa.core.cron.Scheduler;
import com.kylantis.eaa.core.keys.CacheKeys;

public class Sessions {

	public static final String UI_EVENT_BUS_NAMESPACE = "ui/";
	public static final String APP_EVENT_BUS_NAMESPACE = "app/";

	public static final String USER_SESSIONS_HASH = "user_sessions";
	public static final String SESSION_ADDRESSES_HASH = "session_addresses";


	public static void newSession(Integer userId, String sessionToken, int duration, String remoteAddress,
			TimeUnit timeUnit) {

		// Save token in Redis
		Redis.putInt(CacheKeys.SESSION_TOKEN_TO_USER_ID_$TOKEN.replace("$TOKEN", sessionToken), userId, duration);

		// Store Session Token for this userId

		String currentTokens = Redis.get(USER_SESSIONS_HASH, userId.toString());
		String newTokens = (currentTokens != null ? currentTokens + "," : "") + sessionToken;

		Redis.addFieldToHash(USER_SESSIONS_HASH, userId.toString(), newTokens);

		// Store remote address for this session
		Redis.addFieldToHash(SESSION_ADDRESSES_HASH, sessionToken, remoteAddress);

		// Schedule token deletion on expiration
		Scheduler.schedule(new Runnable() {
			
			@Override
			public void run() {
				removeSession(userId, sessionToken);
			}
		}, duration, timeUnit);
	}

	protected static Integer getUserId(String sessionToken) {
		return Redis.getInt(CacheKeys.SESSION_TOKEN_TO_USER_ID_$TOKEN.replace("$TOKEN", sessionToken));
	}

	protected static void removeSession(String sessionToken) {
		removeSession(getUserId(sessionToken), sessionToken);
	}

	protected static void removeSession(Integer userId, String sessionToken) {

		// Remove Session Token for this userId
		List<String> currentTokens = Splitter.on(",").splitToList(Redis.get(USER_SESSIONS_HASH, userId.toString()));
		currentTokens.remove(sessionToken);

		if (currentTokens.size() > 0) {
			Redis.addFieldToHash(USER_SESSIONS_HASH, userId.toString(), Joiner.on(",").join(currentTokens));
		} else {
			Redis.removeFieldFromHash(USER_SESSIONS_HASH, userId.toString());
		}

		// Remove remote address for this sessionToken
		Redis.removeFieldFromHash(SESSION_ADDRESSES_HASH, sessionToken);

		// Remove Session Data
		Redis.del(CacheKeys.SESSION_DATA_$SESSIONTOKEN.replace("$SESSIONTOKEN", sessionToken));
	}

	protected static void set(String sessionToken, String key, String value) {
		String hashKey = CacheKeys.SESSION_DATA_$SESSIONTOKEN.replace("$SESSIONTOKEN", sessionToken);
		Redis.addFieldToHash(hashKey, key, value);
	}

	protected static String get(String sessionToken, String key) {
		String hashKey = CacheKeys.SESSION_DATA_$SESSIONTOKEN.replace("$SESSIONTOKEN", sessionToken);
		return Redis.get(hashKey, key);
	}

	protected static Long getSessionCount() {
		return Redis.getFieldCount(SESSION_ADDRESSES_HASH);
	}

	protected static Long getUniqueSessionCount() {
		return Redis.getFieldCount(USER_SESSIONS_HASH);
	}
	
	protected static String getRemoteAddress(String sessionToken) {
		return Redis.get(SESSION_ADDRESSES_HASH, sessionToken);
	}

	protected static void upgradeSessionAddress(){
		//Updates the remote address on record for a given session
		
	}
}
