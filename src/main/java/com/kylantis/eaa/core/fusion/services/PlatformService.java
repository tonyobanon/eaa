package com.kylantis.eaa.core.fusion.services;

import javax.servlet.http.HttpServletResponse;

import com.kylantis.eaa.core.base.GsonFactory;
import com.kylantis.eaa.core.fusion.BaseService;
import com.kylantis.eaa.core.fusion.EndpointClass;
import com.kylantis.eaa.core.fusion.EndpointMethod;
import com.kylantis.eaa.core.fusion.RouteSet;
import com.kylantis.eaa.core.fusion.WebRoutes;
import com.kylantis.eaa.core.fusion.WebServer;
import com.kylantis.eaa.core.ml.IPlatformModel;
import com.kylantis.eaa.core.setup.InstallOptions;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

@EndpointClass(uri = "/platform/tools")
public class PlatformService extends BaseService<IPlatformModel> {

	@EndpointMethod(uri = "/setup", bodyParams = { "payload" }, method = HttpMethod.POST, isBlocking = true)
	public void doSetup(RoutingContext context) {

		JsonObject body = context.getBodyAsJson();
		
		InstallOptions spec = GsonFactory.newInstance().fromJson(body.getJsonObject("payload").encode(),
				InstallOptions.class);

		//Perform installation
		getPlatformModel().doInstall(spec);
		

		//For this scenario, The client should make the call via Ajax, the server restarts with the updated routes
		//while the client monitors the workflow, until the server is up again 
		
		// @TODO: Real-time route mutation

		// Detaching old routes, while server is active
		// restarting the server instance, while current request is still active

		// Setup Router
		WebServer.setupRouter(RouteSet.ALL);

		// Go to console
		context.response().putHeader("X-Location", WebRoutes.DEFAULT_CONSOLE_URI)
				.setStatusCode(HttpServletResponse.SC_FOUND).end();
	}

}
