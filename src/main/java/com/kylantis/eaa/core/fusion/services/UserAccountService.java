package com.kylantis.eaa.core.fusion.services;

import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletResponse;

import com.kylantis.eaa.core.attributes.BaseUserTableSpec;
import com.kylantis.eaa.core.base.GsonFactory;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.fusion.BaseService;
import com.kylantis.eaa.core.fusion.EndpointClass;
import com.kylantis.eaa.core.fusion.EndpointMethod;
import com.kylantis.eaa.core.fusion.FusionHelper;
import com.kylantis.eaa.core.fusion.Sessions;
import com.kylantis.eaa.core.fusion.WebRoutes;
import com.kylantis.eaa.core.keys.CacheValues;
import com.kylantis.eaa.core.ml.IUserProfileModel;
import com.kylantis.eaa.core.users.UserProfileSpec;
import com.kylantis.eaa.core.utils.Utils;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.RoutingContext;

//@DEV Exception catching is too poor
@EndpointClass(uri = "/users/accounts")
public class UserAccountService extends BaseService<IUserProfileModel> {

	@EndpointMethod(uri = "/phoneAuth", headerParams = { "phone", "pass", "rem" }, requestParams = { "returnUrl" })
	public void loginByPhone(RoutingContext ctx) {

		Long phone = Long.parseLong(ctx.request().getHeader("phone"));
		String pass = ctx.request().getHeader("pass");
		String rem = ctx.request().getHeader("rem");

		String returnUrl = ctx.request().getParam("returnUrl");

		try {
			Integer userId = getUserProfileModel().loginByPhone(phone, pass);
			String sessionToken = Utils.newRandom();

			loginUser(userId, sessionToken, ctx.request().remoteAddress().host(), rem.equals("true") ? CacheValues.SESSION_TOKEN_LONG_EXPIRY_IN_SECS
					: CacheValues.SESSION_TOKEN_SHORT_EXPIRY_IN_SECS);

			if (returnUrl.equals("null")) {
				returnUrl = getUserProfileModel().getRoles(userId).size() > 0 ? WebRoutes.DEFAULT_CONSOLE_URI
						: WebRoutes.DEFAULT_INDEX_URI;
			}

			Cookie cookie = Cookie.cookie(FusionHelper.sessionTokenName(), sessionToken).setPath("/");
			if (rem.equals("true")) {
				cookie.setMaxAge(CacheValues.SESSION_TOKEN_LONG_EXPIRY_IN_SECS);
			}

			ctx.addCookie(cookie);

			ctx.response().setStatusCode(HttpServletResponse.SC_FOUND);
			ctx.response().putHeader("X-Location", returnUrl);
		} catch (ResourceException e) {
			ctx.response().setStatusCode(HttpServletResponse.SC_UNAUTHORIZED);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/emailAuth", headerParams = { "email", "pass", "rem" }, requestParams = { "returnUrl" })
	public void loginByEmail(RoutingContext ctx) {

		String email = ctx.request().getHeader("email");
		String pass = ctx.request().getHeader("pass");
		String rem = ctx.request().getHeader("rem");

		String returnUrl = ctx.request().getParam("returnUrl");
		try {
			Integer userId = getUserProfileModel().loginByEmail(email, pass);
			String sessionToken = Utils.newRandom();

			loginUser(userId, sessionToken, ctx.request().remoteAddress().host(), rem.equals("true") ? CacheValues.SESSION_TOKEN_LONG_EXPIRY_IN_SECS
					: CacheValues.SESSION_TOKEN_SHORT_EXPIRY_IN_SECS);

			if (returnUrl.equals("null")) {
				returnUrl = getUserProfileModel().getRoles(userId).size() > 0 ? WebRoutes.DEFAULT_CONSOLE_URI
						: WebRoutes.DEFAULT_INDEX_URI;
			}

			Cookie cookie = Cookie.cookie(FusionHelper.sessionTokenName(), sessionToken).setPath("/");
			if (rem.equals("true")) {
				cookie.setMaxAge(CacheValues.SESSION_TOKEN_LONG_EXPIRY_IN_SECS);
			}

			ctx.addCookie(cookie);

			ctx.response().setStatusCode(HttpServletResponse.SC_FOUND);
			ctx.response().putHeader("X-Location", returnUrl);
		} catch (ResourceException e) {
			ctx.response().setStatusCode(HttpServletResponse.SC_UNAUTHORIZED);
		}
		ctx.response().end();
	}

	@EndpointMethod(uri = "/register", requestParams = { "returnUrl", "addCookie", "redirect",
			"service" }, bodyParams = { "user" }, method = HttpMethod.PUT, isBlocking = true)
	public void registerUser(RoutingContext ctx) {

		JsonObject body = ctx.getBodyAsJson();
		String user = body.getJsonObject("user").encode();

		String returnUrl = ctx.request().getParam("returnUrl");
		String userRole = ctx.request().getParam("userRole");
		String addCookie = ctx.request().getParam("addCookie");
		String redirect = ctx.request().getParam("redirect");

		//String registrationSource = ctx.request().getParam("registrationSource");

		// @Todo: Add metric showing that service is the registration source

		UserProfileSpec spec = GsonFactory.newInstance().fromJson(user, UserProfileSpec.class);

		try {

			Integer userId = getUserProfileModel().registerUser(spec, userRole);

			if (addCookie.equals("true")) {
				String sessionToken = Utils.newRandom();

				loginUser(userId, sessionToken, ctx.request().remoteAddress().host(), CacheValues.SESSION_TOKEN_SHORT_EXPIRY_IN_SECS);

				Cookie cookie = Cookie.cookie(FusionHelper.sessionTokenName(), sessionToken).setPath("/");
				ctx.addCookie(cookie);
			}

			if (redirect.equals("true")) {

				if (returnUrl.equals("null")) {
					returnUrl = userRole != null ? WebRoutes.DEFAULT_CONSOLE_URI : WebRoutes.DEFAULT_INDEX_URI;
				}

				ctx.response().setStatusCode(HttpServletResponse.SC_FOUND);
				ctx.response().putHeader("X-Location", returnUrl);

			} else {
				ctx.response().setChunked(true).write(userId.toString());
				ctx.response().setStatusCode(HttpServletResponse.SC_OK);
			}

		} catch (ResourceException e) {
			if (e.getErrCode() == ResourceException.RESOURCE_ALREADY_EXISTS) {
				ctx.response().setStatusCode(HttpServletResponse.SC_FORBIDDEN);
				switch (e.getRef()) {
				case BaseUserTableSpec.EMAIL:
					ctx.response().setStatusMessage("Another account already uses this email");
					break;
				case BaseUserTableSpec.PHONE:
					ctx.response().setStatusMessage("Another account already uses this phone number");
					break;
				}
			} else {
				ctx.response().setStatusCode(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
		ctx.response().end();
	}

	private static void loginUser(Integer userId, String sessionToken, String remoteAdress, int expiry) {
		Sessions.newSession(userId, sessionToken, expiry, remoteAdress, TimeUnit.SECONDS);
	}

}
