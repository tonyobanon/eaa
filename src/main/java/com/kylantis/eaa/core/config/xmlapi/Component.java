package com.kylantis.eaa.core.config.xmlapi;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Component {

	@JacksonXmlProperty(isAttribute = true, localName = "class")
	protected String impl;

	public String getImpl() {
		return impl;
	}

	public void setImpl(String impl) {
		this.impl = impl;
	}

}
