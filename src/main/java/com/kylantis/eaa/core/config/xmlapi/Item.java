package com.kylantis.eaa.core.config.xmlapi;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Item {
	
	@JacksonXmlProperty(isAttribute = true, localName = "title")
	protected String title;
	
	@JacksonXmlProperty(isAttribute = true, localName = "value")
	protected String value;
	
}
