package com.kylantis.eaa.core.config.xmlapi;

import java.util.Collection;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Items {

	@JacksonXmlProperty(localName="item")
	@JacksonXmlElementWrapper(useWrapping=false)
    protected Collection<Item> item;
	
}
