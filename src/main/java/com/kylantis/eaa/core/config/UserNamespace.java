package com.kylantis.eaa.core.config;

import com.kylantis.eaa.core.users.RoleRealm;

public class UserNamespace extends Namespace {

	protected UserNamespace() {
		super(Repository.USER_NAMESPACE_PREFIX);
	}
	
	protected UserNamespace(String prefix) {
		super(prefix);
	}

	/**
	 * The userId should be passed in as param[0]
	 */
	@Override
	public Id getKey(Object... params) {
		return new CompositeId(Integer.parseInt(params[0].toString()), this.getPrefix() + "_" + params[1], this);
	}

	@Override
	public Boolean isInternal() {
		return true;
	}

	@Override
	public RoleRealm realm() {
		return RoleRealm.CUSTOMER;
	}

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return null;
	}

}
