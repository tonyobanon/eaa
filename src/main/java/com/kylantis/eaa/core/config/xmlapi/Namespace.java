package com.kylantis.eaa.core.config.xmlapi;

import java.util.Collection;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "namespace")
public class Namespace {

	@JacksonXmlProperty(isAttribute = true, localName = "prefix")
	private String prefix;
	
	@JacksonXmlProperty(localName = "page")
	@JacksonXmlElementWrapper(useWrapping = false)
	private Collection<Page> pages;
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public Collection<Page> getPages() {
		return pages;
	}

	public void setPages(Collection<Page> pages) {
		this.pages = pages;
	}
}
