package com.kylantis.eaa.core.config;

import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.users.RoleRealm;

public abstract class Namespace {

	private static final AdminNamespace ADMIN_NAMESPACE = new AdminNamespace();
	private static final UserNamespace USER_NAMESPACE = new UserNamespace();

	private String prefix;

	protected Namespace(String prefix) {
		this.prefix = prefix;
	}

	public static AdminNamespace forAdmin() {
		return ADMIN_NAMESPACE;
	}

	public static UserNamespace forUser() {
		return USER_NAMESPACE;
	}

	public static UserModuleNamespace forService(String service) {
		return new UserModuleNamespace(service);
	}

	public static Namespace fromPrefix(String prefix) {

		switch (prefix) {

		case Repository.ADMIN_NAMESPACE_PREFIX:
			return ADMIN_NAMESPACE;

		case Repository.USER_NAMESPACE_PREFIX:
			return USER_NAMESPACE;

		default:

			String arr[] = prefix.split("_");

			switch (arr[0] + "_") {

			case Repository.ADMIN_MODULE_NAMESPACEE_PREFIX:
				return new AdminModuleNamespace(arr[1]);

			case Repository.USER_MODULE_NAMESPACE_PREFIX:
				return new UserModuleNamespace(arr[1]);

			default:
				Exceptions.throwRuntime(new IllegalArgumentException("Unable to parse the specified namespace"));
				return null;
			}

		}
	}

	public abstract String name();

	public abstract Boolean isInternal();

	public abstract RoleRealm realm();

	public String getPrefix() {
		return prefix;
	}

	public String getBasePrefix() {
		return prefix.split("_")[0] + "_";
	}

	public Id getChild(String name) {
		return new Id(this.getPrefix() + "_" + name, this);
	}

	public Id getKey(Object... params) {
		return new Id(this.getPrefix() + "_" + params[0], this);
	}

}
