package com.kylantis.eaa.core.config;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.kylantis.eaa.core.attributes.ConfigValueSpec;
import com.kylantis.eaa.core.attributes.UserConfigSpec;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.base.Todo;
import com.kylantis.eaa.core.components.ComponentModel;
import com.kylantis.eaa.core.components.NamespaceProperties;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.dbutils.DBTooling;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IConfigModel;
import com.kylantis.eaa.core.ml.MLRepository;
import com.kylantis.eaa.core.system.ResourceBundleKeyRef;

@ConcreteModel(dependencies = { MLRepository.RESOURCE_BUNDLE_MODEL })
public class ConfigModel extends IConfigModel {

	public static final Id ADMIN_CONFIG_PAGE = Namespace.forAdmin().getKey("configuration");
	
	@Override
	@Todo("Scan for Namespaces instead")
	public void preInstall() {
		
		// Create Default Namespaces

		getComponentModel().newNamespace(new NamespaceProperties().setPrefix(Repository.ADMIN_NAMESPACE_PREFIX)
				.setRoleRealm(Namespace.fromPrefix(Repository.ADMIN_NAMESPACE_PREFIX).realm()).setIsInternal(true));

		getComponentModel().newNamespace(new NamespaceProperties().setPrefix(Repository.USER_NAMESPACE_PREFIX)
				.setRoleRealm(Namespace.fromPrefix(Repository.USER_NAMESPACE_PREFIX).realm()).setIsInternal(true));

		getComponentModel().newNamespace(new NamespaceProperties().setPrefix(Repository.USER_MODULE_NAMESPACE_PREFIX)
				.setRoleRealm(Namespace.fromPrefix(Repository.USER_MODULE_NAMESPACE_PREFIX).realm())
				.setIsInternal(false));

		getComponentModel().newNamespace(new NamespaceProperties().setPrefix(Repository.ADMIN_MODULE_NAMESPACEE_PREFIX)
				.setRoleRealm(Namespace.fromPrefix(Repository.ADMIN_MODULE_NAMESPACEE_PREFIX).realm())
				.setIsInternal(false));

		
		// Create default admin configuration page

		getComponentModel().newPage(new Page().setId(ADMIN_CONFIG_PAGE)
				.setTitle(new ResourceBundleKeyRef().setKey("ext.configuration"))
				.setParent(Namespace.forAdmin().getKey(ComponentModel.ROOT_PARENT_ID)));

	}

	@Override
	public void start() {

	}

	private Iterator<Integer> getAssociatedUsers(UserNamespace namespace) {
		Iterator<Integer> users = null;

		if (namespace instanceof UserModuleNamespace) {

			UserModuleNamespace serviceNamespace = (UserModuleNamespace) namespace;
			users = getMicroserviceModel().getAssociatedUsers(serviceNamespace.getModule());

		} else {

			users = getUserProfileModel().getUsers();
		}

		return users;
	}

	@PlatformInternal
	public Object get(Id id) {

		if (id.getNamespace() instanceof AdminNamespace) {

			return (String) StorageServiceFactory.getDocumentDatabase().getTable(ConfigValueSpec.TABLE_NAME)
					.getItem(ConfigValueSpec.KEY, id.getId()).get(ConfigValueSpec.VALUE);

		} else {

			CompositeId compositeId = (CompositeId) id;

			return StorageServiceFactory.getDocumentDatabase().getTable(UserConfigSpec.TABLE_NAME)
					.getItem(UserConfigSpec.USER_ID, compositeId.getUserId(), UserConfigSpec.KEY, id.getId())
					.get(UserConfigSpec.VALUE);
		}

	}

	public void putAll(Namespace namespace, Map<Id, String> entries) {

		String tableName;
		List<Item> items = new ArrayList<>();

		if (namespace instanceof AdminNamespace) {

			tableName = ConfigValueSpec.TABLE_NAME;
			entries.forEach((k, v) -> {
				items.add(
						new Item().withPrimaryKey(ConfigValueSpec.KEY, k.getId()).withString(ConfigValueSpec.VALUE, v));
			});

		} else {

			tableName = UserConfigSpec.TABLE_NAME;

			entries.forEach((k, v) -> {

				if (k instanceof CompositeId) {
					CompositeId compositeId = (CompositeId) k;
					items.add(new Item().withPrimaryKey(UserConfigSpec.USER_ID, compositeId.getUserId(),
							UserConfigSpec.KEY, compositeId.getId()).with(UserConfigSpec.VALUE, v));
				} else {
					put(k, v);
				}

			});
		}

		if (!items.isEmpty()) {
			DBTooling.batchPut(tableName, items);
		}
	}

	@PlatformInternal
	public void put(Id id, String value) {

		Namespace namespace = id.getNamespace();

		if (namespace instanceof AdminNamespace) {

			Table table = StorageServiceFactory.getDocumentDatabase().getTable(ConfigValueSpec.TABLE_NAME);
			Item item = new Item().withPrimaryKey(ConfigValueSpec.KEY, id.getId()).with(ConfigValueSpec.VALUE, value);
			table.putItem(new PutItemSpec().withItem(item));

		} else {

			if (id instanceof CompositeId) {

				CompositeId compositeId = (CompositeId) id;

				Table table = StorageServiceFactory.getDocumentDatabase().getTable(UserConfigSpec.TABLE_NAME);
				Item item = new Item()
						.withPrimaryKey(UserConfigSpec.USER_ID, compositeId.getUserId(), UserConfigSpec.KEY, id.getId())
						.with(UserConfigSpec.VALUE, value);
				table.putItem(new PutItemSpec().withItem(item));

			} else {

				List<Item> items = new ArrayList<Item>();
				getAssociatedUsers((UserNamespace) namespace).forEachRemaining(i -> {
					items.add(new Item().withPrimaryKey(UserConfigSpec.USER_ID, i, UserConfigSpec.KEY, id.getId())
							.with(UserConfigSpec.VALUE, value));
				});

				DBTooling.batchPut(UserConfigSpec.TABLE_NAME, items);

			}
		}
	}

	public void delete(Id id) {

		Namespace namespace = id.getNamespace();

		if (namespace instanceof AdminNamespace) {

			StorageServiceFactory.getDocumentDatabase().getTable(ConfigValueSpec.TABLE_NAME)
					.deleteItem(ConfigValueSpec.TABLE_NAME, id.getId());

		} else {
			List<PrimaryKey> keys = new ArrayList<PrimaryKey>();
			getAssociatedUsers((UserNamespace) namespace).forEachRemaining(i -> {
				keys.add(new PrimaryKey(UserConfigSpec.USER_ID, i, UserConfigSpec.KEY, id.getId()));
			});

			DBTooling.batchDelete(UserConfigSpec.TABLE_NAME, keys);
		}
	}

	public Map<Object, Object> getValues(Namespace namespace, List<Id> keys) {

		if (namespace instanceof AdminNamespace) {
			List<PrimaryKey> primaryKeys = new ArrayList<>(keys.size());
			keys.forEach(k -> {
				primaryKeys.add(new PrimaryKey(ConfigValueSpec.KEY, k.getId()));
			});

			return DBTooling.batchGetSingleAttr(ConfigValueSpec.TABLE_NAME, ConfigValueSpec.KEY, UserConfigSpec.KEY,
					primaryKeys, UserConfigSpec.VALUE);

		} else {

			List<PrimaryKey> primaryKeys = new ArrayList<>(keys.size());
			keys.forEach(k -> {
				CompositeId compositeId = (CompositeId) k;
				primaryKeys.add(new PrimaryKey(UserConfigSpec.USER_ID, compositeId.getUserId(), UserConfigSpec.KEY,
						compositeId.getId()));
			});

			return DBTooling.batchGetSingleAttr(UserConfigSpec.TABLE_NAME, UserConfigSpec.USER_ID, UserConfigSpec.KEY,
					primaryKeys, UserConfigSpec.VALUE);
		}
	}

}
