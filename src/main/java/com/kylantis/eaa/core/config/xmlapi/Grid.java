package com.kylantis.eaa.core.config.xmlapi;

import java.util.Collection;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Grid {
	
	@JacksonXmlProperty(localName = "component")
	@JacksonXmlElementWrapper(useWrapping = false)
	private Collection<Component> components;

	
	public Collection<Component> getComponents() {
		return components;
	}

	public void setComponents(Collection<Component> components) {
		this.components = components;
	}

}
