package com.kylantis.eaa.core.config;

import com.kylantis.eaa.core.users.RoleRealm;

public class AdminNamespace extends Namespace {

	public AdminNamespace() {
		super(Repository.ADMIN_NAMESPACE_PREFIX);
	}
	
	protected AdminNamespace(String customPrefix) {
		super(customPrefix);
	}

	@Override
	public String name() {
		return "";
	}

	@Override
	public Boolean isInternal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RoleRealm realm() {
		return RoleRealm.ADMIN;
	}
}
