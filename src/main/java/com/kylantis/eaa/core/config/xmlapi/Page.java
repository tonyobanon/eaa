package com.kylantis.eaa.core.config.xmlapi;

import java.util.Collection;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Page {

	@JacksonXmlProperty(isAttribute = true, localName = "id")
	private String id;

	@JacksonXmlProperty(isAttribute = true, localName = "title")
	private String title;

	@JacksonXmlProperty(isAttribute = true, localName = "sortOrder")
	private String sortOrder;

	@JacksonXmlProperty(localName = "grid")
	@JacksonXmlElementWrapper(useWrapping = false)
	private Collection<Grid> grids;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Collection<Grid> getGrids() {
		return grids;
	}

	public void setGrids(Collection<Grid> components) {
		this.grids = components;
	}
	
}
