package com.kylantis.eaa.core.config;

import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.base.PlatformInternal;

public class Id {

	private final String id;
	private Namespace namespace;

	protected Id(String id, Namespace namespace) {
		this.id = id;
		this.namespace = namespace;
	}

	public String getId() {
		return getNamespace() + "_" + id;
	}

	@Override
	public String toString() {
		return getId();
	}

	public Namespace getNamespace() {
		return namespace;
	}

	public Id getChild(String id) {
		return new Id(this.getId() + "_" + id, this.getNamespace());
	}

	@PlatformInternal
	public static Id from(String id) {

		if (!id.contains("_")) {
			Exceptions.throwRuntime(new IllegalArgumentException("Unable to parse the specified Id"));
		}

		String arr[] = id.split("_");

		Namespace namespace = null;

		switch (arr[0] + "_") {

		case Repository.ADMIN_NAMESPACE_PREFIX:
			namespace = Namespace.forAdmin();
			break;

		case Repository.ADMIN_MODULE_NAMESPACEE_PREFIX:
			namespace = new AdminModuleNamespace(arr[1]);
			break;

		case Repository.USER_NAMESPACE_PREFIX:
			namespace = Namespace.forUser();
			break;

		case Repository.USER_MODULE_NAMESPACE_PREFIX:
			namespace = new UserModuleNamespace(arr[1]);
			break;

		default:
			Exceptions.throwRuntime(new IllegalArgumentException("Unable to parse the specified Id"));
			break;
		}

		return new Id(id, namespace);
	}

}