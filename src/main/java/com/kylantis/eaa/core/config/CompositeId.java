package com.kylantis.eaa.core.config;

public class CompositeId extends Id {

	private final Integer userId;

	protected CompositeId(Integer userId, String id, Namespace namespace) {
		super(id, namespace);
		this.userId = userId;
	}

	public Integer getUserId() {
		return this.userId;
	}
	
}
