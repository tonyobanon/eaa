package com.kylantis.eaa.core.config;

public class UserModuleNamespace extends UserNamespace {

	private final String module;
	
	protected UserModuleNamespace(String module) {
		super(Repository.USER_MODULE_NAMESPACE_PREFIX + module + "_");
		this.module = module;
	}

	public String getModule() {
		return module;
	}
}
