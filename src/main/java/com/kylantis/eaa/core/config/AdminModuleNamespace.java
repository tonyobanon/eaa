package com.kylantis.eaa.core.config;

public class AdminModuleNamespace extends AdminNamespace {

	private final String name;

	protected AdminModuleNamespace(String name) {
		super(Repository.ADMIN_MODULE_NAMESPACEE_PREFIX + name + "_");
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public Boolean isInternal() {
		return false;
	}

}
