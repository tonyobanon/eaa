package com.kylantis.eaa.core.imageui;

public class IconOrImage {

	private String icon;
	private String imageURL;

	public String getIcon() {
		return icon;
	}

	public IconOrImage setIcon(String icon) {
		this.icon = icon;
		return this;
	}

	public String getImageURL() {
		return imageURL;
	}

	public IconOrImage setImageURL(String imageURL) {
		this.imageURL = imageURL;
		return this;
	}
}
