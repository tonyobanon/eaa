package com.kylantis.eaa.core;

import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.kylantis.eaa.core.attributes.ConfigValueSpec;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.base.Todo;

//Deeply integrate platform with manual processes
//Ensure compliance with global standards for privacy and security
//Capacity Planning
//*Industry Compliance
//*Debugging
//*Testing
//Performance Rampdown

public class PlatformUtils {

	@Todo("Protect access to this field. Only PlatformService should modify it. Reflection may be my best bet")
	public static Boolean isInstalled = null;

	public static Boolean isInstalled() {
		if (isInstalled == null) {
			try{
				StorageServiceFactory.getDocumentDatabase().getTable(ConfigValueSpec.TABLE_NAME).describe();
				isInstalled = true;
			}catch (ResourceNotFoundException e) {
				isInstalled = false;
			}
		}
		return isInstalled;
	}

	// Country, Company Name, Company Domain, Language

	// Add default application Questionnaire

	// Add demography groups, with time-series data

	// Add default plans

	// Create admin user

	// Add config keys, and their default

	// Create Admin Role

}
