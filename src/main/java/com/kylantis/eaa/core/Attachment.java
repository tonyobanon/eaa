package com.kylantis.eaa.core;

public class Attachment {

	private String name;
	private String blobId;
	
	public Attachment(String name, String blobId) {
		this.name = name;
		this.blobId = blobId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBlobId() {
		return blobId;
	}

	public void setBlobId(String blobId) {
		this.blobId = blobId;
	}
	
}
