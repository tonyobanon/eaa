package com.kylantis.eaa.core;

public @interface BlockerTodo {
  String value() default "";
}
