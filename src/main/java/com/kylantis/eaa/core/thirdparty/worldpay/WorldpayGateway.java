package com.kylantis.eaa.core.thirdparty.worldpay;

import java.util.List;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.base.Logger;
import com.kylantis.eaa.core.billing.AuthorizationRequest;
import com.kylantis.eaa.core.billing.BasePaymentModule;
import com.kylantis.eaa.core.billing.BaseProviderSpec;
import com.kylantis.eaa.core.billing.BaseRequest;
import com.kylantis.eaa.core.billing.GatewayResponse;
import com.kylantis.eaa.core.billing.RefundRequest;
import com.kylantis.eaa.core.forms.InputType;
import com.kylantis.eaa.core.forms.Question;
import com.kylantis.eaa.core.forms.SimpleEntry;
import com.kylantis.eaa.core.system.ResourceBundleModel;
import com.worldpay.gateway.clearwater.client.core.dto.CountryCode;
import com.worldpay.gateway.clearwater.client.core.dto.CurrencyCode;
import com.worldpay.gateway.clearwater.client.core.dto.common.Address;
import com.worldpay.gateway.clearwater.client.core.dto.request.OrderRequest;
import com.worldpay.gateway.clearwater.client.core.dto.response.OrderResponse;
import com.worldpay.sdk.WorldpayRestClient;

import io.vertx.ext.web.RoutingContext;

public class WorldpayGateway extends BasePaymentModule {

	private static final String bundleName = "be_worldpay";
	
	private static final String API_ENDPOINT = "API_ENDPOINT";
	private static final String MERCHANT_CODE = "MERCHANT_CODE";
	private static final String AUTH_USERNAME = "AUTH_USERNAME";
	private static final String AUTH_PASSWORD = "AUTH_PASSWORD";


	/**
	 * Perform Initialization tasks
	 */
	@Override
   public void init() {

	}
	
	@Override
	public List<Question> parameters() {
		return new FluentArrayList<Question>()
				.with(new SimpleEntry(API_ENDPOINT, InputType.TEXT, getValue("worldpay.api.endpoint"))
						.withDefaultValue("https://secure-test.worldpay.com/jsp/merchant/xml/paymentService.jsp"))
				.with(new SimpleEntry(MERCHANT_CODE, InputType.TEXT,
						getValue("worldpay.merchant.code")))
				.with(new SimpleEntry(AUTH_USERNAME, InputType.TEXT,
						getValue("worldpay.auth.username")))
				.with(new SimpleEntry(AUTH_PASSWORD, InputType.SECRET,
						getValue("worldpay.auth.password")));
	}

	private String getValue(String key){
		return ResourceBundleModel.getValue(bundleName, key);
	}
	
	@Override
	public BaseProviderSpec baseSpec() {
		return new BaseProviderSpec() {
			
			@Override
			public String name() {
				return "worldpay";
			}

			@Override
			public String title() {
				return "Worldpay";
			}

			@Override
			public boolean usesCardToken() {
				return true;
			}
			
			@Override
			public String clientTokenOrchestrator() {
				return "function eaa_payment_module_worldpay () {}";
			}
		};
	}



	@Override
	public GatewayResponse authorize(AuthorizationRequest request) {
		
		WorldpayRestClient restClient = new WorldpayRestClient("your-service-key");
		 
		OrderRequest orderRequest = new OrderRequest();
		orderRequest.setToken("your-token");
		orderRequest.setAmount(500);
		orderRequest.setCurrencyCode(CurrencyCode.GBP);
		orderRequest.setName("test name");
		orderRequest.setOrderDescription("Order description");
		orderRequest.setCustomerOrderCode("Order code");
		orderRequest.setSettlementCurrency(CurrencyCode.GBP);
		  
		Address address = new Address();
		address.setAddress1("address1");
		address.setCity("city");
		address.setCountryCode(CountryCode.GB);
		address.setPostalCode("postCode");
		orderRequest.setBillingAddress(address);
		 
		OrderResponse orderResponse = restClient.getOrderService().create(orderRequest);
		//orderResponse.get
		
		
		return null;
	}

	@Override
	public GatewayResponse cancel(BaseRequest request) {
		return null;
	}

	@Override
	public GatewayResponse refund(RefundRequest request) {
		return null;
	}

	@Override
	public GatewayResponse refundOrCancel(BaseRequest request) {
		return null;
	}

	@Override
	public void IpnHandler(RoutingContext context) {

	}

}
