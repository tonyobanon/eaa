package com.kylantis.eaa.core;

public enum ClassIdentityType {
 ANNOTATION, SUPER_CLASS, DIRECT_SUPER_CLASS
}
