package com.kylantis.eaa.core.metrics;

import java.util.Calendar;

import com.kylantis.eaa.core.BlockerTodo;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.base.Todo;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IMetricsModel;
import com.kylantis.eaa.core.ml.MLRepository;
import com.kylantis.eaa.core.setup.InstallOptions;

import redis.clients.jedis.Jedis;

@Todo("This is just a prototype, save to S3 in real implementation")
@BlockerTodo("Rethink Implementation")
@ConcreteModel
public class MetricsModel extends IMetricsModel {

	
	@Override
	public void install(InstallOptions options) {

	}

	@Override
	public void start() {

		// Start a socket server, that gets written to when metric is updated

	}

	// private static int DEFAULT_EXPIRATION_IN_SECS = 16070400;

	private static final Jedis redisClient;

	public Integer getInt(String key) {
		String value = get(key);
		if (value != null) {
			return Integer.parseInt(value);
		} else {
			return 0;
		}
	}

	public void putInt(String key, Integer value) {
		put(key, Integer.toString(value));
	}

	private void IncrementInt(String key) {
		putInt(key, getInt(key) + 1);
	}

	public void incrementInt(String key) {

		if (!key.endsWith("_TIMESERIES")) {
			IncrementInt(key);
		} else {

			Calendar currentTime = Calendar.getInstance();

			int yy = currentTime.get(Calendar.YEAR);
			int mm = currentTime.get(Calendar.MONTH);
			int dd = currentTime.get(Calendar.DAY_OF_MONTH);
			int hh = currentTime.get(Calendar.HOUR_OF_DAY);
			int mn = currentTime.get(Calendar.MINUTE);
			int ss = currentTime.get(Calendar.SECOND);

			IncrementInt(key + "_" + yy);
			IncrementInt(key + "_" + yy + "_" + mm);
			IncrementInt(key + "_" + yy + "_" + mm + "_" + dd);
			IncrementInt(key + "_" + yy + "_" + mm + "_" + dd + "_" + hh);
			IncrementInt(key + "_" + yy + "_" + mm + "_" + dd + "_" + hh + "_" + mn);
			IncrementInt(key + "_" + yy + "_" + mm + "_" + dd + "_" + hh + "_" + mn + "_" + ss);

		}
	}

	public void decrementInt(String key) {
		if (key.endsWith("_CURRENT")) {
			Integer value = getInt(key);
			putInt(key, value - 1);
		}
	}

	public String get(String key) {
		return redisClient.get("metric/" + key);
	}

	public void put(String key, String value) {
		redisClient.set("metric/" + key, value);
	}

	@BlockerTodo("Delete every timeseries data for this key")
	public void delete(String... keys) {
		for (String key : keys) {
			redisClient.del("metric/" + key);
		}
	}
	
	static {
		redisClient = new Jedis(StorageServiceFactory.getRedisEndpoint());
	}

}
