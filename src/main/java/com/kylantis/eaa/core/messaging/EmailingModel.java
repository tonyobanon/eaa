package com.kylantis.eaa.core.messaging;

import java.util.ArrayList;
import java.util.Map;

import com.kylantis.eaa.core.base.ObjectMarshaller;
import com.kylantis.eaa.core.keys.RegistryKeys;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IEmailingModel;
import com.kylantis.eaa.core.ml.MLRepository;
import com.kylantis.eaa.core.setup.InstallOptions;

@ConcreteModel(dependencies = {MLRepository.CONFIG_MODEL})
public class EmailingModel extends IEmailingModel {

	
	@Override
	public void preInstall() {

		getConfigModel().put(RegistryKeys.EMAIL_BLIND_COPIES, ObjectMarshaller.marshal(false));
	}

	@Override
	public void install(InstallOptions options) {

		// Save values

		getConfigModel().put(RegistryKeys.MAIL_PROVIDER_URL, options.getMailCredentials().getProviderUrl());
		getConfigModel().put(RegistryKeys.EMAIL_USERNAME, options.getMailCredentials().getUsername());
		getConfigModel().put(RegistryKeys.EMAIL_USERNAME, options.getMailCredentials().getPassword());
	}

	@Override
	public void start() {

	}

	private static Map<Integer, String> getFirstNames(ArrayList<Integer> userIds) {
		return null;
	}

	private static ArrayList<String> getActualValues(ArrayList<String> userIds, String key) {
		switch (key) {
		case "{{firstname}}":

			break;
		case "{{lastname}}":

			break;
		case "{{address}}":

			break;
		case "{{email}}":

			break;
		case "{{phone}}":

			break;
		case "{{dateJoined}}":

			break;
		}
		return null;
	}

	private static String mergeTemplate(Integer userId, Integer templateId) {
		return null;
	}

}
