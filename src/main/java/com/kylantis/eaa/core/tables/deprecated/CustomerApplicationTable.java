package com.kylantis.eaa.core.tables.deprecated;

import com.kylantis.eaa.core.attributes.CustomerApplicationSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class CustomerApplicationTable extends BaseTable {

	public Integer id;
	public String entryId;
	public String value;
	
	@Override
	public String tableHashKey() {
		return CustomerApplicationSpec.ID;
	}
	
	@Override
	public String tableRangeKey() {
		return CustomerApplicationSpec.ENTRY_ID;
	}

}
