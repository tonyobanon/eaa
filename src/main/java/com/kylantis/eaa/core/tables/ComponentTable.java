package com.kylantis.eaa.core.tables;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.ComponentSpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class ComponentTable extends BaseTable {

	public String id;
	public Integer sortOrder;
	public String page;
	public String implementation;
	public Boolean enabled;
	public String hash;

	@Override
	public String tableHashKey() {
		return ComponentSpec.ID;
	}

	@Override
	public Map<String, IndexConfig> unindexedGSIs() {
		return new FluentHashMap<String, IndexConfig>().with(ComponentSpec.PAGE_INDEX, new IndexConfig(ComponentSpec.PAGE, ComponentSpec.SORT_ORDER)
				.withProjection(new Projection().withProjectionType(ProjectionType.KEYS_ONLY)));

	}

}
