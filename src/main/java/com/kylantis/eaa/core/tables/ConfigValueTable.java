package com.kylantis.eaa.core.tables;

import com.kylantis.eaa.core.attributes.ConfigValueSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class ConfigValueTable extends BaseTable {

	public String key;
	public String value;
	
	public ConfigValueTable() {
	}

	@Override
	public String tableHashKey() {
		return ConfigValueSpec.KEY;
	}

}
