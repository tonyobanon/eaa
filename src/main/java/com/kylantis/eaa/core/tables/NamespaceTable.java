package com.kylantis.eaa.core.tables;

import com.kylantis.eaa.core.attributes.NamespaceSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class NamespaceTable extends BaseTable {

	public String prefix;
	public Integer realm;
	public Boolean isInternal;
	
	@Override
	public String tableHashKey() {
		return NamespaceSpec.PREFIX;
	}

}
