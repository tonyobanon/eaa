package com.kylantis.eaa.core.tables.staging;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.attributes.ServiceFunctionalitiesSpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class ServiceFunctionalitiesTable extends BaseTable {

	public String service;
	public String id;
	public String name;
	public String realm;
	public String dateCreated;
	
	@Override
	public String tableHashKey() {
		return ServiceFunctionalitiesSpec.SERVICE;
	}
	
	@Override
	public String tableRangeKey() {
		return ServiceFunctionalitiesSpec.ID;
	}
	
	@Override
	public Map<String, IndexConfig> unindexedGSIs() {
		Map<String, IndexConfig> map = new HashMap<>();

		map.put(ServiceFunctionalitiesSpec.REALM_INDEX, new IndexConfig(ServiceFunctionalitiesSpec.REALM)
				.withProjection(new Projection()
						.withProjectionType(ProjectionType.INCLUDE)
						.withNonKeyAttributes(ServiceFunctionalitiesSpec.NAME)
						));
		return map;
	}
	@Override
	public Boolean enabled() {
		return false;
	}

}
