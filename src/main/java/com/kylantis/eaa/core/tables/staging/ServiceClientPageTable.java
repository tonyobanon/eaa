package com.kylantis.eaa.core.tables.staging;

import com.kylantis.eaa.core.attributes.ServiceClientPageSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class ServiceClientPageTable extends BaseTable {

	public String service;
	public String id;
	public String title;
	public String pageUri;
	
	public String dateCreated;
	
	@Override
	public String tableHashKey() {
		return ServiceClientPageSpec.SERVICE;
	}

	@Override
	public String tableRangeKey() {
		return ServiceClientPageSpec.ID;
	}
	
	@Override
	public Boolean enabled() {
		return false;
	}
}
