package com.kylantis.eaa.core.tables;

import java.util.List;

import com.kylantis.eaa.core.attributes.CustomerGroupSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class CustomerGroup extends BaseTable {

	public String name;
	public String expression;
	public List<Integer> users;
	public String dateCreated;
	public String dateModified;

	@Override
	public String tableHashKey() {
		return CustomerGroupSpec.NAME;
	}

}
