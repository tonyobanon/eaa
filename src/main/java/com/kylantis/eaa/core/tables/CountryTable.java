package com.kylantis.eaa.core.tables;

import java.util.Set;

import com.kylantis.eaa.core.attributes.CountrySpec;
import com.kylantis.eaa.core.base.BaseTable;

public class CountryTable extends BaseTable {

	// ISO
	public String code;
	public String countryName;

	public String currencyCode;
	public String currencyName;

	public Set<String> spokenLanguages;

	public String languageCode;
	public String dialingCode;

	
	@Override
	public String tableHashKey() {
		return CountrySpec.CODE;
	}

}
