package com.kylantis.eaa.core.tables.staging;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.attributes.BaseServiceSpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class BaseServiceTable extends BaseTable {

	public String id;
	public String title;
	public String summary;
	public String baseUri;
	public String developer;
	public String developerUrl;
	public String logoUrl;

	public String dateCreated;

	@Override
	public String tableHashKey() {
		return BaseServiceSpec.ID;
	}

	@Override
	public Map<String, IndexConfig> unindexedGSIs() {
		Map<String, IndexConfig> map = new HashMap<>();

		map.put(BaseServiceSpec.DEVELOPER_INDEX,
				new IndexConfig(BaseServiceSpec.DEVELOPER)
						.withProjection(new Projection().withProjectionType(ProjectionType.INCLUDE)
								.withNonKeyAttributes(BaseServiceSpec.TITLE, BaseServiceSpec.LOGO_URL)));
		return map;
	}
	
	@Override
	public Boolean enabled() {
		return false;
	}
}
