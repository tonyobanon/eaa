package com.kylantis.eaa.core.tables;

import java.nio.ByteBuffer;

import com.kylantis.eaa.core.attributes.BlobStoreSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class BlobStoreTable extends BaseTable {

	public String id;
	public ByteBuffer data;
	public String mimeType;
	public String dateCreated;

	@Override
	public String tableHashKey() {
		return BlobStoreSpec.ID;
	}

}
