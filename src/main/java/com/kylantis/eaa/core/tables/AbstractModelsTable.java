package com.kylantis.eaa.core.tables;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.AbstractModelsSpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class AbstractModelsTable extends BaseTable {

	public String path;
	public String appId;
	public String model;
	public String title;
	public String date;

	@Override
	public String tableHashKey() {
		return AbstractModelsSpec.PATH;
	}

	@Override
	public Map<String, IndexConfig> unindexedGSIs() {
		return new FluentHashMap<String, IndexConfig>().with(AbstractModelsSpec.APP_ID_INDEX,
				new IndexConfig(AbstractModelsSpec.APP_ID).withProjection(new Projection()
						.withProjectionType(ProjectionType.KEYS_ONLY)));
	}
}
