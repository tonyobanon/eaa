package com.kylantis.eaa.core.tables;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.SearchGraphSpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class SearchGraphTable extends BaseTable {

	public String id;
	public Integer matrixId;

	public Integer entityType;

	public Integer matrixHashKey;
	public String matrix;
	
	@Override
	public String tableHashKey() {
		return SearchGraphSpec.ID;
	}
	
	@Override
	public String tableRangeKey() {
		return SearchGraphSpec.MATRIX_ID;
	}

	@Override
	public Map<String, IndexConfig> indexedGSIs() {

		return new FluentHashMap<String, IndexConfig>().with(SearchGraphSpec.MATRIX_INDEX,
				new IndexConfig(SearchGraphSpec.MATRIX_HASHKEY, SearchGraphSpec.MATRIX).withProjection(new Projection()
						.withProjectionType(ProjectionType.INCLUDE).withNonKeyAttributes(SearchGraphSpec.ENTITY_TYPE)));
	}

}
