package com.kylantis.eaa.core.tables;

import com.kylantis.eaa.core.attributes.LanguageSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class LanguageTable extends BaseTable {

	// ISO
	public String code;
	public String langName;

	@Override
	public String tableHashKey() {
		return LanguageSpec.CODE;
	}

}
