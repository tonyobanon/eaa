package com.kylantis.eaa.core.tables;

import com.kylantis.eaa.core.attributes.CardDetailsSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class CardDetailsTable extends BaseTable {

	public Integer entityId;

	public Long cardNumber;

	public String cardHolderName;

	public Integer expiryMonth;

	public Integer expiryYear;

	public String address1;

	public Integer postalCode;

	public String city;

	public String state;

	public String countryCode;

	public String dateCreated;

	@Override
	public String tableHashKey() {
		return CardDetailsSpec.ENTITY_ID;
	}
	
	@Override
	public String tableRangeKey() {
		return CardDetailsSpec.CARD_NUMBER;
	}

}
