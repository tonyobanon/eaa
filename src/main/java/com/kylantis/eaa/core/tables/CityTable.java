package com.kylantis.eaa.core.tables;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.CitySpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class CityTable extends BaseTable {

	public String id;
	public String territoryCode;
	
	public String name;
	
	public Double latitude;
	public Double longitude;
	
	public String timezone;


	@Override
	public String tableHashKey() {
		return CitySpec.ID;
	}

	@Override
	public Map<String, IndexConfig> unindexedGSIs() {

		return new FluentHashMap<String, IndexConfig>().with(CitySpec.TERRITORY_CODE_INDEX, new IndexConfig(CitySpec.TERRITORY_CODE)
				.withProjection(new Projection().withProjectionType(ProjectionType.INCLUDE).withNonKeyAttributes(CitySpec.NAME)));

	}

}
