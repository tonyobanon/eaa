package com.kylantis.eaa.core.tables;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.PageSpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class PageTable extends BaseTable {

	public String id;
	
	public String title;
	public Integer sortOrder;
	public Integer parent;
	
	public String group;
	public String path;
	public String namespace;
	
	@Override
	public String tableHashKey() {
		return PageSpec.ID;
	}
	
	@Override
	public Map<String, IndexConfig> unindexedGSIs() {
		return new FluentHashMap<String, IndexConfig>()
				.with(PageSpec.NAMESPACE_INDEX, new IndexConfig(PageSpec.NAMESPACE, PageSpec.SORT_ORDER)
						.withProjection(new Projection().withProjectionType(ProjectionType.KEYS_ONLY)))
				.with(PageSpec.PATH_INDEX, new IndexConfig(PageSpec.PATH)
						.withProjection(new Projection().withProjectionType(ProjectionType.KEYS_ONLY)));
	}

}
