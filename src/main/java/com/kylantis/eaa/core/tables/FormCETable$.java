package com.kylantis.eaa.core.tables;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.FormCESpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class FormCETable$ extends BaseTable {

	public String id;
	public String title;
	
	//Format: id__title
	public Set<String> options;
	
	public Boolean allowMultipleChoice;
	
	public Boolean isRequired;
	public Boolean isVisible;
	public Boolean isDefault;

	public Integer sortOrder;
	
	public String form;
	
	public String itemsSource;
	public List<String> defaultSelections;
	
	public String context;
	
	public String hash;
	
	public FormCETable$() {
	}

	@Override
	public String tableHashKey() {
		return FormCESpec.ID;
	}
	

	@Override
	public Map<String, IndexConfig> unindexedGSIs() {

		return new FluentHashMap<String, IndexConfig>().with(FormCESpec.FORM_INDEX, new IndexConfig(FormCESpec.FORM, FormCESpec.SORT_ORDER)
				.withProjection(new Projection().withProjectionType(ProjectionType.KEYS_ONLY)));
	}

}
