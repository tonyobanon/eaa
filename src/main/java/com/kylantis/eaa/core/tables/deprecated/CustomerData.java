package com.kylantis.eaa.core.tables.deprecated;

import com.kylantis.eaa.core.attributes.CustomerDataSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class CustomerData extends BaseTable {

	public String id;
	public String entryId;
	public String value;
	public String dateCreated;
	
	@Override
	public String tableHashKey() {
		return CustomerDataSpec.ID;
	}
	
	@Override
	public String tableRangeKey() {
		return CustomerDataSpec.ENTRY_ID;
	}

}
