package com.kylantis.eaa.core.tables;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.FormSESpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class FormSETable$ extends BaseTable {

	public String id;
	public String title;
	public String valueType;
	public String defaultValue;
	public Boolean isRequired;
	public Boolean isVisible;
	public Boolean isDefault;
	public Integer sortOrder;

	public String form; 
	public String context;
	
	public String hash;
	
	public FormSETable$() {
	}

	@Override
	public String tableHashKey() {
		return FormSESpec.ID;
	}

	@Override
	public Map<String, IndexConfig> unindexedGSIs() {

		return new FluentHashMap<String, IndexConfig>().with(FormSESpec.FORM_INDEX, new IndexConfig(FormSESpec.FORM, FormSESpec.SORT_ORDER)
				.withProjection(new Projection().withProjectionType(ProjectionType.KEYS_ONLY)));
	}

}
