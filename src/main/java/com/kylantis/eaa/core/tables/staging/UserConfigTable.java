package com.kylantis.eaa.core.tables.staging;

import com.kylantis.eaa.core.attributes.UserConfigSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class UserConfigTable extends BaseTable {

	public String userId;
	public String key;
	public String value;
	
	public UserConfigTable() {
	}

	@Override
	public String tableHashKey() {
		return UserConfigSpec.USER_ID;
	}
	
	@Override
	public String tableRangeKey() {
		return UserConfigSpec.KEY;
	}
	@Override
	public Boolean enabled() {
		return false;
	}
}
