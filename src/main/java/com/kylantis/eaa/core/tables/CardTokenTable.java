package com.kylantis.eaa.core.tables;

import com.kylantis.eaa.core.attributes.CardTokenSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class CardTokenTable extends BaseTable {

	public Integer entityId;

	public String cardToken;
	
	public String moduleName;
	
	public String dateCreated;

	@Override
	public String tableHashKey() {
		return CardTokenSpec.ENTITY_ID;
	}
	
	@Override
	public String tableRangeKey() {
		return CardTokenSpec.CARD_TOKEN;
	}

}
