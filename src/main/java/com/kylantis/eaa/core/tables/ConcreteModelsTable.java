package com.kylantis.eaa.core.tables;

import java.util.Map;

import com.kylantis.eaa.core.attributes.ConcreteModelsSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class ConcreteModelsTable extends BaseTable {

	public String path;
	
	//K: model, V: appId
	public Map<String, String> models;

	@Override
	public String tableHashKey() {
		return ConcreteModelsSpec.PATH;
	}
	
}
