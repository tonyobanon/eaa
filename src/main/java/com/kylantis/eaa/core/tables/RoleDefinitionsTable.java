package com.kylantis.eaa.core.tables;

import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.RoleDefinitionSpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class RoleDefinitionsTable  extends BaseTable {

	public String name;
	public Integer realm;
	public List<String> functionalities;
	public String isDefault;

	@Override
	public String tableHashKey() {
		return RoleDefinitionSpec.NAME;
	}
	
	@Override
	public Map<String, IndexConfig> unindexedGSIs() {

		return new FluentHashMap<String, IndexConfig>().with(RoleDefinitionSpec.REALM_INDEX, new IndexConfig(RoleDefinitionSpec.REALM, RoleDefinitionSpec.IS_DEFAULT)
				.withProjection(new Projection().withProjectionType(ProjectionType.KEYS_ONLY)));
	}


} 
