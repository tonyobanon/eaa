package com.kylantis.eaa.core.tables;

import com.kylantis.eaa.core.attributes.StaticBlockSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class StaticBlockTable extends BaseTable {

	public String identifier;
	
	public String contentKey;
	
	public String title;
	
	public Boolean isEnabled;
	
	public String dateCreated;
	
	public Boolean dateUpdated;
	
	
	@Override
	public String tableHashKey() {
		return StaticBlockSpec.IDENTIFIER;
	}

}
