package com.kylantis.eaa.core.tables;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.TerritorySpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class TerritoryTable extends BaseTable {

	public String code;
	public String countryCode;
	
	public String territoryName;


	@Override
	public String tableHashKey() {
		return TerritorySpec.CODE;
	}

	@Override
	public Map<String, IndexConfig> unindexedGSIs() {
		return new FluentHashMap<String, IndexConfig>().with(TerritorySpec.COUNTRY_CODE_INDEX, new IndexConfig(TerritorySpec.COUNTRY_CODE).withProjection(
				new Projection().withProjectionType(ProjectionType.INCLUDE).withNonKeyAttributes(TerritorySpec.TERRITORY_NAME)));
	}

}
