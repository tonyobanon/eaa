package com.kylantis.eaa.core.tables;

import java.util.Set;

import com.kylantis.eaa.core.attributes.CustomerSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class CustomerTable extends BaseTable {

	public String id;
	
	public Set<String> cards;
	public Set<String> bankAccounts;
	
	public String dateCreated;
	
	@Override
	public String tableHashKey() {
		return CustomerSpec.ID;
	}
	
}
