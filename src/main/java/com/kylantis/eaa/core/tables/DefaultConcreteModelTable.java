package com.kylantis.eaa.core.tables;

import com.kylantis.eaa.core.attributes.DefaultConcreteModelSpec;
import com.kylantis.eaa.core.base.BaseTable;

public class DefaultConcreteModelTable extends BaseTable {

	public String path;
	public String model;

	@Override
	public String tableHashKey() {
		return DefaultConcreteModelSpec.PATH;
	}
}
