
package com.kylantis.eaa.core.tables;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.AddressesSpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class AddressesTable extends BaseTable {

	public String id;

	public String ownerId;

	public String street;

	public Integer city;

	public Integer stateHashKey;

	public String state;

	public Integer countryHashKey;

	public String country;

	@Override
	public String tableHashKey() {
		return AddressesSpec.ID;
	}

	@Override
	public Map<String, IndexConfig> unindexedGSIs() {
		return new FluentHashMap<String, IndexConfig>().with(AddressesSpec.CITY_INDEX, new IndexConfig(AddressesSpec.CITY).withProjection(new Projection()
				.withProjectionType(ProjectionType.INCLUDE).withNonKeyAttributes(AddressesSpec.OWNER_ID)));
	}

	@Override
	public Map<String, IndexConfig> indexedGSIs() {

		return new FluentHashMap<String, IndexConfig>().with(AddressesSpec.STATE_INDEX,
				new IndexConfig(AddressesSpec.STATE_HASHKEY, AddressesSpec.STATE).withProjection(new Projection()
						.withProjectionType(ProjectionType.INCLUDE).withNonKeyAttributes(AddressesSpec.OWNER_ID)))

				.with(AddressesSpec.COUNTRY_INDEX,
						new IndexConfig(AddressesSpec.COUNTRY_HASHKEY, AddressesSpec.COUNTRY)
								.withProjection(new Projection().withProjectionType(ProjectionType.INCLUDE)
										.withNonKeyAttributes(AddressesSpec.OWNER_ID)));
	}

}
