package com.kylantis.eaa.core.tables.staging;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.attributes.BaseUserTableSpec;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;

public class BaseUserTable extends BaseTable {

	public Integer id;
	public String pass;

	public String defaultRole;
	public Set<String> roles;

	public String email;
	
	public String fname;
	public String mname;
	public String lname;
	
	public String image;
	
	public Long phone;
	
	public String defaultAddress;
	public Set<String> addresses;
	
	public Date dob;

	public String dateCreated;
	
	public BaseUserTable() {
		
	}

	@Override
	public String tableHashKey() {
		return BaseUserTableSpec.ID;
	}

	@Override
	public Map<String, IndexConfig> unindexedGSIs() {

		Map<String, IndexConfig> map = new HashMap<>();

		map.put(BaseUserTableSpec.EMAIL_INDEX, new IndexConfig(BaseUserTableSpec.EMAIL).withProjection(
				new Projection().withProjectionType(ProjectionType.INCLUDE).withNonKeyAttributes(BaseUserTableSpec.PASS)));

		map.put(BaseUserTableSpec.PHONE_INDEX, new IndexConfig(BaseUserTableSpec.PHONE).withProjection(
				new Projection().withProjectionType(ProjectionType.INCLUDE).withNonKeyAttributes(BaseUserTableSpec.PASS)));

		return map;
	}

}
