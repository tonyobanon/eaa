package com.kylantis.eaa.core.indexing;

import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;

public class IndexConfig {

	//public static final ProvisionedThroughput DEFAULT_PROVISIONED_THROUGHPUT = new ProvisionedThroughput(3000l, 1000l);
	public static final ProvisionedThroughput DEFAULT_PROVISIONED_THROUGHPUT = new ProvisionedThroughput(100l, 50l);
	
	private final KeySchemaElement[] keySchemaElement;
	private Projection projection;
	
	private ProvisionedThroughput provisionedThroughput;
	
	private QueryType queryType;
	
	public IndexConfig(String hashKeyAttribute) {
		
		this.keySchemaElement = new KeySchemaElement[]{new KeySchemaElement(hashKeyAttribute, KeyType.HASH)};
		
		this.projection = new Projection().withProjectionType(ProjectionType.KEYS_ONLY);
		this.provisionedThroughput = DEFAULT_PROVISIONED_THROUGHPUT;
	}
	
   public IndexConfig(String hashKeyAttribute, String rangeKeyAttribute) {
		
		this.keySchemaElement = new KeySchemaElement[2];
		
		this.keySchemaElement[0] = new KeySchemaElement(hashKeyAttribute, KeyType.HASH);
		this.keySchemaElement[1] = new KeySchemaElement(rangeKeyAttribute, KeyType.RANGE);
		
		this.projection = new Projection().withProjectionType(ProjectionType.KEYS_ONLY);
		this.provisionedThroughput = DEFAULT_PROVISIONED_THROUGHPUT;
	}

	public Projection getProjection() {
		return projection;
	}

	public IndexConfig withProjection(Projection projection) {
		this.projection = projection;
		return this;
	}

	public ProvisionedThroughput getProvisionedThroughput() {
		return provisionedThroughput;
	}

	public IndexConfig withProvisionedThroughput(ProvisionedThroughput provisionedThroughput) {
		this.provisionedThroughput = provisionedThroughput;
		return this;
	}

	public KeySchemaElement[] getKeySchemaElement() {
		return keySchemaElement;
	}

	public QueryType getQueryType() {
		return queryType == null ? QueryType.EQ : queryType;
	}

	public IndexConfig withQueryType(QueryType queryType) {
		this.queryType = queryType;
		return this;
	}
	
}
