package com.kylantis.eaa.core.indexing.tables;

import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.attributes.IndexPropertySpec;

public class IndexPropertyTable extends BaseTable {

	public String id;
	public String hashKey;
	public String rangeKey;
	public String projections;
	public Long provisionedThroughput;
	public String tableHashKey;
	public String tableRangeKey;
	public String queryType;

	@Override
	public String tableHashKey() {
		return IndexPropertySpec.ID;
	}
}
