package com.kylantis.eaa.core.indexing.tables;

import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.attributes.IndexEntrySpec;

public class IndexEntryTable extends BaseTable {

	public String entryId;
	public String indexName;
	public int partitionId;
	public int size;


	@Override
	public String tableHashKey() {
		return IndexEntrySpec.ENTRY_ID;
	}
	
	@Override
	public String tableRangeKey() {
		return IndexEntrySpec.INDEX_NAME;
	}
}
