package com.kylantis.eaa.core.indexing.tools;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.amazonaws.services.dynamodbv2.document.AttributeUpdate;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import com.amazonaws.services.dynamodbv2.document.QueryFilter;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.kylantis.eaa.core.base.Logger;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.dbutils.DBTooling;
import com.kylantis.eaa.core.indexing.Index;
import com.kylantis.eaa.core.indexing.PartitionSizeExceeded;
import com.kylantis.eaa.core.indexing.QueryType;
import com.kylantis.eaa.core.indexing.attributes.IndexEntrySpec;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IAttributeModel;

@ConcreteModel
public class AttributeModel extends IAttributeModel{

	// K: table/rangeKey, V: indexName
	protected static Map<String, String> rangeKeyToIndexName = Collections.synchronizedMap(new HashMap<>());

	// K: indexId, V: hashKey
	protected static Map<String, String> indexToHashKey = Collections.synchronizedMap(new HashMap<>());

	// K: indexId, V: hashKey
	protected static Map<String, Collection<String>> indexToNonKeyProjections = Collections
			.synchronizedMap(new HashMap<>());

	// K: indexId, V: readThroughput
	protected static Map<String, Long> indexReadThroughput = Collections.synchronizedMap(new HashMap<>());

	// K: tableName, V: hashKey
	protected static Map<String, String> tableToHashKey = Collections.synchronizedMap(new HashMap<>());

	// K: tableName, V: rangeKey
	protected static Map<String, String> tableToRangeKey = Collections.synchronizedMap(new HashMap<>());

	public void newRangeKey(Index index, List<String> projections, String hashAttribute, String rangeAttribute,
			Long readThroughputCapacity, QueryType queryType, String tableHashKey, String tableRangeKey) {

		Logger.info("Registering new index in " + AttributeModel.class.getSimpleName());

		if (!rangeKeyToIndexName.containsKey(index.getTableName() + "/" + rangeAttribute)) {
			rangeKeyToIndexName.put(index.getTableName() + "/" + rangeAttribute, index.getIndexName());
		}

		if (!indexToHashKey.containsKey(index.getId())) {
			indexToHashKey.put(index.getId(), hashAttribute);
		}

		if (!indexToNonKeyProjections.containsKey(index.getId())) {
			indexToNonKeyProjections.put(index.getId(), projections);
		}

		if (!indexReadThroughput.containsKey(index.getId())) {
			indexReadThroughput.put(index.getId(), readThroughputCapacity);
		}

		if (!tableToHashKey.containsKey(index.getTableName())) {
			tableToHashKey.put(index.getTableName(), tableHashKey);
		}

		if (tableRangeKey != null && !tableToRangeKey.containsKey(index.getTableName())) {
			tableToRangeKey.put(index.getTableName(), tableRangeKey);
		}
	}
	
	public Integer getIndexSize(Index index, String indexRangeKey, Object indexRangeKeyValue, Item item) {
	
		if (indexRangeKeyValue == null) {
			return 0;
		}
	
		int indexSize = 0;
	
		String tableHashKey = AttributeModel.tableToHashKey.get(index.getTableName());
		Object tableHashKeyValue = item.get(tableHashKey);
	
		String tableRangeKey = AttributeModel.tableToRangeKey.get(index.getTableName());
		Object tableRangeKeyValue = null;
	
		if (tableRangeKey != null) {
			tableRangeKeyValue = item.get(tableRangeKey);
		}
	
		indexSize += DBTooling.stringSize(tableHashKey);
		indexSize += DBTooling.getValueSize(tableHashKeyValue);
	
		if (tableRangeKey != null) {
			indexSize += DBTooling.stringSize(tableRangeKey);
			indexSize += DBTooling.getValueSize(tableRangeKeyValue);
		}
	
		indexSize += DBTooling.stringSize(indexRangeKey);
		indexSize += DBTooling.getValueSize(indexRangeKeyValue);
	
		indexSize += DBTooling.stringSize(AttributeModel.indexToHashKey.get(index.getId()));
		indexSize += DBTooling.numberSize(getQueryModel().partitionCount(index) + 1);
	
		for (String projection : AttributeModel.indexToNonKeyProjections.get(index.getId())) {
			indexSize += DBTooling.stringSize(projection);
			indexSize += DBTooling.getValueSize(item.get(projection));
		}
	
		return indexSize;
	}

	public void putValue(String tableName, Item item) {

		if (!tableToHashKey.containsKey(tableName)) {
			return;
		}

		for (Entry<String, Object> e : item.attributes()) {

			String indexRangeKey = e.getKey();

			if (rangeKeyToIndexName.containsKey(tableName + "/" + indexRangeKey)) {

				TablePrimaryKey tableKey = new TablePrimaryKey(tableName, item);

				Object indexRangeKeyValue = e.getValue();
				
				Logger.info("--->  Putting value: " + indexRangeKeyValue + " into " + tableName + "/" + indexRangeKey);

				// Get the indexId
				Index index = Index.fromId(tableName + "/" + rangeKeyToIndexName.get(tableName + "/" + indexRangeKey));

				// Get the entry Id
				String entryId = index.getTableName() + "/" + (String) tableKey.getHashValue();

				Item indexEntry = StorageServiceFactory.getDirectDatabase().getTable(IndexEntrySpec.TABLE_NAME)
						.getItem(IndexEntrySpec.ENTRY_ID, entryId, IndexEntrySpec.INDEX_NAME, index.getIndexName());

				Integer itemSize = this.getIndexSize(index, indexRangeKey, indexRangeKeyValue, item);

				if (indexEntry == null) {
					addIndexEntry(index, tableKey, itemSize);
				} else {
					updateIndexEntrySize(index, tableKey, itemSize);
				}
			}
		}
	}

	private void addIndexEntry(Index index, TablePrimaryKey tableKey, int itemSize) {

		if (itemSize == 0) {
			return;
		}

		// Get the entry Id
		String entryId = index.getTableName() + "/" + (String) tableKey.getHashValue();

		// Generate partition Id
		int partitionId = getQueryModel().nextPartitionKey(index, itemSize);

		// Update the hash attribute of the index
		if (tableKey.getRangeKey() != null) {
			StorageServiceFactory.getDirectDatabase().getTable(index.getTableName()).updateItem(tableKey.getHashKey(),
					tableKey.getHashValue(), tableKey.getRangeKey(), tableKey.getRangeValue(),
					new AttributeUpdate(indexToHashKey.get(index.getId())).addNumeric(partitionId));
		} else {

			StorageServiceFactory.getDirectDatabase().getTable(index.getTableName()).updateItem(tableKey.getHashKey(),
					tableKey.getHashValue(),
					new AttributeUpdate(indexToHashKey.get(index.getId())).addNumeric(partitionId));
		}

		// Add entry to the IndexEntry table
		Item newEntry = new Item()
				.withPrimaryKey(IndexEntrySpec.ENTRY_ID, entryId, IndexEntrySpec.INDEX_NAME, index.getIndexName())
				.withNumber(IndexEntrySpec.PARTITION_ID, partitionId).withInt(IndexEntrySpec.SIZE, itemSize);

		StorageServiceFactory.getDirectDatabase().getTable(IndexEntrySpec.TABLE_NAME)
				.putItem(new PutItemSpec().withItem(newEntry));

		// Increment partition size
		getQueryModel().incrementPartition(index, partitionId, itemSize);
		
		Logger.info("--->  Added index entry for index " + index.getId() + ", with size: " + itemSize);

	}

	private void updateIndexEntrySize(Index index, TablePrimaryKey tableKey, int itemSize) {

		
		
		if (itemSize == 0) {

			// since itemSize is 0, then indexRangeKeyValue is null
			// Also set the hashKey to null, so that DynamoDB
			// removes it from index
			// Take advantage of sparse indexing

			if (tableKey.getRangeKey() != null) {
				StorageServiceFactory.getDirectDatabase().getTable(index.getTableName()).updateItem(
						tableKey.getHashKey(), tableKey.getHashValue(), tableKey.getRangeKey(),
						tableKey.getRangeValue(), new AttributeUpdate(indexToHashKey.get(index.getId())).put(null));
			} else {
				StorageServiceFactory.getDirectDatabase().getTable(index.getTableName()).updateItem(
						tableKey.getHashKey(), tableKey.getHashValue(),
						new AttributeUpdate(indexToHashKey.get(index.getId())).put(null));
			}

			// Then remove from partition
			deleteValue(index.getTableName(), tableKey.getHashValue(), index.getIndexName());
			return;
		}

		// Get the entry Id
		String entryId = index.getTableName() + "/" + (String) tableKey.getHashValue();

		Table indexEntryTable = StorageServiceFactory.getDirectDatabase().getTable(IndexEntrySpec.TABLE_NAME);

		Item indexEntry = indexEntryTable.getItem(IndexEntrySpec.ENTRY_ID, entryId, IndexEntrySpec.INDEX_NAME,
				index.getIndexName());

		boolean partitionChanged = false;

		int partitionId = indexEntry.getInt(IndexEntrySpec.PARTITION_ID);

		int currentItemSize = indexEntry.getInt(IndexEntrySpec.SIZE);

		int sizeOffset = itemSize - currentItemSize;

		if (sizeOffset < 0) {
			Logger.info("--->  Decrementing partition " + partitionId + " for index " + index.getId() + " by "
					+ " size: " + (sizeOffset - sizeOffset - sizeOffset));
			System.out.println("Is " + sizeOffset + " === " + (sizeOffset - sizeOffset - sizeOffset));
			
			getQueryModel().decrementPartition(index, partitionId, sizeOffset - sizeOffset - sizeOffset);

		} else {

			try {
				Logger.info("--->  Incrementing partition " + partitionId + " for index " + index.getId() + " by "
						+ " size: " + sizeOffset);
				getQueryModel().incrementPartition(index, partitionId, sizeOffset);

			} catch (PartitionSizeExceeded ex) {

				// Reassign this entry to another partition

				// But first free up used space on current partition
				getQueryModel().decrementPartition(index, partitionId, currentItemSize);

				// Then re-assign

				// Generate partition Id
				partitionId = getQueryModel().nextPartitionKey(index, itemSize);

				// Update the hash attribute of the index
				if (tableKey.getRangeKey() != null) {
					StorageServiceFactory.getDirectDatabase().getTable(index.getTableName()).updateItem(
							tableKey.getHashKey(), tableKey.getHashValue(), tableKey.getRangeKey(),
							tableKey.getRangeValue(),
							new AttributeUpdate(indexToHashKey.get(index.getId())).addNumeric(partitionId));
				} else {
					StorageServiceFactory.getDirectDatabase().getTable(index.getTableName()).updateItem(
							tableKey.getHashKey(), tableKey.getHashValue(),
							new AttributeUpdate(indexToHashKey.get(index.getId())).addNumeric(partitionId));
				}

				// Increment partition size
				getQueryModel().incrementPartition(index, partitionId, itemSize);

				partitionChanged = true;

			}
		}

		// Update Entry
		
		UpdateItemSpec spec = new UpdateItemSpec()
				.withPrimaryKey(IndexEntrySpec.ENTRY_ID, entryId, IndexEntrySpec.INDEX_NAME, index.getIndexName());

		//.withUpdateExpression(partitionChanged ? "set #p=:v_p " : "" + "set #s=:v_s")
		//.withNameMap(new NameMap().with("#p", IndexEntrySpec.PARTITION_ID).with("#s", IndexEntrySpec.SIZE))
		//.withValueMap(new ValueMap().withInt(":v_p", partitionId).withInt(":v_s", itemSize)));

		
		if(partitionChanged){
			spec.withUpdateExpression(partitionChanged ? "set #p=:v_p " : "" + "set #s=:v_s")
			.withNameMap(new NameMap().with("#p", IndexEntrySpec.PARTITION_ID).with("#s", IndexEntrySpec.SIZE))
			.withValueMap(new ValueMap().withInt(":v_p", partitionId).withInt(":v_s", itemSize));
		}else{
			spec.withUpdateExpression("set #s=:v_s")
			.withNameMap(new NameMap().with("#s", IndexEntrySpec.SIZE))
			.withValueMap(new ValueMap().withInt(":v_s", itemSize));
		}
		
		indexEntryTable.updateItem(spec);
		
	}

	public void putValue(String tableName, Map<String, AttributeValue> attributes) {

		if (!tableToHashKey.containsKey(tableName)) {
			return;
		}

		for (Entry<String, AttributeValue> e : attributes.entrySet()) {

			String indexRangeKey = e.getKey();

			if (rangeKeyToIndexName.containsKey(tableName + "/" + indexRangeKey)) {

				TablePrimaryKey tableKey = new TablePrimaryKey(tableName, attributes);

				Object indexRangeKeyValue = e.getValue();

				// Get the indexId
				Index index = Index.fromId(tableName + "/" + rangeKeyToIndexName.get(tableName + "/" + indexRangeKey));

				// Get the entry Id
				String entryId = index.getTableName() + "/" + (String) tableKey.getHashValue();

				Item indexEntry = StorageServiceFactory.getDirectDatabase().getTable(IndexEntrySpec.TABLE_NAME)
						.getItem(IndexEntrySpec.ENTRY_ID, entryId, IndexEntrySpec.INDEX_NAME, index.getIndexName());

				Integer itemSize = AttributeUtils.getIndexSize(index, indexRangeKey, indexRangeKeyValue, attributes);

				if (indexEntry == null) {
					addIndexEntry(index, tableKey, itemSize);
				} else {
					updateIndexEntrySize(index, tableKey, itemSize);
				}
			}

		}
	}

	public void updateValue(String tableName, Map<String, AttributeValue> updates,
			Collection<KeyAttribute> keys) {

		if (!tableToHashKey.containsKey(tableName)) {
			return;
		}

		TablePrimaryKey tableKey = new TablePrimaryKey(tableName, keys);

		updateValue(tableName, updates, tableKey.getHashKey(), tableKey.getHashValue(), tableKey.getRangeKey(),
				tableKey.getRangeValue());
	}

	public void updateValue(String tableName, Map<String, AttributeValue> updates, KeyAttribute... keys) {

		if (!tableToHashKey.containsKey(tableName)) {
			return;
		}

		TablePrimaryKey tableKey = new TablePrimaryKey(tableName, keys);

		updateValue(tableName, updates, tableKey.getHashKey(), tableKey.getHashValue(), tableKey.getRangeKey(),
				tableKey.getRangeValue());

	}

	public void updateValue(String tableName, Map<String, AttributeValue> updates, String hashKey,
			Object hashValue) {

		if (!tableToHashKey.containsKey(tableName)) {
			return;
		}

		updateValue(tableName, updates, hashKey, hashValue, null, null);
	}

	public void updateValue(String tableName, Map<String, AttributeValue> updates, String hashKey,
			Object hashValue, String rangeKey, Object rangeValue) {

		if (!tableToHashKey.containsKey(tableName)) {
			return;
		}

		for (Entry<String, AttributeValue> update : updates.entrySet()) {

			String indexRangeKey = update.getKey();

			if (rangeKeyToIndexName.containsKey(tableName + "/" + indexRangeKey)) {

				TablePrimaryKey tableKey = new TablePrimaryKey(hashKey, hashValue, rangeKey, rangeValue);

				Object indexRangeKeyValue = update.getValue();

				// Get the indexId
				Index index = Index.fromId(tableName + "/" + rangeKeyToIndexName.get(tableName + "/" + indexRangeKey));

				Integer itemSize = AttributeUtils.getIndexSize(index, indexRangeKey, indexRangeKeyValue, updates);

				updateIndexEntrySize(index, tableKey, itemSize);
			}

		}
	}

	public void deleteValue(String tableName, KeyAttribute... keys) {
		if (!tableToHashKey.containsKey(tableName)) {
			return;
		}

		TablePrimaryKey tableKey = new TablePrimaryKey(tableName, keys);

		deleteValue(tableName, tableKey.getHashValue());
	}

	public void deleteValue(String tableName, Collection<KeyAttribute> keys) {

		if (!tableToHashKey.containsKey(tableName)) {
			return;
		}

		TablePrimaryKey tableKey = new TablePrimaryKey(tableName, keys);

		deleteValue(tableName, tableKey.getHashValue());
	}

	public void deleteValue(String tableName, Object tableHashValue) {
		deleteValue(tableName, tableHashValue, null);
	}

	public void deleteValue(String tableName, Object tableHashValue, String indexName) {

		String entryId = tableName + "/" + (String) tableHashValue;

		Table indexEntryTable = StorageServiceFactory.getDirectDatabase().getTable(IndexEntrySpec.TABLE_NAME);

		QuerySpec spec = new QuerySpec().withHashKey(IndexEntrySpec.ENTRY_ID, entryId);

		if (indexName != null) {
			spec.withQueryFilters(new QueryFilter(IndexEntrySpec.INDEX_NAME).eq(indexName));
		}

		ItemCollection<QueryOutcome> outcome = indexEntryTable.query(spec);

		Iterator<Item> iterator = outcome.iterator();

		while (iterator.hasNext()) {

			Item o = iterator.next();

			Index index = Index.fromId(tableName + "/" + o.getString(IndexEntrySpec.INDEX_NAME));
			int partitionId = o.getInt(IndexEntrySpec.PARTITION_ID);
			int size = o.getInt(IndexEntrySpec.SIZE);

			// Reduce partition size
			getQueryModel().decrementPartition(index, partitionId, size);

			// Remove from index entry table
			indexEntryTable.deleteItem(new DeleteItemSpec().withPrimaryKey(IndexEntrySpec.ENTRY_ID, entryId,
					IndexEntrySpec.INDEX_NAME, index.getIndexName()));
		}
	}

	@Override
	public void start() {
		
	}

}
