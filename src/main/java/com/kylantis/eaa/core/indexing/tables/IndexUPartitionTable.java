package com.kylantis.eaa.core.indexing.tables;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.IndexConfig;
import com.kylantis.eaa.core.indexing.attributes.IndexUPartitionSpec;

public class IndexUPartitionTable extends BaseTable {

	public String id;
	public Integer partitionId;
	public Integer size;

	@Override
	public String tableHashKey() {
		return IndexUPartitionSpec.ID;
	}
	
	@Override
	public String tableRangeKey() {
		return IndexUPartitionSpec.PARTITION_ID;
	}
	
	@Override
	public Map<String, IndexConfig> LSIs() {
		Map<String, IndexConfig> map = new HashMap<>();

		map.put(IndexUPartitionSpec.SIZE_INDEX, new IndexConfig(IndexUPartitionSpec.ID, IndexUPartitionSpec.SIZE)
				.withProjection(new Projection().withProjectionType(ProjectionType.ALL)));
		return map;
	}

}
