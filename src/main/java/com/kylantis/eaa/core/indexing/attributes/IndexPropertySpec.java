package com.kylantis.eaa.core.indexing.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class IndexPropertySpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "IndexPropertyTable";

	public static final String ID = "id";
	public static final String HASH_KEY = "hashKey";
	public static final String RANGE_KEY = "rangeKey";
	public static final String PROJECTIONS = "projections";
	public static final String PROVISIONED_THROUGHPUT = "provisionedThroughput";
	public static final String TABLE_HASH_KEY = "tableHashKey";
	public static final String TABLE_RANGE_KEY = "tableRangeKey";
	public static final String QUERY_TYPE = "queryType";
}
