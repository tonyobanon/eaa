package com.kylantis.eaa.core.indexing.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class IndexEntrySpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "IndexEntryTable";
	
	public static final String ENTRY_ID = "entryId";
	public static final String INDEX_NAME = "indexName";
	public static final String PARTITION_ID = "partitionId";
	public static final String SIZE = "size";
	
}
