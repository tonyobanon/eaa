package com.kylantis.eaa.core.indexing;

import com.kylantis.eaa.core.dbutils.EntityModeller;
import com.kylantis.eaa.core.indexing.tools.QueryModel;

/**
 * This is our entry point
 * */
public class DDBEPLauncher {

	public static void main(String[] args) {
		
		//Before hand, Make sure to set system variables used by StorageServiceFactory, see readme.txt
		
		//Application starts here
		if(isAppFirstRun()){
			
			//Create Tables (for classes whose names end with 'Table')
			EntityModeller.createTables();
			
			//Horray! Tables created
			
		}else{
			
			//The app was probably restarted
			//Do some other stuff
		}
		
		//Start our Query Engine
		new QueryModel().start();
	}

	private static boolean isAppFirstRun(){
		//Perform logic here
		return true;
	}
}
