package com.kylantis.eaa.core.indexing.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class IndexPartitionSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "IndexPartitionTable";

	public static final String ID = "id";
	public static final String PARTITION = "partition";
	public static final String SIZE = "size";

}
