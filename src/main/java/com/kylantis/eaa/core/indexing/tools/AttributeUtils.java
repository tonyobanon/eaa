package com.kylantis.eaa.core.indexing.tools;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.kylantis.eaa.core.dbutils.DBTooling;
import com.kylantis.eaa.core.indexing.Index;

public class AttributeUtils {

	public static KeyAttribute[] getKeyAttributes(String tableName, Map<String, AttributeValue> attr) {
	
		String hashAttribute = AttributeModel.tableToHashKey.get(tableName);
	
		if (attr.size() == 1) {
			return new KeyAttribute[] { new KeyAttribute(hashAttribute, DBTooling.getAttributeValue(attr.get(hashAttribute))) };
		} else {
	
			String rangeAttribute = AttributeModel.tableToRangeKey.get(tableName);
			return new KeyAttribute[] { new KeyAttribute(hashAttribute, DBTooling.getAttributeValue(attr.get(hashAttribute))),
					new KeyAttribute(rangeAttribute, DBTooling.getAttributeValue(attr.get(rangeAttribute))) };
		}
	}

	public static int getIndexSize(Index index, String indexRangeKey, Object indexRangeKeyValue,
			Map<String, AttributeValue> attributes) {
	
		if (indexRangeKeyValue == null) {
			return 0;
		}
	
		int indexSize = 0;
	
		String tableHashKey = AttributeModel.tableToHashKey.get(index.getTableName());
		Object tableHashKeyValue = DBTooling.getAttributeValue(attributes.get(tableHashKey));
	
		String tableRangeKey = AttributeModel.tableToRangeKey.get(index.getTableName());
		Object tableRangeKeyValue = null;
	
		if (tableRangeKey != null) {
			tableRangeKeyValue = DBTooling.getAttributeValue(attributes.get(tableRangeKey));
		}
	
		indexSize += DBTooling.stringSize(tableHashKey);
		indexSize += DBTooling.getValueSize(tableHashKeyValue);
	
		if (tableRangeKey != null) {
			indexSize += DBTooling.stringSize(tableRangeKey);
			indexSize += DBTooling.getValueSize(tableRangeKeyValue);
		}
	
		indexSize += DBTooling.stringSize(indexRangeKey);
		indexSize += DBTooling.getValueSize(indexRangeKeyValue);
	
		indexSize += DBTooling.stringSize(AttributeModel.indexToHashKey.get(index.getId()));
	
		for (String projection : AttributeModel.indexToNonKeyProjections.get(index.getId())) {
			indexSize += DBTooling.stringSize(projection);
			indexSize += DBTooling.getValueSize(attributes.get(projection));
		}
	
		return indexSize;
	
	}

}
