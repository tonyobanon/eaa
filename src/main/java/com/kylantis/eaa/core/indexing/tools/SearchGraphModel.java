package com.kylantis.eaa.core.indexing.tools;

import java.util.function.Consumer;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import com.amazonaws.services.dynamodbv2.document.QueryFilter;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.RangeKeyCondition;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.kylantis.eaa.core.attributes.SearchGraphSpec;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.indexing.Index;
import com.kylantis.eaa.core.indexing.QueryCheckpointSpec;
import com.kylantis.eaa.core.indexing.SearchGraphId;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.ISearchGraphModel;
import com.kylantis.eaa.core.ml.MLRepository;

@ConcreteModel(dependencies = {MLRepository.CONFIG_MODEL, MLRepository.QUERY_MODEL})
public class SearchGraphModel extends ISearchGraphModel {

	public boolean graphSearch(String checkpointId, Consumer<Item> consumer) {

		if (checkpointId == null || !getQueryModel().getQueryCheckpoints().containsKey(checkpointId)) {
			throw new ResourceException(ResourceException.FAILED_VALIDATION);
		}

		QueryCheckpointSpec checkpoint = getQueryModel().getQueryCheckpoints().get(checkpointId);

		int partitionId = checkpoint.getPartitionId();
		KeyAttribute[] lastEvaluatedKey = checkpoint.getLastEvaluatedKey();

		int partitionCount = getQueryModel().partitionCount(new Index(SearchGraphSpec.TABLE_NAME, SearchGraphSpec.MATRIX_INDEX));
		int accumulatedSoFar = 0;

		while (partitionId <= partitionCount) {

			QuerySpec spec = new QuerySpec().withHashKey(SearchGraphSpec.MATRIX_HASHKEY, partitionId)
					.withRangeKeyCondition(
							new RangeKeyCondition(SearchGraphSpec.MATRIX).beginsWith(checkpoint.getKeyword()));

			spec.withMaxResultSize(checkpoint.getLimit() - accumulatedSoFar);

			if (lastEvaluatedKey != null) {
				spec.withExclusiveStartKey(lastEvaluatedKey);
			}

			for (int entityType : checkpoint.getEntityType()) {
				spec.withQueryFilters(new QueryFilter(SearchGraphSpec.ENTITY_TYPE).eq(entityType));
			}

			ItemCollection<QueryOutcome> outcome = graphSearch(spec);

			outcome.forEach(item -> {
				consumer.accept(item);
			});

			accumulatedSoFar += outcome.getAccumulatedItemCount();

			if (accumulatedSoFar >= checkpoint.getLimit()) {

				lastEvaluatedKey = AttributeUtils.getKeyAttributes(SearchGraphSpec.TABLE_NAME,
						outcome.getLastLowLevelResult().getQueryResult().getLastEvaluatedKey());

				getQueryModel().getQueryCheckpoints().get(checkpointId).setPartitionId(partitionId).setLastEvaluatedKey(lastEvaluatedKey);

				return true;

			} else {

				partitionId += 1;
				lastEvaluatedKey = null;

			}

		}

		// All partitions have been scanned
		getQueryModel().getQueryCheckpoints().remove(checkpointId);

		return false;
	}

	private static ItemCollection<QueryOutcome> graphSearch(QuerySpec spec) {
		return StorageServiceFactory.getDirectDatabase().getTable(SearchGraphSpec.TABLE_NAME)
				.getIndex(SearchGraphSpec.MATRIX_INDEX).query(spec);
	}
	
	public void putEntry(SearchGraphId id, int entityType, String matrix[]) {
		
		removeEntry(id);
		
		TableWriteItems items = new TableWriteItems(SearchGraphSpec.TABLE_NAME);

		for (int i = 0; i < matrix.length; i++) {

			Item item = new Item().withPrimaryKey(SearchGraphSpec.ID, id.toString(), SearchGraphSpec.MATRIX_ID, i + 1)
					.withInt(SearchGraphSpec.ENTITY_TYPE, entityType).withString(SearchGraphSpec.MATRIX, matrix[i]);

			items.addItemToPut(item);
		}

		if (items.getItemsToPut() != null && items.getItemsToPut().size() > 0) {
			StorageServiceFactory.getDocumentDatabase().batchWriteItem(items);
		}
	}

	public void removeEntry(SearchGraphId id) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(SearchGraphSpec.TABLE_NAME);

		ItemCollection<QueryOutcome> outcome = table.query(new QuerySpec().withHashKey(SearchGraphSpec.ID, id.toString())
				.withProjectionExpression(SearchGraphSpec.MATRIX_ID));

		outcome.forEach(item -> {
			table.deleteItem(SearchGraphSpec.ID, id.toString(), SearchGraphSpec.MATRIX_ID, item.getInt(SearchGraphSpec.MATRIX_ID));
		});
	}


}
