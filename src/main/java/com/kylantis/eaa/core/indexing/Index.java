package com.kylantis.eaa.core.indexing;

public class Index {

	private String tableName;
	private String indexName;
	
	public Index() {
	}

	public Index(String tableName, String indexName) {
		this.tableName = tableName;
		this.indexName = indexName;
	}

	public String getTableName() {
		return tableName;
	}

	
	public Index withTableName(String tableName) {
		this.tableName = tableName;
		return this;
	}

	public String getIndexName() {
		return indexName;
	}

	public Index withIndexName(String indexName) {
		this.indexName = indexName;
		return this;
	}
	
	public String getId() {
		return getTableName() + "/" + getIndexName();
	}
	
	public static Index fromId(String indexId) {
		String[] arr = indexId.split("/");
		return new Index(arr[0], arr[1]);
	}

}
