package com.kylantis.eaa.core.indexing;

import com.amazonaws.services.dynamodbv2.document.KeyAttribute;

public class QueryCheckpointSpec {

	private int partitionId;

	private Integer[] entityTypes;
	private String keyword;

	private Integer limit;
	private KeyAttribute[] lastEvaluatedKey;

	
	public QueryCheckpointSpec(String keyword) {
		this.keyword = keyword;
		this.entityTypes = new Integer[] {};
		this.limit = 12;
		partitionId = 1;
	}

	public QueryCheckpointSpec(String keyword, Integer limit) {
		this.keyword = keyword;
		this.entityTypes = new Integer[] {};
		this.limit = limit;
		this.partitionId = 1;
	}

	public QueryCheckpointSpec(String keyword, int limit, Integer... entityTypes) {
		this.keyword = keyword;
		this.entityTypes = entityTypes;
		this.limit = limit;
		this.partitionId = 1;
	}

	public int getPartitionId() {
		return partitionId;
	}

	public QueryCheckpointSpec setPartitionId(int partitionId) {
		this.partitionId = partitionId;
		return this;
	}

	public QueryCheckpointSpec incrementPartitionId(int partitionId) {
		this.partitionId += partitionId;
		return this;
	}

	public KeyAttribute[] getLastEvaluatedKey() {
		return lastEvaluatedKey;
	}

	public QueryCheckpointSpec setLastEvaluatedKey(KeyAttribute[] lastEvaluatedKey) {
		this.lastEvaluatedKey = lastEvaluatedKey;
		return this;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer[] getEntityType() {
		return entityTypes;
	}

	public void setEntityType(Integer... entityTypes) {
		this.entityTypes = entityTypes;
	}

}
