package com.kylantis.eaa.core.indexing.tables;

import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.indexing.attributes.IndexPartitionSpec;

public class IndexPartitionTable extends BaseTable {

	public String id;
	public Integer partition;
	public Integer size;

	@Override
	public String tableHashKey() {
		return IndexPartitionSpec.ID;
	}

	@Override
	public String tableRangeKey() {
		return IndexPartitionSpec.PARTITION;
	}
}
