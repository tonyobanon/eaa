package com.kylantis.eaa.core.indexing.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class IndexUPartitionSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "IndexUPartitionTable";

	public static final String ID = "id";
	public static final String PARTITION_ID = "partitionId";
	public static final String SIZE = "size";
	
	public static final String SIZE_INDEX = "sizeIndex";
}
