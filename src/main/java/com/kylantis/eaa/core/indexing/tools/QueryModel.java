package com.kylantis.eaa.core.indexing.tools;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.kylantis.eaa.core.base.Logger;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.base.Todo;
import com.kylantis.eaa.core.dbutils.DBTooling;
import com.kylantis.eaa.core.indexing.Index;
import com.kylantis.eaa.core.indexing.PartitionSizeExceeded;
import com.kylantis.eaa.core.indexing.QueryCheckpointSpec;
import com.kylantis.eaa.core.indexing.QueryType;
import com.kylantis.eaa.core.indexing.attributes.IndexPartitionSpec;
import com.kylantis.eaa.core.indexing.attributes.IndexPropertySpec;
import com.kylantis.eaa.core.indexing.attributes.IndexUPartitionSpec;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IQueryModel;
import com.kylantis.eaa.core.ml.MLRepository;
import com.kylantis.eaa.core.setup.InstallOptions;
import com.kylantis.eaa.core.utils.Utils;

//Query Model class need to keep all partition data it uses in memory

//Optimize, Implement Optimistic Locks for partition key lookups, i.e nextPartitionKey
//Keep partition count, and Size in memory, i.e IndexPartitionTable
//In some cases don't use the Adapter in this class i.e. QueryModel to avoid unnecessary overhead

//Bug alert: When querying a single index in dynamoDBLocal from different threads using different hash keys,
//they both return the same result: typically that of the last thread that completes the query
//Ensure that this happens only for local development. A temporary fix is to set MAX_CONCURRENT_THREAD_PER_QUERY = 1

@Todo("Implement all comments in this class")

@ConcreteModel(dependencies = { MLRepository.CONFIG_MODEL })
public class QueryModel extends IQueryModel {

	// Set to a reasonable value in production
	private static final Integer QUERY_TIMEOUT_IN_MILLIS = 10000;

	private static final Integer MAX_CONCURRENT_THREAD_PER_QUERY = 1;

	// @DEV
	// 64000
	private static final Integer MAX_PARTITION_SIZE = 250;

	private static final Map<String, QueryCheckpointSpec> queryCheckpoints = Collections
			.synchronizedMap(new HashMap<String, QueryCheckpointSpec>());

	// K: indexId, V: numberOfThreads being used.. --> 0 means no current query
	// is happening on index
	private static final Map<String, Integer> openQueries = Collections.synchronizedMap(new HashMap<String, Integer>());

	// This will be used later, for metrics, or throttling
	private static final Map<String, Integer> indexQueryToThreadCount = Collections
			.synchronizedMap(new HashMap<String, Integer>());

	// If true, It indicates that a call to nextPartitionKey() is in execution
	private static final Map<String, Boolean> activePartitionKeyQuery = Collections
			.synchronizedMap(new HashMap<String, Boolean>());




	@Override
	public void preInstall() {

	}

	@Override
	public void install(InstallOptions options) {

	}

	@Override
	public void start() {

		// Scan all entries of the IndexProperty Table to fetch indexed GSIs

		StorageServiceFactory.getDirectDatabase().getTable(IndexPropertySpec.TABLE_NAME).scan(new ScanSpec())
				.forEach(item -> {

					Index index = Index.fromId(item.getString(IndexPropertySpec.ID));
					String indexHashKey = item.getString(IndexPropertySpec.HASH_KEY);
					String indexRangeKey = item.getString(IndexPropertySpec.RANGE_KEY);
					List<String> projections = DBTooling.getProjections(item.get(IndexPropertySpec.PROJECTIONS));
					Long readThroughputCapacity = item.getLong(IndexPropertySpec.PROVISIONED_THROUGHPUT);
					String tableHashKey = item.getString(IndexPropertySpec.TABLE_HASH_KEY);
					String tableRangeKey = item.getString(IndexPropertySpec.TABLE_RANGE_KEY);
					QueryType queryType = QueryType.parse(item.getString(IndexPropertySpec.QUERY_TYPE));

					getAttributeModel().newRangeKey(index, projections, indexHashKey, indexRangeKey, readThroughputCapacity,
							queryType, tableHashKey, tableRangeKey);

					openQueries.put(index.getId(), 0);
					activePartitionKeyQuery.put(index.getId(), false);

				});
	}
	

	public final Map<String, QueryCheckpointSpec> getQueryCheckpoints() {
		return queryCheckpoints;
	}

	public Map<String, Integer> getOpenQueries() {
		return openQueries;
	}

	public String newSearchKey(String keyword, int limit, Integer... entityTypes) {
		String checkpointId = Utils.newRandom();
		queryCheckpoints.put(checkpointId, new QueryCheckpointSpec(keyword, limit, entityTypes));
		return checkpointId;
	}

	// Find a way to throttle query for excessive queries on an index
	// Queries should emit some sort of event that can be sent to websocket
	// clients.
	// Make considerations using the index's read throughput settings

	public void parallelQuery(Index index, QuerySpec spec, Consumer<Item> consumer) {

		openQueries.put(index.getId(), openQueries.get(index.getId()) + 1);

		Table queryTable = StorageServiceFactory.getDirectDatabase().getTable(index.getTableName());
		com.amazonaws.services.dynamodbv2.document.Index queryIndex = queryTable.getIndex(index.getIndexName());

		// long provisionedReadThroghhput =
		// AttributeModel.indexReadThroughput.get(index.getId());

		int partitionCount = partitionCount(index);

		if (partitionCount == 0) {
			return;
		}

		List<Integer> segments = new ArrayList<>();

		if (partitionCount <= MAX_CONCURRENT_THREAD_PER_QUERY) {
			segments.add(partitionCount);
		} else {

			int segmentCount = partitionCount / MAX_CONCURRENT_THREAD_PER_QUERY;

			for (int i = 0; i < segmentCount; i++) {
				segments.add(MAX_CONCURRENT_THREAD_PER_QUERY);
			}

			if (partitionCount % MAX_CONCURRENT_THREAD_PER_QUERY > 0) {
				segments.add(partitionCount % MAX_CONCURRENT_THREAD_PER_QUERY);
			}
		}

		Logger.debug("Starting query operation for index: '" + index.getId() + "' with " + partitionCount
				+ " partition(s), " + segments.size() + " segment(s)");

		int currentPartition = 0;

		for (int partitions : segments) {

			Logger.debug("Querying new segment for index: '" + index.getId() + "'. Total partitions in this segment -> "
					+ partitions);

			Thread currentThread = Thread.currentThread();

			Collection<Integer> partitionsQueried = Collections.synchronizedList(new ArrayList<>());

			for (int i = 0; i < partitions; i++) {

				// Increment current Partition
				currentPartition += 1;

				Integer _currentPartition = currentPartition;

				Thread newThread = new Thread(new Runnable() {

					@Override
					public void run() {

						partitionsQueried.add(_currentPartition);

						spec.getNameMap().put("#__p", AttributeModel.indexToHashKey.get(index.getId()));
						spec.getValueMap().put(":__v_p", _currentPartition);

						String ConditionExpression = spec.getKeyConditionExpression() + " and #__p = :__v_p";

						Logger.debug("Query for partition " + _currentPartition + " in index '" + index.getId()
								+ "' has started with expression: " + ConditionExpression);

						ItemCollection<QueryOutcome> outcome = queryIndex
								.query(new QuerySpec().withKeyConditionExpression(ConditionExpression)
										.withNameMap(spec.getNameMap()).withValueMap(spec.getValueMap()));

						Iterator<Item> iterator = outcome.iterator();

						while (iterator.hasNext()) {
							consumer.accept(iterator.next());
						}

						partitionsQueried.remove(_currentPartition);

						Logger.debug("Query for partition " + _currentPartition + " in index '" + index.getId()
								+ "' has completed successfully using expression: " + ConditionExpression);

						if (partitionsQueried.isEmpty()) {
							currentThread.interrupt();
						}
					}
				});

				newThread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {

					@Override
					public void uncaughtException(Thread t, Throwable ex) {
						Logger.fatal("The following error occurred while performing query for partition "
								+ _currentPartition + " in index '" + index.getId() + "' -> " + ex.getMessage());
					}
				});

				newThread.start();

			}

			try {

				Thread.sleep(QUERY_TIMEOUT_IN_MILLIS);

				Logger.fatal(
						"A partition query operation in index '" + index.getId() + "' is taking too long to return");
				throw new RuntimeException(
						"A partition query operation in index '" + index.getId() + "' is taking too long to return");

			} catch (InterruptedException e) {
				continue;
			}
		}

		openQueries.put(index.getId(), openQueries.get(index.getId()) - 1);

		Logger.debug("Query operation for index: '" + index.getId() + "' with " + partitionCount + " partition(s), "
				+ segments.size() + " segment(s) has completed successfully");

	}

	public void newQueryOptimizedGSI(Index index, List<String> projections, QueryType queryType,
			String indexHashKey, String indexRangeKey, Long readThroughputCapacity, String tableHashKey,
			String tableRangeKey) {

		Logger.info("Registering new QueryOptimizedGSI: '" + index.getId());

		String indexId = index.getId();

		// Create entry in IndexPartition Table
		newPartition(index, 1);

		// Create entry in IndexProperty Table

		Table indexProperty = StorageServiceFactory.getDirectDatabase().getTable(IndexPropertySpec.TABLE_NAME);

		Item item = new Item().withPrimaryKey(IndexPropertySpec.ID, indexId)
				.withString(IndexPropertySpec.HASH_KEY, indexHashKey)
				.withString(IndexPropertySpec.RANGE_KEY, indexRangeKey)
				.with(IndexPropertySpec.PROJECTIONS, DBTooling.toProjectionString(projections))
				.withNumber(IndexPropertySpec.PROVISIONED_THROUGHPUT, readThroughputCapacity)
				.withString(IndexPropertySpec.TABLE_HASH_KEY, tableHashKey)
				.withString(IndexPropertySpec.QUERY_TYPE, queryType.toString());

		if (tableRangeKey != null) {
			item.withString(IndexPropertySpec.TABLE_RANGE_KEY, tableRangeKey);
		}

		indexProperty.putItem(new PutItemSpec().withItem(item));

		getAttributeModel().newRangeKey(index, projections, indexHashKey, indexRangeKey, readThroughputCapacity, queryType,
				tableHashKey, tableRangeKey);

		openQueries.put(index.getId(), 0);
		activePartitionKeyQuery.put(index.getId(), false);

	}

	private static void newPartition(Index index, Integer partitionId) {

		Logger.info("Creating partition " + partitionId + " for index '" + index.getId() + "'");

		Table indexPartition = StorageServiceFactory.getDirectDatabase().getTable(IndexPartitionSpec.TABLE_NAME);
		Item indexMapping = new Item()
				.withPrimaryKey(IndexPartitionSpec.ID, index.getId(), IndexPartitionSpec.PARTITION, partitionId)
				.withInt(IndexPartitionSpec.SIZE, 0);
		indexPartition.putItem(new PutItemSpec().withItem(indexMapping));
	}

	@Todo("Remove this attribute from entity if not used")
	private static QueryType getQueryType(Index index) {

		Table table = StorageServiceFactory.getDirectDatabase().getTable(IndexPropertySpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(IndexPropertySpec.ID, index.getId())
				.withProjectionExpression(IndexPropertySpec.QUERY_TYPE));

		return QueryType.parse(item.getString(IndexPropertySpec.QUERY_TYPE));
	}

	@Todo("Partition count should be stored in memory")
	// @Todo("Add an attribute to the IndexProperty table to keep track of
	// partition count, instead of querying IndexPartition Table table")
	public Integer partitionCount(Index index) {

		Logger.debug("Getting partition count for index '" + index.getId() + "'");

		String indexId = index.getId();

		Table table = StorageServiceFactory.getDirectDatabase().getTable(IndexPartitionSpec.TABLE_NAME);

		QuerySpec spec = new QuerySpec().withKeyConditionExpression("#indexId = :indexId")
				.withNameMap(new NameMap().with("#indexId", IndexPartitionSpec.ID))
				.withValueMap(new ValueMap().withString(":indexId", indexId)).withConsistentRead(true);

		ItemCollection<QueryOutcome> outcome = table.query(spec);

		List<Integer> count = new ArrayList<>();

		// @DEV Prototype
		outcome.forEach(i -> {
			count.add(1);
		});

		// return outcome.getAccumulatedItemCount();
		return count.size();
	}

	private static Integer getPartitionSize(Index index, Integer partitionId) {

		Logger.debug("Getting size of partition " + partitionId + " of index '" + index.getId() + "'");

		String indexId = index.getId();

		Table table = StorageServiceFactory.getDirectDatabase().getTable(IndexPartitionSpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec()
				.withPrimaryKey(IndexPartitionSpec.ID, indexId, IndexPartitionSpec.PARTITION, partitionId)
				.withProjectionExpression(IndexPartitionSpec.SIZE));

		return item.getInt(IndexPartitionSpec.SIZE);
	}

	public Integer nextPartitionKey(Index index, int size) {

		Logger.debug("Getting next partition key of index '" + index.getId() + "' for item with size of " + size);

		String indexId = index.getId();

		activePartitionKeyQuery.put(indexId, true);
		
		
		// Check if any unfilled partition exists on this index

		int partitionId = getExistingFreeSpace(index, size);
		if (partitionId != -1) {
			return partitionId;
		}
		
		
		// Get current partition and size

		int currentPartionId = partitionCount(index);
		int currentPatitionSize = getPartitionSize(index, currentPartionId);

		
		if (currentPatitionSize + size < QueryModel.MAX_PARTITION_SIZE) {
			return currentPartionId;
		} else {

			// Add free space to current partition

			int freeSpace = MAX_PARTITION_SIZE - currentPatitionSize;
			if (freeSpace > 0) {
				incrementPartitionFreeSpace(index, currentPartionId, freeSpace);
			}

			// Return new partition Id

			int newPartitionKey = currentPartionId + 1;
			newPartition(index, newPartitionKey);
			
			return newPartitionKey;
		}
	}

	private static Integer getExistingFreeSpace(Index index, int size) {

		Logger.debug("Fetching free partitions of index '" + index.getId() + "' with size >= " + size);

		Table indexUPartition = StorageServiceFactory.getDirectDatabase().getTable(IndexUPartitionSpec.TABLE_NAME);

		QuerySpec spec = new QuerySpec().withKeyConditionExpression("#index = :v_index and #size >= :v_size")
				.withNameMap(new NameMap().with("#index", IndexUPartitionSpec.ID))
				.withNameMap(new NameMap().with("#size", IndexUPartitionSpec.SIZE))
				.withValueMap(new ValueMap().withString(":v_index", index.getId()))
				.withValueMap(new ValueMap().withInt(":v_size", size)).withConsistentRead(true)
				.withProjectionExpression(IndexUPartitionSpec.PARTITION_ID).withMaxResultSize(1);

		ItemCollection<QueryOutcome> outcome = indexUPartition.getIndex(IndexUPartitionSpec.SIZE_INDEX).query(spec);

		if (outcome.getAccumulatedItemCount() > 0) {
			return outcome.firstPage().iterator().next().getInt(IndexUPartitionSpec.PARTITION_ID);
		}

		return -1;
	}

	public void incrementPartition(Index index, int partitionId, int size) throws PartitionSizeExceeded {

		Logger.info("Increasing size of partition " + partitionId + " of index '" + index.getId() + "' by " + size);

		if (getPartitionSize(index, partitionId) + size > MAX_PARTITION_SIZE) {
			throw new PartitionSizeExceeded();
		}

		Table indexPartition = StorageServiceFactory.getDirectDatabase().getTable(IndexPartitionSpec.TABLE_NAME);

		indexPartition.updateItem(new UpdateItemSpec()
				.withPrimaryKey(IndexPartitionSpec.ID, index.getId(), IndexPartitionSpec.PARTITION, partitionId)
				.withUpdateExpression("SET #k = #k + :inc")
				// .withConditionExpression("#k + :inc <= :max")
				.withNameMap(new NameMap().with("#k", IndexPartitionSpec.SIZE))
				.withValueMap(new ValueMap().withInt(":inc", size)
		// .withInt(":max", MAX_PARTITION_SIZE)
		)).getItem();

		if (partitionCount(index) > partitionId) {
			decrementPartitionFreeSpace(index, partitionId, size);
		}

	}

	public void decrementPartition(Index index, int partitionId, int size) {

		Logger.info("Reducing size of partition " + partitionId + " of index '" + index.getId() + "' by " + size);

		Table indexPartition = StorageServiceFactory.getDirectDatabase().getTable(IndexPartitionSpec.TABLE_NAME);

		indexPartition.updateItem(new UpdateItemSpec()
				.withPrimaryKey(IndexPartitionSpec.ID, index.getId(), IndexPartitionSpec.PARTITION, partitionId)
				.withUpdateExpression("SET #k = #k - :inc")
				.withNameMap(new NameMap().with("#k", IndexPartitionSpec.SIZE))
				.withValueMap(new ValueMap().withNumber(":inc", size)));

		if (partitionCount(index) > partitionId) {
			incrementPartitionFreeSpace(index, partitionId, size);
		}
	}

	private static void incrementPartitionFreeSpace(Index index, int partitionId, int size) {

		Logger.info(
				"Increasing free space for partition " + partitionId + " of index '" + index.getId() + "' by " + size);

		Table indexUPartition = StorageServiceFactory.getDirectDatabase().getTable(IndexUPartitionSpec.TABLE_NAME);

		boolean entryExist = indexUPartition.getItem(IndexUPartitionSpec.ID, index.getId(),
				IndexUPartitionSpec.PARTITION_ID, partitionId
		// .withProjectionExpression(IndexUPartitionSpec.ID)
		) != null;

		if (!entryExist) {

			// Add entry
			Item partitionItem = new Item()

					.withPrimaryKey(IndexUPartitionSpec.ID, index.getId(), IndexUPartitionSpec.PARTITION_ID,
							partitionId)

					.withNumber(IndexUPartitionSpec.SIZE, size);

			indexUPartition.putItem(partitionItem);

		} else {

			// Increment by size
			indexUPartition.updateItem(new UpdateItemSpec()
					.withPrimaryKey(IndexUPartitionSpec.ID, index.getId(), IndexUPartitionSpec.PARTITION_ID,
							partitionId)
					.withUpdateExpression("SET #k = #k + :inc")
					.withNameMap(new NameMap().with("#k", IndexUPartitionSpec.SIZE))
					.withValueMap(new ValueMap().withInt(":inc", size)));

		}
	}

	private static void decrementPartitionFreeSpace(Index index, int partitionId, int size) {

		Logger.info(
				"Decreasing free space for partition " + partitionId + " of index '" + index.getId() + "' by " + size);

		Table indexUPartition = StorageServiceFactory.getDirectDatabase().getTable(IndexUPartitionSpec.TABLE_NAME);

		// Decrement by size
		Item item = indexUPartition.updateItem(new UpdateItemSpec()
				.withPrimaryKey(IndexUPartitionSpec.ID, index.getId(), IndexUPartitionSpec.PARTITION_ID, partitionId)
				.withUpdateExpression("SET #k = #k - :dec")
				.withNameMap(new NameMap().with("#k", IndexUPartitionSpec.SIZE))
				.withValueMap(new ValueMap().withInt(":dec", size)).withReturnValues(ReturnValue.ALL_NEW)).getItem();

		if (item.getInt(IndexUPartitionSpec.SIZE) == 0) {
			indexUPartition.deleteItem(IndexUPartitionSpec.ID, index.getId(), IndexUPartitionSpec.PARTITION_ID,
					partitionId);
		}
	}

}
