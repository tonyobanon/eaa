package com.kylantis.eaa.core.forms;

import com.kylantis.eaa.core.config.Id;

public class BaseSimpleEntry extends Question {

	private String title;
	private Integer sortOrder;
	private String context;

	private Boolean isRequired;
	private Boolean isVisible;
	private Boolean isDefault;

	public BaseSimpleEntry(Id id, String title) {
		super(id);
		this.title = title;
		this.sortOrder = 0;
	}

	public String getTitle() {
		return title;
	}

	public BaseSimpleEntry setTitle(String title) {
		this.title = title;
		return this;
	}

	public BaseSimpleEntry setId(Id id) {
		super.id = id;
		return this;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public BaseSimpleEntry setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
		return this;
	}

	public String getContext() {
		return context;
	}

	public BaseSimpleEntry setContext(String context) {
		this.context = context;
		return this;
	}

	public Boolean getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(Boolean isRequired) {
		this.isRequired = isRequired;
	}

	public Boolean getIsVisible() {
		return isVisible;
	}

	public void setIsVisible(Boolean isVisible) {
		this.isVisible = isVisible;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

}
