package com.kylantis.eaa.core.forms;

import com.kylantis.eaa.core.config.Id;

public abstract class Question {

	protected Id id;

	public Question(Id id) {
		this.id = id;
	}

	public Id getId() {
		return id;
	}
	
	public String hash(){
		return Integer.toString(hashCode());
	}
}
