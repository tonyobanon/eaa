package com.kylantis.eaa.core.forms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.FormCESpec;
import com.kylantis.eaa.core.attributes.FormSESpec;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.base.Todo;
import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.dbutils.DBTooling;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IFormModel;
import com.kylantis.eaa.core.ml.MLRepository;
import com.kylantis.eaa.core.setup.InstallOptions;

@ConcreteModel(dependencies = { MLRepository.CONFIG_MODEL, MLRepository.ROLES_MODEL })
public class FormModel extends IFormModel {

	private static final String COMPOSITE_ENTRY_OPTION_SEPERATOR = "__";


	
	@Override
	public void preInstall() {
	}

	@Override
	public void install(InstallOptions options) {
	}

	@Override
	public void start() {
	}

	@PlatformInternal
	public List<Id> getQuestionKeys(Id form) {

		List<Id> entries = Collections.emptyList();

		DBTooling.queryIndex(
				StorageServiceFactory.getDocumentDatabase().getTable(FormSESpec.TABLE_NAME)
						.getIndex(FormSESpec.FORM_INDEX),
				"#id", new NameMap().with("#id", FormSESpec.ID).with("#k", FormSESpec.FORM),
				new ValueMap().withString(":v", form.getId()), "#k = :v").forEach(i -> {
					entries.add(Id.from(i.getString(FormSESpec.ID)));
				});

		DBTooling.queryIndex(
				StorageServiceFactory.getDocumentDatabase().getTable(FormCESpec.TABLE_NAME)
						.getIndex(FormCESpec.FORM_INDEX),
				"#id", new NameMap().with("#id", FormCESpec.ID).with("#k", FormCESpec.FORM),
				new ValueMap().withString(":v", form.getId()), "#k = :v").forEach(i -> {
					entries.add(Id.from(i.getString(FormCESpec.ID)));
				});
		return entries;
	}

	@PlatformInternal
	public List<Question> getQuestions(Id form) {

		List<Question> entries = new ArrayList<>();

		entries.addAll(getSimpleEntries(listSimpleEntries(form)));

		entries.addAll(getCompositeEntries(listCompositeEntries(form)));

		return entries;
	}

	private static QuestionDescriptor getQuestionDescriptor(Id id) {

		Item seQuestion = StorageServiceFactory.getDocumentDatabase().getTable(FormSESpec.TABLE_NAME)
				.getItem(new GetItemSpec().withPrimaryKey(FormSESpec.ID, id.getId()).withProjectionExpression("#i, #d")
						.withNameMap(FluentHashMap.forNameMap().with("#i", FormSESpec.ID).with("#d",
								FormSESpec.IS_DEFAULT)));

		Item ceQuestion = StorageServiceFactory.getDocumentDatabase().getTable(FormCESpec.TABLE_NAME)
				.getItem(new GetItemSpec().withPrimaryKey(FormCESpec.ID, id.getId()).withProjectionExpression("#i, #d")
						.withNameMap(FluentHashMap.forNameMap().with("#i", FormCESpec.ID).with("#d",
								FormCESpec.IS_DEFAULT)));

		if (seQuestion != null) {
			return new QuestionDescriptor(QuestionType.SIMPLE, seQuestion.getBoolean(FormSESpec.IS_DEFAULT));
		}

		if (ceQuestion != null) {
			return new QuestionDescriptor(QuestionType.COMPOSITE, ceQuestion.getBoolean(FormCESpec.IS_DEFAULT));
		}

		return null;
	}

	@PlatformInternal
	@Todo("Add Support for batch operations")
	public void newQuestion(Id form, Question spec) {
		newQuestion(form, false, spec);
	}

	@PlatformInternal
	public void newQuestion(Id form, boolean isDefault, Question spec) {
		if (spec instanceof SimpleEntry) {
			newSimpleEntry(form, isDefault, (SimpleEntry) spec);
		} else if (spec instanceof CompositeEntry) {
			newCompositeEntry(form, isDefault, (CompositeEntry) spec);
		}
	}

	@PlatformInternal
	public void newCompositeEntry(Id form, boolean isDefault, CompositeEntry spec) {

		List<String> optionsList = new ArrayList<String>();

		spec.getItems().forEach((k, v) -> {
			if (k.contains(COMPOSITE_ENTRY_OPTION_SEPERATOR) || v.contains(COMPOSITE_ENTRY_OPTION_SEPERATOR)) {
				throw new ResourceException(ResourceException.FAILED_VALIDATION);
			}
			optionsList.add(k + COMPOSITE_ENTRY_OPTION_SEPERATOR + v);
		});

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(FormCESpec.TABLE_NAME);

		Item item = new Item().withPrimaryKey(FormCESpec.ID, spec.getId().getId())
				.withString(FormCESpec.TITLE, spec.getTitle())
				.withStringSet(FormCESpec.OPTIONS, new FluentArrayList<String>().withAll(optionsList))
				.withBoolean(FormCESpec.ALLOW_MULTIPLE_CHOICE, spec.isAllowMultipleChoice())
				.withBoolean(FormCESpec.IS_VISIBLE, spec.getIsVisible())
				.withBoolean(FormCESpec.IS_REQUIRED, spec.getIsRequired()).withBoolean(FormCESpec.IS_DEFAULT, isDefault)
				.withList(FormCESpec.DEFAULT_SELECTIONS, spec.getDefaultSelections())
				.withInt(FormCESpec.SORT_ORDER, spec.getSortOrder()).withString(FormCESpec.FORM, form.getId())
				.with(FormCESpec.CONTEXT, spec.getContext()).with(FormCESpec.ITEMS_SOURCE, spec.getItemsSource())
				.with(FormCESpec.HASH, spec.hash());

		try {
			table.putItem(new PutItemSpec().withItem(item)
					.withNameMap(new FluentHashMap<String, String>().with("#k", FormCESpec.ID))
					.withConditionExpression("attribute_not_exists(#k)"));
		} catch (ConditionalCheckFailedException e) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS);
		}
	}

	@PlatformInternal
	public void newSimpleEntry(Id form, boolean isDefault, SimpleEntry spec) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(FormSESpec.TABLE_NAME);

		Item item = new Item().withPrimaryKey(FormSESpec.ID, spec.getId().getId())
				.withString(FormSESpec.TITLE, spec.getTitle())
				.withString(FormSESpec.VALUE_TYPE, spec.getInputType().toString())
				.with(FormSESpec.DEFAULT_VALUE, spec.getDefaultValue())
				.withBoolean(FormSESpec.IS_REQUIRED, spec.getIsRequired())
				.withBoolean(FormSESpec.IS_VISIBLE, spec.getIsVisible()).withBoolean(FormSESpec.IS_DEFAULT, isDefault)
				.withInt(FormSESpec.SORT_ORDER, spec.getSortOrder()).withString(FormSESpec.FORM, form.getId())
				.with(FormSESpec.CONTEXT, spec.getContext()).with(FormSESpec.HASH, spec.hash());

		try {
			table.putItem(new PutItemSpec().withItem(item)
					.withNameMap(new FluentHashMap<String, String>().with("#k", FormSESpec.ID))
					.withConditionExpression("attribute_not_exists(#k)"));
		} catch (ConditionalCheckFailedException e) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS);
		}
	}

	public void deleteSimpleEntry(Id id) {
		try {
			StorageServiceFactory.getDocumentDatabase().getTable(FormSESpec.TABLE_NAME).deleteItem(FormSESpec.ID,
					id.getId());
		} catch (ResourceNotFoundException ex) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}
	}

	public void deleteCompositeEntry(Id id) {
		try {
			StorageServiceFactory.getDocumentDatabase().getTable(FormCESpec.TABLE_NAME).deleteItem(FormCESpec.ID,
					id.getId());
		} catch (ResourceNotFoundException ex) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}
	}

	public void deleteQuestion(Id id) {
		deleteQuestion(id, false);
	}
	
	@PlatformInternal
	public void deleteQuestions(Id form, boolean forceDelete) {

		// Check for SimpleEntry references
		QuerySpec SEspec = new QuerySpec().withKeyConditionExpression("#k = :v")
				.withNameMap(FluentHashMap.forNameMap().with("#k", FormSESpec.FORM).with("#id", FormSESpec.ID))
				.withValueMap(new ValueMap().withString(":v", form.getId())).withProjectionExpression("#k,#id");

		ItemCollection<QueryOutcome> SEoutcome = StorageServiceFactory.getDocumentDatabase()
				.getTable(FormSESpec.TABLE_NAME).getIndex(FormSESpec.FORM_INDEX).query(SEspec);

		if (forceDelete) {
			SEoutcome.forEach(i -> {
				deleteSimpleEntry(Id.from(i.getString(FormSESpec.ID)));
			});
		} else if (SEoutcome.firstPage().iterator().hasNext()) {
			throw new ResourceException(ResourceException.RESOURCE_STILL_IN_USE);
		}

		// Check for CompositeEntry references
		QuerySpec CEspec = new QuerySpec().withKeyConditionExpression("#k = :v")
				.withNameMap(FluentHashMap.forNameMap().with("#k", FormCESpec.FORM).with("#id", FormCESpec.ID))
				.withValueMap(FluentHashMap.forValueMap().with(":v", form.getId())).withProjectionExpression("#k,#id");

		ItemCollection<QueryOutcome> CEoutcome = StorageServiceFactory.getDocumentDatabase()
				.getTable(FormCESpec.TABLE_NAME).getIndex(FormCESpec.FORM_INDEX).query(CEspec);

		if (forceDelete) {
			CEoutcome.forEach(i -> {
				deleteCompositeEntry(Id.from(i.getString(FormCESpec.ID)));
			});
		} else if (CEoutcome.firstPage().iterator().hasNext()) {
			throw new ResourceException(ResourceException.RESOURCE_STILL_IN_USE);
		}
	}

	@PlatformInternal
	public void deleteQuestion(Id id, boolean forceDelete) {

		QuestionDescriptor desc = getQuestionDescriptor(id);

		if (desc == null) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}

		if (desc.isDefault() && !forceDelete) {
			throw new ResourceException(ResourceException.DELETE_NOT_ALLOWED);
		}

		switch (desc.getType()) {
		case SIMPLE:
			deleteSimpleEntry(id);
			break;
		case COMPOSITE:
			deleteCompositeEntry(id);
			break;
		}
	}

	@PlatformInternal
	private static List<SimpleEntry> getSimpleEntries(List<Object> keys) {

		List<Item> outcome = DBTooling.batchGet(FormSESpec.TABLE_NAME, FormSESpec.ID, keys,
				"#id,#ti,#vt,#dv,#ir,#idf,#iv,#so,#ct",
				FluentHashMap.forNameMap().with("#id", FormSESpec.ID).with("#ti", FormSESpec.TITLE)
						.with("#vt", FormSESpec.VALUE_TYPE).with("#dv", FormSESpec.DEFAULT_VALUE)
						.with("#ir", FormSESpec.IS_REQUIRED).with("#idf", FormSESpec.IS_DEFAULT)
						.with("#iv", FormSESpec.IS_VISIBLE).with("#so", FormSESpec.SORT_ORDER)
						.with("#ct", FormSESpec.CONTEXT));

		List<SimpleEntry> entries = new ArrayList<>();

		outcome.forEach(i -> {

			SimpleEntry o = new SimpleEntry(Id.from(i.getString(FormSESpec.ID)),
					InputType.valueOf(i.getString(FormSESpec.VALUE_TYPE)), i.getString(FormSESpec.TITLE));
			o.withDefaultValue((String) i.get(FormSESpec.DEFAULT_VALUE));
			o.setIsRequired(i.getBoolean(FormSESpec.IS_REQUIRED));
			o.setIsVisible(i.getBoolean(FormSESpec.IS_VISIBLE));
			o.setIsDefault(i.getBoolean(FormSESpec.IS_DEFAULT));
			o.setSortOrder(i.getInt(FormSESpec.SORT_ORDER));
			o.setContext(i.getString(FormSESpec.CONTEXT));

			entries.add(o);
		});

		return entries;
	}

	@PlatformInternal
	private static List<CompositeEntry> getCompositeEntries(List<Object> keys) {

		List<Item> outcome = DBTooling.batchGet(FormCESpec.TABLE_NAME, FormCESpec.ID, keys,
				"#id,#ti,#opt,#amc,#iv,#so,#ir,#idf,#ct,#its,#ds",
				FluentHashMap.forNameMap().with("#id", FormCESpec.ID).with("#ti", FormCESpec.TITLE)
						.with("#opt", FormCESpec.OPTIONS).with("#amc", FormCESpec.ALLOW_MULTIPLE_CHOICE)
						.with("#iv", FormCESpec.IS_VISIBLE).with("#so", FormCESpec.SORT_ORDER)
						.with("#ir", FormCESpec.IS_REQUIRED).with("#idf", FormCESpec.IS_DEFAULT)
						.with("#ct", FormCESpec.CONTEXT).with("#its", FormCESpec.ITEMS_SOURCE)
						.with("#ds", FormCESpec.DEFAULT_SELECTIONS));

		List<CompositeEntry> entries = new ArrayList<>();

		outcome.forEach(i -> {

			CompositeEntry o = new CompositeEntry(Id.from(i.getString(FormCESpec.ID)), i.getString(FormCESpec.TITLE));

			o.setAllowMultipleChoice(i.getBoolean(FormCESpec.ALLOW_MULTIPLE_CHOICE));
			o.setIsRequired(i.getBoolean(FormCESpec.IS_REQUIRED));
			o.setIsVisible(i.getBoolean(FormCESpec.IS_VISIBLE));
			o.setIsDefault(i.getBoolean(FormCESpec.IS_DEFAULT));
			o.setSortOrder(i.getInt(FormCESpec.SORT_ORDER));
			o.setContext(i.getString(FormCESpec.CONTEXT));
			o.setItemsSource(i.getString(FormCESpec.ITEMS_SOURCE));
			o.setDefaultSelections(i.getList(FormCESpec.DEFAULT_SELECTIONS));

			for (String option : i.getStringSet(FormCESpec.OPTIONS)) {
				String[] optionArr = option.split(Pattern.quote(COMPOSITE_ENTRY_OPTION_SEPERATOR));
				String optionId = optionArr[0];
				String optionName = optionArr[1];
				o.withItem(optionId, optionName);
			}

			entries.add(o);
		});

		return entries;

	}

	/**
	 *
	 *
	 * @return {@link List<Id>}
	 **/
	public  List<Object> listSimpleEntries(Id form) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(FormSESpec.TABLE_NAME);

		QuerySpec spec = new QuerySpec().withKeyConditionExpression("#k = :v").withProjectionExpression("#id")
				.withNameMap(FluentHashMap.forNameMap().with("#k", FormSESpec.FORM).with("#id", FormSESpec.ID))
				.withValueMap(FluentHashMap.forValueMap().with(":v", form.getId()));

		List<Object> keys = new ArrayList<>();
		table.getIndex(FormSESpec.FORM_INDEX).query(spec).forEach(i -> {
			keys.add(i.getString(FormSESpec.ID));
		});

		return (keys);
	}

	/**
	 *
	 *
	 * @return {@link List<Id>}
	 **/
	public List<Object> listCompositeEntries(Id form) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(FormCESpec.TABLE_NAME);

		QuerySpec spec = new QuerySpec().withKeyConditionExpression("#k = :v").withProjectionExpression("#id")
				.withNameMap(FluentHashMap.forNameMap().with("#k", FormCESpec.FORM).with("#id", FormCESpec.ID))
				.withValueMap(FluentHashMap.forValueMap().with(":v", form.getId()));

		List<Object> keys = new ArrayList<>();
		table.getIndex(FormCESpec.FORM_INDEX).query(spec).forEach(i -> {
			keys.add(i.getString(FormCESpec.ID));
		});

		return (keys);
	}

}
