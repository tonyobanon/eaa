package com.kylantis.eaa.core.forms;

import com.kylantis.eaa.core.config.Id;

public class SimpleEntry extends BaseSimpleEntry {

	private InputType inputType;
	private String defaultValue;
	
	private String textValue;

	public SimpleEntry(Id id, InputType inputType, String title) {
		super(id, title);
		this.inputType = inputType;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public SimpleEntry withDefaultValue(Boolean defaultValue) {
		return withDefaultValue(defaultValue.toString());
	}

	public SimpleEntry withDefaultValue(Long defaultValue) {
		return withDefaultValue(defaultValue.toString());
	}

	public SimpleEntry withDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
		return this;
	}

	public InputType getInputType() {
		return inputType;
	}

	public SimpleEntry withInputType(InputType inputType) {
		this.inputType = inputType;
		return this;
	}
	
	public String getTextValue() {
		return textValue;
	}

	public SimpleEntry withTextValue(String textValue) {
		this.textValue = textValue;
		return this;
	}

}
