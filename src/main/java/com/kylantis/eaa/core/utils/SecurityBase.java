
package com.kylantis.eaa.core.utils;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.util.encoders.Hex;

public class SecurityBase
{
    public static String encrypt(final String s) throws NoSuchAlgorithmException {
        final Random random = new Random();
        final byte[] array = { 0 };
        random.nextBytes(array);
        final String s2 = new String(byteToHex(array[0]));
        final String string = s2 + s;
        final MessageDigest instance = MessageDigest.getInstance("MD5");
        instance.update(string.getBytes());
        final byte[] digest = instance.digest();
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < digest.length; ++i) {
            sb.append(byteToHex(digest[i]));
        }
        sb.append(":");
        sb.append(s2);
        return sb.toString();
    }
    
    public static boolean checkPassword(final String s, final String s2) throws NoSuchAlgorithmException {
       /*
    	final String substring = s.substring(0, 32);
        final String substring2 = s.substring(33);
        final MessageDigest instance = MessageDigest.getInstance("MD5");
        instance.update((substring2 + s2).getBytes());
        final byte[] digest = instance.digest();
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < digest.length; ++i) {
            sb.append(byteToHex(digest[i]));
        }
        */
        return s.equals(s2.toString());
    }
    
    private static String byteToHex(final byte b) {
        final StringBuffer sb = new StringBuffer();
        sb.append(toHexChar(b >>> 4 & 0xF));
        sb.append(toHexChar(b & 0xF));
        return sb.toString();
    }
    
    private static char toHexChar(final int n) {
        if (0 <= n && n <= 9) {
            return (char)(48 + n);
        }
        return (char)(97 + (n - 10));
    }
    

    public static String getSHA512Digest(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException{
    	
    	MessageDigest md = MessageDigest.getInstance("SHA-512");
    	byte[] rawHash = md.digest(input.getBytes());
    	//Convert raw bytes to Hex
		byte[] hexBytes = Hex.encode(rawHash);
		//Convert an array of Hex bytes to a String
		return new String(hexBytes, "UTF-8");
    } 
    
    public static String getSHA512HashFromMac(String key, String value) throws NoSuchAlgorithmException, UnsupportedEncodingException{
    	
    	try{

    		//Get an hmac_sha1 key from the raw key bytes
    		byte[] keyBytes = key.getBytes();
    		Key signingKey = new SecretKeySpec(keyBytes, "HmacSHA512");
    		
    		//Get an hmac_sha1 Mac instance and initialize with the signing key
    		Mac mac = Mac.getInstance("HmacSHA512");
    		mac.init(signingKey);
    		
    		//Compute the hmac on input data bytes
    		byte[] rawHmac = mac.doFinal(value.getBytes());
    		
    		//Convert raw bytes to Hex
			byte[] hexBytes = Hex.encode(rawHmac);
    		
			//Convert an array of Hex bytes to a String
    		return new String(hexBytes, "UTF-8");
    		
    	}catch(Exception e){
    		throw new RuntimeException(e);
    	}
    } 
    
}
