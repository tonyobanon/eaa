
package com.kylantis.eaa.core.utils;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

public class UtilsExt
{
    public static final String CDATA_START = "<![CDATA[";
    public static final String CDATA_END = "]]>";
    
    public static int min(final int n, final int n2) {
        if (n < n2) {
            return n;
        }
        return n2;
    }
    
    public static int max(final int n, final int n2) {
        if (n > n2) {
            return n;
        }
        return n2;
    }
    
    public static int countOccurrences(final char c, final String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        int n = 0;
        for (int i = 0; i < s.length(); ++i) {
            i = s.indexOf(c, i);
            if (i == -1) {
                return n;
            }
            ++n;
        }
        return n;
    }
    
    public static boolean isBlank(final String s) {
        return s == null || s.length() == 0;
    }
    
    public static String wrapInCDATA(final String s) {
        final StringBuffer sb = new StringBuffer("<![CDATA[");
        sb.append(s);
        sb.append("]]>");
        return sb.toString();
    }
    
    public static String removeCharAtEndIfPresent(final String s, final char c) {
        if (s == null) {
            return s;
        }
        final int length = s.length();
        if (s.charAt(length - 1) == c) {
            return s.substring(0, length - 1);
        }
        return s;
    }
    
    public static String removeMultipleSlashes(final String s) {
        if (s == null) {
            return s;
        }
        String replace = s;
        int length;
        do {
            length = replace.length();
            replace = replace.replace("//", "/").replace("\\\\", "\\");
        } while (replace.length() < length);
        return replace;
    }
    
    public static String toWindowsPath(final String s) {
        if (s == null) {
            return s;
        }
        String replace = s;
        int length;
        do {
            length = replace.length();
            replace = replace.replace("/", "\\");
        } while (replace.length() < length);
        return replace;
    }
    
    public static String unWrapInCDATA(final String s) {
        if (!s.startsWith("<![CDATA[")) {
            return s;
        }
        if (!s.endsWith("]]>")) {
            return s;
        }
        return s.substring("<![CDATA[".length(), s.length() - "]]>".length());
    }
    
    public static int getEOLTextIdx(final String s) {
        if (s == null) {
            return -1;
        }
        final int index = s.indexOf(13);
        final int index2 = s.indexOf(10);
        if (index == -1 && index2 == -1) {
            return -1;
        }
        if (index != -1 && index2 == -1) {
            return index - 1;
        }
        if (index == -1 && index2 != -1) {
            return index2 - 1;
        }
        return min(index, index2) - 1;
    }
    
    public static boolean StringsAreDifferent(final String s, final String s2) {
        return (s == null && s2 != null) || (s != null && s2 == null) || ((s != null || s2 != null) && (s == null || !s.equals(s2)));
    }
    
    public static String trim(final String s, final int n) {
        if (s == null || s.length() <= n) {
            return s;
        }
        return s.substring(0, n);
    }
    
    public static String trim(final StringBuffer sb, final int n) {
        return trim(sb.toString(), n);
    }
    
    public static String trim(final String s, final int n, final boolean b) {
        if (s == null || s.length() <= n) {
            return s;
        }
        if (!b) {
            return s.substring(0, n);
        }
        return s.substring(0, Math.max(n - 3, 0)) + "...";
    }
    
    public static String padRight(final String s, final int n) {
        return padRight(s, n, ' ');
    }
    
    public static String trimTrailing(final String s) {
        return s.replaceAll("\\s+$", "");
    }
    
    public static String padRight(final String s, final int n, final char c) {
        String string = s;
        for (int i = s.length(); i < n; ++i) {
            string += c;
        }
        return string.substring(0, n);
    }
    
    public static String padLeft(final int n, final int n2) {
        return padLeft(new Integer(n).toString(), n2, ' ');
    }
    
    public static String padLeft(final long n, final int n2) {
        return padLeft(new Long(n).toString(), n2, ' ');
    }
    
    public static String padLeft(final long n, final int n2, final char c) {
        return padLeft(new Long(n).toString(), n2, c);
    }
    
    public static String padLeft(final String s, final int n) {
        return padLeft(s, n, ' ');
    }
    
    public static String padLeft(final String s, final int n, final char c) {
        String string = "";
        for (int i = s.length(); i < n; ++i) {
            string += c;
        }
        return (string + s).substring(0, n);
    }
    
    public static String padCenter(final String s, final int n, final char c) {
        String s2 = s;
        if (s2 == null) {
            s2 = "";
        }
        final int n2 = n - s2.length();
        String string = "";
        for (int i = 0; i < n2 / 2; ++i) {
            string += c;
        }
        String s3 = string + s2;
        for (int j = 0; j < n / 2 + 1; ++j) {
            s3 += c;
        }
        return s3.substring(0, n);
    }
    
    public static String padCenter(final String s, final int n) {
        return padCenter(s, n, ' ');
    }
    
    public static String makeString(final int n, final char c) {
        String string = "";
        for (int i = 0; i < n; ++i) {
            string += c;
        }
        return string;
    }
    
    public static String upperFirstLetter(final String s) {
        if (s == null) {
            return s;
        }
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }
    
    public static String removeCharFromString(final String s, final char c) {
        String string = s;
        if (string == null) {
            return string;
        }
        for (int index = 0; (index = string.indexOf(c, index)) != -1; string = string.substring(0, index) + string.substring(index + 1)) {}
        return string;
    }
    
    public static String removeStringFromString(final String s, final String s2) {
        String string = s;
        if (string == null || s2 == null) {
            return string;
        }
        for (int length = s2.length(), index = 0; (index = string.indexOf(s2, index)) != -1; string = string.substring(0, index) + string.substring(index + length)) {}
        return string;
    }
    
    public static String spaces(final int n) {
        return makeString(n, ' ');
    }
    
    public static String line(final int n) {
        return makeString(n, '_');
    }
    
    public static List<String> getListOfItemsFromString(final String s) {
        if (s == null) {
            return new ArrayList<String>();
        }
        final String[] split = s.split("[ ,;]");
        final ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < split.length; ++i) {
            final String s2 = split[i];
            if (s2 != null) {
                final String trim = s2.trim();
                if (trim.length() > 0) {
                    list.add(trim);
                }
            }
        }
        return list;
    }
    
    public static List<String> getListOfItemsFromEncodedString(final String s) {
        if (s == null) {
            return new ArrayList<String>();
        }
        return getListOfItemsFromString(s.replace('{', ';').replace('}', ';'));
    }
    
    public static String getIdIncreasingEncodedString(final String s) {
        if (s == null) {
            return s;
        }
        final List<String> listOfItemsFromEncodedString = getListOfItemsFromEncodedString(s);
        final ArrayList<String> list = new ArrayList<String>();
        final int size = listOfItemsFromEncodedString.size();
        final int n = size / 2;
        if (n <= 1) {
            return s;
        }
        if (size % 2 != 0) {
            return s;
        }
        for (int i = 0; i < n; ++i) {
            int n2 = -1;
            int n3 = -1;
            int n4 = -1;
            for (int j = 0; j < size; j += 2) {
                final int int1 = Integer.parseInt(listOfItemsFromEncodedString.get(j));
                if (int1 < n3 || n3 == -1) {
                    n3 = int1;
                    n4 = j;
                }
                if (int1 > n2) {
                    n2 = int1;
                }
            }
            if (n3 == -1) {
                return s;
            }
            list.add(listOfItemsFromEncodedString.get(n4));
            list.add(listOfItemsFromEncodedString.get(n4 + 1));
            listOfItemsFromEncodedString.set(n4, String.valueOf(n2 + 1));
        }
        String string = "";
        for (int k = 0; k < size; k += 2) {
            string = string + "{" + (String)list.get(k) + "}" + (String)list.get(k + 1);
        }
        return string;
    }
    
    public static Date createDateFromDate(final Date date) {
        final Date date2 = new Date();
        date2.setDate(date.getDate());
        date2.setMonth(date.getMonth());
        date2.setYear(date.getYear());
        return date2;
    }
    
    public static final String escapeHtml(final String s) {
        if (s == null) {
            return null;
        }
        final StringBuffer sb = new StringBuffer();
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            switch (char1) {
                case 60: {
                    sb.append("&lt;");
                    break;
                }
                case 62: {
                    sb.append("&gt;");
                    break;
                }
                case 38: {
                    sb.append("&amp;");
                    break;
                }
                case 34: {
                    sb.append("&quot;");
                    break;
                }
                case 39: {
                    sb.append("&#39;");
                    break;
                }
                default: {
                    sb.append(char1);
                    break;
                }
            }
        }
        return sb.toString();
    }
    
    public static final String unEscapeHtml(final String s) {
        if (s == null) {
            return null;
        }
        return s.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&amp;", "&").replaceAll("&quot;", "\"").replaceAll("&#39;", "\\");
    }
    
    public static final String replaceRegisteredSuperscriptFromHtml(final String s) {
        if (s == null) {
            return null;
        }
        return s.replace("&lt;sup&gt;&amp;reg;&lt;/sup&gt;", "�").replace("&amp;lt;sup&amp;gt;&amp;amp;reg;&amp;lt;/sup&amp;gt;", "�").replace("&lt;sup&gt;&amp;trade;&lt;/sup&gt;", "\u2122").replace("&amp;lt;sup&amp;gt;&amp;amp;trade;&amp;lt;/sup&amp;gt;", "\u2122");
    }
    
    public static final String escapePropertiesValue(final String s) {
        if (s == null) {
            return null;
        }
        final StringBuffer sb = new StringBuffer();
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            switch (char1) {
                case 10: {
                    sb.append("\\n");
                    break;
                }
                default: {
                    sb.append(char1);
                    break;
                }
            }
        }
        return sb.toString();
    }
    
    public static String emailAddressForFilename(final String s) {
        return s.replace("@", "_at_").replace(" ", "_");
    }

    public static String quoteReplacement(final String s) {
        if (s.indexOf(92) == -1 && s.indexOf(36) == -1) {
            return s;
        }
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); ++i) {
            final char char1 = s.charAt(i);
            if (char1 == '\\') {
                sb.append('\\');
                sb.append('\\');
            }
            else if (char1 == '$') {
                sb.append('\\');
                sb.append('$');
            }
            else {
                sb.append(char1);
            }
        }
        return sb.toString();
    }
    
    public static String getFileExtension(final String s) {
        final int lastIndex = s.lastIndexOf(46);
        if (lastIndex == -1) {
            return "";
        }
        return s.substring(lastIndex + 1);
    }
    
    public static String getFileWithoutExtension(final String s) {
        final int lastIndex = s.lastIndexOf(46);
        if (lastIndex == -1) {
            return s;
        }
        return s.substring(0, lastIndex);
    }
    
    public static String constructProductImageName(final String s, final int n, final String s2, final String s3, final int n2) {
        return constructProductImageName(s, n, s2, s3, n2, "jpg");
    }
    
    public static String constructProductImageName(final String s, final int n, final String s2, final String s3, final int n2, final String s4) {
        final String constructProductImageDirName = constructProductImageDirName(s, s3, n2);
        String string;
        if (s4 == null) {
            string = ".jpg";
        }
        else if (s4.length() == 0) {
            string = "";
        }
        else {
            string = "." + s4;
        }
        return constructProductImageDirName + s + "_" + n + "_" + s2 + string;
    }
    
    public static String constructProductImageDirName(final String s, final String s2, final int n) {
        final String removeCharFromString = removeCharFromString(s, '-');
        String s3 = "/";
        if (!isBlank(s2)) {
            s3 = s3 + s2 + "/";
        }
        final int min = Math.min(n, 8);
        int n2 = 0;
        for (int i = 0; i < min; ++i) {
            s3 = s3 + removeCharFromString.substring(n2, n2 + 1) + "/";
            ++n2;
        }
        return s3;
    }
    
    public static String constructProductImageNameNoPath(final String s, final int n, final String s2) {
        return constructProductImageNameNoPath(s, n, s2, "jpg");
    }
    
    public static String constructProductImageNameNoPath(final String s, final int n, final String s2, final String s3) {
        String string;
        if (s3 == null) {
            string = ".jpg";
        }
        else if (s3.length() == 0) {
            string = "";
        }
        else {
            string = "." + s3;
        }
        return s + "_" + n + "_" + s2 + string;
    }
    
    public static String getUUID() {
        final char[] charArray = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        final char[] array = { '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '-', '\0', '\0', '\0', '\0', '-', '4', '\0', '\0', '\0', '-', '\0', '\0', '\0', '\0', '-', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0' };
        for (int i = 0; i < 36; ++i) {
            if (array[i] == '\0') {
                final int n = (int)(Math.random() * 16.0);
                array[i] = charArray[(i == 19) ? ((n & 0x3) | 0x8) : (n & 0xF)];
            }
        }
        return new String(array);
    }
    
    public static String msToMinsSecsMs(final long n) {
        final long n2 = n / 1000L;
        final long n3 = n2 / 60L;
        return n3 + " Mins " + (n2 - n3 * 60L) + " Secs";
    }
}
