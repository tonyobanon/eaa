
package com.kylantis.eaa.core.utils;

import java.security.NoSuchAlgorithmException;

/**
 * Class for managing password encryption. You may customize this class by adding your own
 * encryption algorithm.
 */
public class Security
{
    /**
     * Encrypts the password using the chosen algorithm and returns the encrypted password.
     * 
     * @param password
     *            in clear text
     * @return Returns an encrypted password
     * @throws NoSuchAlgorithmException
     *             thrown if the chosen algorithm isn't available.
     */
    public static String encrypt(String password) throws NoSuchAlgorithmException
    {
        return SecurityBase.encrypt(password);
    }

    /**
     * Compare the hashed password from the DB with the one in clear text provided by the user. The
     * clear text password must be encrypted using the chosen algorithm and then compared with the
     * hashed password stored in the database.
     * 
     * @param dbPassword
     *            raw encrypted password value from the database
     * @param password
     *            the password in clear text as entered by the user
     * @return Returns true if the password matches the one in the database. Otherwise returns
     *         false.
     * @throws NoSuchAlgorithmException
     *             thrown if the chosen algorithm isn't available.
     */
    public static boolean checkPassword(String dbPassword, String password)
            throws NoSuchAlgorithmException
    {
        return SecurityBase.checkPassword(dbPassword, password);
    }

}
