package com.kylantis.eaa.core.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.base.Logger;

public class Utils {

	private static SecureRandom secureRandom = new SecureRandom();

	public static String newRandom() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
	public static String join(String arr[], int startIndex){
		StringBuilder sb = new StringBuilder(24);
		for(int i = startIndex; i < arr.length; i++){
			sb.append(arr[i]);
		}
		return sb.toString();
	}

	public static Properties getProperties(String filePath) {

		Logger.info("Reading properties file in: " + filePath);
		
		InputStream in = null;
		try {
			in = new FileInputStream(new File(filePath));
		} catch (FileNotFoundException e1) {
			Exceptions.throwRuntime(e1);
		}

		Properties o = new Properties();
		try {
			o.load(in);
			return o;
		} catch (IOException e) {
			Exceptions.throwRuntime(e);
		}
		return null;
	}

	public static File asFile(URL uri) {

		File tempFile = null;
		try {
			tempFile = File.createTempFile(UUID.randomUUID().toString(), null);

			InputStream in = uri.openStream();
			OutputStream out = Files.newOutputStream(tempFile.toPath());

			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}

		} catch (IOException e) {
			Exceptions.throwRuntime(e);
		}
		return tempFile;
	}
	
	public static String newSecureRandom() {
		return new BigInteger(130, secureRandom).toString(32);
	}

	/**
	 * This returns a set of randomly generated bytes
	 */
	public static byte[] randomBytes(int length) {
		byte[] b = new byte[length];
		SecureRandom rand = secureRandom;
		rand.nextBytes(b);
		return b;
	}

	public static Boolean isPortAvailable(InetAddress host, Integer port) {
		/*
		 * try { new Socket(host, port).close(); return true; } catch
		 * (SocketException e) { return false; } catch (IOException e) {
		 * Exceptions.throwRuntime(e); return false; }
		 */

		// Let's catch any exceptions, when the server attempts to start
		return true;
	}

	public static String[] readLines(File o) throws IOException {

		List<String> lines = new ArrayList<String>();

		Charset charset = Charset.forName("UTF-8");
		BufferedReader reader = Files.newBufferedReader(Paths.get(o.toURI()), charset);
		String line = null;
		while ((line = reader.readLine()) != null) {
			lines.add(line);
		}

		reader.close();
		return lines.toArray(new String[lines.size()]);
	}

	public static String[] readLines(InputStream in) throws IOException {

		List<String> lines = new ArrayList<String>();

		Charset charset = Charset.forName("UTF-8");
		BufferedReader reader = new BufferedReader(new InputStreamReader(in, charset));
		String line = null;
		while ((line = reader.readLine()) != null) {
			lines.add(line);
		}

		in.close();
		return lines.toArray(new String[lines.size()]);
	}

	public static String getString(InputStream in) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			int c;
			while ((c = in.read()) != -1) {
				baos.write(c);
			}
			in.close();
		} catch (IOException e) {
			Exceptions.throwRuntime(e);
		}
		return baos.toString();
	}

	public static String getString(String fileName) throws IOException {

		InputStream in = new FileInputStream(new File(fileName));
		return getString(in);
	}

	public static void saveString(String o, OutputStream out) throws IOException {

		StringReader in = new StringReader(o);
		int c;
		while ((c = in.read()) != -1) {
			out.write(c);
		}

		in.close();
		out.close();
	}

	public static void saveString(String o, String fileName) throws IOException {
		OutputStream out = new FileOutputStream(new File(fileName));
		saveString(o, out);
	}

	public static void copyTo(InputStream in, OutputStream out) throws IOException {
		int c;
		while ((c = in.read()) != -1) {
			out.write(c);
		}
		in.close();
		out.close();
	}

	public static String getArgument(String[] args, String key) {

		for (String arg : args) {
			if (arg.startsWith("-" + key + "=")) {
				return arg.split("=")[1];
			}
		}
		return null;
	}

	public static Boolean hasFlag(String[] args, String key) {

		for (String arg : args) {
			if (arg.equals("-" + key) || arg.equals("--" + key)) {
				return true;
			}
		}
		return false;
	}

	public static String toMACAddress(byte[] mac) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length; i++) {
			sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? ":" : ""));
		}
		return sb.toString().toLowerCase();
	}

	public static byte[] decompressBytes(final byte[] array) {
		try {
			final Inflater inflater = new Inflater();
			inflater.setInput(array, 0, array.length);
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(array.length);
			final byte[] array2 = new byte[1024];
			while (!inflater.finished()) {
				byteArrayOutputStream.write(array2, 0, inflater.inflate(array2));
			}
			byteArrayOutputStream.close();
			final byte[] byteArray = byteArrayOutputStream.toByteArray();
			inflater.end();
			return byteArray;
		} catch (IOException | DataFormatException ex) {
			throw new RuntimeException(ex);
		}
	}

	public static byte[] compressBytes(final byte[] bytes) {
		try {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bytes.length);
			final Deflater deflater = new Deflater();
			deflater.setInput(bytes);
			deflater.finish();
			final byte[] array = new byte[1024];
			while (!deflater.finished()) {
				byteArrayOutputStream.write(array, 0, deflater.deflate(array));
			}
			byteArrayOutputStream.close();
			return byteArrayOutputStream.toByteArray();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public static final String prettify(String input) {
		
		//First, capitalize first letter
		return input.substring(0, 1).toUpperCase() + input.substring(1);
		
	}
}
