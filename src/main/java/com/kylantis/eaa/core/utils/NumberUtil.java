package com.kylantis.eaa.core.utils;

public class NumberUtil {

	public static Number asReal(Number value) {
		return value.intValue() < 0 ? 0 : (value.intValue() <= 1) ? value : 1;
	}
	
	public static Number asAngle(Number value) {
		return value.intValue() < 1 ? 1 : (value.intValue() <= 90) ? value : 90;
	}

	public static String asPercentile(Number value) {
		return value.intValue() < 0 ? "0%" : (value.intValue() <= 100) ? value.toString() + "%" : "100%";
	}

}
