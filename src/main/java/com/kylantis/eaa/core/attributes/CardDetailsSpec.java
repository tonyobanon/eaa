package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class CardDetailsSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "CardDetailsTable";

	public static final String ENTITY_ID = "entityId";

	public static final String CARD_NUMBER = "cardNumber";
	public static final String CARD_HOLDER_NAME = "cardHolderName";
	public static final String EXPIRY_MONTH = "expiryMonth";
	public static final String EXPIRY_YEAR = "expiryYear";

	public static final String ADDRESS_1 = "address1";
	public static final String POSTAL_CODE = "postalCode";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String COUNTRY_CODE = "countryCode";

	public static final String DATE_CREATED = "dateCreated";

}
