package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class BaseServiceSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "BaseServiceTable";

	public static final String ID = "id";
	public static final String TITLE = "title";
	public static final String SUMMARY = "summary";
	public static final String DEVELOPER = "developer";
	public static final String DEVELOPER_URL = "developerUrl";
	public static final String BASE_URI = "baseUri";
	public static final String LOGO_URL = "logoUrl";
	public static final String DATE_CREATED = "dateCreated";
	
	public static final String DEVELOPER_INDEX = "developerIndex";
}
