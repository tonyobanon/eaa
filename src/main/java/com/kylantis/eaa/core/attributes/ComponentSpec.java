package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class ComponentSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "ComponentTable";

	public static final String ID = "id";
	public static final String SORT_ORDER = "sortOrder";
	public static final String PAGE = "page";
	public static final String IMPLEMENTATION = "implementation";
	public static final String HASH = "hash";
	public static final String ENABLED = "enabled";
	
	public static final String PAGE_INDEX = "pageIndex";
	
}