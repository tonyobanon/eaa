package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class CitySpec {
	
	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "CityTable";
	
	public static final String ID = "id";
	public static final String TERRITORY_CODE = "territoryCode";
	public static final String NAME = "name";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String TIMEZONE = "timezone";
	
	public static final String TERRITORY_CODE_INDEX = "territoryCodeIndex";
}
