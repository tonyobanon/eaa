package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class FormSESpec {
	
	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "FormSETable";
	
	public static final String ID = "id";
	public static final String TITLE = "title";
	public static final String VALUE_TYPE = "valueType";
	public static final String DEFAULT_VALUE = "defaultValue";
	public static final String IS_REQUIRED = "isRequired";
	public static final String IS_VISIBLE = "isVisible";
	public static final String IS_DEFAULT = "isDefault";
	public static final String SORT_ORDER = "sortOrder";
	
	public static final String FORM = "form";
	public static final String CONTEXT = "context";
	
	public static final String HASH = "hash";
	
	public static final String FORM_INDEX = "formIndex";
	
}
