package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class PageSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "PageTable";
	
	public static final String ID = "id";
	public static final String TITLE = "title";
	public static final String SORT_ORDER = "sortOrder";
	public static final String PARENT = "parent";
	public static final String GROUP = "group";
	public static final String PATH = "path";
	public static final String NAMESPACE = "namespace";

	public static final String PATH_INDEX = "pathIndex";
	public static final String NAMESPACE_INDEX = "namespaceIndex";

}
