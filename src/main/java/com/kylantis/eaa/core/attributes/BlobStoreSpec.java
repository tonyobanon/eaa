package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class BlobStoreSpec {
	
public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "BlobStoreTable";
	
	public static final String ID = "id";
	public static final String DATA = "data";
	public static final String MIME_TYPE = "mimeType";
	public static final String DATE_CREATED = "dateCreated";
	
}
