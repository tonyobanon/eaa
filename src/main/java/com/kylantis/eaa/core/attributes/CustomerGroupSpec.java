package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class CustomerGroupSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "CustomerGroupTable";

	public static final String NAME = "name";
	public static final String EXPRESSION = "expression";
	public static final String USERS = "users";
	public static final String DATE_CREATED = "dateCreated";
	public static final String DATE_MODIFIED = "dateModified";

}
