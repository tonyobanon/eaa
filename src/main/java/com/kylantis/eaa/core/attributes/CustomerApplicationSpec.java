package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class CustomerApplicationSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "CustomerApplicationTable";
	
	public static final String ID = "id";
	public static final String ENTRY_ID = "entryId";
	public static final String VALUE = "value";
}
