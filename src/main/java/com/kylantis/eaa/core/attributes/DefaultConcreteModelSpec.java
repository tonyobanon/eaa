package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class DefaultConcreteModelSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter()
			+ "DefaultConcreteModelTable";

	public static final String PATH = "path";
	public static final String MODEL = "model";
}
