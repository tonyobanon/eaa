package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class ServiceClientPageSpec {
	
	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "ServiceClientPageTable";
	
	public static final String SERVICE = "service";
	public static final String ID = "id";
	public static final String TITLE = "title";
	public static final String PAGE_URI = "pageUri";
	public static final String DATE_CREATED = "dateCreated";
	
}