package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class BaseUserTableSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "BaseUserTable";
	

	public static final String ID = "id";
	public static final String PASS = "pass";
	
	public static final String DEFAULT_ROLE = "defaultRole";
	public static final String ROLES = "roles";
	
	public static final String IMAGE = "image";
	
	public static final String EMAIL = "email";
	
	public static final String FNAME = "fname";
	public static final String LNAME = "lname";
	public static final String MNAME = "mname";

	public static final String PHONE = "phone";
	
	public static final String DOB = "dob";
	
	public static final String DEFAULT_ADDRESS = "defaultAddress";
	public static final String ADDRESSES = "addresses";
	
	public static final String DATE_CREATED = "dateCreated";
	
	public static final String ROLE_NAME_INDEX = "roleNameIndex";
	public static final String EMAIL_INDEX = "emailIndex";
	public static final String PHONE_INDEX = "phoneIndex";
	
}
