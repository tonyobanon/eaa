package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class ConcreteModelsSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "ConcreteModelsTable";

	public static final String PATH = "path";
	public static final String MODELS = "models";
}
