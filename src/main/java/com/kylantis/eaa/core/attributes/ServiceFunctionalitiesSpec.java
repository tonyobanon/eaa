package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class ServiceFunctionalitiesSpec {
	
public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "ServiceFunctionalitiesTable";

	public static final String SERVICE = "service";
	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String REALM = "realm";
	public static final String DATE_CREATED = "dateCreated";
	
	public static final String REALM_INDEX = "realmIndex";
}
