package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class AbstractModelsSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "AbstractModelsTable";

	public static final String PATH = "path";
	public static final String APP_ID = "appId";
	public static final String MODEL = "model";
	public static final String TITLE = "title";
	public static final String DATE = "date";
	
	public static final String APP_ID_INDEX = "appIdIndex";
}
