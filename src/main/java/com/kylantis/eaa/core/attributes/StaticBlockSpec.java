package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class StaticBlockSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "StaticBlockTable";

	public static final String IDENTIFIER = "identifier";
	public static final String CONTENT_KEY = "contentKey";
	public static final String TITLE = "title";
	public static final String IS_ENABLED = "isEnabled";
	public static final String DATE_CREATED = "dateCreated";
	public static final String DATE_UPDATED = "dateUpdated";
	
	
}
