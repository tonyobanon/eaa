package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class CustomerSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "CustomerTable";
	
	public static final String ID = "id";
	public static final String BANK_ACCOUNTS = "bankAccounts";
	public static final String CARDS = "cards";
	
	public static final String DATE_CREATED = "dateCreated";
}
