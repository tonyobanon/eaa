package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class RoleDefinitionSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "RoleDefinitionsTable";
	
	public static final String NAME = "name";
	public static final String REALM = "realm";
	public static final String FUNCTIONALITIES = "functionalities";
	public static final String IS_DEFAULT = "isDefault";

	public static final String REALM_INDEX = "realmIndex";
}
