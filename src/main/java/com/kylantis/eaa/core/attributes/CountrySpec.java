package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class CountrySpec {
	
	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "CountryTable";
	
	public static final String CODE = "code";
	public static final String COUNTRY_NAME = "countryName";
	
	public static final String CURRENCY_CODE = "currencyCode";
	public static final String CURRENCY_NAME = "currencyName";
	
	public static final String SPOKEN_LANGUAGES = "spokenLanguages";
	
	public static final String LANGUAGE_CODE = "languageCode";
	public static final String DIALING_CODE = "dialingCode";
	
}
