package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class ConfigValueSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "ConfigValueTable";
	
	public static final String KEY = "key";
	public static final String VALUE = "value";
	
}