package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class SearchGraphSpec {

public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "SearchGraphTable";
	
	public static final String ID = "id";
	public static final String MATRIX_ID = "matrixId";
	
	public static final String ENTITY_TYPE = "entityType";
	public static final String MATRIX_HASHKEY = "matrixHashKey";
	public static final String MATRIX = "matrix";
	
	
	public static final String MATRIX_INDEX = "matrixIndex";
	
	
}
