package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class CardTokenSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "CardTokenTable";

	public static final String ENTITY_ID = "entityId";

	public static final String CARD_TOKEN = "cardToken";
	
	public static final String MODULE_NAME = "moduleName";

	public static final String DATE_CREATED = "dateCreated";

}
