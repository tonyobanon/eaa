package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class CustomerDataSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "CustomerDataTable";
	
	public static final String ID = "id";
	public static final String ENTRY_ID = "entryId";
	public static final String VALUE = "value";
	public static final String DATE_CREATED = "dateCreated";
}
