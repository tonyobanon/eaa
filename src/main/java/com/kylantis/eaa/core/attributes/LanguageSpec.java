package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class LanguageSpec {
	
	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "LanguageTable";
	
	public static final String CODE = "code";
	public static final String LANG_NAME = "langName";

	
}
