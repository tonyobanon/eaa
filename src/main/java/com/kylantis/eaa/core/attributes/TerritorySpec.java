package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class TerritorySpec {
	
	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "TerritoryTable";
	
	public static final String CODE = "code";
	public static final String TERRITORY_NAME = "territoryName";
	
	public static final String COUNTRY_CODE = "countryCode";
	
	public static final String COUNTRY_CODE_INDEX = "countryCodeIndex";
}
