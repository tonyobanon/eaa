package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class NamespaceSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "NamespaceTable";

	public static final String PREFIX = "prefix";
	public static final String IS_INTERNAL = "isInternal";
	public static final String REALM = "realm";
}
