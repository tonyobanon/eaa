package com.kylantis.eaa.core.attributes;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.dbutils.EntityModeller;

public class AddressesSpec {

	public static final String TABLE_NAME = Application.getId() + EntityModeller.getTableNameDelimiter() + "AddressesTable";

	public static final String ID = "id";

	public static final String OWNER_ID = "ownerId";

	public static final String STREET = "street";

	public static final String CITY = "city";

	public static final String STATE_HASHKEY = "stateHashKey";
	public static final String STATE = "state";

	public static final String COUNTRY_HASHKEY = "countryHashKey";
	public static final String COUNTRY = "country";

	// Properly distributed
	public static final String CITY_INDEX = "cityIndex";

	// Scarcely distributed

	public static final String STATE_INDEX = "stateIndex";
	public static final String COUNTRY_INDEX = "countryIndex";
}
