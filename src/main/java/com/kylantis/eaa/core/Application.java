package com.kylantis.eaa.core;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import com.kylantis.eaa.core.base.ClasspathScanner;
import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.base.Logger;
import com.kylantis.eaa.core.base.Logger.verboseLevel;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.components.ComponentModel;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.cron.Scheduler;
import com.kylantis.eaa.core.dbutils.EntityModeller;
import com.kylantis.eaa.core.fusion.MicroServiceOptions;
import com.kylantis.eaa.core.fusion.RouteSet;
import com.kylantis.eaa.core.fusion.WebServer;
import com.kylantis.eaa.core.ml.MLRepository;
import com.kylantis.eaa.core.ml.ModelBootstrap;
import com.kylantis.eaa.core.ml.ModelLocator;
import com.kylantis.eaa.core.utils.Utils;

public class Application {

	private static boolean startupContext = true;
	private static Properties config;

	public static final String VERSION = "0.0.1";
	public static final String DEFAULT_DOMAIN = "localhost";

	public static void main(String[] args) {

		// @DEV
		Logger.enableSystemOutput();
		Logger.verboseMode(verboseLevel.DEBUG.toString());

		Logger.info("Starting Engine ..");

		config = Utils
				.getProperties(ClasspathScanner.getAppBaseDir().toString() + File.separator + "config.properties");

		StorageServiceFactory.start();

		boolean isInstalled = PlatformUtils.isInstalled();

		if (!isInstalled) {

			EntityModeller.createTables();

			Logger.debug("Scanning models");

			new ModelLocator().scan();

			Logger.debug("Creating UI Pages");

			ModelLocator.getModels().forEach(model -> {
				model.pages().forEach(p -> {
					((ComponentModel) ModelLocator.get(MLRepository.COMPONENT_MODEL)).newPage(p);
				});
			});

			Logger.debug("Pre-installing models");

			ModelLocator.getModels().forEach(model -> {
				model.preInstall();
			});

		} else {

			ModelBootstrap.start();

			new ModelLocator().scan();

			Logger.debug("Starting models");

			ModelLocator.getModels().forEach(model -> {
				model.start();
			});
		}

		// Start Scheduler Utility

		Scheduler.start();

		// Launch the web server
		try {

			WebServer.start(new MicroServiceOptions().withPort(8080)
					.withRouteSet(isInstalled ? RouteSet.ALL : RouteSet.SETUP_ONLY));

		} catch (IOException e) {
			Exceptions.throwRuntime(e);
		}

		Application.startupContext = false;
	}

	public static String getConfig(String key) {
		return config.getProperty(key);
	}

	public static Integer getConfigAsInt(String key) {
		return Integer.parseInt(config.getProperty(key));
	}

	public static Boolean getConfigAsBool(String key) {
		return Boolean.parseBoolean(config.getProperty(key));
	}

	public static final String getId() {
		return "EAA";
	}

	public static boolean isStartupContext() {
		return startupContext;
	}

	static {

	}

}
