package com.kylantis.eaa.core.locations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Pattern;

import com.amazonaws.services.dynamodbv2.document.BatchGetItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableKeysAndAttributes;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.google.common.collect.ImmutableSet;
import com.kylantis.eaa.core.Unexposed;
import com.kylantis.eaa.core.attributes.AddressesSpec;
import com.kylantis.eaa.core.attributes.CitySpec;
import com.kylantis.eaa.core.attributes.CountrySpec;
import com.kylantis.eaa.core.attributes.LanguageSpec;
import com.kylantis.eaa.core.attributes.TerritorySpec;
import com.kylantis.eaa.core.base.CacheResult;
import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.base.Todo;
import com.kylantis.eaa.core.dbutils.DBTooling;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.ILocationModel;
import com.kylantis.eaa.core.setup.InstallOptions;
import com.kylantis.eaa.core.tables.CityTable;
import com.kylantis.eaa.core.tables.CountryTable;
import com.kylantis.eaa.core.tables.LanguageTable;
import com.kylantis.eaa.core.tables.TerritoryTable;
import com.kylantis.eaa.core.users.AddressSpec;

@ConcreteModel()
public class LocationModel extends ILocationModel {

	// @DEV
	private final static String ADMIN1_CODES_FILE_NAME = "resources/admin1CodesASCII2.txt";

	// @DEV
	private final static String CITIES_1000_FILE_NAME = "resources/cities1001_.txt";

	private final static String COUNTRY_INFO_FILE_NAME = "resources/countryInfo.txt";

	private final static String LANGUAGE_CODES_FILE_NAME = "resources/languageCodes2.txt";

	@Override
	public void preInstall() {

		// Load Countries
		saveCountries(loadCountries());

		// Load Territories
		saveTerritories(loadTerritories(null));

		saveCities(loadCities(null));

		// Load Languages
		saveLanguages(loadLanguages());

	}

	public void install(InstallOptions options) {

		// saveTerritories(loadTerritories(options.getAudience() ==
		// Audiences.REGIONAL ? options.getCountry() : null));
		// saveCities(loadCities(options.getAudience() == Audiences.REGIONAL ?
		// options.getCountry() : null));
	}

	@Override
	public void start() {

	}

	@Todo
	private static String nextAddressKey() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	public String getCurrencyCode(String countryCode) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(CountrySpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(CountrySpec.CODE, countryCode)
				.withProjectionExpression("#cc").withNameMap(new NameMap().with("#cc", CountrySpec.CURRENCY_CODE)));

		return item.getString(CountrySpec.CURRENCY_CODE);
	}

	public String getCurrencyName(String countryCode) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(CountrySpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(CountrySpec.CODE, countryCode)
				.withProjectionExpression("#cn").withNameMap(new NameMap().with("#cn", CountrySpec.CURRENCY_NAME)));

		return item.getString(CountrySpec.CURRENCY_NAME);
	}

	public String getTerritoryName(String territoryCode) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(TerritorySpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(TerritorySpec.CODE, territoryCode)
				.withProjectionExpression("#tn").withNameMap(new NameMap().with("#tn", TerritorySpec.TERRITORY_NAME)));

		return item.getString(TerritorySpec.TERRITORY_NAME);
	}

	public String getCountryName(String countryCode) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(CountrySpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(CountrySpec.CODE, countryCode)
				.withProjectionExpression("#cn").withNameMap(new NameMap().with("#cn", CountrySpec.COUNTRY_NAME)));

		return item.getString(CountrySpec.COUNTRY_NAME);
	}

	public String getCityName(String cityId) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(CitySpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(CitySpec.ID, cityId).withProjectionExpression("#cn")
				.withNameMap(new NameMap().with("#cn", CitySpec.NAME)));

		return item.getString(CitySpec.NAME);
	}

	public Map<String, AddressSpec> getAddresses(Collection<String> addressIds) {

		TableKeysAndAttributes spec = new TableKeysAndAttributes(AddressesSpec.TABLE_NAME);
		spec.addHashOnlyPrimaryKeys(AddressesSpec.ID, addressIds).withProjectionExpression("#str,#cty,#st,#cn")
				.withNameMap(new NameMap().with("#str", AddressesSpec.STREET).with("#cty", AddressesSpec.CITY)
						.with("#st", AddressesSpec.STATE).with("#cn", AddressesSpec.COUNTRY));

		BatchGetItemOutcome outcome = StorageServiceFactory.getDocumentDatabase().batchGetItem(spec);
		List<Item> items = outcome.getTableItems().get(AddressesSpec.TABLE_NAME);

		Map<String, AddressSpec> addresses = new HashMap<>(items.size());

		if (items.isEmpty()) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}

		for (Item item : items) {

			AddressSpec address = new AddressSpec();
			address.setId(item.getString(AddressesSpec.ID));
			address.setStreet(item.getString(AddressesSpec.STREET));
			address.setCity(item.getInt(AddressesSpec.CITY));
			address.setState(item.getString(AddressesSpec.STATE));
			address.setCountry(item.getString(AddressesSpec.COUNTRY));

			addresses.put(address.getId(), address);
		}

		return addresses;
	}

	public Integer getAddressOwner(String id) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(AddressesSpec.TABLE_NAME);

		Item item = table.getItem(new GetItemSpec().withPrimaryKey(AddressesSpec.ID, id)
				.withProjectionExpression(AddressesSpec.OWNER_ID));

		if (item == null) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}

		return (item.getInt(AddressesSpec.OWNER_ID));
	}

	public AddressSpec getAddress(String id) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(AddressesSpec.TABLE_NAME);

		Item item = table.getItem(
				new GetItemSpec().withPrimaryKey(AddressesSpec.ID, id).withProjectionExpression("#str,#cty,#st,#cn")
						.withNameMap(new NameMap().with("#str", AddressesSpec.STREET).with("#cty", AddressesSpec.CITY)
								.with("#st", AddressesSpec.STATE).with("#cn", AddressesSpec.COUNTRY)));

		if (item == null) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}

		AddressSpec address = new AddressSpec();
		address.setId(id);
		address.setStreet(item.getString(AddressesSpec.STREET));
		address.setCity(item.getInt(AddressesSpec.CITY));
		address.setState(item.getString(AddressesSpec.STATE));
		address.setCountry(item.getString(AddressesSpec.COUNTRY));

		return address;
	}

	@PlatformInternal
	public void deleteAddress(String id) {
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(AddressesSpec.TABLE_NAME);
		table.deleteItem(AddressesSpec.ID, id);
	}

	@PlatformInternal
	public String newAddresss(String ownerId, AddressSpec spec) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(AddressesSpec.TABLE_NAME);

		String addressId = nextAddressKey();

		Item item = new Item().withPrimaryKey(AddressesSpec.ID, addressId).with(AddressesSpec.OWNER_ID, ownerId)

				.withString(AddressesSpec.STREET, spec.getStreet())

				.withInt(AddressesSpec.CITY, spec.getCity()).withString(AddressesSpec.STATE, spec.getState())
				.withString(AddressesSpec.COUNTRY, spec.getCountry());

		table.putItem(new PutItemSpec().withItem(item)
				.withConditionExpression("attribute_not_exists(" + AddressesSpec.ID + ")"));

		return addressId;
	}

	@CacheResult
	public Map<String, String> getCountryNames() {

		Map<String, String> names = new HashMap<>();

		ScanSpec spec = new ScanSpec().withConsistentRead(false).withProjectionExpression("#n, #c")
				.withNameMap(new NameMap().with("#n", CountrySpec.COUNTRY_NAME).with("#c", CountrySpec.CODE));
		ItemCollection<ScanOutcome> result = StorageServiceFactory.getDocumentDatabase()
				.getTable(CountrySpec.TABLE_NAME).scan(spec);
		result.forEach(item -> {
			names.putIfAbsent(item.getString(CountrySpec.CODE), item.getString(CountrySpec.COUNTRY_NAME));
		});
		return names;
	}

	public Map<String, String> getCurrencyNames() {

		Map<String, String> names = new HashMap<>();

		ScanSpec spec = new ScanSpec().withConsistentRead(false).withProjectionExpression("#n, #c")
				.withNameMap(new NameMap().with("#n", CountrySpec.CURRENCY_NAME).with("#c", CountrySpec.CURRENCY_CODE));
		ItemCollection<ScanOutcome> result = StorageServiceFactory.getDocumentDatabase()
				.getTable(CountrySpec.TABLE_NAME).scan(spec);
		result.forEach(item -> {
			names.putIfAbsent(item.getString(CountrySpec.CURRENCY_CODE), item.getString(CountrySpec.CURRENCY_NAME));
		});
		return names;
	}

	public Map<String, String> getAvailableLocales(String countryCode) {
		Map<String, String> locales = new HashMap<>();
		for (Locale o : Locale.getAvailableLocales()) {
			if (o.getCountry().equals(countryCode)) {
				locales.put(o.toLanguageTag(), o.getDisplayName());
			}
		}
		return locales;
	}

	public Map<String, String> getAllLocales() {
		Map<String, String> locales = new HashMap<>();
		for (Locale o : Locale.getAvailableLocales()) {
			locales.put(o.toLanguageTag(), o.getDisplayName());
		}
		return locales;
	}

	public Map<String, String> getAllTimezones() {
		Map<String, String> timezones = new HashMap<>();
		for (String id : TimeZone.getAvailableIDs()) {
			timezones.put(id, id);
		}
		return timezones;
	}
	
	@Unexposed
	public Map<String, String> getLanguageNames(List<String> languageCodes) {

		Object[] keys = new String[languageCodes.size()];

		for (int i = 0; i < languageCodes.size(); i++) {
			keys[i] = languageCodes.get(i);
		}

		TableKeysAndAttributes spec = new TableKeysAndAttributes(LanguageSpec.TABLE_NAME);
		spec.addHashOnlyPrimaryKeys(LanguageSpec.CODE, keys);

		BatchGetItemOutcome outcome = StorageServiceFactory.getDocumentDatabase().batchGetItem(spec);
		List<Item> items = outcome.getTableItems().get(LanguageSpec.TABLE_NAME);

		Map<String, String> languages = new HashMap<>();

		for (Item item : items) {
			languages.put(item.getString(LanguageSpec.CODE), item.getString(LanguageSpec.LANG_NAME));
		}
		return languages;
	}

	@CacheResult
	public Map<String, String> getTerritoryNames(String countryCode) {

		Map<String, String> territories = new HashMap<>();

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(TerritorySpec.TABLE_NAME);

		QuerySpec spec = new QuerySpec().withKeyConditionExpression("#k_cc = :v_cc")
				.withNameMap(new NameMap().with("#k_cc", TerritorySpec.COUNTRY_CODE))
				.withValueMap(new ValueMap().withString(":v_cc", countryCode)).withConsistentRead(false);

		ItemCollection<QueryOutcome> outcome = table.getIndex(TerritorySpec.COUNTRY_CODE_INDEX).query(spec);

		outcome.forEach(item -> {
			territories.put(item.getString(TerritorySpec.CODE), item.getString(TerritorySpec.TERRITORY_NAME));
		});

		return territories;
	}

	@CacheResult
	public Map<Integer, String> getCityNames(String territoryCode) {

		Map<Integer, String> cities = new HashMap<>();

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(CitySpec.TABLE_NAME);

		QuerySpec spec = new QuerySpec().withKeyConditionExpression("#k_tc = :v_tc")
				.withNameMap(new NameMap().with("#k_tc", CitySpec.TERRITORY_CODE))
				.withValueMap(new ValueMap().withString(":v_tc", territoryCode)).withConsistentRead(false);

		ItemCollection<QueryOutcome> outcome = table.getIndex(CitySpec.TERRITORY_CODE_INDEX).query(spec);

		outcome.forEach(item -> {
			cities.put(item.getInt(CitySpec.ID), item.getString(CitySpec.NAME));
		});

		return cities;
	}

	@CacheResult
	public Coordinates getCoordinates(int cityCode) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(CitySpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(CitySpec.ID, cityCode)
				.withProjectionExpression(CitySpec.LATITUDE + ", " + CitySpec.LONGITUDE));

		return new Coordinates(item.getDouble(CitySpec.LATITUDE), item.getDouble(CitySpec.LONGITUDE));
	}

	@CacheResult
	public Map<String, String> getSpokenLanguages(String countryCode) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(CountrySpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(CountrySpec.CODE, countryCode)
				.withProjectionExpression(CountrySpec.LANGUAGE_CODE + ", " + CountrySpec.SPOKEN_LANGUAGES));

		// Languages to fetch
		ArrayList<String> languages = new ArrayList<>();

		// First, add the country default language
		String languageCode = Locale.forLanguageTag(item.getString(CountrySpec.LANGUAGE_CODE)).getLanguage();
		languages.add(languageCode);

		// Then, add other spoken languages
		for (String o : item.getStringSet(CountrySpec.SPOKEN_LANGUAGES)) {
			String spokenLanguage = Locale.forLanguageTag(o).getLanguage();
			if (!languages.contains(spokenLanguage)) {
				languages.add(spokenLanguage);
			}
		}
		return getLanguageNames(languages);
	}

	@CacheResult
	public String getTimezone(int cityCode) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(CitySpec.TABLE_NAME);
		Item item = table.getItem(
				new GetItemSpec().withPrimaryKey(CitySpec.ID, cityCode).withProjectionExpression(CitySpec.TIMEZONE));
		return item.getString(CitySpec.TIMEZONE);
	}

	private static void saveLanguages(ArrayList<LanguageTable> languages) {

		int segments = Math.round(((float) languages.size()) / 25f) + 1;

		int currentIndex = 0;

		for (int i = 0; i < segments; i++) {

			TableWriteItems items = new TableWriteItems(LanguageSpec.TABLE_NAME);

			for (int j = 0; j < 25; j++) {

				if (currentIndex < languages.size()) {

					LanguageTable o = languages.get(currentIndex);

					items.addItemToPut(new Item().withString(LanguageSpec.CODE, o.code)
							.withString(LanguageSpec.LANG_NAME, o.langName)

					);

					currentIndex += 1;
				} else {
					break;
				}
			}

			if (items.getItemsToPut() != null && items.getItemsToPut().size() > 0) {
				StorageServiceFactory.getDocumentDatabase().batchWriteItem(items);
			}

		}
	}

	private static void saveCountries(ArrayList<CountryTable> countries) {

		ArrayList<Item> items = new ArrayList<>(countries.size());

		for (CountryTable o : countries) {
			items.add(new Item()

					.withString(CountrySpec.COUNTRY_NAME, o.countryName).withString(CountrySpec.CODE, o.code)

					.with(CountrySpec.CURRENCY_CODE, o.currencyCode.equals("") ? null : o.currencyCode)
					.with(CountrySpec.CURRENCY_NAME, o.currencyName.equals("") ? null : o.currencyName)

					.with(CountrySpec.DIALING_CODE, o.dialingCode.equals("") ? null : o.dialingCode)
					.with(CountrySpec.LANGUAGE_CODE, o.languageCode.equals("") ? null : o.languageCode)
					.withStringSet(CountrySpec.SPOKEN_LANGUAGES, o.spokenLanguages));
		}

		DBTooling.batchPut(CountrySpec.TABLE_NAME, items);
	}

	private static void saveTerritories(ArrayList<TerritoryTable> territories) {

		ArrayList<Item> items = new ArrayList<>(territories.size());

		for (TerritoryTable o : territories) {
			items.add(new Item().withString(TerritorySpec.CODE, o.code)
					.withString(TerritorySpec.TERRITORY_NAME, o.territoryName)
					.withString(TerritorySpec.COUNTRY_CODE, o.countryCode));
		}

		DBTooling.batchPut(TerritorySpec.TABLE_NAME, items);
	}

	private static void saveCities(ArrayList<CityTable> cities) {

		ArrayList<Item> items = new ArrayList<>(cities.size());

		for (CityTable o : cities) {
			items.add(new Item().withString(CitySpec.ID, o.id).withString(CitySpec.NAME, o.name)
					.withString(CitySpec.TERRITORY_CODE, o.territoryCode).withString(CitySpec.TIMEZONE, o.timezone)
					.withDouble(CitySpec.LONGITUDE, o.longitude).withDouble(CitySpec.LATITUDE, o.latitude));
		}

		DBTooling.batchPut(CitySpec.TABLE_NAME, items);
	}

	@Todo("Use java.util.Collections.sort to arrange alphabetically")
	private static ArrayList<CountryTable> loadCountries() {

		ArrayList<CountryTable> countries = new ArrayList<>();

		BufferedReader buffer;
		String line = null;

		try {

			buffer = new BufferedReader(new InputStreamReader(
					ClassLoader.getSystemResourceAsStream(COUNTRY_INFO_FILE_NAME), Charset.forName("UTF-8")));

			while ((line = buffer.readLine()) != null) {

				if (line.startsWith("#")) {
					continue;
				}

				String[] values = line.split("\t");

				CountryTable o = new CountryTable();
				o.code = (values[CountryInfoFeatures.ISO_CODE]);
				o.countryName = (values[CountryInfoFeatures.COUNTRY]);

				o.currencyCode = (values[CountryInfoFeatures.CURRENCY_CODE]);
				o.currencyName = (values[CountryInfoFeatures.CURRENCY_NAME]);

				if (values[CountryInfoFeatures.LANGUAGES].contains(",")) {
					o.languageCode = (values[CountryInfoFeatures.LANGUAGES].split(Pattern.quote(","))[0]);
					o.spokenLanguages = ImmutableSet
							.copyOf((values[CountryInfoFeatures.LANGUAGES].split(Pattern.quote(","))));
				} else {
					o.languageCode = (values[CountryInfoFeatures.LANGUAGES]);
					o.spokenLanguages = ImmutableSet.copyOf(new String[] { values[CountryInfoFeatures.LANGUAGES] });
				}

				o.dialingCode = (values[CountryInfoFeatures.PHONE_DIALING_CODE]);

				countries.add(o);
			}
		} catch (IOException e) {
			Exceptions.throwRuntime(e);
		}

		return countries;
	}

	private static ArrayList<TerritoryTable> loadTerritories(String countryCode) {

		ArrayList<TerritoryTable> territories = new ArrayList<>();

		BufferedReader buffer;
		String line = null;

		try {

			buffer = new BufferedReader(new InputStreamReader(
					ClassLoader.getSystemResourceAsStream(ADMIN1_CODES_FILE_NAME), Charset.forName("UTF-8")));

			while ((line = buffer.readLine()) != null) {

				if (line.startsWith("#")) {
					continue;
				}

				String[] values = line.split("\t");

				if (countryCode != null && !values[Admin1CodesFeatures.TERRITORY_CODE].split(Pattern.quote("."))[0]
						.equals(countryCode)) {
					continue;
				}

				TerritoryTable o = new TerritoryTable();
				o.code = values[Admin1CodesFeatures.TERRITORY_CODE];
				o.territoryName = (values[Admin1CodesFeatures.NAME]);
				o.countryCode = (values[Admin1CodesFeatures.TERRITORY_CODE].split(Pattern.quote("."))[0]);

				territories.add(o);
			}

		} catch (IOException e) {
			Exceptions.throwRuntime(e);
		}

		return territories;
	}

	private static ArrayList<CityTable> loadCities(String countryCode) {

		ArrayList<CityTable> cities = new ArrayList<>();

		BufferedReader buffer;
		String line = null;

		try {

			buffer = new BufferedReader(new InputStreamReader(
					ClassLoader.getSystemResourceAsStream(CITIES_1000_FILE_NAME), Charset.forName("UTF-8")));

			while ((line = buffer.readLine()) != null) {

				if (line.startsWith("#")) {
					continue;
				}

				String[] values = line.split("\t");

				if (countryCode != null && !values[Cities1000Features.COUNTRY_CODE].equals(countryCode)) {
					continue;
				}

				CityTable o = new CityTable();
				o.id = (values[Cities1000Features.GEONAMEID]);
				o.name = (values[Cities1000Features.NAME]);
				o.territoryCode = values[Cities1000Features.COUNTRY_CODE] + "." + values[Cities1000Features.ADMIN1];
				o.timezone = (values[Cities1000Features.TIMEZONE]);
				o.latitude = (Double.parseDouble(values[Cities1000Features.LATITUDE]));
				o.longitude = (Double.parseDouble(values[Cities1000Features.LONGITUDE]));

				cities.add(o);
			}

		} catch (IOException e) {
			Exceptions.throwRuntime(e);
		}

		return cities;
	}

	private static ArrayList<LanguageTable> loadLanguages() {

		ArrayList<LanguageTable> languages = new ArrayList<>();

		BufferedReader buffer;
		String line = null;

		try {

			buffer = new BufferedReader(new InputStreamReader(
					ClassLoader.getSystemResourceAsStream(LANGUAGE_CODES_FILE_NAME), Charset.forName("UTF-8")));

			while ((line = buffer.readLine()) != null) {

				if (line.startsWith("#")) {
					continue;
				}

				String[] values = line.split(",");

				LanguageTable o = new LanguageTable();
				o.code = (values[0].replace("\"", ""));
				o.langName = (values[1].replace("\"", ""));

				languages.add(o);
			}

		} catch (IOException e) {
			Exceptions.throwRuntime(e);
		}

		return languages;
	}

}
