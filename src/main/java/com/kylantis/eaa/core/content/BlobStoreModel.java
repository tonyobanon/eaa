package com.kylantis.eaa.core.content;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.kylantis.eaa.core.Dates;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.BlobStoreSpec;
import com.kylantis.eaa.core.attributes.StaticBlockSpec;
import com.kylantis.eaa.core.base.ObjectMarshaller;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.dbutils.DBTooling;
import com.kylantis.eaa.core.fusion.WebRoutes;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IBlobStoreModel;
import com.kylantis.eaa.core.setup.InstallOptions;
import com.kylantis.eaa.core.utils.Utils;

@ConcreteModel
public class BlobStoreModel extends IBlobStoreModel {

	@Override
	public void install(InstallOptions options) {
	}

	@Override
	public void start() {

	}
	
	public String save(String id, Serializable obj) throws IOException {
		return save(id, SerializationUtils.serialize(obj));
	}

	public String save(String id, InputStream in) throws IOException {
		return save(id, IOUtils.toByteArray(in));
	}
	
	public String save(String id, byte[] data) throws IOException {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BlobStoreSpec.TABLE_NAME);

		if(id == null){
			id = Utils.newRandom();
		}
		String mimeType = WebRoutes.TIKA_INSTANCE.detect(data);

		Item item = new Item().withPrimaryKey(BlobStoreSpec.ID, id).withBinary(BlobStoreSpec.DATA, data)
				.withString(BlobStoreSpec.MIME_TYPE, mimeType)
				.withString(BlobStoreSpec.DATE_CREATED, Dates.currentDate());

			table.putItem(new PutItemSpec().withItem(item));
		
		return id;
	}

	public void delete(String id) {
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BlobStoreSpec.TABLE_NAME);
		table.deleteItem(BlobStoreSpec.ID, id);
	}

	public Blob get(String id) {
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BlobStoreSpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(BlobStoreSpec.ID, id)
				.withProjectionExpression("#mime,#data")
				.withNameMap(new NameMap().with("#mime", BlobStoreSpec.MIME_TYPE).with("#data", BlobStoreSpec.DATA)));
		return new Blob(item.getString(BlobStoreSpec.MIME_TYPE), item.getBinary(BlobStoreSpec.DATA));
	}
	

	public void newStaticBlock(StaticBlock block) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(StaticBlockSpec.TABLE_NAME);

		Item item = new Item().withPrimaryKey(StaticBlockSpec.IDENTIFIER, block.getIdentifier())
				.withString(StaticBlockSpec.TITLE, block.getTitle())
				.withString(StaticBlockSpec.CONTENT_KEY, block.getContentKey())
				.withBoolean(StaticBlockSpec.IS_ENABLED, block.getIsEnabled())
				.withString(StaticBlockSpec.DATE_CREATED, Dates.currentDate())
				.withString(StaticBlockSpec.DATE_UPDATED, Dates.currentDate());

		try {
			table.putItem(new PutItemSpec().withItem(item)
					.withNameMap(new FluentHashMap<String, String>().with("#k", StaticBlockSpec.IDENTIFIER))
					.withConditionExpression("attribute_not_exists(#k)"));
		} catch (ConditionalCheckFailedException e) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS);
		}
	}

	public void deleteStaticBlock(String identifier) {
		try {
			StorageServiceFactory.getDocumentDatabase().getTable(StaticBlockSpec.TABLE_NAME)
					.deleteItem(StaticBlockSpec.IDENTIFIER, identifier);
		} catch (ResourceNotFoundException ex) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}
	}

	@PlatformInternal
	public Map<String, StaticBlock> listStaticBlocks() {

		Map<String, StaticBlock> blocks = new HashMap<>();

		DBTooling.scanTable(StaticBlockSpec.TABLE_NAME, "#id,#t,#ck,#is,#dc,#du",
				new FluentHashMap<String, String>().with("#id", StaticBlockSpec.IDENTIFIER)
						.with("#t", StaticBlockSpec.TITLE).with("#ck", StaticBlockSpec.CONTENT_KEY)
						.with("#is", StaticBlockSpec.IS_ENABLED).with("#dc", StaticBlockSpec.DATE_CREATED)
						.with("#du", StaticBlockSpec.DATE_UPDATED))

				.forEach(i -> {

					StaticBlock o = new StaticBlock();
					o.setIdentifier(i.getString(StaticBlockSpec.IDENTIFIER));
					o.setTitle(i.getString(StaticBlockSpec.TITLE));
					o.setContentKey(i.getString(StaticBlockSpec.CONTENT_KEY));
					o.setIsEnabled(i.getBoolean(StaticBlockSpec.IS_ENABLED));
					o.setDateCreated(
							(Date) ObjectMarshaller.unmarshalDate(i.getString(StaticBlockSpec.DATE_CREATED)));
					o.setDateUpdated(
							(Date) ObjectMarshaller.unmarshalDate(i.getString(StaticBlockSpec.DATE_UPDATED)));

					blocks.put(o.getIdentifier(), o);

				});
		return blocks;
	}

}
