package com.kylantis.eaa.core.content;

public class Blob {

	private String mimeType;
	private byte[] data;
	
	public Blob(String mimeType, byte[] data) {
		this.mimeType = mimeType;
		this.data = data;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	
}
