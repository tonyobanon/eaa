package com.kylantis.eaa.core.keys;

public class CacheValues {

	public static final int SESSION_TOKEN_SHORT_EXPIRY_IN_SECS = 3600;

	public static final int SESSION_TOKEN_LONG_EXPIRY_IN_SECS = 16070400;

}
