package com.kylantis.eaa.core.keys;

import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.config.Namespace;

public class RegistryKeys {

	public static final Id MAX_ALLOWED_LNAME_CHANGE_OPS = Namespace.forAdmin()
			.getKey("MAX_ALLOWED_LNAME_CHANGE_OPS");

	public static final Id COMPANY_NAME = Namespace.forAdmin().getKey("COMPANY_NAME");
	public static final Id COMPANY_LOGO_URL = Namespace.forAdmin().getKey("COMPANY_LOGO_URL");
	public static final Id COMPANY_COUNTRY = Namespace.forAdmin().getKey("COMPANY_COUNTRY");
	public static final Id COMPANY_LANGUAGE = Namespace.forAdmin().getKey("COMPANY_LANGUAGE");
	public static final Id COMPANY_AUDIENCE = Namespace.forAdmin().getKey("COMPANY_AUDIENCE");

	public static final Id QUESTIONNAIRE_HEADER_SIZE = Namespace.forAdmin().getKey("QUESTIONNAIRE_HEADER_SIZE");
	public static final Id QUESTIONNAIRE_BODY_SIZE = Namespace.forAdmin().getKey("QUESTIONNAIRE_BODY_SIZE");

	public static final Id EMAIL_BLIND_COPIES = Namespace.forAdmin().getKey("EMAIL_BLIND_COPIES");
	public static final Id SEND_EXTRA_EMAILS_TO = Namespace.forAdmin().getKey("SEND_EXTRA_EMAILS_TO");
	public static final Id MAIL_PROVIDER_URL = Namespace.forAdmin().getKey("MAIL_PROVIDER_URL");
	public static final Id EMAIL_USERNAME = Namespace.forAdmin().getKey("EMAIL_USERNAME");
	public static final Id EMAIL_PASSWORD = Namespace.forAdmin().getKey("EMAIL_PASSWORD");

	public static final Id DEFAULT_PAYMENT_PROVIDER = Namespace.forAdmin().getKey("DEFAULT_PAYMENT_PROVIDER");
	public static final Id ENFORCE_PAYMENT_DETAIL_ENTRY = Namespace.forAdmin()
			.getKey("ENFORCE_PAYMENT_DETAIL_ENTRY");

	public static final Id DEFAULT_CURRENCY = Namespace.forAdmin().getKey("DEFAULT_CURRENCY");
	public static final Id DEFAULT_TIMEZONE = Namespace.forAdmin().getKey("DEFAULT_TIMEZONE");
	public static final Id DEFAULT_LOCALE = Namespace.forAdmin().getKey("DEFAULT_LOCALE");

	public static final Id APP_VERSION = Namespace.forAdmin().getKey("APP_VERSION");
}
