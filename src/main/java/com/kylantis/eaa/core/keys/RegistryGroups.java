package com.kylantis.eaa.core.keys;

import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.config.Namespace;

public class RegistryGroups {

	public static final Id REGISTRY_PROVIDER_PROFILE = Namespace.forAdmin().getKey("PROVIDER_PROFILE");
	public static final Id REGISTRY_PAYMENTS = Namespace.forAdmin().getKey("PAYMENTS");
	public static final Id REGISTRY_USERS = Namespace.forAdmin().getKey("USERS");
	public static final Id REGISTRY_BUSINESS_ENTITIES = Namespace.forAdmin().getKey("BUSINESS_ENTITIES");
	public static final Id REGISTRY_QUESTIONNAIRE = Namespace.forAdmin().getKey("QUESTIONNAIRE");
	public static final Id REGISTRY_SYSTEM_SETTINGS = Namespace.forAdmin().getKey("SYSTEM_SETTINGS");

	public static final Id REGISTRY_DASHBOARD = Namespace.forAdmin().getKey("DASHBOARD");
	public static final Id REGISTRY_MAILING = Namespace.forAdmin().getKey("MAILING");

}
