package com.kylantis.eaa.core.keys;

public class AppConfigKey {

	public static final String CLASSES_MODELS_EXT = "classes.models.ext";
	public static final String CLASSES_SERVICES_EXT = "classes.services.ext";
	public static final String CLASSES_CRONJOBS_EXT = "classes.cronjobs.ext";
	public static final String CLASSES_MODULES_PAYMENT_EXT = "classes.modules.payment.ext";

	public static final String STORAGE_FACTORY_REFRESH_INTERVAL = "storage.factory.refreshInterval";

	public static final String ENABLE_CROSS_MODEL_CALLS_FOR_SERVICES = "services.enableCrossModelCalls";
	public static final String ENABLE_CROSS_MODEL_CALLS_FOR_MODELS = "models.enableCrossModelCalls";

}
