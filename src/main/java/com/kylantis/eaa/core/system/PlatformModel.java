package com.kylantis.eaa.core.system;

import com.kylantis.eaa.core.PlatformUtils;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IPlatformModel;
import com.kylantis.eaa.core.ml.ModelLocator;
import com.kylantis.eaa.core.setup.ApplicationSetupSpec;
import com.kylantis.eaa.core.setup.InstallOptions;

@ConcreteModel
public class PlatformModel extends IPlatformModel {

	
	@Override
	public void install(InstallOptions options) {
		
	}

	@Override
	public void start() {
		
	}

	public void doInstall(InstallOptions spec) {
	
		// consolidate service, models
		
		
		
		//Install all models
		
		ModelLocator.getModels().forEach(model -> {
			model.install(spec);
		});
		
		
		
		
		PlatformUtils.isInstalled = true;
	}

	@Override
	public void installApp(ApplicationSetupSpec spec) {
		
		//Retrieve bundle from s3, 
		//store in the apps table, along with metadata
		
		//extract it, to a temp folder, build dependencies
		//parse it, scan for artifacts (i.e. models, e.t.c)
		//and save it into classpath
		
		
		//create tables
		//install, and start models
		//start services
	}

	@Override
	public void uninstallApp(String appId) {
		

		
		
	}

}
