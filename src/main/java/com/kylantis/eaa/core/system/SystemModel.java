package com.kylantis.eaa.core.system;

import com.kylantis.eaa.core.Unexposed;
import com.kylantis.eaa.core.config.Namespace;
import com.kylantis.eaa.core.keys.RegistryKeys;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.ISystemModel;
import com.kylantis.eaa.core.ml.MLRepository;
import com.kylantis.eaa.core.setup.InstallOptions;

@ConcreteModel(dependencies = { MLRepository.CONFIG_MODEL })
public class SystemModel extends ISystemModel {

	@Override
	public void install(InstallOptions options) {

		//Add registry entries
		
		Namespace registry = Namespace.forAdmin();
		
		getConfigModel().put(registry.getKey(RegistryKeys.COMPANY_NAME), options.getCompanyName());
		getConfigModel().put(registry.getKey(RegistryKeys.COMPANY_LOGO_URL), options.getCompanyLogoUrl());
		getConfigModel().put(registry.getKey(RegistryKeys.COMPANY_COUNTRY), options.getCountry());
		getConfigModel().put(registry.getKey(RegistryKeys.COMPANY_LANGUAGE), options.getLanguage());
		getConfigModel().put(registry.getKey(RegistryKeys.COMPANY_AUDIENCE), options.getAudience());
		
		
		 getConfigModel().put(registry.getKey(RegistryKeys.DEFAULT_CURRENCY), options.getCurrency());
		 getConfigModel().put(registry.getKey(RegistryKeys.DEFAULT_LOCALE), options.getLocale());
		 getConfigModel().put(registry.getKey(RegistryKeys.DEFAULT_TIMEZONE), options.getTimezone());
			
	}

	@Unexposed
	public String getDefaultLocale() {
		return (String) getConfigModel().get(Namespace.forAdmin().getKey(RegistryKeys.DEFAULT_LOCALE));
	}
	
	@Unexposed
	public String getDefaultTimezone() {
		return (String) getConfigModel().get(Namespace.forAdmin().getKey(RegistryKeys.DEFAULT_TIMEZONE));
	}

	@Unexposed
	public String getDefaultCurrency() {
		return (String) getConfigModel().get(Namespace.forAdmin().getKey(RegistryKeys.DEFAULT_CURRENCY));
	}

	@Override
	public void start() {
		
	}

}
