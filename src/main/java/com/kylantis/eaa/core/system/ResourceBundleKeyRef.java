package com.kylantis.eaa.core.system;

public class ResourceBundleKeyRef extends BaseObject {

	private String key;

	public String getKey() {
		return key;
	}

	public ResourceBundleKeyRef setKey(String key) {
		this.key = key;
		return this;
	}
	
	@Override
	public final int hashCode() {
		return key.intern().hashCode();
	}
}
