package com.kylantis.eaa.core.system;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.base.Logger;
import com.kylantis.eaa.core.base.Redis;
import com.kylantis.eaa.core.base.ResourceScanner;
import com.kylantis.eaa.core.base.Todo;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IResourceBundleModel;
import com.kylantis.eaa.core.ml.MLRepository;
import com.kylantis.eaa.core.setup.InstallOptions;
import com.kylantis.eaa.core.utils.Utils;

@ConcreteModel(dependencies = { MLRepository.LOCATION_MODEL })
public class ResourceBundleModel extends IResourceBundleModel {

	private static String ext = ".properties";
	private static Pattern pattern = Pattern.compile("_(a-z){2,3}((-)a-zA-Z){2,3}\\Q.properties\\E\\z");

	private static final String BUNDLE_PREFIX = "rb_";

	
	@Override
	public void install(InstallOptions options) {

	}

	@Todo("Make RB entries persistent, add support for auto translation via google translate")
	@Override
	public void start() {
		// Read resource bundle files
		new ResourceScanner(ext).scan().forEach(p -> {

			boolean isBundle = false;
			String filePath = p.toString();
			Matcher m = pattern.matcher(filePath);

			while (m.find()) {

				isBundle = true;

				String match = m.group();

				Logger.info("Saving resource bundle: " + match);

				// String bundleName = p.getFileName().toString().replace(match,
				// "");
				String locale = match.replace("_", "").replace(".properties", "");

				Properties bundle = new Properties();
				try {
					bundle.load(Files.newInputStream(p));
				} catch (IOException e) {
					Exceptions.throwRuntime(e);
				}

				// Add To Redis
				bundle.forEach((k, v) -> {
					put(locale, k.toString(), v.toString());
				});
			}

			if (!isBundle) {
				Logger.warn(filePath + " could not be identified as a resource bundle");
			}
		});

	}

	public void put(String locale, String key, String value) {
		if (key.startsWith("ext.")) {
			key = key.replace(" ", "_");
		}
		Redis.addFieldToHash(BUNDLE_PREFIX + locale, key, value);
	}

	public String get(String locale, String key) {

		if (key.startsWith("ext.")) {
			key = key.replace(" ", "_");
		}

		String value = Redis.get(BUNDLE_PREFIX + locale, key);
		if (value == null && key.startsWith("ext.")) {
			value = Utils.prettify(key.split(Pattern.quote("."))[1].replace("_", " "));
		}
		return value;
	}

	public Map<String, String> getAll(String locale) {
		return Redis.getSet(BUNDLE_PREFIX + locale);
	}

}
