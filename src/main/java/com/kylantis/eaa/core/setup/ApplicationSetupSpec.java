package com.kylantis.eaa.core.setup;

public class ApplicationSetupSpec {

	private String bundleS3Bucket;
	private String bundleS3Key;

	public String getBundleS3Bucket() {
		return bundleS3Bucket;
	}

	public ApplicationSetupSpec setBundleS3Bucket(String bundleS3Bucket) {
		this.bundleS3Bucket = bundleS3Bucket;
		return this;
	}

	public String getBundleS3Key() {
		return bundleS3Key;
	}

	public ApplicationSetupSpec setBundleS3Key(String bundleS3Key) {
		this.bundleS3Key = bundleS3Key;
		return this;
	}

}
