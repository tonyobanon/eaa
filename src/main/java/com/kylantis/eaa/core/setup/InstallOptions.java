package com.kylantis.eaa.core.setup;

import java.util.ArrayList;
import java.util.Map;

import com.kylantis.eaa.core.messaging.MailCredentialSpec;
import com.kylantis.eaa.core.users.UserProfileSpec;

public class InstallOptions {

	//Company Information
	
	public String companyName;
	public String companyLogoUrl;
	
	public String country;
	public String language;
	
	public String audience;
	
	
	//Administrators
	public ArrayList<UserProfileSpec> admins;

	
	//Mail Credentials -->
	public MailCredentialSpec mailCredentials;
	
	
	//Payment Information -->
	public String paymentProvider;
	public Map<String, String> paymentProviderConfig;
	
	
	//System Settings
	public String currency;
	public String timezone;
	public String locale;
	

	public String getCompanyName() {
		return companyName;
	}

	public String getCountry() {
		return country;
	}

	public String getLanguage() {
		return language;
	}

	public String getAudience() {
		return audience;
	}

	public ArrayList<UserProfileSpec> getAdmins() {
		return admins;
	}

	public String getCompanyLogoUrl() {
		return companyLogoUrl;
	}
	
	public MailCredentialSpec getMailCredentials() {
		return mailCredentials;
	}

	public String getPaymentProvider() {
		return paymentProvider;
	}

	public Map<String, String> getPaymentProviderConfig() {
		return paymentProviderConfig;
	}

	public String getCurrency() {
		return currency;
	}

	public String getTimezone() {
		return timezone;
	}

	public String getLocale() {
		return locale;
	}
	
}
