package com.kylantis.eaa.core.setup;

import java.util.List;

import com.kylantis.eaa.core.forms.Question;
import com.kylantis.eaa.core.microservices.ServiceClientPage;
import com.kylantis.eaa.core.microservices.ServiceFunctionalitySpec;

public class ServiceRegistrationRequest {

	private String id;
	private String title;
	private String summary;
	private String baseUri;
	private String developer;
	private String developerUrl;
	private String logoUrl;

	private List<ServiceClientPage> pages;
	private List<ServiceFunctionalitySpec> functionalities;
	private List<Question> userConfigParams;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getBaseUri() {
		return baseUri;
	}

	public void setBaseUri(String baseUri) {
		this.baseUri = baseUri;
	}

	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}

	public String getDeveloperUrl() {
		return developerUrl;
	}

	public void setDeveloperUrl(String developerUrl) {
		this.developerUrl = developerUrl;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public List<ServiceClientPage> getPages() {
		return pages;
	}

	public void setPages(List<ServiceClientPage> pages) {
		this.pages = pages;
	}

	public List<ServiceFunctionalitySpec> getFunctionalities() {
		return functionalities;
	}

	public void setFunctionalities(List<ServiceFunctionalitySpec> functionalities) {
		this.functionalities = functionalities;
	}

	public List<Question> getUserConfigParams() {
		return userConfigParams;
	}

	public void setUserConfigParams(List<Question> userConfigParams) {
		this.userConfigParams = userConfigParams;
	}

}
