package com.kylantis.eaa.core.base;

public class ResourceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public static final int RESOURCE_ALREADY_EXISTS = 0;
	public static final int RESOURCE_NOT_FOUND = 1;
	
	public static final int FAILED_VALIDATION = 2;
	public static final int UPDATE_NOT_ALLOWED = 3;
	
	public static final int DELETE_NOT_ALLOWED = 4;
	public static final int RESOURCE_STILL_IN_USE = 5;
	
	public static final int ACCESS_NOT_ALLOWED = 6;
	

	private final int errCode;
	private final String ref;

	public ResourceException(int errCode) {
		this.errCode = errCode;
		this.ref = null;
	}
	
	public ResourceException(int errCode, String ref) {
		this.errCode = errCode;
		this.ref = ref;
	}

	public int getErrCode() {
		return errCode;
	}

	public String getRef() {
		return ref;
	}
}
