package com.kylantis.eaa.core.base;

public interface LogPipeline {
	void println(String line);
}
