package com.kylantis.eaa.core.base;

import java.util.Collection;

public interface LoggerInterceptor {

	void takeSnapshot(Collection<String> logEntries);
	
}
