package com.kylantis.eaa.core.base;

import java.util.Date;

import com.kylantis.eaa.core.Dates;

public class ObjectMarshaller {

	/* Boolean */
	public static String marshal(Boolean object) {
		return object.equals(Boolean.TRUE) ? "1" : "0";
	}

	public static Boolean unmarshalBool(String object) {
		return object.equals("1") ? Boolean.TRUE : Boolean.FALSE;
	}

	/* Date */
	public static String marshal(Date object) {
		return Dates.toString(object);
	}

	public static Date unmarshalDate(String object) {
		return Dates.toDate(object);
	}

}
