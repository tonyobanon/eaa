package com.kylantis.eaa.core.base;

public @interface Todo {
  String value() default "";
}
