package com.kylantis.eaa.core.base;

public class ObjectUtils {

	public static boolean isEmpty(Object obj) {

		switch (obj.getClass().getName()) {
		case "java.lang.String":
			return isStringEmpty((String) obj);
		default:
			return false;
		}
	}

	private static boolean isStringEmpty(String s) {
		return s.equals("") || s == null;
	}

}
