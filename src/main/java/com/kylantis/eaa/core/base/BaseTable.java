package com.kylantis.eaa.core.base;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.kylantis.eaa.core.indexing.IndexConfig;

public abstract class BaseTable {
	
	public ProvisionedThroughput provisionedThroughput(){
		return IndexConfig.DEFAULT_PROVISIONED_THROUGHPUT;
	}
	
	public abstract String tableHashKey();
	
	public String tableRangeKey(){
		return null;
	}
	
	public Map<String, IndexConfig> LSIs(){
		return new HashMap<>();
	}
	
	public Map<String, IndexConfig> unindexedGSIs(){
		return new HashMap<>();
	}
	
	public Map<String, IndexConfig> indexedGSIs(){
		return new HashMap<>();
	}
	
	public Boolean enabled() {
		return true;
	}
}
