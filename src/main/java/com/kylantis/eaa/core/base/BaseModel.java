package com.kylantis.eaa.core.base;

import java.util.List;

import com.kylantis.eaa.core.components.Grid;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.setup.InstallOptions;

public abstract class BaseModel {

	public static final String PATH_METHOD = "path";	
	public static final String PAGES_METHOD = "pages";
	
	/**
	 * This method is used by models to populate data into their tables after
	 * Install Options are available, as well as add default metric data. It also
	 * should contain logic required to start the model
	 */
	public void install(InstallOptions options) {

	}

	public void start() {
		
	}
	
	public Grid render(Id page) {
		return null;
	}
	
	public abstract String path();
	
	public abstract List<Page> pages();

	/**
	 * This method is used by models to populate data into their tables before
	 * Install Options are available, as well as add default metric data.
	 */
	public void preInstall() {
	}

	public void unInstall() {
	}

}
