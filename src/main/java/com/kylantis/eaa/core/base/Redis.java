package com.kylantis.eaa.core.base;

import java.util.Map;

import redis.clients.jedis.Jedis;

public class Redis {

	private static int DEFAULT_EXPIRATION_IN_SECS = 3600;

	private static String endpoint;
	private static Jedis redisClient;

	public static void start() {
		Logger.info("Starting redis client");

		endpoint = StorageServiceFactory.getRedisEndpoint();
		redisClient = new Jedis(endpoint);

		Logger.info("Redis client successfully started");
	}

	private static void swap() {

		if (endpoint.equals(StorageServiceFactory.getRedisEndpoint())) {
			return;
		}

		Logger.info("Hotswapping redis client");

		Jedis newRedisClient = new Jedis(StorageServiceFactory.getRedisEndpoint());
		Jedis oldRedisClient = redisClient;

		synchronized (redisClient) {
			redisClient = newRedisClient;
		}
		endpoint = StorageServiceFactory.getRedisEndpoint();
		oldRedisClient.disconnect();

		Logger.info("Redis client swapped successfully");
	}

	public static void reset() {

		if (redisClient == null) {
			start();
		} else {
			swap();
		}
	}

	public static Integer getInt(String key) {
		String value = get(key);
		if (value != null) {
			return Integer.parseInt(value);
		} else {
			return null;
		}
	}

	public static String get(String key) {
		validateString(key);
		return redisClient.get(key);
	}

	public static void put(String key, String value) {
		redisClient.setex(key, DEFAULT_EXPIRATION_IN_SECS, value);
	}

	public static void put(String key, String value, int seconds) {
		redisClient.setex(key, seconds, value);
	}

	public static void putInt(String key, Integer value) {
		put(key, Integer.toString(value));
	}

	public static void putInt(String key, Integer value, int seconds) {
		put(key, Integer.toString(value), seconds);
	}

	public static Integer getInt(String key, String field) {
		String value = get(key, field);
		if (value != null) {
			return Integer.parseInt(value);
		} else {
			return null;
		}
	}

	public static String get(String hashKey, String field) {
		validateString(hashKey);
		validateString(field);
		return redisClient.hget(hashKey, field);
	}

	public static Map<String, String> getSet(String key) {
		validateString(key);
		return redisClient.hgetAll(key);
	}

	public static void addFieldToHash(String key, String field, String value) {
		redisClient.hset(key, field, value);
	}

	public static void removeFieldFromHash(String key, String... fields) {
		redisClient.hdel(key, fields);
	}

	public static void getFieldKeys(String hashKey) {
		redisClient.hkeys(hashKey);
	}

	public static Long getFieldCount(String hashKey) {
		return redisClient.hlen(hashKey);
	}

	public static void del(String... key) {
		redisClient.del(key);
	}

	private static void validateString(String key) {
		if (key == null || key == "null") {
			throw new RuntimeException("A null key was used in Redis Cache");
		}
	}

	public static void clear() {
		Logger.info("Clearing redis cache");
		redisClient.flushAll();
		redisClient.flushDB();
	}

}
