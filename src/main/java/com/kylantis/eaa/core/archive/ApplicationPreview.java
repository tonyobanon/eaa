package com.kylantis.eaa.core.archive;

import com.kylantis.eaa.core.users.UserContact;

public class ApplicationPreview {

	private Integer id;
	private UserContact userContact;
	private Integer previousApplications;
	
	public ApplicationPreview() {
	}

	public ApplicationPreview(Integer id, UserContact userContact, Integer previousApplications) {
		this.id = id;
		this.userContact = userContact;
		this.previousApplications = previousApplications;
	}

	public UserContact getUserContact() {
		return userContact;
	}

	public void setUserContact(UserContact userContact) {
		this.userContact = userContact;
	}

	public Integer getPreviousApplications() {
		return previousApplications;
	}

	public void setPreviousApplications(Integer previousApplications) {
		this.previousApplications = previousApplications;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
