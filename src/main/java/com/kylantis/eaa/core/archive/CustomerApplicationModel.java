//package com.kylantis.eaa.core.archive;
//
//import java.io.File;
//import java.io.InputStream;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import com.amazonaws.services.dynamodbv2.document.Item;
//import com.amazonaws.services.dynamodbv2.document.Table;
//import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
//import com.kylantis.eaa.core.Dates;
//import com.kylantis.eaa.core.FluentArrayList;
//import com.kylantis.eaa.core.PlatformUtils;
//import com.kylantis.eaa.core.attributes.CustomerApplicationSpec;
//import com.kylantis.eaa.core.base.BaseModel;
//import com.kylantis.eaa.core.base.Logger;
//import com.kylantis.eaa.core.base.Model;
//import com.kylantis.eaa.core.base.StorageServiceFactory;
//import com.kylantis.eaa.core.base.Todo;
//import com.kylantis.eaa.core.blobs.BlobStoreModel;
//import com.kylantis.eaa.core.classes.ConfigNamespaces;
//import com.kylantis.eaa.core.classes.SectionType;
//import com.kylantis.eaa.core.dbutils.DBTooling;
//import com.kylantis.eaa.core.forms.BaseSimpleEntry;
//import com.kylantis.eaa.core.forms.CompositeEntry;
//import com.kylantis.eaa.core.forms.FormFactory;
//import com.kylantis.eaa.core.forms.FormModel;
//import com.kylantis.eaa.core.forms.FormSection;
//import com.kylantis.eaa.core.forms.InputType;
//import com.kylantis.eaa.core.forms.Question;
//import com.kylantis.eaa.core.forms.SectionProperties;
//import com.kylantis.eaa.core.forms.SectionsBuilder;
//import com.kylantis.eaa.core.forms.SimpleEntry;
//import com.kylantis.eaa.core.install.InstallOptions;
//import com.kylantis.eaa.core.keys.BlobKeys;
//import com.kylantis.eaa.core.keys.RegistryGroups;
//import com.kylantis.eaa.core.keys.RegistryKeys;
//import com.kylantis.eaa.core.locations.LocationModel;
//import com.kylantis.eaa.core.metrics.MetricKeys;
//import com.kylantis.eaa.core.metrics.MetricsModel;
//import com.kylantis.eaa.core.pdf.PDFForm;
//import com.kylantis.eaa.core.pdf.SizeSpec;
//import com.kylantis.eaa.core.users.UserContact;
//import com.kylantis.eaa.core.users.UserProfileModel;
//
//@Model(dependencies = { RegistryModel.class, FormModel.class, LocationModel.class, MetricsModel.class })
//public class CustomerApplicationModel extends BaseModel {
//
//	@Todo("Instead of manually creating sections and entries, read these from a config file ")
//	@Override
//	public void preInstall() {
//
//		// Create Registry Group
//
//		RegistryModel.newGroup(RegistryGroups.REGISTRY_QUESTIONNAIRE, "Questionnaire");
//
//		// Add default registry entries
//
//		RegistryModel.newParameter(RegistryGroups.REGISTRY_QUESTIONNAIRE, new SimpleEntry(
//				RegistryKeys.QUESTIONNAIRE_HEADER_SIZE, InputType.NUMBER_2L, "Questionnaire Header Size"), "3");
//
//		RegistryModel.newParameter(RegistryGroups.REGISTRY_QUESTIONNAIRE,
//				new SimpleEntry(RegistryKeys.QUESTIONNAIRE_BODY_SIZE, InputType.NUMBER_2L, "Questionnaire Body Size"),
//				"2");
//
//		
//		
//		// Create default questionnaire sections
//
//		newSection(new FormSection(QuestionairreSections.PERSONAL_SECTION, "Personal Information").setSortOrder(1));
//		newSection(new FormSection(QuestionairreSections.NEXT_OF_KIN_SECTION, "Next of Kin Information").setSortOrder(2));
//		newSection(new FormSection(QuestionairreSections.IDENTIFICATION_SECTION, "Identification Information")
//						.setSortOrder(3));
//		newSection(new FormSection(QuestionairreSections.HCP_SECTION, "Health Provider Information").setSortOrder(4));
//		newSection(new FormSection(QuestionairreSections.WORK_SECTION, "Work Information").setSortOrder(5));
//		newSection(new FormSection(QuestionairreSections.PAYMENT_SECTION, "Payment Information").setSortOrder(6));
//		newSection(new FormSection(QuestionairreSections.SIGNATURE_SECTION, "Signature").setSortOrder(7));
//
//		// Add default questionnaire entries
//
//		newQuestion(QuestionairreSections.PERSONAL_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_USER_ID, InputType.NUMBER, "User Id").setSortOrder(1));
//
//		newQuestion(QuestionairreSections.PERSONAL_SECTION,
//				new CompositeEntry(QuestionairreEntryKeys.QE_GENDER, "Sex")
//						.setAllowMultipleChoice(false).withItems(new FluentArrayList<BaseSimpleEntry>()
//								.with(new BaseSimpleEntry("m", "Male")).with(new BaseSimpleEntry("f", "Female")))
//						.setSortOrder(2));
//
//		newQuestion(QuestionairreSections.NEXT_OF_KIN_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_NEXT_OF_KIN_NAME, InputType.PLAIN, "Name").setSortOrder(1));
//
//		newQuestion(QuestionairreSections.NEXT_OF_KIN_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_NEXT_OF_KIN_ADDRESS, InputType.PLAIN, "Address")
//						.setSortOrder(2));
//
//		newQuestion(QuestionairreSections.NEXT_OF_KIN_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_NEXT_OF_KIN_CITY, InputType.PLAIN, "City").setSortOrder(3));
//
//		newQuestion(QuestionairreSections.NEXT_OF_KIN_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_NEXT_OF_KIN_STATE, InputType.PLAIN, "State").setSortOrder(4));
//
//		newQuestion(QuestionairreSections.NEXT_OF_KIN_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_NEXT_OF_KIN_COUNTRY, InputType.PLAIN, "Country")
//						.setSortOrder(5));
//
//		newQuestion(QuestionairreSections.NEXT_OF_KIN_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_NEXT_OF_KIN_PHONE, InputType.PLAIN, "Phone").setSortOrder(6));
//
//		newQuestion(QuestionairreSections.IDENTIFICATION_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_MEANS_OF_IDENTIFICATION, InputType.IMAGE,
//						"valid means of identification (National ID, Driver's licensse or international passport)")
//								.setSortOrder(1));
//
//		newQuestion(QuestionairreSections.IDENTIFICATION_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_UTILITY_BILL, InputType.IMAGE, "recent utility biil")
//						.setSortOrder(2));
//
//		newQuestion(QuestionairreSections.IDENTIFICATION_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_GUARANTOR_CUSTOMER_ID, InputType.NUMBER,
//						"Guarantor's Customer Id").setSortOrder(3));
//
//		newQuestion(QuestionairreSections.IDENTIFICATION_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_GUARANTOR_NAME, InputType.PLAIN, "Guarantor's Name")
//						.setSortOrder(4));
//
//		newQuestion(QuestionairreSections.IDENTIFICATION_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_GUARANTOR_SIGNATURE, InputType.SIGNATURE,
//						"Guarantor's signature").setSortOrder(5));
//
//		newQuestion(QuestionairreSections.HCP_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CURRENT_HCP_NAME, InputType.PLAIN, "Name").setSortOrder(1));
//
//		newQuestion(QuestionairreSections.HCP_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CURRENT_HCP_ADDRESS, InputType.PLAIN, "Address")
//						.setSortOrder(2));
//
//		newQuestion(QuestionairreSections.HCP_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CURRENT_HCP_CITY, InputType.PLAIN, "City").setSortOrder(3));
//
//		newQuestion(QuestionairreSections.HCP_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CURRENT_HCP_STATE, InputType.PLAIN, "State").setSortOrder(4));
//
//		newQuestion(QuestionairreSections.HCP_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CURRENT_HCP_COUNTRY, InputType.PLAIN, "Country")
//						.setSortOrder(5));
//
//		newQuestion(QuestionairreSections.HCP_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CURRENT_HCP_PHONE, InputType.PLAIN, "Phone").setSortOrder(6));
//
//		newQuestion(QuestionairreSections.WORK_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CURRENT_EMPLOYER_NAME, InputType.PLAIN, "Name of Employer")
//						.setSortOrder(1));
//
//		newQuestion(QuestionairreSections.WORK_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_STAFF_ID, InputType.PLAIN, "Staff Id").setSortOrder(2));
//
//		newQuestion(QuestionairreSections.WORK_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_MONTHLY_RENUMERATION, InputType.AMOUNT, "Monthly Income")
//						.setSortOrder(3));
//
//		newQuestion(QuestionairreSections.WORK_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CURRENT_EMPLOYER_ADDRESS, InputType.PLAIN, "Address")
//						.setSortOrder(4));
//
//		newQuestion(QuestionairreSections.WORK_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CURRENT_EMPLOYER_CITY, InputType.PLAIN, "City")
//						.setSortOrder(5));
//
//		newQuestion(QuestionairreSections.WORK_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CURRENT_EMPLOYER_STATE, InputType.PLAIN, "State")
//						.setSortOrder(6));
//
//		newQuestion(QuestionairreSections.WORK_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CURRENT_EMPLOYER_COUNTRY, InputType.PLAIN, "Country")
//						.setSortOrder(7));
//
//		newQuestion(QuestionairreSections.PAYMENT_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CARD_NUMBER, InputType.NUMBER, "Card Number")
//						.setSortOrder(1));
//
//		newQuestion(QuestionairreSections.PAYMENT_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CARD_EXP_MONTH, InputType.NUMBER_2L, "Card Exp. Month")
//						.setSortOrder(2));
//
//		newQuestion(QuestionairreSections.PAYMENT_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CARD_EXP_YEAR, InputType.NUMBER_4L, "Card Exp. Year")
//						.setSortOrder(3));
//
//		newQuestion(QuestionairreSections.PAYMENT_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_CARD_CVC, InputType.NUMBER_3L, "Card CVC").setSortOrder(4));
//
//		newQuestion(QuestionairreSections.PAYMENT_SECTION,
//				new SimpleEntry(QuestionairreEntryKeys.QE_SIGNATURE, InputType.SIGNATURE, "Signature").setSortOrder(5));
//
//	}
//
//	@Override
//	public void install(InstallOptions options) {
//		
//		//Generate PDF questionnaire
//		generateQuestionnarePDF();
//		
//	}
//
//	@Override
//	public void start() {
//
//	}
//
//	public static void generateQuestionnarePDF() {
//		try {
//
//			int headerSize = RegistryModel.getInt(RegistryKeys.QUESTIONNAIRE_HEADER_SIZE);
//			int bodySize = RegistryModel.getInt(RegistryKeys.QUESTIONNAIRE_BODY_SIZE);
//
//			// Generate Customer Application PDF form
//			File tempFile = FormFactory.toPDF(new SizeSpec(bodySize), new SizeSpec(headerSize),
//					getQuestionairreSheet());
//
//			InputStream in = Files.newInputStream(Paths.get(tempFile.toURI()));
//
//			BlobStoreModel.save(BlobKeys.CUSTOMER_APPLICATION_PDF_FORM, in);
//
//			tempFile.delete();
//
//		} catch (Exception e) {
//			Logger.error(e.getMessage());
//		}
//	}
//
//	public static List<FormSection> getSectionsAndChildren() {
//		List<FormSection> sections = new ArrayList<>();
//		getSections().forEach(group -> {
//			FormSection section = new FormSection(group.getId(), group.getTitle(), group.getSummary());
//			section.setSortOrder(group.getSortOrder());
//			section.withEntries(getEntries(group.getId()));
//		});
//		return sections;
//	}
//
//	public static void newSection(String id, String title, String summary) {
//		FormModel.newSection(SectionType.QUESTIONNAIRE, new FormSection(id, title, summary));
//	}
//
//	public static void deleteSection(String id) {
//		FormModel.deleteSection(ConfigNamespaces.QUESTIONNAIRE + id, SectionType.QUESTIONNAIRE);
//	}
//
//	public static List<Question> getEntries(String section) {
//		return FormModel.getQuestions(ConfigNamespaces.QUESTIONNAIRE + section);
//	}
//
//	public static void deleteQuestion(String id) {
//		FormModel.deleteQuestion(ConfigNamespaces.QUESTIONNAIRE + id);
//	}
//
//	public static SectionProperties getSection(String id) {
//		 return FormModel.getSection(ConfigNamespaces.QUESTIONNAIRE + id);
//	}
//	
//	public static List<SectionProperties> getSections(Iterable<String> ids) {
//		List<Object> idList = new ArrayList<>();
//		ids.forEach( s -> {
//			idList.add(ConfigNamespaces.QUESTIONNAIRE + s);
//		});
//		return FormModel.getSection(idList);
//	}
//
//	public static List<SectionProperties> getSections() {
//		return FormModel.listSections(SectionType.QUESTIONNAIRE, true);
//	}
//	
//	public static void newSection(FormSection section) {
//		FormModel.newSection(SectionType.QUESTIONNAIRE, section.setId(ConfigNamespaces.QUESTIONNAIRE + section.getId()));
//	}
//
//	public static void newQuestion(String section, Question o, boolean isDefault) {
//		section = ConfigNamespaces.QUESTIONNAIRE + section;
//		if (o instanceof SimpleEntry) {
//			SimpleEntry e = ((SimpleEntry)o);
//			e.setId(ConfigNamespaces.QUESTIONNAIRE + e.getId());
//			FormModel.newSimpleEntry(section, isDefault, e);
//		} else if (o instanceof CompositeEntry) {
//			CompositeEntry e = ((CompositeEntry)o);
//			e.setId(ConfigNamespaces.QUESTIONNAIRE + e.getId());
//			FormModel.newCompositeEntry(section, isDefault, e);
//		}
//	}
//	
//	public static void newQuestion(String section, Question o) {
//		newQuestion(section, o, !PlatformUtils.isInstalled());
//	}
//
//	private static PDFForm getQuestionairreSheet() {
//
//		// Read questionnaire data from database
//
//		Map<String, List<Question>> sectionQuestions = new HashMap<String, List<Question>>();
//		List<FormSection> sections = new ArrayList<>();
//
//		for (SectionProperties section : FormModel.listSections(SectionType.QUESTIONNAIRE)) {
//
//			sections.add(new FormSection(section.getId(), section.getTitle(), section.getSummary()));
//			sectionQuestions.put(section.getId(), new ArrayList<>());
//
//			sectionQuestions.get(section.getId()).addAll(FormModel.getQuestions(ConfigNamespaces.QUESTIONNAIRE + section.getId()));
//		}
//
//		// Create PDFForm instance
//
//		PDFForm sheet = new PDFForm();
//
//		for (FormSection o : sections) {
//			sheet.withSection(o.withEntries(sectionQuestions.get(o.getId())));
//		}
//
//		sheet.setSubtitleLeft(LocationModel.getCountryName(RegistryModel.get(RegistryKeys.COMPANY_COUNTRY)))
//				.setSubtitleRight(RegistryModel.get(RegistryKeys.COMPANY_NAME))
//				.setTitle("Health Insurance Questionairre").withInputTypePrefix(InputType.AMOUNT,
//						LocationModel.getCurrencyCode(RegistryModel.get(RegistryKeys.COMPANY_COUNTRY)));
//
//		return sheet;
//	}
//
//	public static Integer getApplicationCount() {
//		return MetricsModel.getInt(MetricKeys.CUSTOMER_APPLICATION_COUNT_CURRENT);
//	}
//
//	public static void declineApplication(Integer applicationId) {
//
//		MetricsModel.decrementInt(MetricKeys.CUSTOMER_APPLICATION_COUNT_CURRENT);
//	}
//
//	public static String getProvidedAnswer(Integer applicationId, String entryId) {
//		Table table = StorageServiceFactory.getDocumentDatabase().getTable(CustomerApplicationSpec.TABLE_NAME);
//		Item item = table.getItem(new GetItemSpec().withPrimaryKey(CustomerApplicationSpec.ID, applicationId,
//				CustomerApplicationSpec.ENTRY_ID, entryId));
//		return (String) item.get(CustomerApplicationSpec.VALUE);
//	}
//
//	public static void acceptApplication(Integer applicationId) {
//
//		// Send Message to User via email and text
//		UserContact contact = UserProfileModel
//				.getUserContact(Integer.parseInt(getProvidedAnswer(applicationId, QuestionairreEntryKeys.QE_USER_ID)));
//
//		MetricsModel.decrementInt(MetricKeys.CUSTOMER_APPLICATION_COUNT_CURRENT);
//
//		// Move data from CustomerApplication to CustomerData
//
//		// Onboard Customer
//
//	}
//
//	private static Integer nextKey() {
//		return MetricsModel.getInt(MetricKeys.CUSTOMER_APPLICATION_COUNT_ARCHIVE) + 1;
//	}
//
//	public static void submitApplication(Map<String, String> input) {
//
//		input.put(QuestionairreEntryKeys.QE_DATE, Dates.currentDate());
//
//		Integer applicationId = nextKey();
//		MetricsModel.incrementInt(MetricKeys.CUSTOMER_APPLICATION_COUNT_ARCHIVE);
//
//		// Aggregate Item to put
//
//		List<Item> items = new ArrayList<>();
//		input.forEach((k, v) -> {
//			items.add(new Item().withInt(CustomerApplicationSpec.ID, applicationId)
//					.withString(CustomerApplicationSpec.ENTRY_ID, k).withString(CustomerApplicationSpec.VALUE, v));
//		});
//
//		DBTooling.batchPut(CustomerApplicationSpec.TABLE_NAME, items);
//
//		MetricsModel.incrementInt(MetricKeys.CUSTOMER_APPLICATION_COUNT_CURRENT);
//		MetricsModel.incrementInt(MetricKeys.CUSTOMER_APPLICATION_COUNT_TIMESERIES);
//
//		MetricsModel.incrementInt(MetricKeys.USER_TO_APPLICATION_COUNT_$USER_ARCHIVE.replace("$USER",
//				input.get(QuestionairreEntryKeys.QE_USER_ID)));
//
//	}
//
//	public static Map<Integer, ApplicationPreview> listApplications() {
//
//		Map<Integer, ApplicationPreview> result = new HashMap<>();
//
//		for (int i = 0; i < getApplicationCount(); i++) {
//
//			Integer applicationId = i + 1;
//
//			String userId = getProvidedAnswer(applicationId, QuestionairreEntryKeys.QE_USER_ID);
//			UserContact userContact = UserProfileModel.getUserContact(Integer.parseInt(userId));
//			int applicationCount = MetricsModel
//					.getInt(MetricKeys.USER_TO_APPLICATION_COUNT_$USER_ARCHIVE.replace("$USER", userId));
//
//			result.put(applicationId, new ApplicationPreview(applicationId, userContact, applicationCount));
//		}
//		return result;
//	}
//
//	@Todo("Use batch get")
//	public static List<QuestionnaireInputSection> getCustomerApplication(Integer applicationId) {
//
//		SectionsBuilder builder = SectionsBuilder.create();
//
//		// list sections
//		FormModel.listSections(SectionType.QUESTIONNAIRE, false).forEach(section -> {
//
//			// Add section
//			builder.withSection(section.getId(), section.getTitle());
//
//			// Get all question names for this section
//			FormModel.listQuestionNames(ConfigNamespaces.QUESTIONNAIRE + section.getId()).forEach((id, title) -> {
//
//				// Get the corresponding value in the application
//				builder.withInput(section.getId(), id, title, getProvidedAnswer(applicationId, id));
//			});
//		});
//
//		return builder.build();
//	}
//
//}
