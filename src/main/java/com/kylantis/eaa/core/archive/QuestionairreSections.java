package com.kylantis.eaa.core.archive;

public class QuestionairreSections {

	public static final String PERSONAL_SECTION = "PERSONAL_SECTION";
	public static final String NEXT_OF_KIN_SECTION = "NEXT_OF_KIN_SECTION";
	public static final String IDENTIFICATION_SECTION = "IDENTIFICATION_SECTION";
	public static final String HCP_SECTION = "HCP_SECTION";
	public static final String WORK_SECTION = "WORK_SECTION";
	public static final String PAYMENT_SECTION = "PAYMENT_SECTION";
	public static final String SIGNATURE_SECTION = "SIGNATURE_SECTION";

}
