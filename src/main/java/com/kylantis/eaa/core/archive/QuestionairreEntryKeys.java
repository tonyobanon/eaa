package com.kylantis.eaa.core.archive;

import com.kylantis.eaa.core.base.Todo;

@Todo("Refractor useless concatenation")
public class QuestionairreEntryKeys {

	public static final String QE_USER_ID = "" + "USER_ID";
	public static final String QE_GENDER = "" + "GENDER";

	public static final String QE_NEXT_OF_KIN_NAME = "" + "NEXT_OF_KIN_NAME";
	public static final String QE_NEXT_OF_KIN_ADDRESS = "" + "NEXT_OF_KIN_ADDRESS";
	public static final String QE_NEXT_OF_KIN_CITY = "" + "NEXT_OF_KIN_CITY";
	public static final String QE_NEXT_OF_KIN_STATE = "" + "NEXT_OF_KIN_STATE";
	public static final String QE_NEXT_OF_KIN_COUNTRY = "" + "NEXT_OF_KIN_COUNTRY";
	public static final String QE_NEXT_OF_KIN_PHONE = "" + "NEXT_OF_KIN_PHONE";

	public static final String QE_MEANS_OF_IDENTIFICATION = "" + "MEANS_OF_IDENTIFICATION";
	public static final String QE_UTILITY_BILL = "" + "UTILITY_BILL";
	public static final String QE_GUARANTOR_NAME = "" + "GUARANTOR_NAME";
	public static final String QE_GUARANTOR_CUSTOMER_ID = "" + "GUARANTOR_CUSTOMER_ID";
	public static final String QE_GUARANTOR_SIGNATURE = "" + "GUARANTOR_SIGNATURE";

	public static final String QE_CURRENT_HCP_NAME = "" + "CURRENT_HCP_NAME";
	public static final String QE_CURRENT_HCP_ADDRESS = "" + "CURRENT_HCP_ADDRESS";
	public static final String QE_CURRENT_HCP_CITY = "" + "CURRENT_HCP_CITY";
	public static final String QE_CURRENT_HCP_STATE = "" + "CURRENT_HCP_STATE";
	public static final String QE_CURRENT_HCP_COUNTRY = "" + "CURRENT_HCP_COUNTRY";
	public static final String QE_CURRENT_HCP_PHONE = "" + "CURRENT_HCP_PHONE";

	public static final String QE_CURRENT_EMPLOYER_NAME = "" + "CURRENT_EMPLOYER_NAME";
	public static final String QE_STAFF_ID = "" + "STAFF_ID";
	public static final String QE_MONTHLY_RENUMERATION = "" + "MONTHLY_RENUMERATION";
	public static final String QE_CURRENT_EMPLOYER_ADDRESS = "" + "CURRENT_EMPLOYER_ADDRESS";
	public static final String QE_CURRENT_EMPLOYER_CITY = "" + "CURRENT_EMPLOYER_CITY";
	public static final String QE_CURRENT_EMPLOYER_STATE = "" + "CURRENT_EMPLOYER_STATE";
	public static final String QE_CURRENT_EMPLOYER_COUNTRY = "" + "CURRENT_EMPLOYER_COUNTRY";

	public static final String QE_CARD_NUMBER = "" + "CARD_NUMBER";
	public static final String QE_CARD_EXP_MONTH = "" + "CARD_EXP_MONTH";
	public static final String QE_CARD_EXP_YEAR = "" + "CARD_EXP_YEAR";
	public static final String QE_CARD_CVC = "" + "CARD_CVC";

	public static final String QE_SIGNATURE = "" + "SIGNATURE";

	public static final String QE_DATE = "" + "DATE";

}
