package com.kylantis.eaa.core.archive;

import java.util.ArrayList;
import java.util.List;

public class QuestionnaireInput {

	private List<QuestionnaireInputSection> sections = new ArrayList<QuestionnaireInputSection>();

	public List<QuestionnaireInputSection> getSections() {
		return sections;
	}

	public void setSections(List<QuestionnaireInputSection> sections) {
		this.sections = sections;
	}

	public QuestionnaireInput withSection(QuestionnaireInputSection section) {
		this.sections.add(section);
		return this;
	}

}
