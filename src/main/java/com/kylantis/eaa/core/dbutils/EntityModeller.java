package com.kylantis.eaa.core.dbutils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.GlobalSecondaryIndex;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.LocalSecondaryIndex;
import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.ClassIdentityType;
import com.kylantis.eaa.core.base.BaseTable;
import com.kylantis.eaa.core.base.ClasspathScanner;
import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.base.Logger;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.indexing.Index;
import com.kylantis.eaa.core.indexing.IndexConfig;
import com.kylantis.eaa.core.indexing.tools.QueryModel;

public class EntityModeller {

	public static void createTables() {

		// Scan for entities
		Logger.info("Scanning for entities");

		for (Class<? extends BaseTable> entity : new ClasspathScanner<>("Table", BaseTable.class, ClassIdentityType.SUPER_CLASS).scanClasses()) {

			BaseTable instance = null;

			try {
				instance = entity.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				Exceptions.throwRuntime(e);
			}
			
			if(!instance.enabled()) {
				Logger.info("Skipping table '" + entity.getSimpleName() + "'");
				continue;
			}

			Logger.info("Creating table '" + entity.getSimpleName() + "'");

			CreateTableRequest CTR = new CreateTableRequest();
			CTR.setTableName(Application.getId() + EntityModeller.getTableNameDelimiter() + entity.getSimpleName());
			CTR.setProvisionedThroughput(instance.provisionedThroughput());

			Collection<KeySchemaElement> primaryKeys = new ArrayList<>();
			primaryKeys.add(new KeySchemaElement(instance.tableHashKey(), KeyType.HASH));
			if (instance.tableRangeKey() != null) {
				primaryKeys.add(new KeySchemaElement(instance.tableRangeKey(), KeyType.RANGE));
			}
			CTR.setKeySchema(primaryKeys);

			List<AttributeDefinition> attributeDefinitions = new ArrayList<>();

			attributeDefinitions.add(new AttributeDefinition(instance.tableHashKey(),
					DBTooling.getScalarType(entity, instance.tableHashKey())));
			if (instance.tableRangeKey() != null) {
				attributeDefinitions.add(new AttributeDefinition(instance.tableRangeKey(),
						DBTooling.getScalarType(entity, instance.tableRangeKey())));
			}

			List<LocalSecondaryIndex> localSecondaryIndexes = new ArrayList<LocalSecondaryIndex>(
					instance.LSIs().size());

			List<GlobalSecondaryIndex> globalSecondaryIndexes = new ArrayList<GlobalSecondaryIndex>(
					instance.indexedGSIs().size() + instance.unindexedGSIs().size());

			for (Entry<String, IndexConfig> LSI : instance.LSIs().entrySet()) {

				IndexConfig index = LSI.getValue();

				localSecondaryIndexes.add(new LocalSecondaryIndex().withIndexName(LSI.getKey())
						.withKeySchema(index.getKeySchemaElement()).withProjection(index.getProjection()));

				attributeDefinitions.add(new AttributeDefinition(index.getKeySchemaElement()[1].getAttributeName(),
						DBTooling.getScalarType(entity, index.getKeySchemaElement()[1].getAttributeName())));
			}

			for (Entry<String, IndexConfig> unindexedGSI : instance.unindexedGSIs().entrySet()) {

				IndexConfig index = unindexedGSI.getValue();

				globalSecondaryIndexes.add(new GlobalSecondaryIndex().withIndexName(unindexedGSI.getKey())
						.withKeySchema(index.getKeySchemaElement()).withProjection(index.getProjection())
						.withProvisionedThroughput(index.getProvisionedThroughput()));

				for (KeySchemaElement keySchema : index.getKeySchemaElement()) {
					attributeDefinitions.add(new AttributeDefinition(keySchema.getAttributeName(),
							DBTooling.getScalarType(entity, keySchema.getAttributeName())));
				}
			}

			for (Entry<String, IndexConfig> indexedGSI : instance.indexedGSIs().entrySet()) {
				IndexConfig index = indexedGSI.getValue();

				// Verify Index keys

				if (!DBTooling.isNumber(entity, index.getKeySchemaElement()[0].getAttributeName())) {
					throw new RuntimeException("Only Number-Typed hash attributes are allowed for this GSI");
				}

				if (!DBTooling.isScalarType(entity, index.getKeySchemaElement()[1].getAttributeName())) {
					throw new RuntimeException("Only Scalar-Typed range attributes are allowed for GSIs");
				}

				globalSecondaryIndexes.add(new GlobalSecondaryIndex().withIndexName(indexedGSI.getKey())
						.withKeySchema(index.getKeySchemaElement()).withProjection(index.getProjection())
						.withProvisionedThroughput(index.getProvisionedThroughput()));

				for (KeySchemaElement keySchema : index.getKeySchemaElement()) {
					attributeDefinitions.add(new AttributeDefinition(keySchema.getAttributeName(),
							DBTooling.getScalarType(entity, keySchema.getAttributeName())));
				}

				// Get table hash attribute

				String tableHashKey = null;
				String tableRangeKey = null;

				for (KeySchemaElement e : CTR.getKeySchema()) {
					if (e.getKeyType().equals("HASH")) {
						tableHashKey = e.getAttributeName();
					} else {
						tableRangeKey = e.getAttributeName();
					}
				}

				new QueryModel().newQueryOptimizedGSI(new Index(CTR.getTableName(), indexedGSI.getKey()),
						index.getProjection().getNonKeyAttributes(), index.getQueryType(),
						index.getKeySchemaElement()[0].getAttributeName(),
						index.getKeySchemaElement()[1].getAttributeName(),
						index.getProvisionedThroughput().getReadCapacityUnits(), tableHashKey, tableRangeKey);
			}

			if (globalSecondaryIndexes.size() > 0) {
				CTR.setGlobalSecondaryIndexes(globalSecondaryIndexes);
			}

			if (localSecondaryIndexes.size() > 0) {
				CTR.setLocalSecondaryIndexes(localSecondaryIndexes);
			}

			CTR.setAttributeDefinitions(attributeDefinitions);

			try {
				StorageServiceFactory.getDocumentDatabase().createTable(CTR).waitForActive();
			} catch (InterruptedException e) {
				// Continue
			}

		}

	}
	
	public static String getTableNameDelimiter() {
		return "_";
	}


}
