package com.kylantis.eaa.core.ml;

import java.util.List;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.components.Page;

@AbstractModel
public abstract class ICustomerGroupModel extends ModelAdapter<ICustomerGroupModel> {

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	@Override
	public final String path() {
		return MLRepository.CUSTOMER_GROUP_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "Customer Group Management";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
