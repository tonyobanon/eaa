package com.kylantis.eaa.core.ml;

import java.util.List;
import java.util.Map;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.config.Namespace;

@AbstractModel
public abstract class IConfigModel extends ModelAdapter<IConfigModel> {

	@PlatformInternal
	public abstract Object get(Id id);

	public abstract void putAll(Namespace namespace, Map<Id, String> entries);

	@PlatformInternal
	public abstract void put(Id id, String value);

	public abstract void delete(Id id);

	public abstract Map<Object, Object> getValues(Namespace namespace, List<Id> keys);

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}

	@Override
	public final String path() {
		return MLRepository.CONFIG_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "Configuration Management";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>()
				.with(new Page());
	}
}
