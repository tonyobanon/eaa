package com.kylantis.eaa.core.ml;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.BlockerTodo;
import com.kylantis.eaa.core.base.BaseModel;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.cron.Scheduler;
import com.kylantis.eaa.core.fusion.ServiceException;
import com.kylantis.eaa.core.indexing.tools.AttributeModel;
import com.kylantis.eaa.core.keys.AppConfigKey;

public abstract class ModelAdapter<T extends BaseModel> extends BaseModel {

	public static final String IDENTITY_METHOD = "identity";	
	
	private static Boolean enableCrossModelCallsForModels;
	private static Boolean enableCrossModelCallsForServices;

	private final String abstractModel;

	protected ModelAdapter() {
		abstractModel = this.getClass().getGenericSuperclass().getTypeName();
	}

	/**
	 * If an abstract model does override this method, then a StackOverflow
	 * exception will occur in ModelLocator, because of the complex generic
	 * references in this class
	 */
	@Override
	public String path() {
		return ModelLocator.abstractModelsPaths.get(abstractModel);
	}

	protected abstract IdentityType identity();

	protected abstract String title();

	boolean isInternal() {
		return false;
	}

	@BlockerTodo("Provide some sort of optimization for this operation")
	public final BaseModel getModel(String path) {

		BaseModel model = ModelLocator.get(path);

		if (model == null) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}

		// Add invocation context in another thread
		Scheduler.now(new Runnable() {
			@Override
			public void run() {
				ModelBootstrap.addContext(this.getClass(), model.getClass());
			}
		});

		if (!isCrossCallAllowed()) {
			if (!model.path().equals(this.path())) {
				throw new ServiceException(ResourceException.FAILED_VALIDATION,
						"Model Invocation not allowed from " + this.path() + " to " + model.path());
			}
		}

		return model;
	}

	private final Boolean isCrossCallAllowed() {
		return identity().equals(IdentityType.SERVICE) ? enableCrossModelCallsForServices
				: enableCrossModelCallsForModels;
	}

	static {
		ModelAdapter.enableCrossModelCallsForModels = Application
				.getConfigAsBool(AppConfigKey.ENABLE_CROSS_MODEL_CALLS_FOR_MODELS);
		ModelAdapter.enableCrossModelCallsForServices = Application
				.getConfigAsBool(AppConfigKey.ENABLE_CROSS_MODEL_CALLS_FOR_SERVICES);
	}

	///////////////////////////////////////// HELPER METHODS
	///////////////////////////////////////// ///////////////////////////////////////////////////

	public final IAttributeModel getAttributeModel() {

		// This is used in QueryModel before model initialization take
		// place

		if (Application.isStartupContext()) {
			return new AttributeModel();
		}
		return (IAttributeModel) this.getModel(MLRepository.ATTRIBUTE_MODEL);
	}

	public final IBlobStoreModel getBlobStoreModel() {
		return (IBlobStoreModel) this.getModel(MLRepository.BLOB_STORE_MODEL);
	}

	public final IComponentModel getComponentModel() {
		return (IComponentModel) this.getModel(MLRepository.COMPONENT_MODEL);
	}

	public final IConfigModel getConfigModel() {
		return (IConfigModel) this.getModel(MLRepository.CONFIG_MODEL);
	}

	public final ICustomerGroupModel getCustomerGroupModel() {
		return (ICustomerGroupModel) this.getModel(MLRepository.CUSTOMER_GROUP_MODEL);
	}

	public final ICustomerModel getCustomerModel() {
		return (ICustomerModel) this.getModel(MLRepository.CUSTOMER_MODEL);
	}

	public final IExpressionsModel getExpressionsModel() {
		return (IExpressionsModel) this.getModel(MLRepository.EXPRESSIONS_MODEL);
	}

	public final IFormModel getFormModel() {
		return (IFormModel) this.getModel(MLRepository.FORM_MODEL);
	}

	public final ILocationModel getLocationModel() {
		return (ILocationModel) this.getModel(MLRepository.LOCATION_MODEL);
	}

	public final IMetricsModel getMetricsModel() {
		return (IMetricsModel) this.getModel(MLRepository.METRICS_MODEL);
	}

	public final IMicroserviceModel getMicroserviceModel() {
		return (IMicroserviceModel) this.getModel(MLRepository.MICROSERVICE_MODEL);
	}

	public final IPaymentModel getPaymentModel() {
		return (IPaymentModel) this.getModel(MLRepository.PAYMENT_MODEL);
	}

	public final IPlatformModel getPlatformModel() {
		return (IPlatformModel) this.getModel(MLRepository.PLATFORM_MODEL);
	}

	public final IQueryModel getQueryModel() {
		return (IQueryModel) this.getModel(MLRepository.QUERY_MODEL);
	}

	public final IResourceBundleModel getResourceBundleModel() {
		return (IResourceBundleModel) this.getModel(MLRepository.RESOURCE_BUNDLE_MODEL);
	}

	public final IRolesModel getRolesModel() {
		return (IRolesModel) this.getModel(MLRepository.ROLES_MODEL);
	}

	public final ISearchGraphModel getSearchGraphModel() {
		return (ISearchGraphModel) this.getModel(MLRepository.SEARCH_GRAPH_MODEL);
	}

	public final ISystemModel getSystemModel() {
		return (ISystemModel) this.getModel(MLRepository.SYSTEM_MODEL);
	}

	public final IUserProfileModel getUserProfileModel() {
		return (IUserProfileModel) this.getModel(MLRepository.USER_PROFILE_MODEL);
	}

	///////////////////////////////////////// HELPER METHODS
	///////////////////////////////////////// ///////////////////////////////////////////////////

}
