package com.kylantis.eaa.core.ml;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.ClassIdentityType;
import com.kylantis.eaa.core.PlatformUtils;
import com.kylantis.eaa.core.base.BaseModel;
import com.kylantis.eaa.core.base.ClasspathScanner;
import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.base.Logger;
import com.kylantis.eaa.core.keys.AppConfigKey;

/**
 * This class is responsible for the following: <br>
 * 
 * * It discovers, and registers models, and all their dependencies <br>
 * * It exposes the Service Locator design pattern for all registered models
 * <br>
 * 
 */
public class ModelLocator {

	/**
	 * This stores the abstract model, registered from a given path
	 */
	private static final Map<String, Class<?>> abstractModels = new HashMap<>();

	/** This somewhat inverts <abstractModels> above */
	protected static final Map<String, String> abstractModelsPaths = new HashMap<>();

	/**
	 * This stores all concrete models for a given path
	 */
	private static final Map<String, Collection<Class<? extends BaseModel>>> concreteModels = new HashMap<>();

	/**
	 * This stores the current concrete model for a given path
	 */
	private static final Map<String, Class<? extends BaseModel>> models = new HashMap<>();

	/**
	 * This stores the current concrete model for a given path, based on their order
	 * in the model dependency tree
	 */
	private static final LinkedHashMap<String, BaseModel> linkedModels = new LinkedHashMap<>();

	private final String appId;
	private final Path baseDir;

	public ModelLocator() {
		this.appId = null;
		this.baseDir = null;
	}

	public ModelLocator(String appId, Path baseDir) {
		this.appId = appId;
		this.baseDir = baseDir;
	}

	public static final BaseModel get(String path) {

		// Note:
		// In TableAdapter, we directly instantiate AttributeModel
		// In EntityModeller, we directly instantiate QueryModel

		return linkedModels.get(path);
	}

	public final void scan() {

		ModelBootstrap.start();

		String ext = Application.getConfig(AppConfigKey.CLASSES_MODELS_EXT);

		// Scan for models

		// Sequence 1: Register abstract models, for each given path
		Logger.debug("Scanning for abstract models");
		new ClasspathScanner<>(ext, BaseModel.class, ClassIdentityType.SUPER_CLASS).setBaseDir(this.baseDir)
				.scanClasses().forEach(model -> {
					addAbstractModel(model);
				});

		// Sequence 2: Add concrete models for each path
		Logger.debug("Scanning for concrete models");
		new ClasspathScanner<>(ext, BaseModel.class, ClassIdentityType.SUPER_CLASS).setBaseDir(this.baseDir)
				.scanClasses().forEach(model -> {
					addConcreteModels(model);
				});

		// Sequence 3: get concrete models for each path
		Logger.debug("Adding concrete models for each path");
		ModelBootstrap.getDefaultConcreteModels(concreteModels.keySet()).forEach((path, model) -> {
			models.put((String) path, ModelBootstrap.getModelClass((String) model));
		});

		// Sequence 4: based, on the model dependency hierarchy, add models to the
		// linkedModels
		Logger.debug("Adding concrete models for each path, based on their dependency hierarchy");
		models.forEach((k, v) -> {
			addConcreteModel(null, v);
		});

		Logger.info("\n");
		linkedModels.forEach((k, v) -> {
			Logger.debug(k + ": " + v.getClass().getName());
		});
	}

	public static final Collection<BaseModel> getModels() {
		return linkedModels.values();
	}

	private static final Class<?> getSuperAbstractModel(Class<?> clazz) {

		Class<?> parent = clazz.getSuperclass();

		if (!parent.equals(Object.class)) {

			if (parent.equals(ModelAdapter.class) || parent.equals(BaseModel.class)) {
				return null;
			}

			if (isAbstractModel(parent)) {
				// found
				return parent;
			} else {
				// still looking
				return getSuperAbstractModel(parent);
			}

		} else {
			return null;
		}
	}

	private final void addAbstractModel(Class<? extends BaseModel> model) {

		// Verify that this is a valid concrete model
		if (!isAbstractModel(model)) {
			return;
		}

		Logger.trace("Abstract model '" + model.getName() + "' found");

		BaseModel instance = null;
		try {

			// Scan for a suitable concrete model, for this abstract model to get inherited
			// properties

			Logger.trace("Looking for a suitable concrete model");
			String ext = Application.getConfig(AppConfigKey.CLASSES_MODELS_EXT);
			List<?> result = new ClasspathScanner<>(ext, model, ClassIdentityType.SUPER_CLASS).setBaseDir(this.baseDir)
					.scanClasses();

			if (result.isEmpty()) {
				Logger.trace("No concrete model was found. Skipping..");
				return;
			} else {
				instance = ((Class<? extends BaseModel>) result.get(0)).newInstance();
				Logger.trace("Suitable concrete model: '" + ((Class<?>) result.get(0)).getName() + "' was found");
			}

		} catch (InstantiationException | IllegalAccessException e) {
			Exceptions.throwRuntime(e);
		}

		String path = instance.path();
		Boolean isInternal = ((ModelAdapter<?>) instance).isInternal();

		if (!abstractModels.containsKey(path) || isInternal) {

			Logger.debug("Adding abstract model | '" + path + "' -> " + model.getName());

			abstractModels.put(path, model);
			abstractModelsPaths.put(model.getName(), path);

			// Register Abstract Model

			if (!PlatformUtils.isInstalled()) {
				ModelBootstrap.registerAbstractModel(path, Application.getId(), model,
						((ModelAdapter<?>) instance).title());
			} else if (this.appId != null) {
				ModelBootstrap.registerAbstractModel(path, this.appId, model, ((ModelAdapter<?>) instance).title());
			}
		}
	}

	private final void addConcreteModels(Class<? extends BaseModel> model) {

		// Verify that this is a valid concrete model
		if (!isConcreteModel(model)) {
			return;
		}

		// Verify that this concrete model's abstract model, was registered for its
		// path, in <typeMappings>

		BaseModel instance = null;
		try {
			instance = model.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			Exceptions.throwRuntime(e);
		}

		Class<?> abstractModelClass = getSuperAbstractModel(model);

		assert (abstractModelClass.isAssignableFrom(model));

		if (abstractModels.get(instance.path()) != null
				&& abstractModels.get(instance.path()).equals(abstractModelClass)) {

			if (!concreteModels.containsKey(instance.path())) {
				concreteModels.put(instance.path(), new ArrayList<>());
			}

			Logger.debug("Adding concrete model | '" + instance.path() + "' -> " + model.getName());

			concreteModels.get(instance.path()).add(model);

			// Register Concrete Model

			if (!PlatformUtils.isInstalled()) {

				ModelBootstrap.registerConcreteModel(instance.path(), Application.getId(), model);

				// set as default for path
				ModelBootstrap.registerDefaultConcreteModel(instance.path(), model);

			} else if (this.appId != null) {
				ModelBootstrap.registerConcreteModel(instance.path(), this.appId, model);

				// set as default for path, if necessary
				String abstractModel = ModelBootstrap.getModelName(abstractModelClass);
				if (ModelBootstrap.getApp(abstractModel).equals(this.appId)) {
					ModelBootstrap.registerDefaultConcreteModel(instance.path(), model);
				}
			}
		} else {
			Logger.error("No abstract model ('" + abstractModelClass + "') was not found on path: " + instance.path());
		}
	}

	private static final void addConcreteModel(String dependants, Class<? extends BaseModel> model) {

		ConcreteModel modelAnnotation = model.getAnnotation(ConcreteModel.class);

		if (modelAnnotation != null) {

			if (!modelAnnotation.enabled()) {
				return;
			}

			for (String dep : modelAnnotation.dependencies()) {

				Class<? extends BaseModel> dependency = models.get(dep);

				if (dependants != null) {
					if (dependants.contains(dependency.getSimpleName())) {
						// Circular reference was detected
						Exceptions.throwRuntime(new RuntimeException("Circular reference was detected: "
								+ (dependants + " -> " + model.getSimpleName() + " -> " + dependency.getSimpleName())
										.replaceAll(dependency.getSimpleName(),
												"(" + dependency.getSimpleName() + ")")));
					}
					addConcreteModel(dependants + " -> " + model.getSimpleName(), dependency);

				} else {

					if (dependency == null) {

						// No concrete model exists, for the dependency path
						Logger.error("No concrete model exists, for the dependency path: " + dep);
					}

					addConcreteModel(model.getSimpleName(), dependency);
				}
			}
		}

		try {

			BaseModel instance = model.newInstance();

			if (!linkedModels.containsKey(instance.path())) {
				linkedModels.put(instance.path(), instance);
			}

		} catch (InstantiationException | IllegalAccessException e) {
			Exceptions.throwRuntime(e);
		}
	}

	private static final boolean isConcreteModel(Class<? extends BaseModel> clazz) {

		// Should be annotated with @ConcreteModel
		if (clazz.getAnnotation(ConcreteModel.class) == null) {
			return false;
		}

		// Should be concrete
		if (Modifier.isAbstract(clazz.getModifiers())) {
			return false;
		}

		// Should extend an abstract model
		if (getSuperAbstractModel(clazz) == null) {
			return false;
		}

		// should have an identity type of Model
		try {

			ModelAdapter<?> model = (ModelAdapter<?>) clazz.newInstance();

			if (!model.identity().equals(IdentityType.MODEL)) {
				return false;
			}

		} catch (InstantiationException | IllegalAccessException e) {
			Exceptions.throwRuntime(e);
		}

		return true;
	}

	private static final boolean isAbstractModel(Class<?> clazz) {

		// Should be annotated with @AbstractModel
		if (clazz.getAnnotation(AbstractModel.class) == null) {
			return false;
		}

		// should be abstract
		if (!Modifier.isAbstract(clazz.getModifiers())) {
			return false;
		}

		// should extend ModelAdapter
		if (!ModelAdapter.class.isAssignableFrom(clazz)) {
			return false;
		}

		// should provide non-abstract final implementations of path(), identity(),
		// pages()
		try {

			Method[] methods = { clazz.getDeclaredMethod(ModelAdapter.PATH_METHOD),
					clazz.getDeclaredMethod(ModelAdapter.PAGES_METHOD),
					clazz.getDeclaredMethod(ModelAdapter.IDENTITY_METHOD) };

			if (!isConcreteFinalMethod(methods)) {
				return false;
			}

		} catch (NoSuchMethodException | SecurityException e) {
			return false;
		}

		return true;
	}

	private static boolean isConcreteFinalMethod(Method... methods) {
		for (Method m : methods) {
			int modifiers = m.getModifiers();
			if (m == null || Modifier.isAbstract(modifiers) || !Modifier.isFinal(modifiers)) {
				return false;
			}
		}
		return true;
	}

}
