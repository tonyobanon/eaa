package com.kylantis.eaa.core.ml;

import java.util.List;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.components.Page;

@AbstractModel
public abstract class IExpressionsModel extends ModelAdapter<IExpressionsModel> {

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}

	@Override
	public final String path() {
		return MLRepository.EXPRESSIONS_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "Expressions Query Management";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
