package com.kylantis.eaa.core.ml;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.content.Blob;
import com.kylantis.eaa.core.content.StaticBlock;

@AbstractModel
public abstract class IBlobStoreModel extends ModelAdapter<IBlobStoreModel> {
	
	public abstract String save(String id, Serializable obj) throws IOException;

	public abstract String save(String id, InputStream in) throws IOException;
	
	public abstract String save(String id, byte[] data) throws IOException;

	public abstract void delete(String id);

	public abstract Blob get(String id);
	
	public abstract void newStaticBlock(StaticBlock block);

	public abstract void deleteStaticBlock(String identifier);

	@PlatformInternal
	public abstract Map<String, StaticBlock> listStaticBlocks();

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	
	@Override
	public final String path() {
		return MLRepository.BLOB_STORE_MODEL;
	}
	
	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "Binary Storage";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
