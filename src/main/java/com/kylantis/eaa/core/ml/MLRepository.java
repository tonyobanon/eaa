package com.kylantis.eaa.core.ml;

public class MLRepository {

	public static final String COMPONENT_MODEL = "core_component";
	
	public static final String EXPRESSIONS_MODEL = "core_expression";

	public static final String USER_PROFILE_MODEL = "core_userprofile";

	public static final String CUSTOMER_GROUP_MODEL = "core_customergroup";

	public static final String ADDRESS_MODEL = "core_address";

	public static final String ROLES_MODEL = "core_role";

	public static final String CUSTOMER_MODEL = "core_customer";

	public static final String SYSTEM_MODEL = "core_system";

	public static final String RESOURCE_BUNDLE_MODEL = "core_resourcebundle";

	public static final String PLATFORM_MODEL = "core_platform";

	public static final String MICROSERVICE_MODEL = "core_microservice";

	public static final String METRICS_MODEL = "core_metrics";

	public static final String EMAILING_MODEL = "core_emailing";

	public static final String LOCATION_MODEL = "core_location";

	public static final String ATTRIBUTE_MODEL = "core_attribute";

	public static final String QUERY_MODEL = "core_query";

	public static final String SEARCH_GRAPH_MODEL = "core_searchgraph";

	public static final String FORM_MODEL = "core_forms";

	public static final String BLOB_STORE_MODEL = "core_blobstore";

	public static final String CONFIG_MODEL = "core_config";

	public static final String PAYMENT_MODEL = "core_payment";
}
