package com.kylantis.eaa.core.ml;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.users.AddressSpec;
import com.kylantis.eaa.core.users.UserContact;
import com.kylantis.eaa.core.users.UserProfile;
import com.kylantis.eaa.core.users.UserProfileSpec;

@AbstractModel
public abstract class IUserProfileModel extends ModelAdapter<IUserProfileModel> {

	@PlatformInternal
	public abstract Iterator<Integer> getUsers();

	@PlatformInternal
	public abstract void deleteUser(Integer userId);

	public abstract Integer loginByEmail(String email, String password);

	public abstract Integer loginByPhone(Long phone, String password);

	public abstract Integer registerUser(UserProfileSpec spec);

	public abstract Integer registerUser(UserProfileSpec spec, String role);

	public abstract UserProfile getProfile(Integer userId);

	public abstract UserContact getUserContact(Integer userId);

	public abstract List<String> getRoles(Integer userId);

	public abstract Map<String, AddressSpec> getAddresses(Integer userId);

	public abstract String getDefaultRole(Integer userId);

	public abstract String getDefaultAddress(Integer userId);

	public abstract String addAddress(Integer userId, AddressSpec address);

	public abstract void removeAddress(Integer userId, String addressId);

	public abstract Integer getNameChangeCount(Integer userId);

	public abstract void updateName(Integer userId, String lname);

	public abstract boolean isNameChangeAllowed(Integer userId);

	public abstract void updateDefaultRole(Integer userId, String role);

	public abstract void updateDefaultAddress(Integer userId, String addressId);

	public abstract void updateEmail(Integer userId, String email);

	public abstract void updatePhone(Integer userId, long phone);

	public abstract void updateImage(Integer userId, String blobId);

	@Override
	protected final IdentityType identity() {
		return IdentityType.MODEL;
	}

	@Override
	public final String path() {
		return MLRepository.USER_PROFILE_MODEL;
	}

	@Override
	protected String title() {
		return "User Profile Management";
	}

	@Override
	final boolean isInternal() {
		return true;
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
