package com.kylantis.eaa.core.ml;

import java.util.List;
import java.util.Map;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.components.Page;

@AbstractModel
public abstract class IResourceBundleModel extends ModelAdapter<IResourceBundleModel> {

	public abstract void put(String locale, String key, String value);
	
	public abstract String get(String locale, String key);

	public abstract Map<String, String> getAll(String locale);

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	@Override
	public final String path() {
		return MLRepository.RESOURCE_BUNDLE_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "Resource Bundle Management";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
