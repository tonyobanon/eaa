package com.kylantis.eaa.core.ml;

import java.util.List;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.components.Page;

@AbstractModel
public abstract class IMetricsModel extends ModelAdapter<IMetricsModel> {

	public abstract Integer getInt(String key);

	public abstract void putInt(String key, Integer value);

	public abstract void incrementInt(String key);

	public abstract void decrementInt(String key);

	public abstract String get(String key);

	public abstract void put(String key, String value);

	public abstract void delete(String... keys);

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	@Override
	public final String path() {
		return MLRepository.METRICS_MODEL;
	}

	@Override
	protected String title() {
		return "Metrics Management";
	}

	@Override
	final boolean isInternal() {
		return true;
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
