package com.kylantis.eaa.core.ml;

import java.util.List;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.components.Page;

@AbstractModel
public abstract class IEmailingModel extends ModelAdapter<IEmailingModel> {

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	@Override
	public final String path() {
		return MLRepository.EMAILING_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "Emailing";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
