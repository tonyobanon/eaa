package com.kylantis.eaa.core.ml;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.Unexposed;
import com.kylantis.eaa.core.base.CacheResult;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.locations.Coordinates;
import com.kylantis.eaa.core.users.AddressSpec;

@AbstractModel
public abstract class ILocationModel extends ModelAdapter<ILocationModel> {

	public abstract String getCurrencyCode(String countryCode);

	public abstract String getCurrencyName(String countryCode);

	public abstract String getTerritoryName(String territoryCode);

	public abstract String getCountryName(String countryCode);

	public abstract String getCityName(String cityId);

	public abstract Map<String, AddressSpec> getAddresses(Collection<String> addressIds);

	public abstract Integer getAddressOwner(String id);

	public abstract AddressSpec getAddress(String id);

	@PlatformInternal
	public abstract void deleteAddress(String id);

	@PlatformInternal
	public abstract String newAddresss(String ownerId, AddressSpec spec);

	@CacheResult
	public abstract Map<String, String> getCountryNames();

	public abstract Map<String, String> getCurrencyNames();

	public abstract Map<String, String> getAvailableLocales(String countryCode);

	public abstract Map<String, String> getAllLocales();

	public abstract Map<String, String> getAllTimezones();

	@Unexposed
	public abstract Map<String, String> getLanguageNames(List<String> languageCodes);

	@CacheResult
	public abstract Map<String, String> getTerritoryNames(String countryCode);

	@CacheResult
	public abstract Map<Integer, String> getCityNames(String territoryCode);

	@CacheResult
	public abstract Coordinates getCoordinates(int cityCode);

	@CacheResult
	public abstract Map<String, String> getSpokenLanguages(String countryCode);

	@CacheResult
	public abstract String getTimezone(int cityCode);

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	@Override
	public final String path() {
		return MLRepository.LOCATION_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "Locations Data Management";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
