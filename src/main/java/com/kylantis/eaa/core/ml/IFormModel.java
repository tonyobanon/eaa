package com.kylantis.eaa.core.ml;

import java.util.List;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.forms.CompositeEntry;
import com.kylantis.eaa.core.forms.Question;
import com.kylantis.eaa.core.forms.SimpleEntry;

@AbstractModel
public abstract class IFormModel extends ModelAdapter<IFormModel> {

	@PlatformInternal
	public abstract List<Id> getQuestionKeys(Id form);

	@PlatformInternal
	public abstract List<Question> getQuestions(Id form);

	@PlatformInternal
	public abstract void newQuestion(Id form, Question spec);

	@PlatformInternal
	public abstract void newQuestion(Id form, boolean isDefault, Question spec);

	@PlatformInternal
	public abstract void newCompositeEntry(Id form, boolean isDefault, CompositeEntry spec);

	@PlatformInternal
	public abstract void newSimpleEntry(Id form, boolean isDefault, SimpleEntry spec);

	public abstract void deleteSimpleEntry(Id id);

	public abstract void deleteCompositeEntry(Id id);

	public abstract void deleteQuestion(Id id);
	
	@PlatformInternal
	public abstract void deleteQuestions(Id form, boolean forceDelete);

	@PlatformInternal
	public abstract void deleteQuestion(Id id, boolean forceDelete);

	public abstract List<Object> listSimpleEntries(Id form);

	public abstract List<Object> listCompositeEntries(Id form);

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	
	@Override
	public final String path() {
		return MLRepository.FORM_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}
	
	@Override
	protected String title() {
		return "Forms Storage";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
