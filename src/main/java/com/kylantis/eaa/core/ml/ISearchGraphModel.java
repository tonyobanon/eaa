package com.kylantis.eaa.core.ml;

import java.util.List;
import java.util.function.Consumer;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.indexing.SearchGraphId;

@AbstractModel
public abstract class ISearchGraphModel extends ModelAdapter<ISearchGraphModel> {

	public abstract boolean graphSearch(String checkpointId, Consumer<Item> consumer);

	public abstract void putEntry(SearchGraphId id, int entityType, String matrix[]);

	public abstract void removeEntry(SearchGraphId id);

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	@Override
	public final String path() {
		return MLRepository.SEARCH_GRAPH_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}
	
	@Override
	protected String title() {
		return "Search";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
