package com.kylantis.eaa.core.ml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kylantis.eaa.core.ComputeIntensive;
import com.kylantis.eaa.core.Dates;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.AbstractModelsSpec;
import com.kylantis.eaa.core.attributes.ConcreteModelsSpec;
import com.kylantis.eaa.core.attributes.DefaultConcreteModelSpec;
import com.kylantis.eaa.core.base.BaseModel;
import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.base.Logger;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.dbutils.DBTooling;

public class ModelBootstrap {

	private static final String delim = "_";

	private static final Map<String, String> modelClasses = Maps.newHashMap();
	private static final List<String> softDependencies = Lists.newArrayList();

	@ComputeIntensive
	public static void start() {

		// soft dependency: "makes use of"
		// hard dependency: "depends on"

		Logger.debug("Fetching model classes ..");

		// Add abstract models

		StorageServiceFactory.getDocumentDatabase().getTable(AbstractModelsSpec.TABLE_NAME)
				.scan(new ScanSpec()
						.withProjectionExpression(AbstractModelsSpec.MODEL + "," + AbstractModelsSpec.APP_ID))
				.iterator().forEachRemaining(i -> {
					modelClasses.put(i.getString(AbstractModelsSpec.MODEL), i.getString(AbstractModelsSpec.APP_ID));
				});

		// Add concrete models

		StorageServiceFactory.getDocumentDatabase().getTable(ConcreteModelsSpec.TABLE_NAME).scan(new ScanSpec())
				.iterator().forEachRemaining(i -> {
					i.getMap(ConcreteModelsSpec.MODELS).forEach((k, v) -> {
						modelClasses.put(k, (String) v);
					});
				});

		Logger.debug("Successfully fetched model classes with total count: " + modelClasses.size());
	}

	public static void fetchDefaultApps() {

	}

	protected static void addContext(Class<?> source, Class<? extends BaseModel> destination) {

		// Add soft dependency

		String sourceApp = modelClasses.get(getModelName(source));
		String destinationApp = modelClasses.get(getModelName(destination));

		String k = sourceApp + delim + destinationApp;

		if (!softDependencies.contains(k)) {
			softDependencies.add(k);
		}
	}

	protected static List<String> getDependencies(String appId) {

		List<String> result = new ArrayList<>();

		getHardDependencies(appId).forEach((path, models) -> {
			models.values().forEach(app_id -> {
				result.add(app_id);
			});
		});

		softDependencies.forEach(e -> {
			if (e.endsWith(delim + appId)) {
				result.add(e.split(Pattern.quote(delim))[0]);
			}
		});

		return result;
	}

	protected static Map<String, Map<String, String>> getHardDependencies(String appId) {

		Map<String, Map<String, String>> result = Maps.newHashMap();

		// get model paths owned by this app
		List<String> modelPaths = getModelPaths(appId);

		// convert as proper primary keys
		List<PrimaryKey> keys = new ArrayList<>();
		modelPaths.forEach(path -> {
			keys.add(new PrimaryKey(ConcreteModelsSpec.PATH, path));
		});

		// use batch get operation, to fetch concrete model of the paths
		DBTooling.batchGetSingleAttr(ConcreteModelsSpec.TABLE_NAME, ConcreteModelsSpec.PATH, null, keys,
				ConcreteModelsSpec.MODELS).forEach((k, v) -> {
					result.put((String) k, (Map<String, String>) v);
				});

		return result;
	}

	private static List<String> getModelPaths(String appId) {

		List<String> paths = new ArrayList<>();

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(AbstractModelsSpec.TABLE_NAME);

		QuerySpec spec = new QuerySpec().withKeyConditionExpression("#k = :v")
				.withNameMap(new NameMap().with("#k", AbstractModelsSpec.APP_ID))
				.withValueMap(new ValueMap().withString(":v", appId));

		table.getIndex(AbstractModelsSpec.APP_ID_INDEX).query(spec).iterator().forEachRemaining(item -> {
			paths.add(item.getString(AbstractModelsSpec.PATH));
		});

		return paths;
	}

	protected static void registerAbstractModel(String path, String appId, Class<?> model, String title) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(AbstractModelsSpec.TABLE_NAME);

		Item item = new Item().withPrimaryKey(AbstractModelsSpec.PATH, path)
				.withString(AbstractModelsSpec.APP_ID, appId)
				.withString(AbstractModelsSpec.MODEL, getModelName(model))
				.withString(AbstractModelsSpec.TITLE, title)
				.withString(AbstractModelsSpec.DATE, Dates.currentDate());

		try {
			table.putItem(new PutItemSpec().withItem(item)
					.withNameMap(new FluentHashMap<String, String>().with("#p", AbstractModelsSpec.PATH))
					.withConditionExpression("attribute_not_exists(#p)"));

			modelClasses.put(getModelName(model), appId);

		} catch (ConditionalCheckFailedException ex) {

			String existingModel = table.getItem(new GetItemSpec().withPrimaryKey(AbstractModelsSpec.PATH, path)
					.withProjectionExpression(AbstractModelsSpec.MODEL)).getString(AbstractModelsSpec.MODEL);

			Logger.error("Could not register abstract model ('" + getModelName(model) + "')" + " because ('"
					+ existingModel + "') is already registered for '" + path + "'");
		}
	}

	protected static void registerConcreteModel(String path, String appId, Class<?> model) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(ConcreteModelsSpec.TABLE_NAME);

		//Create new map, if necessary for this path
		UpdateItemSpec spec1 = new UpdateItemSpec().withPrimaryKey(ConcreteModelsSpec.PATH, path)
				.withReturnValues(ReturnValue.NONE)
				.withNameMap(new FluentHashMap<String, String>().with("#m1",
						ConcreteModelsSpec.MODELS))
				.withValueMap(new FluentHashMap<String, Object>().with(":empty", new HashMap<>()))
				.withUpdateExpression("SET #m1 = if_not_exists(#m1, :empty)");

		table.updateItem(spec1);

		//Update map, by inserting the concrete model into it
		UpdateItemSpec spec2 = new UpdateItemSpec().withPrimaryKey(ConcreteModelsSpec.PATH, path)
				.withReturnValues(ReturnValue.NONE)
				.withNameMap(new FluentHashMap<String, String>().with("#m2", getModelName(model)).with("#m1",
						ConcreteModelsSpec.MODELS))
				.withValueMap(new FluentHashMap<String, Object>().with(":appId", appId))
				.withConditionExpression("attribute_not_exists(#m1.#m2)")
				.withUpdateExpression("SET #m1.#m2 = :appId");

		table.updateItem(spec2);
		modelClasses.put(getModelName(model), appId);
	}

	protected static void registerDefaultConcreteModel(String path, Class<?> model) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(DefaultConcreteModelSpec.TABLE_NAME);

		Item item = new Item().withPrimaryKey(DefaultConcreteModelSpec.PATH, path)
				.withString(DefaultConcreteModelSpec.MODEL, getModelName(model));

		table.putItem(new PutItemSpec().withItem(item));
	}

	protected static String getDefaultConcreteModel(String path) {
		return StorageServiceFactory.getDocumentDatabase().getTable(DefaultConcreteModelSpec.TABLE_NAME)
				.getItem(DefaultConcreteModelSpec.PATH, path).getString(DefaultConcreteModelSpec.MODEL);
	}

	protected static Map<Object, Object> getDefaultConcreteModels(Collection<String> paths) {

		List<PrimaryKey> keys = new ArrayList<>();
		paths.forEach(path -> {
			keys.add(new PrimaryKey(DefaultConcreteModelSpec.PATH, path));
		});
	
		return DBTooling.batchGetSingleAttr(DefaultConcreteModelSpec.TABLE_NAME, DefaultConcreteModelSpec.PATH, null,
				keys, DefaultConcreteModelSpec.MODEL);
	}

	protected static String getApp(String model) {
		return modelClasses.get(model);
	}

	protected static String getModelName(Class<?> model) {
		return model.getName();
	}

	protected static Class<? extends BaseModel> getModelClass(String name) {
		try {
			return (Class<? extends BaseModel>) Class.forName(name);
		} catch (ClassNotFoundException e) {
			Exceptions.throwRuntime(e);
			return null;
		}
	}

}
