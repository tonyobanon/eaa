package com.kylantis.eaa.core.ml;

import java.util.List;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.setup.ApplicationSetupSpec;
import com.kylantis.eaa.core.setup.InstallOptions;

@AbstractModel
public abstract class IPlatformModel extends ModelAdapter<IPlatformModel> {

	public abstract void doInstall(InstallOptions spec);
	
	public abstract void installApp(ApplicationSetupSpec spec);

	public abstract void uninstallApp(String appId);

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}

	@Override
	public final String path() {
		return MLRepository.PLATFORM_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}
	
	@Override
	protected String title() {
		return "Platform Management";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
