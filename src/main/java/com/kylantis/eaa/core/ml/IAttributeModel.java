package com.kylantis.eaa.core.ml;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.indexing.Index;
import com.kylantis.eaa.core.indexing.QueryType;

@AbstractModel
public abstract class IAttributeModel extends ModelAdapter<IAttributeModel> {
	
	public abstract void newRangeKey(Index index, List<String> projections, String hashAttribute, String rangeAttribute,
			Long readThroughputCapacity, QueryType queryType, String tableHashKey, String tableRangeKey);

	public abstract Integer getIndexSize(Index index, String indexRangeKey, Object indexRangeKeyValue, Item item);
	
	public abstract void putValue(String tableName, Item item);

	public abstract void putValue(String tableName, Map<String, AttributeValue> attributes);

	public abstract void updateValue(String tableName, Map<String, AttributeValue> updates,
			Collection<KeyAttribute> keys);

	public abstract void updateValue(String tableName, Map<String, AttributeValue> updates, KeyAttribute... keys);

	public abstract void updateValue(String tableName, Map<String, AttributeValue> updates, String hashKey,
			Object hashValue);

	public abstract void updateValue(String tableName, Map<String, AttributeValue> updates, String hashKey,
			Object hashValue, String rangeKey, Object rangeValue);

	public abstract void deleteValue(String tableName, KeyAttribute... keys);

	public abstract void deleteValue(String tableName, Collection<KeyAttribute> keys);
	
	public abstract void deleteValue(String tableName, Object tableHashValue);

	public abstract void deleteValue(String tableName, Object tableHashValue, String indexName);

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	
	@Override
	public final String path() {
		return MLRepository.ATTRIBUTE_MODEL;
	}
	
	@Override
	final boolean isInternal() {
		return true;
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}

	@Override
	protected String title() {
		return "DB Query Attribute Management";
	}
}
