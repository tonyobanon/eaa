package com.kylantis.eaa.core.ml;

import java.util.List;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.components.Page;

@AbstractModel
public abstract class ISystemModel extends ModelAdapter<ISystemModel> {
	
	public abstract String getDefaultLocale();
	
	public abstract String getDefaultTimezone();

	public abstract String getDefaultCurrency();

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	@Override
	public final String path() {
		return MLRepository.SYSTEM_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "System Configuration Management";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
