package com.kylantis.eaa.core.ml;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ConcreteModel {
	
	boolean enabled() default true;

	String[] dependencies() default {};
	
}
