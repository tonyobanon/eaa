package com.kylantis.eaa.core.ml;

import java.util.List;
import java.util.Map;

import com.kylantis.eaa.core.ComputeIntensive;
import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.components.AbstractComponent;
import com.kylantis.eaa.core.components.NamespaceProperties;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.config.Namespace;
import com.kylantis.eaa.core.users.RoleRealm;

@AbstractModel
public abstract class IComponentModel extends ModelAdapter<IComponentModel> {

	@PlatformInternal
	public abstract void newNamespace(NamespaceProperties namespace);

	@PlatformInternal
	public abstract NamespaceProperties getNamespace(Namespace namespace);

	@PlatformInternal
	public abstract RoleRealm getNamespaceRealm(Namespace namespace);

	@PlatformInternal
	public abstract Map<String, NamespaceProperties> listNamespaces();

	@PlatformInternal
	public abstract List<NamespaceProperties> getNamespaces(List<Namespace> namespaces);

	@PlatformInternal
	public abstract void deleteNamespace(Namespace namespace, boolean forceDelete);

	public abstract void newPage(Page page);

	@PlatformInternal
	public abstract void deletePage(Id page, boolean forceDelete);

	@PlatformInternal
	public abstract List<Id> listPageKeys(Namespace namespace);

	public abstract List<Page> getPages(List<Object> keys);

	public abstract Page getPage(Id id);

	public abstract Namespace getPageNamespace(Id id);

	public abstract void buildComponentTree();

	@ComputeIntensive
	@PlatformInternal
	public abstract Map<Id, String> getComponentHashes();

	@PlatformInternal
	public abstract List<String> listComponentKeys(Id page);

	public abstract List<AbstractComponent> getComponents(List<Object> keys);

	public abstract AbstractComponent getComponent(String id);

	public abstract void toggleComponent(String id);
	
	@PlatformInternal
	public abstract void newComponent(AbstractComponent component);

	public abstract void deleteComponent(String id, boolean deleteImpl);

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	
	@Override
	public final String path() {
		return MLRepository.COMPONENT_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "Component Management";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
