package com.kylantis.eaa.core.ml;

import java.util.Iterator;
import java.util.List;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.microservices.ServiceFunctionalitySpec;
import com.kylantis.eaa.core.setup.ServiceRegistrationRequest;

@AbstractModel
public abstract class IMicroserviceModel extends ModelAdapter<IMicroserviceModel> {

	public abstract void registerService(ServiceRegistrationRequest spec);

	public abstract void newFunctionalities(List<ServiceFunctionalitySpec> spec) ;

	@PlatformInternal
	public abstract Iterator<Integer> getAssociatedUsers(String service);

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	@Override
	public final String path() {
		return MLRepository.MICROSERVICE_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "Microservice Management";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
