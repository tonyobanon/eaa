package com.kylantis.eaa.core.ml;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.billing.BaseProviderSpec;
import com.kylantis.eaa.core.billing.PaymentCardDetails;
import com.kylantis.eaa.core.billing.PaymentCardToken;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.forms.Question;

@AbstractModel
public abstract class IPaymentModel extends ModelAdapter<IPaymentModel> {

	public abstract List<Question> getModuleParams(String name);

	/**
	 * 
	 * <b> Sample Use-cases: <b> <br>
	 * <b>1.</b> On customer registration to determine whether token or card details
	 * will be collected by the user ,e.t.c
	 */
	public abstract BaseProviderSpec getDefaultProviderSpec();

	public abstract Map<String, String> getProviderNames();

	public abstract Collection<PaymentCardDetails> getEntityCardDetails(Integer entityId);

	public abstract void addEntityCardDetails(Integer entityId, PaymentCardDetails cardDetails);

	public abstract void addEntityCardToken(Integer entityId, PaymentCardToken cardToken);

	public abstract void newTransaction();

	public abstract void updateTransactionStatus();

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	
	@Override
	public final String path() {
		return MLRepository.PAYMENT_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "Payment Management";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
