package com.kylantis.eaa.core.ml;

import java.util.List;
import java.util.Map;

import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.users.RoleRealm;
import com.kylantis.eaa.core.users.RoleUpdateAction;

@AbstractModel
public abstract class IRolesModel extends ModelAdapter<IRolesModel> {

	public abstract void newRole(String name, RoleRealm realm);

	public abstract void deleteRole(String name);

	public abstract Map<String, RoleRealm> listRoles();

	public abstract List<String> listRoles(RoleRealm realm);

	public abstract void updateFunctionality(String name, RoleUpdateAction action, String functionality);

	public abstract List<String> getFunctionalities(String name);

	public abstract RoleRealm getRealm(String name);

	public abstract List<String> getAvailableFunctionalities(RoleRealm realm);

	public abstract String getDefaultRole(RoleRealm realm);

	public abstract boolean isDefaultRole(String name);

	public abstract boolean isRoleValid(String name);

	public abstract void addRole(Integer userId, String role);

	public abstract void removeRole(Integer userId, String role);

	@Override
	public final IdentityType identity() {		
		return IdentityType.MODEL;
	}
	@Override
	public final String path() {
		return MLRepository.ROLES_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "Roles Management";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
