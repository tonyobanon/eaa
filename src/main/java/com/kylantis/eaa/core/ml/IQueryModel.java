package com.kylantis.eaa.core.ml;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.kylantis.eaa.core.FluentArrayList;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.indexing.Index;
import com.kylantis.eaa.core.indexing.PartitionSizeExceeded;
import com.kylantis.eaa.core.indexing.QueryCheckpointSpec;
import com.kylantis.eaa.core.indexing.QueryType;

@AbstractModel
public abstract class IQueryModel extends ModelAdapter<IQueryModel> {
	
	public abstract Map<String, QueryCheckpointSpec> getQueryCheckpoints();

	public abstract Map<String, Integer> getOpenQueries();

	public abstract String newSearchKey(String keyword, int limit, Integer... entityTypes);

	public abstract void parallelQuery(Index index, QuerySpec spec, Consumer<Item> consumer);

	public abstract void newQueryOptimizedGSI(Index index, List<String> projections, QueryType queryType,
			String indexHashKey, String indexRangeKey, Long readThroughputCapacity, String tableHashKey,
			String tableRangeKey);

	public abstract Integer partitionCount(Index index);

	public abstract Integer nextPartitionKey(Index index, int size);

	public abstract void incrementPartition(Index index, int partitionId, int size) throws PartitionSizeExceeded;

	public abstract void decrementPartition(Index index, int partitionId, int size);

	@Override
	public final IdentityType identity() {
		return IdentityType.MODEL;
	}
	@Override
	public final String path() {
		return MLRepository.QUERY_MODEL;
	}

	@Override
	final boolean isInternal() {
		return true;
	}

	@Override
	protected String title() {
		return "Database Queries Management";
	}
	
	@Override
	public List<Page> pages() {
		return new FluentArrayList<Page>();
	}
}
