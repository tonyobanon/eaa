package com.kylantis.eaa.core.ml;

public enum DependencyType {
	HARD(1), SOFT(0);
	
	private final Integer type;

	private DependencyType(int type) {
		this.type = type;
	}

	public Integer getType() {
		return type;
	}

	public static DependencyType from(int type) {
		switch (type) {
		case 0:
			return DependencyType.SOFT;
		case 1:
		default:
			return HARD;
		}
	}
	
}
