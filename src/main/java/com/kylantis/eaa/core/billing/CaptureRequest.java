package com.kylantis.eaa.core.billing;

public class CaptureRequest extends BaseRequest{

	private String reference;
	private Number amount;
	private String currencyCode;
	
	public String getReference() {
		return reference;
	}
	
	public void setReference(String reference) {
		this.reference = reference;
	}
	
	public Number getAmount() {
		return amount;
	}
	
	public void setAmount(Number amount) {
		this.amount = amount;
	}
	
	public String getCurrencyCode() {
		return currencyCode;
	}
	
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
}
