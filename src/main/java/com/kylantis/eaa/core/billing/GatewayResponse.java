package com.kylantis.eaa.core.billing;

public class GatewayResponse {
	
	private BaseRequest request;
	
	private boolean wasSucessful;
	
	private boolean needsVerfication;
	
	private String rawResponse;
		
	private String dateCreated;

	public boolean isWasSucessful() {
		return wasSucessful;
	}

	public void setWasSucessful(boolean wasSucessful) {
		this.wasSucessful = wasSucessful;
	}

	public String getRawResponse() {
		return rawResponse;
	}

	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public BaseRequest getRequest() {
		return request;
	}

	public void setRequest(BaseRequest request) {
		this.request = request;
	}

	public boolean isNeedsVerfication() {
		return needsVerfication;
	}

	public void setNeedsVerfication(boolean needsVerfication) {
		this.needsVerfication = needsVerfication;
	}

}
