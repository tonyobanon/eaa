package com.kylantis.eaa.core.billing;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.kylantis.eaa.core.Application;
import com.kylantis.eaa.core.ClassIdentityType;
import com.kylantis.eaa.core.base.ClasspathScanner;
import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.base.Logger;
import com.kylantis.eaa.core.components.Form;
import com.kylantis.eaa.core.config.ConfigModel;
import com.kylantis.eaa.core.config.Namespace;
import com.kylantis.eaa.core.forms.Question;
import com.kylantis.eaa.core.keys.AppConfigKey;

import io.vertx.ext.web.RoutingContext;

public abstract class BasePaymentModule {

	private static Map<String, BasePaymentModule> modules;

	public abstract BaseProviderSpec baseSpec();

	public abstract void init();

	public abstract List<Question> parameters();

	/**
	 * 
	 * This authorizes a card payment transaction
	 **/
	public abstract GatewayResponse authorize(AuthorizationRequest request);

	/**
	 * 
	 * This cancels a transaction that has been authorized
	 */
	public abstract GatewayResponse cancel(BaseRequest request);

	/**
	 * 
	 * This initiates a refund for a transaction that has been captured
	 */
	public abstract GatewayResponse refund(RefundRequest request);

	/**
	 * This initiates a refund if a transaction has been captured, or a
	 * cancellation if the transaction has been authorized
	 */
	public abstract GatewayResponse refundOrCancel(BaseRequest request);

	/**
	 * This initiates a refund if a transaction has been captured, or a
	 * cancellation if the transaction has been authorized
	 */
	public abstract void IpnHandler(RoutingContext context);

	final String registryKeyPrefix() {
		return "payment_module_" + baseSpec().name() + "_";
	}

	protected final String getConfig(String param) {
		return (String) ConfigModel.get(Namespace.forAdmin().getKey(registryKeyPrefix() + param));
	}

	protected static final Collection<BasePaymentModule> getModules() {
		if(modules == null) {
			return discoverModules();
		}
		return modules.values();
	}

	protected static final BasePaymentModule getModule(String module) {
		return modules.get(module);
	}

	static final Collection<BasePaymentModule> discoverModules() {

		modules = new LinkedHashMap<>();
		 
		// Find all built-in payment modules
		Logger.debug("Scanning for payment modules ..");

		String ext = Application.getConfig(AppConfigKey.CLASSES_MODULES_PAYMENT_EXT);

		for (Class<? extends BasePaymentModule> model : new ClasspathScanner<>(ext, BasePaymentModule.class,
				ClassIdentityType.SUPER_CLASS).scanClasses()) {

			try {

				Logger.info("Discovered payment module: " + model.getSimpleName());
				modules.put(model.getSimpleName(), model.newInstance());

			} catch (InstantiationException | IllegalAccessException e) {
				Exceptions.throwRuntime(e);
			}
		}
		
		return modules.values();
	}

}
