package com.kylantis.eaa.core.billing;

public class PaymentCardDetails extends PaymentCard {

	private Long cardNumber;

	private String cardHolderName;

	private Integer expiryMonth;

	private Integer expiryYear;

	private String address1;

	private Integer postalCode;

	private String city;

	private String state;

	private String countryCode;
	
	private String dateCreated;

	public Long getCardNumber() {
		return cardNumber;
	}

	public PaymentCardDetails setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
		return this;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public PaymentCardDetails setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
		return this;
	}

	public Integer getExpiryMonth() {
		return expiryMonth;
	}

	public PaymentCardDetails setExpiryMonth(Integer expiryMonth) {
		this.expiryMonth = expiryMonth;
		return this;
	}

	public Integer getExpiryYear() {
		return expiryYear;
	}

	public PaymentCardDetails setExpiryYear(Integer expiryYear) {
		this.expiryYear = expiryYear;
		return this;
	}

	public String getAddress1() {
		return address1;
	}

	public PaymentCardDetails setAddress1(String address1) {
		this.address1 = address1;
		return this;
	}

	public Integer getPostalCode() {
		return postalCode;
	}

	public PaymentCardDetails setPostalCode(Integer postalCode) {
		this.postalCode = postalCode;
		return this;
	}

	public String getCity() {
		return city;
	}

	public PaymentCardDetails setCity(String city) {
		this.city = city;
		return this;
	}

	public String getState() {
		return state;
	}

	public PaymentCardDetails setState(String state) {
		this.state = state;
		return this;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public PaymentCardDetails setCountryCode(String countryCode) {
		this.countryCode = countryCode;
		return this;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public PaymentCardDetails setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
		return this;
	}

}
