package com.kylantis.eaa.core.billing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.kylantis.eaa.core.Dates;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.CardDetailsSpec;
import com.kylantis.eaa.core.attributes.CardTokenSpec;
import com.kylantis.eaa.core.base.ObjectMarshaller;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.config.ConfigHelper;
import com.kylantis.eaa.core.config.ConfigModel;
import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.config.Namespace;
import com.kylantis.eaa.core.forms.BaseSimpleEntry;
import com.kylantis.eaa.core.forms.Question;
import com.kylantis.eaa.core.keys.RegistryKeys;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IPaymentModel;
import com.kylantis.eaa.core.ml.MLRepository;
import com.kylantis.eaa.core.setup.InstallOptions;
import com.kylantis.eaa.core.system.ResourceBundleKeyRef;
import com.kylantis.eaa.core.utils.Utils;

@ConcreteModel(dependencies = { MLRepository.CONFIG_MODEL })
public class PaymentModel extends IPaymentModel {
	
	@Override
	public void preInstall() {
		
		// Create payment modules configuration page
		getComponentModel().newPage(new Page().setId(ADMIN_PAYMENT_MODULES_PAGE)
				.setTitle(new ResourceBundleKeyRef().setKey("ext.payment_modules"))
				.setParent(ConfigModel.ADMIN_CONFIG_PAGE));

		BasePaymentModule.getModules().forEach(module -> {

			// Create Payment Module Configuration Page
			getComponentModel().newPage(new Page().setId(ADMIN_PAYMENT_MODULES_PAGE.getChild(module.baseSpec().name()))
					.setTitle(new ResourceBundleKeyRef().setKey("ext." + module.baseSpec().name()))
					.setParent(ADMIN_PAYMENT_MODULES_PAGE));
			
			getModuleParams(module.baseSpec().name()).forEach((o) -> {
				getConfigModel().newParameter(module.baseSpec().name(), o, null);
			});
		});

		// Create config parameters for the user space and their default, i.e, User
		// Management

	}
	
	public void getDefaultPage() {
		return getConfigModel().ADMIN_CONFIG_PAGE.getChild("payment_modules")
	}

	@Override
	public void install(InstallOptions options) {

		// Add registry entries

		Namespace registry = Namespace.forAdmin();

		getConfigModel().put(registry.getKey(RegistryKeys.DEFAULT_PAYMENT_PROVIDER), options.getPaymentProvider());
		getConfigModel().put(registry.getKey(RegistryKeys.ENFORCE_PAYMENT_DETAIL_ENTRY), ObjectMarshaller.marshal(true));

		// Save all config parameters for the default payment provider
		getConfigModel().putAll(registry, ConfigHelper.transform(options.getPaymentProviderConfig()));
	}

	@Override
	public void start() {

		// Discover payment modules
		BasePaymentModule.discoverModules();
	}

	public List<Question> getModuleParams(String name) {

		BasePaymentModule module = BasePaymentModule.getModule(name);
		List<Question> parameters = new ArrayList<>();

		module.parameters().forEach(param -> {
			BaseSimpleEntry e = (BaseSimpleEntry) param;
			parameters.add(e.setId(module.registryKeyPrefix() + e.getId()));
		});

		
		
		return parameters;
	}

	/**
	 * 
	 * <b> Sample Use-cases: <b> <br>
	 * <b>1.</b> On customer registration to determine whether token or card details
	 * will be collected by the user ,e.t.c
	 */
	public BaseProviderSpec getDefaultProviderSpec() {
		return BasePaymentModule
				.getModule((String) getConfigModel().get(Namespace.forAdmin().getKey(RegistryKeys.DEFAULT_PAYMENT_PROVIDER)))
				.baseSpec();
	}

	public Map<String, String> getProviderNames() {
		Map<String, String> providers = new HashMap<>();

		BasePaymentModule.getModules().forEach(module -> {
			providers.put(module.baseSpec().name(), Utils.prettify(
					getResourceBundleModel().get(getSystemModel().getDefaultLocale(), "ext." + module.baseSpec().name())));
		});

		return providers;
	}

	public Collection<PaymentCardDetails> getEntityCardDetails(Integer entityId) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(CardDetailsSpec.TABLE_NAME);
		QuerySpec spec = new QuerySpec()
				.withNameMap(new FluentHashMap<String, String>().with("#k", CardDetailsSpec.ENTITY_ID))
				.withValueMap(new FluentHashMap<String, Object>().with(":v", entityId))
				.withKeyConditionExpression("#k = :v");
		ItemCollection<QueryOutcome> outcome = table.query(spec);

		if (outcome.getAccumulatedItemCount() == 0) {
			return new ArrayList<>();
		} else {
			List<PaymentCardDetails> result = new ArrayList<>();
			outcome.forEach(item -> {

				PaymentCardDetails cardDetails = new PaymentCardDetails();
				cardDetails.setCardNumber(item.getLong(CardDetailsSpec.CARD_NUMBER));
				cardDetails.setCardHolderName(item.getString(CardDetailsSpec.CARD_HOLDER_NAME));
				cardDetails.setExpiryMonth(item.getInt(CardDetailsSpec.EXPIRY_MONTH));
				cardDetails.setExpiryYear(item.getInt(CardDetailsSpec.EXPIRY_YEAR));
				cardDetails.setAddress1(item.getString(CardDetailsSpec.ADDRESS_1));
				cardDetails.setCity(item.getString(CardDetailsSpec.CITY));
				cardDetails.setPostalCode(item.getInt(CardDetailsSpec.POSTAL_CODE));
				cardDetails.setState(item.getString(CardDetailsSpec.STATE));
				cardDetails.setCountryCode(item.getString(CardDetailsSpec.COUNTRY_CODE));
				cardDetails.setDateCreated(item.getString(CardDetailsSpec.DATE_CREATED));

				result.add(cardDetails);
			});
			return result;
		}
	}

	public void addEntityCardDetails(Integer entityId, PaymentCardDetails cardDetails) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(CardDetailsSpec.TABLE_NAME);

		Item item = new Item()
				.withPrimaryKey(CardDetailsSpec.ENTITY_ID, entityId, CardDetailsSpec.CARD_NUMBER,
						cardDetails.getCardNumber())
				.withString(CardDetailsSpec.CARD_HOLDER_NAME, cardDetails.getCardHolderName())
				.withInt(CardDetailsSpec.EXPIRY_MONTH, cardDetails.getExpiryMonth())
				.withInt(CardDetailsSpec.EXPIRY_YEAR, cardDetails.getExpiryYear())

				.withString(CardDetailsSpec.ADDRESS_1, cardDetails.getAddress1())
				.withString(CardDetailsSpec.CITY, cardDetails.getCity())
				.withInt(CardDetailsSpec.POSTAL_CODE, cardDetails.getPostalCode())
				.withString(CardDetailsSpec.STATE, cardDetails.getState())
				.withString(CardDetailsSpec.COUNTRY_CODE, cardDetails.getCountryCode())

				.withString(CardDetailsSpec.DATE_CREATED, Dates.currentDate());

		PutItemSpec spec = new PutItemSpec().withItem(item)
				.withNameMap(new FluentHashMap<String, String>().with("#k", CardDetailsSpec.ENTITY_ID));

		spec.withConditionExpression("attribute_not_exists(#k)");

		try {
			table.putItem(spec);
		} catch (ConditionalCheckFailedException ex) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS);
		}
	}

	public void addEntityCardToken(Integer entityId, PaymentCardToken cardToken) {
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(CardTokenSpec.TABLE_NAME);

		Item item = new Item()
				.withPrimaryKey(CardTokenSpec.ENTITY_ID, entityId, CardTokenSpec.CARD_TOKEN, cardToken.getToken())
				.withString(CardTokenSpec.MODULE_NAME, cardToken.getModule())
				.withString(CardTokenSpec.DATE_CREATED, Dates.currentDate());

		PutItemSpec spec = new PutItemSpec().withItem(item)
				.withNameMap(new FluentHashMap<String, String>().with("#k", CardTokenSpec.ENTITY_ID));

		spec.withConditionExpression("attribute_not_exists(#k)");

		try {
			table.putItem(spec);
		} catch (ConditionalCheckFailedException ex) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS);
		}
	}

	public void newTransaction() {

	}

	public void updateTransactionStatus() {

	}

	// On user registration, a small amount is authorized to verify that the
	// card is chargeable and valid

	static {

	}

}
