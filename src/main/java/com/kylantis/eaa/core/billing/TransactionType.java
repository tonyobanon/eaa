package com.kylantis.eaa.core.billing;

public enum TransactionType {
  AUTHORIZE, CANCEL, REFUND, REFUND_OR_CANCEL
}
