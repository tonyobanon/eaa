package com.kylantis.eaa.core.billing;

public class PaymentCardToken extends PaymentCard {

	private String module;
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}
	
}
