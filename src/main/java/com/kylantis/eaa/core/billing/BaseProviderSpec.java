package com.kylantis.eaa.core.billing;

public interface BaseProviderSpec {

	/**
	 * Module name
	 * */
	String name();

	/**
	 * Flag to indicate whether this module requires a user token or card information */
	boolean usesCardToken();
	
	/**
	 * This returns a javascript file, that orchestrates user client token generation.
	 * The contract is that it should contain a global javascript function called eaa_payment_module_{name}
	 * that return an object containing a value: function, at key: getClientToken
	 * 
	 * */
	String clientTokenOrchestrator();

}
