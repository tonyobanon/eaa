package com.kylantis.eaa.core.billing;

public class AuthorizationRequest extends BaseRequest {

	private String description;

	private String currencyCode;

	private Number amount;

	private PaymentCard card;

	private String ipAddress;

	private String sessionId;

	private String userEmail;

	
	public String getDescription() {
		return description;
	}

	public AuthorizationRequest setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public AuthorizationRequest setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
		return this;
	}

	public Number getAmount() {
		return amount;
	}

	public AuthorizationRequest setAmount(Number amount) {
		this.amount = amount;
		return this;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public AuthorizationRequest setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
		return this;
	}

	public String getSessionId() {
		return sessionId;
	}

	public AuthorizationRequest setSessionId(String sessionId) {
		this.sessionId = sessionId;
		return this;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public AuthorizationRequest setUserEmail(String userEmail) {
		this.userEmail = userEmail;
		return this;
	}

	public PaymentCard getCard() {
		return card;
	}

	public AuthorizationRequest setCard(PaymentCard card) {
		this.card = card;
		return this;
	}
	
}
