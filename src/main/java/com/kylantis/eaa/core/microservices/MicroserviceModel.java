package com.kylantis.eaa.core.microservices;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.kylantis.eaa.core.BlockerTodo;
import com.kylantis.eaa.core.Dates;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.BaseServiceSpec;
import com.kylantis.eaa.core.attributes.ServiceFunctionalitiesSpec;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.dbutils.DBTooling;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IMicroserviceModel;
import com.kylantis.eaa.core.setup.ServiceRegistrationRequest;

@ConcreteModel
public class MicroserviceModel extends IMicroserviceModel {

	
	@Override
	public void start() {
		
	}
	
	public void registerService(ServiceRegistrationRequest spec) {
		
		//Create entry in BaseServiceSpec
		
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseServiceSpec.TABLE_NAME);

		Item item = new Item().withPrimaryKey(BaseServiceSpec.ID, spec.getId())
				.withString(BaseServiceSpec.TITLE, spec.getTitle())
				.withString(BaseServiceSpec.SUMMARY, spec.getSummary())
				.withString(BaseServiceSpec.DEVELOPER, spec.getDeveloper())
				.withString(BaseServiceSpec.DEVELOPER_URL, spec.getDeveloperUrl())
				.withString(BaseServiceSpec.BASE_URI, spec.getBaseUri())
				.withString(BaseServiceSpec.LOGO_URL, spec.getLogoUrl())
				.withString(BaseServiceSpec.DATE_CREATED, Dates.currentDate());

		try {
			table.putItem(new PutItemSpec().withItem(item)
					.withNameMap(new FluentHashMap<String, String>().with("#k", BaseServiceSpec.ID))
					.withConditionExpression("attribute_not_exists(#k)"));
		} catch (ConditionalCheckFailedException e) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS);
		}
		
		
		//register cname
		
		
		
		
		//add custom user section
		
		
		
		
		
		
		//Scan through all default roles () to add them to their functionality spec
		
		
		
	}
	

	public void newFunctionalities(List<ServiceFunctionalitySpec> spec) {

		List<Item> items = new ArrayList<>();

		spec.forEach(e -> {

			Item item = new Item()
					.withPrimaryKey(ServiceFunctionalitiesSpec.SERVICE, e.getService(), ServiceFunctionalitiesSpec.ID,
							e.getId())
					.withString(ServiceFunctionalitiesSpec.NAME, e.getName())
					.withString(ServiceFunctionalitiesSpec.REALM, e.getRealm())
					.withString(ServiceFunctionalitiesSpec.DATE_CREATED, e.getDateCreated());

			items.add(item);

		});

		DBTooling.batchPut(ServiceFunctionalitiesSpec.TABLE_NAME, items);
	}

	
	@BlockerTodo("Implement")
	@PlatformInternal
	public Iterator<Integer> getAssociatedUsers(String service){
		return new ArrayList<Integer>().iterator();
	}
	
}
