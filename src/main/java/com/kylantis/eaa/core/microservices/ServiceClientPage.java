package com.kylantis.eaa.core.microservices;

public class ServiceClientPage {

	private String service;
	private String id;
	private String title;
	private String pageUri;
	
	public ServiceClientPage(String service, String id, String title, String pageUri) {
		this.service = service;
		this.id = id;
		this.title = title;
		this.pageUri = pageUri;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPageUri() {
		return pageUri;
	}

	public void setPageUri(String pageUri) {
		this.pageUri = pageUri;
	}
	
}
