package com.kylantis.eaa.core.components;

import java.util.ArrayList;
import java.util.List;

public class Row {

	private List<BaseComponent> elements = new ArrayList<>();

	protected Row() {
		
	}
	
	public List<BaseComponent> getElements() {
		return elements;
	}

	protected Row add(BaseComponent element) {
		this.elements.add(element);
		return this;
	}

}
