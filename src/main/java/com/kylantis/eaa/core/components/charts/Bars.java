package com.kylantis.eaa.core.components.charts;

public enum Bars {

	HORIZONTAL("horoizontal"),

	VERTICAL("vertical");

	private final Object value;

	private Bars(Object value) {
		this.value = value;
	}

	public Object getValue() {
		return this.value;
	}

}
