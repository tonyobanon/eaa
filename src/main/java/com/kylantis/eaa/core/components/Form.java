package com.kylantis.eaa.core.components;

import java.util.List;

import com.google.common.collect.Lists;
import com.kylantis.eaa.core.components.styling.MDColor;
import com.kylantis.eaa.core.components.styling.Sizes;
import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.forms.Question;

public abstract class Form extends BaseComponent {

	public abstract List<Question> entries();

	@Override
	public ComponentType type() {
		return ComponentType.FORM;
	}

	public void getDefaultValue(Id question, String value) {
		switch (question.getId()) {
		// Conditions
		}
		return;
	}

	@Override
	public Iterable<MDColor> themeColor() {
		return Lists.newArrayList(MDColor.white_1000);
	}

	@Override
	public Sizes minWidth() {
		return Sizes.XL;
	}
}
