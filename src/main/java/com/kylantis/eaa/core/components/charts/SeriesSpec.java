package com.kylantis.eaa.core.components.charts;

import com.kylantis.eaa.core.components.styling.MDColor;

public class SeriesSpec {

	private Annotations annotations;
	private Number areaOpacity;
	private MDColor color;
	private String labelInLegend;
	private Number[] lineDashStyle;
	private Number lineWidth;
	private PointShape pointShape;
	private Number pointSize;
	private Boolean pointsVisible;
	private Number targetAxisIndex;
	private Boolean visibleInLegend;

	public Annotations getAnnotations() {
		return annotations;
	}

	/**
	 * This is to be applied to annotations for this series.
	 */
	public SeriesSpec setAnnotations(Annotations annotations) {
		this.annotations = annotations;
		return this;
	}

	public Number getAreaOpacity() {
		return areaOpacity;
	}

	/**
	 * Overrides the global <b>areaOpacity</b> for this series
	 */
	public SeriesSpec setAreaOpacity(Number areaOpacity) {
		this.areaOpacity = areaOpacity;
		return this;
	}

	public MDColor getColor() {
		return color;
	}

	/** The color to use for this series */
	public SeriesSpec setColor(MDColor color) {
		this.color = color;
		return this;
	}

	public String getLabelInLegend() {
		return labelInLegend;
	}

	/** The description of the series to appear in the chart legend. */
	public SeriesSpec setLabelInLegend(String labelInLegend) {
		this.labelInLegend = labelInLegend;
		return this;
	}

	public Number[] getLineDashStyle() {
		return lineDashStyle;
	}

	/** Overrides the global <b>lineDashStyle</b> value for this series. */
	public SeriesSpec setLineDashStyle(Number[] lineDashStyle) {
		this.lineDashStyle = lineDashStyle;
		return this;
	}

	public Number getLineWidth() {
		return lineWidth;
	}

	/** Overrides the global <b>lineWidth</b> value for this series. */
	public SeriesSpec setLineWidth(Number lineWidth) {
		this.lineWidth = lineWidth;
		return this;
	}

	public PointShape getPointShape() {
		return pointShape;
	}

	/** Overrides the global <b>pointShape</b> value for this series. */
	public SeriesSpec setPointShape(PointShape pointShape) {
		this.pointShape = pointShape;
		return this;
	}

	public Number getPointSize() {
		return pointSize;
	}

	/** Overrides the global <b>pointSize</b> value for this series. */
	public SeriesSpec setPointSize(Number pointSize) {
		this.pointSize = pointSize;
		return this;
	}

	public Boolean getPointsVisible() {
		return pointsVisible;
	}

	/** Overrides the global <b>pointsVisible</b> value for this series. */
	public SeriesSpec setPointsVisible(Boolean pointsVisible) {
		this.pointsVisible = pointsVisible;
		return this;
	}

	public Number getTargetAxisIndex() {
		return targetAxisIndex;
	}

	/**
	 * Which axis to assign this series to, where 0 is the default axis, and 1 is
	 * the opposite axis. Default value is 0; set to 1 to define a chart where
	 * different series are rendered against different axes. At least one series
	 * much be allocated to the default axis. You can define a different scale for
	 * different axes.
	 */
	public SeriesSpec setTargetAxisIndex(Number targetAxisIndex) {
		this.targetAxisIndex = targetAxisIndex;
		return this;
	}

	public Boolean getVisibleInLegend() {
		return visibleInLegend;
	}

	/**
	 * A boolean value, where true means that the series should have a legend entry,
	 * and false means that it should not. Default is true.
	 */
	public SeriesSpec setVisibleInLegend(Boolean visibleInLegend) {
		this.visibleInLegend = visibleInLegend;
		return this;
	}

}
