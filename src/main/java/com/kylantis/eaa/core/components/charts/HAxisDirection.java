package com.kylantis.eaa.core.components.charts;

public enum HAxisDirection {

	FORWARD(1), REVERSE(-1);

	private final Integer direction;

	private HAxisDirection(Integer direction) {
		this.direction = direction;
	}

	public Integer getDirection() {
		return direction;
	}

	public static HAxisDirection from(Integer direction) {
		switch (direction) {
		case -1:
			return REVERSE;
		case 1:
		default:
			return FORWARD;
		}
	}

}