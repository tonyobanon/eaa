package com.kylantis.eaa.core.components.charts;

public class AnnotationsAxisSpec {

	private AnnotationsStem stem;
	private AnnotationsStyle style;

	public AnnotationsStem getStem() {
		return stem;
	}

	/** You can use this to control the color and stem length */
	public void setStem(AnnotationsStem stem) {
		this.stem = stem;
	}

	public AnnotationsStyle getStyle() {
		return style;
	}

	public void setStyle(AnnotationsStyle style) {
		this.style = style;
	}
}
