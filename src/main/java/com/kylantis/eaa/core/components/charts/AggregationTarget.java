package com.kylantis.eaa.core.components.charts;

public enum AggregationTarget {

	/** Group selected data by x-value. */
	CATEGORY("category"),

	/** Group selected data by series. */
	SERIES("series"),

	/**
	 * Group selected data by x-value if all selections have the same x-value, and
	 * by series otherwise.
	 */
	AUTO("auto"),

	/** Show only one tooltip per selection. */
	NONE("none");

	private final String target;

	private AggregationTarget(String target) {
		this.target = target;
	}

	@Override
	public String toString() {
		return target;
	}
}
