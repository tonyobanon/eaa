package com.kylantis.eaa.core.components;

public enum ComponentType {
	ANNOTATION_CHART(1), AREA_CHART(2), BAR_CHART(3), BUBBLE_CHART(4), CALENADR(5), CALENDAR_CHART(6), CARD(
			7), CAROUSEL(8), CHAT_MESSENGER(9), COLUMN_CHART(10), EDITOR(11), FORM(12), GANT_CHART(13), HISTOGRAM(
					14), INTERVAL_CHART(15), INVOICE(16), LINE_CHART(17), LIST_GROUP(18), MAIL(19), MAP(20), MODAL(
							21), PHOTO_GALLERY(22), PIE_CHART(23), PROGRESS_BAR(24), SCATTER_CHART(25), SEARCH_RESULT(
									26), SIDEBAR(27), SOCIAL(28), SPINNER(
											29), STAT(30), TAB(31), TABLE(32), TIMELINE(33), TOAST(34), TOPBAR(35);

	private final Integer type;

	private ComponentType(int type) {
		this.type = type;
	}

	public Integer getType() {
		return type;
	}

	public static ComponentType from(int type) {
		switch (type) {
		case 1:
			return ComponentType.ANNOTATION_CHART;
		case 2:
			return ComponentType.AREA_CHART;
		case 3:
			return ComponentType.BAR_CHART;
		case 4:
			return ComponentType.BUBBLE_CHART;
		case 5:
			return ComponentType.CALENADR;
		case 6:
			return ComponentType.CALENDAR_CHART;
		case 7:
			return ComponentType.CARD;
		case 8:
			return ComponentType.CAROUSEL;
		case 9:
			return ComponentType.CHAT_MESSENGER;
		case 10:
			return ComponentType.COLUMN_CHART;
		case 11:
			return ComponentType.EDITOR;
		case 12:
			return ComponentType.FORM;
		case 13:
			return ComponentType.GANT_CHART;
		case 14:
			return ComponentType.HISTOGRAM;
		case 15:
			return ComponentType.INTERVAL_CHART;
		case 16:
			return ComponentType.INVOICE;
		case 17:
			return ComponentType.LINE_CHART;
		case 18:
			return ComponentType.LIST_GROUP;
		case 19:
			return ComponentType.MAIL;
		case 20:
			return ComponentType.MAP;

		case 21:
			return ComponentType.MODAL;
		case 22:
			return ComponentType.PHOTO_GALLERY;
		case 23:
			return ComponentType.PIE_CHART;
		case 24:
			return ComponentType.PROGRESS_BAR;
		case 25:
			return ComponentType.SCATTER_CHART;
		case 26:
			return ComponentType.SEARCH_RESULT;
		case 27:
			return ComponentType.SIDEBAR;
		case 28:
			return ComponentType.SOCIAL;
		case 29:
			return ComponentType.SPINNER;
		case 30:
			return ComponentType.STAT;
		case 31:
			return ComponentType.TAB;
		case 32:
			return ComponentType.TABLE;
		case 33:
			return ComponentType.TIMELINE;
		case 34:
			return ComponentType.TOAST;
		case 35:
			return ComponentType.TOPBAR;

		default:
			throw new IllegalArgumentException("Unknown Component Type");
		}
	}
}
