package com.kylantis.eaa.core.components.charts;

public class Legend {

	private LegendAlignment alignment;
	private Number maxLines;
	private LegendPosition position;
	private TextStyle textStyle;

	public LegendAlignment getAlignment() {
		return alignment;
	}

	/**
	 * Alignment of the legend. <br>
	 * Start, center, and end are relative to the style -- vertical or horizontal --
	 * of the legend. For example, in a 'right' legend, 'start' and 'end' are at the
	 * top and bottom, respectively; for a 'top' legend, 'start' and 'end' would be
	 * at the left and right of the area, respectively.
	 * 
	 * <br>
	 * The default value depends on the legend's position. For 'bottom' legends, the
	 * default is 'center'; other legends default to 'start'.
	 */
	public Legend setAlignment(LegendAlignment alignment) {
		this.alignment = alignment;
		return this;
	}

	public Number getMaxLines() {
		return maxLines;
	}

	/**
	 * Maximum number of lines in the legend. Set this to a number greater than one
	 * to add lines to your legend. Note: The exact logic used to determine the
	 * actual number of lines rendered is still in flux. <br>
	 * This option currently works only when legend.position is 'top'. Default: 1
	 */
	public Legend setMaxLines(Number maxLines) {
		this.maxLines = maxLines;
		return this;
	}

	public LegendPosition getPosition() {
		return position;
	}

	/** Position of the legend. Default: 'right' */
	public Legend setPosition(LegendPosition position) {
		this.position = position;
		return this;
	}

	public TextStyle getTextStyle() {
		return textStyle;
	}

	/** Specifies the legend text style. */
	public Legend setTextStyle(TextStyle textStyle) {
		this.textStyle = textStyle;
		return this;
	}

}
