package com.kylantis.eaa.core.components.charts;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target({ FIELD, ElementType.TYPE, ElementType.CONSTRUCTOR })
public @interface Review {
	String value();
}
