package com.kylantis.eaa.core.components;

import com.kylantis.eaa.core.base.Todo;
import com.kylantis.eaa.core.components.styling.MDColor;

public interface Theme {

	@Todo
	public static Theme getDefault() {
		//Fetch default theme from database
		//@DEV
		return new DefaultTheme();
	}

	public MDColor getColor(BaseComponent type);
	
	public MDColor getBackgroundColor(BaseComponent type);
	
	public ValueTransformStrategy getValueTransformStrategy();
}
