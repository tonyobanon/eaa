package com.kylantis.eaa.core.components.charts;

/**
 * 
 * For date axis labels, this is a subset of the date formatting
 * <a href="http://icu-project.org/apiref/icu4c/classSimpleDateFormat.html#_details">ICU pattern set</a>
. For instance, 'MMM d, y' will display the value "Jul 1, 2011" for the date
 * of July first in 2011. 
 * <br>
 * The actual formatting applied to the label is derived from the locale the API
 * has been loaded with.
 * <br>
 * This option is only supported for a continuous axis.
 * 
 */
@Review("Add more formats")
public enum HAxisDateFormat {

	/** displays numbers with no formatting (e.g., 8000000) */
	MMM_D_Y("MMM d, y");

	private final String format;

	private HAxisDateFormat(String format) {
		this.format = format;
	}

	@Override
	public String toString() {
		return format;
	}

	public String getFormat() {
		return format;
	}
}
