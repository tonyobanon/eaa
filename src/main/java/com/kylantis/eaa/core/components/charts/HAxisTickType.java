package com.kylantis.eaa.core.components.charts;

public enum HAxisTickType {
	DATE("d"), NUMBER("n");

	private final String type;

	private HAxisTickType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return type;
	}
}
