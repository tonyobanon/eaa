package com.kylantis.eaa.core.components.charts;

public enum HAxisScaleType {

	/** No logarithmic scaling is performed. */
	NULL("null"),

	/**
	 * Logarithmic scaling. Negative and zero values are not plotted. This option is
	 * the same as setting logscale to true on the HAxis
	 */
	LOG("log"),

	/**
	 * Logarithmic scaling in which negative and zero values are plotted. The
	 * plotted value of a negative number is the negative of the log of the absolute
	 * value. Values close to 0 are plotted on a linear scale.
	 */
	MIRROR_LOG("mirrorLog");
	
	private final String type;

	private HAxisScaleType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return type;
	}
}
