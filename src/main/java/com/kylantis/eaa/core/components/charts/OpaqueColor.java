package com.kylantis.eaa.core.components.charts;

import com.kylantis.eaa.core.components.styling.MDColor;
import com.kylantis.eaa.core.utils.NumberUtil;

public class OpaqueColor {

	private MDColor color;
	private Number opacity;

	public OpaqueColor(MDColor color, Number opacity) {
		this.color = color;
		this.opacity = opacity;
	}

	public MDColor getColor() {
		return color;
	}

	public void setColor(MDColor color) {
		this.color = color;
	}

	public Number getOpacity() {
		return opacity;
	}

	public void setOpacity(Number opacity) {
		this.opacity = NumberUtil.asReal(opacity);
	}

}
