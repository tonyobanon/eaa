package com.kylantis.eaa.core.components.charts;

public enum CrossHairTrigger {

	FOCUS("focus"), SELECTION("selection"), BOTH("both");

	private final String trigger;

	private CrossHairTrigger(String trigger) {
		this.trigger = trigger;
	}

	@Override
	public String toString() {
		return this.trigger;
	}

}
