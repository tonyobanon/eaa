package com.kylantis.eaa.core.components;

public abstract class TopBar extends BaseComponent {

	@Override
	public ComponentType type() {
		return ComponentType.TOPBAR;
	}
	
}
