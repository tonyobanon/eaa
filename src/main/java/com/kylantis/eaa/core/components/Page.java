package com.kylantis.eaa.core.components;

import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.config.Namespace;
import com.kylantis.eaa.core.system.ResourceBundleKeyRef;

public class Page {

	private Id id;
	private ResourceBundleKeyRef title;
	private Integer sortOrder;
	private Id parent;
	private PageGroup group;
	private String path;

	private Namespace namespace;

	public Page() {
		this.sortOrder = 0;
		this.group = PageGroup.SIDEBAR;
	}

	public Id getId() {
		return id;
	}

	public Page setId(Id id) {
		this.id = id;
		return this;
	}

	public ResourceBundleKeyRef getTitle() {
		return title;
	}

	public Page setTitle(ResourceBundleKeyRef title) {
		this.title = title;
		return this;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public Page setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
		return this;
	}

	public Id getParent() {
		return parent;
	}

	public Page setParent(Id parent) {
		this.parent = parent;
		return this;
	}

	public PageGroup getGroup() {
		return group;
	}

	public Page setGroup(PageGroup group) {
		this.group = group;
		return this;
	}

	public Namespace getNamespace() {
		return namespace;
	}

	public Page setNamespace(Namespace namespace) {
		this.namespace = namespace;
		return this;
	}

	public String getPath() {
		return path;
	}

	public Page setPath(String path) {
		this.path = path;
		return this;
	}
	
	public String hash() {
		return Integer.toString(this.hashCode());
	}

	@Override
	public final int hashCode() {
		// Exclude parent and sortOrder
		return (namespace.hashCode() + path.intern().hashCode()) - (group.hashCode() + title.hashCode());
	}
}
