package com.kylantis.eaa.core.components;

import java.io.Serializable;

import com.kylantis.eaa.core.config.Id;

public class UIContext implements Serializable {

	private static final long serialVersionUID = 1L;

	private Id currentPage;
	private Grid grid;

	public Id getCurrentPage() {
		return currentPage;
	}

	public UIContext setCurrentPage(Id currentPage) {
		this.currentPage = currentPage;
		return this;
	}

	public Grid getGrid() {
		return grid;
	}

	public UIContext setGrid(Grid grid) {
		this.grid = grid;
		return this;
	}

}
