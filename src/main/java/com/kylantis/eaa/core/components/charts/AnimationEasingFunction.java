package com.kylantis.eaa.core.components.charts;

public enum AnimationEasingFunction {

	/**Constant speed.*/
	LINEAR("linear"), 
	
	/**Ease in - Start slow and speed up.*/
	IN("in"), 
	
	/**Ease out - Start fast and slow down.*/
	OUT("out"), 
	
	/**Ease in and out - Start slow, speed up, then slow down.*/
	IN_AND_OUT("inAndOut");

	private final String function;

	private AnimationEasingFunction(String function) {
		this.function = function;
	}

	@Override
	public String toString() {
		return function;
	}

}
