package com.kylantis.eaa.core.components;

public enum FontFace {

	ARIAL("Arial"), COMIC_SANS("Comic Sans MS"), COURIER_NEW("Courier New"), TAHOMA("Tahoma"), TIMES_NEW_ROMAN(
			"Times New Roman"), VERDANA("Verdana");

	private String name;

	private FontFace(final String name) {
		this.name = name;
	}

	public String getFontName() {
		return name;
	}
}
