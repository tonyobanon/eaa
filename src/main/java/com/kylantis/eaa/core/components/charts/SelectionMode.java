package com.kylantis.eaa.core.components.charts;

public enum SelectionMode {

	/** users can select only a single data point. */
	SINGLE("single"),

	/** Users may select multiple data points. */
	MULTIPLE("multiple");

	private final String value;

	private SelectionMode(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
