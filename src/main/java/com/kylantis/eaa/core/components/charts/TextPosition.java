package com.kylantis.eaa.core.components.charts;

public enum TextPosition {

	/** Draw the axis text inside the chart area. */
	IN("in"),

	/** Draw the axis text outside the chart area. */
	OUT("out"),

	/** Omit the axis text. */
	NONE("none");

	private final String position;

	private TextPosition(String position) {
		this.position = position;
	}

	public String getPosition() {
		return position;
	}

	@Override
	public String toString() {
		return position;
	}
}
