package com.kylantis.eaa.core.components.charts;

import java.util.ArrayList;

import com.kylantis.eaa.core.components.styling.MDColor;

public class Axis {

	private Number baseline;
	private MDColor baselineColor;
	private Integer direction;
	private String format;
	private AxisGridlines gridlines;
	private AxisGridlines minorGridlines;
	private Boolean logScale;
	private HAxisScaleType scaleType;
	private TextPosition textPosition;
	private TextStyle textStyle;
	private ArrayList<HAxisTick> ticks;
	private String title;
	private TextStyle titleTextStyle;

	private Boolean allowContainerBoundaryTextCufoff;
	private Boolean slantedText;
	private Number slantedTextAngle;
	private Number maxAlternation;
	private Number maxTextLines;
	private Number minTextSpacing;
	private Number showTextEvery;

	private Number maxValue;
	private Number minValue;
	private HAxisViewWindowMode viewWindowMode;

	public Number getBaseline() {
		return baseline;
	}

	/**
	 * The baseline for the horizontal/vertical axis. <br>
	 * For a vertical axis, If the baseline is larger than the highest grid line or
	 * smaller than the lowest grid line, it will be rounded to the closest
	 * gridline. <br>
	 * If and only if <b>this</b> is a horizontal axis, then this option is only
	 * supported for a continuous axis.
	 */
	public Axis setBaseline(Number baseline) {
		this.baseline = baseline;
		return this;
	}

	public MDColor getBaselineColor() {
		return baselineColor;
	}

	/**
	 * The color of the baseline for the horizontal/vertical axis. <br>
	 * If and only if <b>this</b> is a horizontal axis, then this option is only
	 * supported for a continuous axis.
	 */
	public Axis setBaselineColor(MDColor baselineColor) {
		this.baselineColor = baselineColor;
		return this;
	}

	public HAxisDirection getDirection() {
		return HAxisDirection.from(direction);
	}

	/**
	 * The direction in which the values along the horizontal/vertical axis grow.
	 */
	public Axis setDirection(HAxisDirection direction) {
		this.direction = direction.getDirection();
		return this;
	}

	public String getFormat() {
		return format;
	}

	/**
	 * A format for numeric axis labels. If and only if <b>this</b> is a horizontal
	 * axis, then this option is only supported for a continuous axis.
	 */
	public void setFormat(HAxisNumberFormat format) {
		this.format = format.getFormat();
	}

	/**
	 * A format for date axis labels.
	 * 
	 * * If and only if <b>this</b> is a horizontal axis, then this option is only
	 * supported for a continuous axis.
	 */
	public Axis setFormat(HAxisDateFormat format) {
		this.format = format.getFormat();
		return this;
	}

	public AxisGridlines getGridlines() {
		return gridlines;
	}

	/**
	 * This configures the gridlines on the horizontal/vertical axis.
	 * 
	 * * If and only if <b>this</b> is a horizontal axis, then this option is only
	 * supported for a continuous axis.
	 */
	public Axis setGridlines(AxisGridlines gridlines) {
		this.gridlines = gridlines;
		return this;
	}

	public AxisGridlines getMinorGridlines() {
		return minorGridlines;
	}

	/**
	 * This configures the minor gridlines on the horizontal/vertical axis. It is
	 * similar to gridlines
	 * 
	 * If and only if <b>this</b> is a horizontal axis, then this option is only
	 * supported for a continuous axis.
	 * 
	 */
	public Axis setMinorGridlines(AxisGridlines minorGridlines) {
		this.minorGridlines = minorGridlines;
		return this;
	}

	public Boolean getLogScale() {
		return logScale;
	}

	/**
	 * This makes the horizontal/vertical axis a logarithmic scale (requires all
	 * values to be positive). <br>
	 * Default: false <br>
	 * 
	 * If and only if <b>this</b> is a horizontal axis, then this option is only
	 * supported for a continuous axis.
	 */
	public Axis setLogScale(Boolean logScale) {
		this.logScale = logScale;
		return this;
	}

	public HAxisScaleType getScaleType() {
		return scaleType;
	}

	/**
	 * This makes the horizontal/vertical axis a logarithmic scale. <br>
	 * This option is only supported for a continuous axis (whether it is a
	 * horizontal or vertical axis).
	 * 
	 */
	public Axis setScaleType(HAxisScaleType scaleType) {
		this.scaleType = scaleType;
		return this;
	}

	public TextPosition getTextPosition() {
		return textPosition;
	}

	/**
	 * Position of the horizontal/vertical axis text, relative to the chart area.
	 */
	public Axis setTextPosition(TextPosition textPosition) {
		this.textPosition = textPosition;
		return this;
	}

	public TextStyle getTextStyle() {
		return textStyle;
	}

	/** This specifies the horizontal/vertical axis text style. */
	public Axis setTextStyle(TextStyle textStyle) {
		this.textStyle = textStyle;
		return this;
	}

	public ArrayList<HAxisTick> getTicks() {
		return ticks;
	}

	/**
	 * Add a new X or Y axis tick.
	 * 
	 * If and only if <b>this</b> is a horizontal axis, then this option is only
	 * supported for a continuous axis.
	 * 
	 * @see HAxisNumberTick
	 * @see HAxisDateTick
	 */
	public Axis addTick(HAxisTick tick) {
		this.ticks.add(tick);
		return this;
	}

	/**
	 * Set ticks for the X or Y axis.
	 * 
	 * If and only if <b>this</b> is a horizontal axis, then this option is only
	 * supported for a continuous axis.
	 * 
	 * @see HAxisNumberTick
	 * @see HAxisDateTick
	 */
	public Axis setTicks(ArrayList<HAxisTick> ticks) {
		this.ticks = ticks;
		return this;
	}

	public String getTitle() {
		return title;
	}

	/** This property specifies the title of the horizontal/vertical axis. */
	public Axis setTitle(String title) {
		this.title = title;
		return this;
	}

	public TextStyle getTitleTextStyle() {
		return titleTextStyle;
	}

	/** This specifies the horizontal/vertical axis title text style. */
	public Axis setTitleTextStyle(TextStyle titleTextStyle) {
		this.titleTextStyle = titleTextStyle;
		return this;
	}

	public Boolean getAllowContainerBoundaryTextCufoff() {
		return allowContainerBoundaryTextCufoff;
	}

	/**
	 * If false, will hide outermost labels rather than allow them to be cropped by
	 * the chart container. If true, will allow label cropping. <br>
	 * This option is only supported for a discrete horizontal axis. <br>
	 * Default: false. <br>
	 * 
	 */
	public Axis setAllowContainerBoundaryTextCufoff(Boolean allowContainerBoundaryTextCufoff) {
		this.allowContainerBoundaryTextCufoff = allowContainerBoundaryTextCufoff;
		return this;
	}

	public Boolean getSlantedText() {
		return slantedText;
	}

	/**
	 * If true, draw the horizontal axis text at an angle, to help fit more text
	 * along the axis; if false, draw horizontal axis text upright. Default behavior
	 * is to slant text if it cannot all fit when drawn upright. Notice that this
	 * option is available only when the <b>textPosition</b> is set to 'out' (which
	 * is the default). <br>
	 * This option is only supported for a discrete horizontal axis.
	 */
	public Axis setSlantedText(Boolean slantedText) {
		this.slantedText = slantedText;
		return this;
	}

	public Number getSlantedTextAngle() {
		return slantedTextAngle;
	}

	/**
	 * The angle of the horizontal axis text, if it's drawn slanted. Ignored if
	 * <b>slantedText</b> is false, or is in auto mode, and the chart decided to
	 * draw the text horizontally. <br>
	 * This option is only supported for a discrete horizontal axis. <br>
	 * Default: 30. <br>
	 */
	public Axis setSlantedTextAngle(Number slantedTextAngle) {
		this.slantedTextAngle = slantedTextAngle;
		return this;
	}

	public Number getMaxAlternation() {
		return maxAlternation;
	}

	/**
	 * Maximum number of levels of horizontal axis text. If axis text labels become
	 * too crowded, the server might shift neighboring labels up or down in order to
	 * fit labels closer together. This value specifies the most number of levels to
	 * use; the server can use fewer levels, if labels can fit without overlapping.
	 * <br>
	 * This option is only supported for a discrete horizontal axis. <br>
	 * Default: 2
	 */
	public Axis setMaxAlternation(Number maxAlternation) {
		this.maxAlternation = maxAlternation;
		return this;
	}

	public Number getMaxTextLines() {
		return maxTextLines;
	}

	/**
	 * Maximum number of lines allowed for the text labels. Labels can span multiple
	 * lines if they are too long, and the number of lines is, by default, limited
	 * by the height of the available space. <br>
	 * This option is only supported for a discrete horizontal axis. <br>
	 */
	public Axis setMaxTextLines(Number maxTextLines) {
		this.maxTextLines = maxTextLines;
		return this;
	}

	public Number getMinTextSpacing() {
		return minTextSpacing;
	}

	/**
	 * Minimum horizontal spacing, in pixels, allowed between two adjacent text
	 * labels. If the labels are spaced too densely, or they are too long, the
	 * spacing can drop below this threshold, and in this case one of the
	 * label-unclutter measures will be applied (e.g, truncating the lables or
	 * dropping some of them). <br>
	 * This option is only supported for a discrete horizontal axis. <br>
	 * It defaults to the fontSize of the <b>textStyle</b>.
	 */
	public Axis setMinTextSpacing(Number minTextSpacing) {
		this.minTextSpacing = minTextSpacing;
		return this;
	}

	public Number getShowTextEvery() {
		return showTextEvery;
	}

	/**
	 * How many horizontal axis labels to show, where 1 means show every label, 2
	 * means show every other label, and so on. Default is to try to show as many
	 * labels as possible without overlapping. <br>
	 * 
	 * This option is only supported for a discrete horizontal axis. <br>
	 */
	public Axis setShowTextEvery(Number showTextEvery) {
		this.showTextEvery = showTextEvery;
		return this;
	}

	public Number getMaxValue() {
		return maxValue;
	}

	/**
	 * Moves the max value of the horizontal/vertical axis to the specified value;
	 * In most charts, this will be rightward(for horizontal axis), and upward (for
	 * vertical axis). Ignored if this is set to a value smaller than the maximum
	 * x/y value of the data. <b>viewWindow.max</b> overrides this property. <br>
	 * 
	 * This option is not supported for a non-continuous horizontal axis (if
	 * applicable). <br>
	 */
	public Axis setMaxValue(Number maxValue) {
		this.maxValue = maxValue;
		return this;
	}

	public Number getMinValue() {
		return minValue;
	}

	/**
	 * Moves the min value of the horizontal/vertical axis to the specified value; *
	 * In most charts, this will be leftward(for horizontal axis), and downward (for
	 * vertical axis). Ignored if this is set to a value greater than the minimum
	 * x/y value of the data. <b>viewWindow.min</b> overrides this property. <br>
	 * 
	 * This option is not supported for a non-continuous horizontal axis (if
	 * applicable). <br>
	 */
	public Axis setMinValue(Number minValue) {
		this.minValue = minValue;
		return this;
	}

	public HAxisViewWindowMode getViewWindowMode() {
		return viewWindowMode;
	}

	/**
	 * This specifies how to scale the horizontal axis to render the values within
	 * the chart area. <br>
	 * This option is only supported for a continuous axis. <br>
	 * Default: Equivalent to 'pretty', but <b>viewWindow.min</b> and
	 * <b>viewWindow.max</b> take precedence if used.
	 */
	public Axis setViewWindowMode(HAxisViewWindowMode viewWindowMode) {
		this.viewWindowMode = viewWindowMode;
		return this;
	}

}
