package com.kylantis.eaa.core.components.charts;

import com.kylantis.eaa.core.components.BaseComponent;
import com.kylantis.eaa.core.components.styling.BackgroundColor;
import com.kylantis.eaa.core.utils.NumberUtil;

public class ChartArea {

	private BackgroundColor backgroundColor;
	
	private String left;
	private String top;
	private String width;
	private String height;

	public ChartArea(BaseComponent component) {
		this.backgroundColor = new BackgroundColor(component);
	}

	public BackgroundColor getBackgroundColor() {
		return backgroundColor;
	}

	/** Chart area background color. */
	public void setBackgroundColor(BackgroundColor backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public String getLeft() {
		return left;
	}

	/** How far to draw the chart from the left border. */
	public void setLeft(Number left) {
		this.left = NumberUtil.asPercentile(left);
	}

	public String getTop() {
		return top;
	}

	/** How far to draw the chart from the top border. */
	public void setTop(Number top) {
		this.top = NumberUtil.asPercentile(top);
	}

	public String getWidth() {
		return width;
	}

	/** Chart area width in percentile. */
	public void setWidth(Number width) {
		this.width = NumberUtil.asPercentile(width);
	}

	public String getHeight() {
		return height;
	}

	/** Chart area height in percentile. */
	public void setHeight(Number height) {
		this.height = NumberUtil.asPercentile(height);
	}

}
