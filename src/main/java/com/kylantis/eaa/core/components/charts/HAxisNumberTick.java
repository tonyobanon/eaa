package com.kylantis.eaa.core.components.charts;

import com.kylantis.eaa.core.components.Theme;
import com.kylantis.eaa.core.components.ValueTransformContext;

public class HAxisNumberTick extends HAxisTickElement {

	private Number v;
	private String f;
	
	public HAxisNumberTick(Number v) {
		this.v = v;
		this.f = (String) Theme.getDefault().getValueTransformStrategy().transform(ValueTransformContext.CHART_HAXIS_NUMBER_TICK, v);
	}

	public Number getV() {
		return v;
	}

	public String getF() {
		return f;
	}

}
