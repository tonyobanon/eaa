package com.kylantis.eaa.core.components;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.kylantis.eaa.core.ClassIdentityType;
import com.kylantis.eaa.core.ComputeIntensive;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.ComponentSpec;
import com.kylantis.eaa.core.attributes.NamespaceSpec;
import com.kylantis.eaa.core.attributes.PageSpec;
import com.kylantis.eaa.core.base.ClasspathScanner;
import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.base.Logger;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.config.Namespace;
import com.kylantis.eaa.core.dbutils.DBTooling;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IComponentModel;
import com.kylantis.eaa.core.system.ResourceBundleKeyRef;
import com.kylantis.eaa.core.users.RoleRealm;
import com.kylantis.eaa.core.users.RoleUpdateAction;

@ConcreteModel
public class ComponentModel extends IComponentModel {

	public static final String ROOT_PARENT_ID = "0";

	@Override
	public void preInstall() {

		// Scan for namespaces
		new ClasspathScanner<>("Namespace", Namespace.class, ClassIdentityType.SUPER_CLASS).scanClasses().forEach(n -> {

			// Make all declared constructor(s) accessible
			for (Constructor<?> constr : n.getDeclaredConstructors()) {
				constr.setAccessible(true);
			}

			// Create namespace instance
			Namespace namespace = null;
			try {
				namespace = n.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				Logger.error(e.getMessage());
			}

			if (namespace != null) {
				// Create namespace
				newNamespace(NamespaceProperties.from(namespace));
			}
		});
	}

	@Override
	public void start() {

		Map<Id, String> components = new HashMap<>();

		// get all namespaces
		this.listNamespaces().forEach((k, v) -> {

			// get all pages
			this.listPageKeys(Namespace.fromPrefix(k)).forEach(p -> {

				Page page = this.getPage(p);

				Grid grid = this.getModel(page.getPath()).render(p);

				for (AbstractComponent c : grid.getElements()) {

					components.put(page.getId(), c.hash());

					// create component, if necessary
					newComponent(c);
				}

			});
		});

		components.forEach((p, c) -> {
			listComponentKeys(p).forEach(id -> {

				if (!components.containsValue(id)) {
					deleteComponent(id, false);
				}
			});
		});

	}

	@PlatformInternal
	public void newNamespace(NamespaceProperties namespace) {

		Logger.debug("Creating namespace: " + namespace.getPrefix());

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(NamespaceSpec.TABLE_NAME);

		Item item = new Item().withPrimaryKey(NamespaceSpec.PREFIX, namespace.getPrefix())
				.withBoolean(NamespaceSpec.IS_INTERNAL, namespace.isInternal())
				.withInt(NamespaceSpec.REALM, namespace.getRoleRealm().getRealm());

		try {
			table.putItem(new PutItemSpec().withItem(item)
					.withNameMap(new FluentHashMap<String, String>().with("#k", NamespaceSpec.PREFIX))
					.withConditionExpression("attribute_not_exists(#k)"));
		} catch (ConditionalCheckFailedException e) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS);
		}
	}

	@PlatformInternal
	public NamespaceProperties getNamespace(Namespace namespace) {

		Item i = StorageServiceFactory.getDocumentDatabase().getTable(NamespaceSpec.TABLE_NAME)
				.getItem(new GetItemSpec().withPrimaryKey(NamespaceSpec.PREFIX, namespace.getPrefix())
						.withProjectionExpression("#p,#i,#r")
						.withNameMap(new FluentHashMap<String, String>().with("#p", NamespaceSpec.PREFIX)
								.with("#i", NamespaceSpec.IS_INTERNAL).with("#r", NamespaceSpec.REALM)));

		NamespaceProperties o = new NamespaceProperties();
		o.setPrefix(i.getString(NamespaceSpec.PREFIX));
		o.setIsInternal(i.getBoolean(NamespaceSpec.IS_INTERNAL));
		o.setRoleRealm(RoleRealm.from(i.getInt(NamespaceSpec.REALM)));

		return o;
	}

	@PlatformInternal
	public RoleRealm getNamespaceRealm(Namespace namespace) {
		Item i = StorageServiceFactory.getDocumentDatabase().getTable(NamespaceSpec.TABLE_NAME)
				.getItem(new GetItemSpec().withPrimaryKey(NamespaceSpec.PREFIX, namespace.getPrefix())
						.withProjectionExpression("#r")
						.withNameMap(FluentHashMap.forNameMap().with("#r", NamespaceSpec.REALM)));
		return RoleRealm.from(i.getInt(NamespaceSpec.REALM));
	}

	@PlatformInternal
	public Map<String, NamespaceProperties> listNamespaces() {

		Map<String, NamespaceProperties> namespaces = new HashMap<>();

		DBTooling.scanTable(NamespaceSpec.TABLE_NAME, "#p,#i,#r", new FluentHashMap<String, String>()
				.with("#p", NamespaceSpec.PREFIX).with("#i", NamespaceSpec.IS_INTERNAL).with("#r", NamespaceSpec.REALM))
				.forEach(i -> {

					NamespaceProperties o = new NamespaceProperties();
					o.setPrefix(i.getString(NamespaceSpec.PREFIX));
					o.setIsInternal(i.getBoolean(NamespaceSpec.IS_INTERNAL));
					o.setRoleRealm(RoleRealm.from(i.getInt(NamespaceSpec.REALM)));

					namespaces.put(o.getPrefix(), o);

				});
		return namespaces;
	}

	@PlatformInternal
	public List<NamespaceProperties> getNamespaces(List<Namespace> namespaces) {

		List<Object> keys = new ArrayList<>(namespaces.size());
		namespaces.forEach(nx -> {
			keys.add(nx.getPrefix());
		});

		List<NamespaceProperties> result = new ArrayList<>();

		DBTooling.batchGet(NamespaceSpec.TABLE_NAME, NamespaceSpec.PREFIX, keys, "#p,#i,#r",
				new FluentHashMap<String, String>().with("#p", NamespaceSpec.PREFIX)
						.with("#i", NamespaceSpec.IS_INTERNAL).with("#r", NamespaceSpec.REALM))
				.forEach(i -> {

					NamespaceProperties o = new NamespaceProperties();
					o.setPrefix(i.getString(NamespaceSpec.PREFIX));
					o.setIsInternal(i.getBoolean(NamespaceSpec.IS_INTERNAL));
					o.setRoleRealm(RoleRealm.from(i.getInt(NamespaceSpec.REALM)));

					result.add(o);
				});

		return result;
	}

	@PlatformInternal
	public void deleteNamespace(Namespace namespace, boolean forceDelete) {

		listPageKeys(namespace).forEach(e -> {
			if (!forceDelete) {
				throw new ResourceException(ResourceException.RESOURCE_STILL_IN_USE);
			}
			deletePage(e, forceDelete);
		});

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(NamespaceSpec.TABLE_NAME);
		table.deleteItem(NamespaceSpec.PREFIX, namespace.getPrefix());
	}

	public void newPage(Page page) {

		// Validate
		if (!page.getId().getNamespace().getBasePrefix().equals(page.getParent().getNamespace().getBasePrefix())) {
			throw new ResourceException(ResourceException.FAILED_VALIDATION,
					"The page and its parent page must belong to the same namespace.");
		}

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(PageSpec.TABLE_NAME);

		Item item = new Item().withPrimaryKey(PageSpec.ID, page.getId().getId())
				.withString(PageSpec.TITLE, page.getTitle().getKey()).withInt(PageSpec.SORT_ORDER, page.getSortOrder())
				.withString(PageSpec.PARENT, page.getParent() != null ? page.getParent().getId() : ROOT_PARENT_ID)
				.withString(PageSpec.PATH, page.getPath()).withString(PageSpec.GROUP, page.getGroup().toString())
				.withString(PageSpec.NAMESPACE, page.getNamespace().getPrefix());

		try {
			table.putItem(new PutItemSpec().withItem(item)
					.withNameMap(new FluentHashMap<String, String>().with("#k", PageSpec.ID))
					.withConditionExpression("attribute_not_exists(#k)"));
		} catch (ConditionalCheckFailedException e) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS);
		}
	}

	@PlatformInternal
	public void deletePage(Id page, boolean forceDelete) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(PageSpec.TABLE_NAME);
		table.deleteItem(PageSpec.ID, page.getId());

		// Delete all components
		listComponentKeys(page).forEach(e -> {
			if (!forceDelete) {
				throw new ResourceException(ResourceException.RESOURCE_STILL_IN_USE);
			}
			deleteComponent(e, false);
		});
	}

	@PlatformInternal
	public List<Id> listPageKeys(Namespace namespace) {

		List<Item> result = DBTooling.queryIndex(
				StorageServiceFactory.getDocumentDatabase().getTable(PageSpec.TABLE_NAME)
						.getIndex(PageSpec.NAMESPACE_INDEX),
				"#id", FluentHashMap.forNameMap().with("#id", PageSpec.ID).with("#k", PageSpec.NAMESPACE),
				FluentHashMap.forValueMap().with(":v", namespace.getPrefix()), "#k = :v");

		List<Id> keys = new ArrayList<>();

		result.forEach(i -> {
			keys.add(Id.from(i.getString(PageSpec.ID)));
		});

		return keys;
	}

	public List<Page> getPages(List<Object> keys) {

		List<Item> outcome = DBTooling.batchGet(PageSpec.TABLE_NAME, PageSpec.ID, keys, "#id,#t,#p,#so,#g,#pt,#n",
				FluentHashMap.forNameMap().with("#id", PageSpec.ID).with("#t", PageSpec.TITLE)
						.with("#p", PageSpec.PARENT).with("#so", PageSpec.SORT_ORDER).with("#g", PageSpec.GROUP)
						.with("#pt", PageSpec.PATH).with("#n", PageSpec.NAMESPACE));

		List<Page> entries = new ArrayList<>();

		outcome.forEach(i -> {

			Page o = new Page();

			o.setId(Id.from(i.getString(PageSpec.ID)));
			o.setTitle(new ResourceBundleKeyRef().setKey(i.getString(PageSpec.TITLE)));
			o.setParent(Id.from(i.getString(PageSpec.PARENT)));
			o.setSortOrder(i.getInt(PageSpec.SORT_ORDER));
			o.setGroup(PageGroup.from(i.getInt(PageSpec.GROUP)));
			o.setNamespace(Namespace.fromPrefix(i.getString(PageSpec.NAMESPACE)));

			entries.add(o);
		});

		return entries;
	}

	public Page getPage(Id id) {

		Item i = StorageServiceFactory.getDocumentDatabase().getTable(PageSpec.TABLE_NAME)
				.getItem(new GetItemSpec().withPrimaryKey(PageSpec.ID, id.getId())
						.withProjectionExpression("#id,#t,#p,#so,#g,#pt,#n")
						.withNameMap(FluentHashMap.forNameMap().with("#id", PageSpec.ID).with("#t", PageSpec.TITLE)
								.with("#p", PageSpec.PARENT).with("#so", PageSpec.SORT_ORDER).with("#g", PageSpec.GROUP)
								.with("#pt", PageSpec.PATH).with("#n", PageSpec.NAMESPACE)));
		Page o = new Page();

		o.setId(Id.from(i.getString(PageSpec.ID)));
		o.setTitle(new ResourceBundleKeyRef().setKey(i.getString(PageSpec.TITLE)));
		o.setParent(Id.from(i.getString(PageSpec.PARENT)));
		o.setSortOrder(i.getInt(PageSpec.SORT_ORDER));
		o.setGroup(PageGroup.from(i.getInt(PageSpec.GROUP)));
		o.setNamespace(Namespace.fromPrefix(i.getString(PageSpec.NAMESPACE)));

		return o;
	}

	public Namespace getPageNamespace(Id id) {
		Item i = StorageServiceFactory.getDocumentDatabase().getTable(PageSpec.TABLE_NAME)
				.getItem(new GetItemSpec().withPrimaryKey(PageSpec.ID, id.getId()).withProjectionExpression("#n")
						.withNameMap(FluentHashMap.forNameMap().with("#n", PageSpec.NAMESPACE)));
		return Namespace.fromPrefix(i.getString(PageSpec.NAMESPACE));
	}

	public void buildComponentTree() {

		// Scan for all Namespaces, Group by Realm and Cache it

		// Scan for all Pages, and Cache It, i.e listPageKeys , e.t.c

		// Scan for all Components and Cache It

	}

	@ComputeIntensive
	@PlatformInternal
	public Map<Id, String> getComponentHashes() {
		Map<Id, String> hashes = new HashMap<>();
		DBTooling
				.scanTable(ComponentSpec.TABLE_NAME, "#i, #h",
						FluentHashMap.forNameMap().with("#i", ComponentSpec.ID).with("#h", ComponentSpec.HASH))
				.forEach(i -> {
					hashes.put(Id.from(i.getString(ComponentSpec.ID)), i.getString(ComponentSpec.HASH));
				});
		return hashes;
	}

	/**
	 * 
	 * @return {@link List<Id>}
	 */
	@PlatformInternal
	public List<String> listComponentKeys(Id page) {

		List<Item> result = DBTooling.queryIndex(
				StorageServiceFactory.getDocumentDatabase().getTable(ComponentSpec.TABLE_NAME)
						.getIndex(ComponentSpec.PAGE_INDEX),
				"#id", FluentHashMap.forNameMap().with("#id", ComponentSpec.ID).with("#k", ComponentSpec.PAGE),
				FluentHashMap.forValueMap().with(":v", page.getId()), "#k = :v");

		List<String> keys = new ArrayList<>();

		result.forEach(i -> {
			keys.add(i.getString(ComponentSpec.ID));
		});

		return keys;
	}

	// ComponentSpec.ENABLED
	public List<AbstractComponent> getComponents(List<Object> keys) {

		// @TODO: Fetch from cache if available

		List<Item> outcome = DBTooling.batchGet(ComponentSpec.TABLE_NAME, ComponentSpec.ID, keys, "#im",
				FluentHashMap.forNameMap().with("#im", ComponentSpec.IMPLEMENTATION));

		List<AbstractComponent> entries = new ArrayList<>();

		outcome.forEach(i -> {
			try {
				entries.add((BaseComponent) Class.forName(i.getString(ComponentSpec.IMPLEMENTATION)).newInstance());
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
			}
		});

		return entries;
	}

	// ComponentSpec.ENABLED
	public AbstractComponent getComponent(String id) {

		// @TODO: Fetch from cache if available

		Item i = StorageServiceFactory.getDocumentDatabase().getTable(ComponentSpec.TABLE_NAME)
				.getItem(new GetItemSpec().withPrimaryKey(PageSpec.ID, id).withProjectionExpression("#im,#en")
						.withNameMap(FluentHashMap.forNameMap().with("#im", ComponentSpec.IMPLEMENTATION).with("#en",
								ComponentSpec.ENABLED)));

		if (!i.getBoolean(ComponentSpec.ENABLED)) {
			return null;
		}

		try {
			BaseComponent component = (BaseComponent) Class.forName(i.getString(ComponentSpec.IMPLEMENTATION))
					.newInstance();

			return component;
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}
	}

	/**
	 * This either enables or disables a component
	 */
	public void toggleComponent(String id) {

	}

	@PlatformInternal
	public void newComponent(AbstractComponent component) {

		if (component.page() == null) {
			Logger.error("Component: " + component.hash() + " could not be created because no parent page was found.");
			return;
		}

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(ComponentSpec.TABLE_NAME);

		String id = component.hash();

		Item item = new Item().withPrimaryKey(ComponentSpec.ID, id)
				.withInt(ComponentSpec.SORT_ORDER, component.sortOrder())
				.withString(ComponentSpec.PAGE, component.page().getId())
				.withString(ComponentSpec.IMPLEMENTATION, component.getClass().getSimpleName())
				.withBoolean(ComponentSpec.ENABLED, true).withString(ComponentSpec.HASH, component.hash());

		try {
			table.putItem(new PutItemSpec().withItem(item)
					.withNameMap(FluentHashMap.forNameMap().with("#k", ComponentSpec.ID))
					.withConditionExpression("attribute_not_exists(#k)"));
		} catch (ConditionalCheckFailedException e) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS);
		}

		// Get role realm for this component
		RoleRealm realm = getNamespaceRealm(getPageNamespace(component.page()));

		// Register as new functionality for the default role in the specified
		// realm
		getRolesModel().updateFunctionality(getRolesModel().getDefaultRole(realm), RoleUpdateAction.ADD, id);
	}

	public void deleteComponent(String id, boolean deleteImpl) {

		AbstractComponent component = getComponent(id);

		StorageServiceFactory.getDocumentDatabase().getTable(ComponentSpec.TABLE_NAME).deleteItem(ComponentSpec.ID, id);

		// Remove functionality from the default role in the associated
		// realm

		// Get role realm for this component
		RoleRealm realm = getNamespaceRealm(getPageNamespace(component.page()));

		// Update default role
		getRolesModel().updateFunctionality(getRolesModel().getDefaultRole(realm), RoleUpdateAction.REMOVE, id);

		if (deleteImpl) {

			// Delete Component Class
			Path path = ClasspathScanner.getAppBaseDir()
					.resolve(component.getClass().getSimpleName().replaceAll(Pattern.quote("."), File.separator));
			try {
				Files.delete(path);
			} catch (IOException e) {
				Exceptions.throwRuntime(e);
			}
		}
	}
}
