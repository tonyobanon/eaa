package com.kylantis.eaa.core.components;

public interface ValueTransformStrategy {
	
	public Object transform(ValueTransformContext ctx, Object o);
	
}
