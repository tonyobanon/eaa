package com.kylantis.eaa.core.components.charts;

public enum LegendAlignment {
	
	/**Aligned to the start of the area allocated for the legend.*/
 START("start"), 
 
 /**Centered in the area allocated for the legend.*/
 CENTER("center"), 
 
 /**Aligned to the end of the area allocated for the legend.*/
 END("end");
	
	private final String value;

	private LegendAlignment(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return this.value;
	}
}
