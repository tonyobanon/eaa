package com.kylantis.eaa.core.components;

import com.kylantis.eaa.core.components.charts.ScaleType;
import com.kylantis.eaa.core.components.styling.MDColor;
import com.kylantis.eaa.core.config.Id;

public class MyAnnotationChart extends AnnotationChart {

	public MyAnnotationChart() {
		this.setThickness(3).setScaleType(ScaleType.FIXED).setColors(MDColor.teal_600);
	}

	@Override
	protected String title() {
		return "My Annotation Chart";
	}

	@Override
	protected Id page() {
		return AdminUI.DASHBOARD_INDEX_PAGE;
	}

	@Override
	AbstractDataSource setDataSource() {
		// Return a Data Source here
		return null;
	}

}
