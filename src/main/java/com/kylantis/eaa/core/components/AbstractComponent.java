package com.kylantis.eaa.core.components;

import com.kylantis.eaa.core.config.Id;

public abstract class AbstractComponent {

	protected static final String DEFAULT_COMPONENT_SUMMARY = "";
	protected static final Integer DEFAULT_COMPONENT_SORT_ORDER = 0;

	protected abstract String title();

	protected Id page() {
		return null;
	}

	protected abstract ComponentType type();

	protected String summary() {
		return DEFAULT_COMPONENT_SUMMARY;
	}

	protected int sortOrder() {
		return DEFAULT_COMPONENT_SORT_ORDER;
	}

	public String hash() {
		return Integer.toString(this.hashCode());
	}
	
	@Override
	public final int hashCode() {
		// Exclude sortOrder, summary
		return page().hashCode() - title().intern().hashCode();
	}

}
