package com.kylantis.eaa.core.components.charts;

public class HAxisTick {

	private HAxisTickType type;
	private HAxisTickElement data;

	public HAxisTickType getType() {
		return type;
	}

	public HAxisTick setType(HAxisTickType type) {
		this.type = type;
		return this;
	}

	public HAxisTickElement getData() {
		return data;
	}

	public HAxisTick setData(HAxisTickElement data) {
		this.data = data;
		return this;
	}
}
