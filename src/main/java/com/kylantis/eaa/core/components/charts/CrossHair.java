package com.kylantis.eaa.core.components.charts;

import com.kylantis.eaa.core.components.styling.MDColor;
import com.kylantis.eaa.core.utils.NumberUtil;

public class CrossHair {

	private MDColor color;
	private OpaqueColor focused;
	private Number opacity;
	private Orientation orientation;
	private OpaqueColor selected;
	private CrossHairTrigger trigger;

	public MDColor getColor() {
		return color;
	}

	/** The crosshair color */
	public void setColor(MDColor color) {
		this.color = color;
	}

	public OpaqueColor getFocused() {
		return focused;
	}

	/** Set the crosshair properties upon focus. */
	public void setFocused(OpaqueColor focused) {
		this.focused = focused;
	}

	public Number getOpacity() {
		return opacity;
	}

	/**
	 * The crosshair opacity, with 0.0 being fully transparent and 1.0 fully opaque.
	 */
	public void setOpacity(Number opacity) {
		this.opacity = NumberUtil.asReal(opacity);
	}

	public Orientation getOrientation() {
		return orientation;
	}

	/**
	 * The crosshair orientation, which can be 'vertical' for vertical hairs only,
	 * 'horizontal' for horizontal hairs only, or 'both' for traditional crosshairs.
	 */
	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}

	public OpaqueColor getSelected() {
		return selected;
	}

	/** Set the crosshair properties upon selected. */
	public void setSelected(OpaqueColor selected) {
		this.selected = selected;
	}

	public CrossHairTrigger getTrigger() {
		return trigger;
	}

	/** Set when to display crosshairs: on 'focus', 'selection', or 'both'. */
	public void setTrigger(CrossHairTrigger trigger) {
		this.trigger = trigger;
	}
}
