package com.kylantis.eaa.core.components;

public enum PageGroup {

	SIDEBAR(1), TOPBAR(2);

	private final Integer group;

	private PageGroup(int group) {
		this.group = group;
	}

	public Integer getGroup() {
		return group;
	}

	public static PageGroup from(int group) {
		switch (group) {
		case 1:
			return SIDEBAR;
		case 2:
		default:
			return TOPBAR;
		}
	}

	@Override
	public String toString() {
		switch (this) {
		case SIDEBAR:
		default:
			return "SideBar";
		case TOPBAR:
			return "TopBar";
		}
	}

}
