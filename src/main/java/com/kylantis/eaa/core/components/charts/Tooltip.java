package com.kylantis.eaa.core.components.charts;

public class Tooltip {

	private Boolean ignoreBounds;
	private Boolean isHtml;

	private Boolean showColorCode;
	private TextStyle textStyle;
	private TooltipTrigger trigger;

	public Boolean getIgnoreBounds() {
		return ignoreBounds;
	}

	/**
	 * If set to <b>true</b>, allows the drawing of tooltips to flow outside of the
	 * bounds of the chart on all sides. <br>
	 * Note: This only applies to HTML tooltips. If this is enabled with SVG
	 * tooltips, any overflow outside of the chart bounds will be cropped. <br>
	 * Default: false
	 *
	 */
	public Tooltip setIgnoreBounds(Boolean ignoreBounds) {
		this.ignoreBounds = ignoreBounds;
		return this;
	}

	public Boolean getIsHtml() {
		return isHtml;
	}

	/**
	 * If set to true, use HTML-rendered (rather than SVG-rendered) tooltips. <br>
	 * Default: false
	 */
	public Tooltip setIsHtml(Boolean isHtml) {
		this.isHtml = isHtml;
		return this;
	}

	public Boolean getShowColorCode() {
		return showColorCode;
	}

	/**
	 * If true, show colored squares next to the series information in the tooltip.
	 * The default is true when <b>focusTarget</b> is set to 'category', otherwise
	 * the default is false.
	 */
	public Tooltip setShowColorCode(Boolean showColorCode) {
		this.showColorCode = showColorCode;
		return this;
	}

	public TextStyle getTextStyle() {
		return textStyle;
	}

	/** This specifies the tooltip text style. */
	public Tooltip setTextStyle(TextStyle textStyle) {
		this.textStyle = textStyle;
		return this;
	}

	public TooltipTrigger getTrigger() {
		return trigger;
	}

	/**
	 * The user interaction that causes the tooltip to be displayed <br>
	 * Default: 'focus'
	 */
	public Tooltip setTrigger(TooltipTrigger trigger) {
		this.trigger = trigger;
		return this;
	}

}
