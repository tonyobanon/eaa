package com.kylantis.eaa.core.components.charts;

public enum TimeUnit {

	MILLISECONDS("milliseconds"), SECONDS("seconds"), MINUTES("minutes"), HOURS("hours"), DAYS("days"), MONTHS(
			"months"), YEARS("years");

	private final String unit;

	private TimeUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		return unit;
	}

}
