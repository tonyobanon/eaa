package com.kylantis.eaa.core.components.charts;

import com.kylantis.eaa.core.components.BarChart;
import com.kylantis.eaa.core.components.ColumnChart;

public class Annotations {

	private Boolean alwaysOutside;
	private AnnotationBoxStyle boxStyle;
	private AnnotationsAxisSpec datum;
	private AnnotationsAxisSpec domain;
	private Boolean highContrast;
	private AnnotationsStem stem;
	private AnnotationsStyle style;
	private TextStyle textStyle;

	public AnnotationBoxStyle getBoxStyle() {
		return boxStyle;
	}

	/** This controls the appearance of the boxes surrounding annotations: */
	public void setBoxStyle(AnnotationBoxStyle boxStyle) {
		this.boxStyle = boxStyle;
	}

	public AnnotationsAxisSpec getAnnotationSpec(AnnotationsEntityType type) {
		switch (type) {
		case DOMAIN:
			return this.domain;
		case DATUM:
		default:
			return this.datum;
		}
	}

	/**
	 * This lets you override the default choice for annotations provided for either
	 * individual data elements or a domain.
	 * 
	 * @param type
	 *            Either "datum" or "domain"
	 */
	public void setAnnotationSpec(AnnotationsEntityType type, AnnotationsAxisSpec spec) {
		switch (type) {
		case DOMAIN:
			this.domain = spec;
			break;
		case DATUM:
			this.datum = spec;
			break;
		}
	}

	public Boolean getHighContrast() {
		return highContrast;
	}

	/**
	 * This lets you override the default choice of the annotation color. By
	 * default, this is true, which causes Charts to select an annotation color with
	 * good contrast: light colors on dark backgrounds, and dark on light. If you
	 * set this to false and don't specify your own annotation color, the default
	 * series color for the annotation will be used
	 */
	public void setHighContrast(Boolean highContrast) {
		this.highContrast = highContrast;
	}

	public AnnotationsStem getStem() {
		return stem;
	}

	/**
	 * This lets you override the default choice for the stem style. Note that the
	 * stem length option has no effect on annotations with style 'line': for 'line'
	 * datum annotations, the stem length is always the same as the text, and for
	 * 'line' domain annotations, the stem extends across the entire chart.
	 */
	public void setStem(AnnotationsStem stem) {
		this.stem = stem;
	}

	public AnnotationsStyle getStyle() {
		return style;
	}

	/** This option lets you override the default choice of the annotation type. */
	public void setStyle(AnnotationsStyle style) {
		this.style = style;
	}

	public TextStyle getTextStyle() {
		return textStyle;
	}

	/** This controls the appearance of the text of the annotation: */
	public void setTextStyle(TextStyle textStyle) {
		this.textStyle = textStyle;
	}

	public Boolean getAlwaysOutside() {
		return alwaysOutside;
	}

	/**
	 * For {@link BarChart } and {@link ColumnChart } only. If set to true, draws all annotations outside of the Bar/Column.
	 * Default false;
	 * */
	public Annotations setAlwaysOutside(Boolean alwaysOutside) {
		this.alwaysOutside = alwaysOutside;
		return this;
	}

}
