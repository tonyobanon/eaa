package com.kylantis.eaa.core.components;

import java.util.HashMap;
import java.util.Map;

import com.kylantis.eaa.core.components.styling.MDColor;
import com.kylantis.eaa.core.components.styling.Sizes;
import com.kylantis.eaa.core.utils.NumberUtil;

public abstract class BaseComponent extends AbstractComponent {

	private Map<Object, Object> updates = new HashMap<>();

	public abstract Sizes minWidth();

	public abstract Iterable<MDColor> themeColor();

	
	protected void render() {

		//Get rendering context
		
		
	}
	
	protected void addUpdate() {
		
	}

	protected abstract void init();

	protected String asPercentile(Number value) {
		return NumberUtil.asPercentile(value);
	}

	protected Number asReal(Number value) {
		return NumberUtil.asReal(value);
	}
}
