package com.kylantis.eaa.core.components.charts;

import com.kylantis.eaa.core.components.styling.MDColor;

public class AnnotationBoxStyle {

	private MDColor stroke;
	private Number strokeWidth;
	private Number rx;
	private Number ry;
	private AnnotationBoxStyleGradient gradient;

	public MDColor getStroke() {
		return stroke;
	}

	/** Color of the box outline. */
	public void setStroke(MDColor stroke) {
		this.stroke = stroke;
	}

	public Number getStrokeWidth() {
		return strokeWidth;
	}

	/** Thickness of the box outline. */
	public void setStrokeWidth(Number strokeWidth) {
		this.strokeWidth = strokeWidth;
	}

	public Number getRx() {
		return rx;
	}

	/** x-radius of the corner curvature. */
	public void setRx(Number rx) {
		this.rx = rx;
	}

	public Number getRy() {
		return ry;
	}

	/** y-radius of the corner curvature. */
	public void setRy(Number ry) {
		this.ry = ry;
	}

	public AnnotationBoxStyleGradient getGradient() {
		return gradient;
	}

	/** Attributes for linear gradient fill. */
	public void setGradient(AnnotationBoxStyleGradient gradient) {
		this.gradient = gradient;
	}

}
