package com.kylantis.eaa.core.components.charts;

public enum TooltipTrigger {

	/** The tooltip will be displayed when the user hovers over the element.. */
	FOCUS("focus"),

	/** The tooltip will not be displayed. */
	NONE("none"),

	/** The tooltip will be displayed when the user selects the element. */
	SELECTION("selection");

	private final String value;

	private TooltipTrigger(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return this.value;
	}
}
