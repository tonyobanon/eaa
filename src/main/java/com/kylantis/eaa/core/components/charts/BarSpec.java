package com.kylantis.eaa.core.components.charts;

import com.kylantis.eaa.core.utils.NumberUtil;

public class BarSpec {

	private String groupWidth;

	public String getGroupWidth() {
		return groupWidth;
	}

	/**
	 * The width of a group of bars. It is specified as: Percentage of the available
	 * width for each group, value of 100 means that groups have no space between
	 * them.
	 */
	public BarSpec setGroupWidth(Number groupWidth) {
		this.groupWidth = NumberUtil.asPercentile(groupWidth) + "%";
		return this;
	}

}
