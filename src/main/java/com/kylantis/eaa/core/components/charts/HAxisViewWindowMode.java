package com.kylantis.eaa.core.components.charts;

public enum HAxisViewWindowMode {

	/**
	 * Scale the horizontal values so that the maximum and minimum data values are
	 * rendered a bit inside the left and right of the chart area.
	 */
	PRETTY("pretty"),

	/**
	 * Scale the horizontal values so that the maximum and minimum data values touch
	 * the left and right of the chart area.
	 */
	MAXIMIZED("maximized");

	private final String mode;

	private HAxisViewWindowMode(String mode) {
		this.mode = mode;
	}

	@Override
	public String toString() {
		return mode;
	}
}
