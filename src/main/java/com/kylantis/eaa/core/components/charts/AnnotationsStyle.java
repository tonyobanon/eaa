package com.kylantis.eaa.core.components.charts;

public enum AnnotationsStyle {
	
	LINE("line"), POINT("point");
	
	private final String style;

	private AnnotationsStyle(String style) {
		this.style = style;
	}

	@Override
	public String toString() {
		return style;
	}

}
