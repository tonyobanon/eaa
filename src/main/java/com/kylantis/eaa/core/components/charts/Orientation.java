package com.kylantis.eaa.core.components.charts;

public enum Orientation {
	
	VERTICAL("vertical"), HORIZONTAL("horizontal"), BOTH("both");

	private final String orientation;

	private Orientation(String orientation) {
		this.orientation = orientation;
	}

	@Override
	public String toString() {
		return orientation;
	}

}
