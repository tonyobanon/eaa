package com.kylantis.eaa.core.components.charts;

/**
 * 
 * This represents the format used to display the date information. The format
 * of this field is as specified by the <a href=
 * "http://icu-project.org/apiref/icu4c/classSimpleDateFormat.html#_details">ICU
 * pattern set</a> .
 * 
 */
@Review("Add more formats")
public enum DateFormat {

	MMMM_DD_YYYY("MMMM dd, yyyy"), HH_mm_MMMM_DD_YYYY("HH:mm MMMM dd, yyyy");

	private final String format;

	private DateFormat(String format) {
		this.format = format;
	}

	@Override
	public String toString() {
		return format;
	}

	public String getFormat() {
		return format;
	}
}
