package com.kylantis.eaa.core.components;

import com.kylantis.eaa.core.config.Id;

public class ComponentProperties {

	private Id id;

	private String title;
	private String summary;

	private Integer sortOrder;

	private ComponentType type;
	private Class<AbstractComponent> implementation;

	public Id getId() {
		return id;
	}

	public void setId(Id id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public ComponentType getType() {
		return type;
	}

	public void setType(ComponentType type) {
		this.type = type;
	}

	public Class<AbstractComponent> getImplementation() {
		return implementation;
	}

	public void setImplementation(Class<AbstractComponent> implementation) {
		this.implementation = implementation;
	}

}
