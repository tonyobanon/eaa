package com.kylantis.eaa.core.components.charts;

public enum AnnotationsEntityType {

	/**
	 * This represents individual data elements (such as values displayed with each
	 * bar on a chart).
	 */
	DATUM("datum"),

	/**
	 * This represents a domain (the major axis of the chart, such as the X axis on
	 * a typical line chart).
	 */
	DOMAIN("domain");

	private final String type;

	private AnnotationsEntityType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return type;
	}
}
