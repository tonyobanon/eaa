package com.kylantis.eaa.core.components.charts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.kylantis.eaa.core.components.styling.MDColor;

public class AxisGridlines {

	private static final String FORMAT = "format";

	private MDColor color;
	private Number count;
	private Map<TimeUnit, Map<String, ArrayList<String>>> units = new HashMap<>();

	public MDColor getColor() {
		return color;
	}

	/**
	 * The color of the horizontal/vertical gridlines inside the chart area.
	 * Default: '#CCC'
	 */
	public void setColor(MDColor color) {
		this.color = color;
	}

	public Number getCount() {
		return count;
	}

	/**
	 * The number of horizontal/vertical gridlines inside the chart area. Minimum
	 * value is 2. Specify -1 to automatically compute the number of gridlines.
	 * Default: 5 <br>
	 * If <b>this</b> is a <b>minorGridline</b>, then this is the number of
	 * horizontal/vertical minor gridlines between two regular gridlines, and defaults to 0
	 */
	public void setCount(Number count) {
		this.count = count;
	}

	public Map<TimeUnit, Map<String, ArrayList<String>>> getUnits() {
		return units;
	}

	/**
	 * Overrides the default format for various aspects of date/datetime/timeofday
	 * data types when used with chart computed gridlines. Allows formatting for
	 * years, months, days, hours, minutes, seconds, and milliseconds.
	 */
	public void addUnit(TimeUnit timeUnit, HAxisDateFormat format) {
		this.units.get(timeUnit).get(FORMAT).add(format.getFormat());
	}

}
