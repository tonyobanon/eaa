package com.kylantis.eaa.core.components.charts;

public enum ChartStackOptions {

	/** elements will not stack. This is the default option. */
	FALSE(false),

	/** stacks elements for all series at each domain value. */
	TRUE(true),

	/**
	 * stacks elements for all series at each domain value and rescales them such
	 * that they add up to 100%, with each element's value calculated as a
	 * percentage of 100%.
	 */
	PERCENT("percent"),

	/**
	 * stacks elements for all series at each domain value and rescales them such
	 * that they add up to 1, with each element's value calculated as a fraction of
	 * 1.
	 */
	RELATIVE("relative");

	private final Object value;

	private ChartStackOptions(Object value) {
		this.value = value;
	}

	public Object getValue() {
		return this.value;
	}

}
