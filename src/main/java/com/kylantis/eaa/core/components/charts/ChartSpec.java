package com.kylantis.eaa.core.components.charts;

public class ChartSpec {

	private String subtitle;
	private String title;

	public String getSubtitle() {
		return subtitle;
	}

	/**
	 * For Material Charts, this option specifies the subtitle. Only Material Charts
	 * support subtitles.
	 */
	public ChartSpec setSubtitle(String subtitle) {
		this.subtitle = subtitle;
		return this;
	}

	public String getTitle() {
		return title;
	}

	/** For Material Charts, this option specifies the title. */
	public ChartSpec setTitle(String title) {
		this.title = title;
		return this;
	}

}
