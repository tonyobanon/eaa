package com.kylantis.eaa.core.components.charts;

public enum ScaleType {

	/**
	 * The Y axis will span the minimum to the maximum values of the series. If you
	 * have more than one series, use allmaximized.
	 */
	MAXIMIZED("maximized"),

	/**
	 * 
	 * The Y axis varies, depending on the data values values: <br>
	 * <b>*</b> If all values are >=0, the Y axis will span from zero to the maximum
	 * data value.<br>
	 * <b>*</b> If all values are <=0, the Y axis will span from zero to the minimum
	 * data value. <br>
	 * <b>*</b> If values are both positive and negative, the Y axis will range from
	 * the series maximum to the series minimum. <br>
	 * For multiple series, use 'allfixed'.
	 */
	FIXED("fixed"),

	/**
	 * Same as 'maximized,' but used when multiple scales are displayed. Both charts
	 * will be maximized within the same scale, which means that one will be
	 * misrepresented against the Y axis, but hovering over each series will display
	 * its true value.
	 */
	ALL_MAXIMIZED("allmaximized"),

	/**
	 * Same as 'fixed,' but used when multiple scales are displayed. This setting
	 * adjusts each scale to the series to which it applies (use this in conjunction
	 * with <b>scaleColumns</b>).
	 */
	ALL_FIXED("allfixed");

	private final Object value;

	private ScaleType(Object value) {
		this.value = value;
	}

	public Object getValue() {
		return this.value;
	}

}
