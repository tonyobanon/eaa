package com.kylantis.eaa.core.components.styling;

public enum Sizes {
	XXS, XS, S, M, L, XL, XXL, F;
	
	public static Sizes forChart() {
		return Sizes.XXL;
	}
}
