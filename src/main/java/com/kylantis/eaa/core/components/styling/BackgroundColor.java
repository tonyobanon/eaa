package com.kylantis.eaa.core.components.styling;

import com.kylantis.eaa.core.components.BaseComponent;
import com.kylantis.eaa.core.components.Theme;
import com.kylantis.eaa.core.components.charts.Review;;

public class BackgroundColor {

	private MDColor stroke;
	private Number strokeWidth;
	private MDColor fill;

	public BackgroundColor() {

	}

	@Review("Determine the exact propertly that should be used. fill or stroke")
	public BackgroundColor(BaseComponent component) {
		this.fill = Theme.getDefault().getBackgroundColor(component);
	}

	public MDColor getStroke() {
		return stroke;
	}

	/**
	 * 
	 * @param stroke
	 *            The color of the chart border, as an HTML color string. Default:
	 *            '#666'
	 */
	public BackgroundColor setStroke(MDColor stroke) {
		this.stroke = stroke;
		return this;
	}

	public Number getStrokeWidth() {
		return strokeWidth;
	}

	/**
	 *
	 * @param strokeWidth
	 *            The border width, in pixels. Default: 0
	 */
	public BackgroundColor setStrokeWidth(Number strokeWidth) {
		this.strokeWidth = strokeWidth;
		return this;
	}

	public MDColor getFill() {
		return fill;
	}

	/**
	 * 
	 * @param fill
	 *            The chart fill color, as an HTML color string. Default: 'white'
	 */
	public BackgroundColor setFill(MDColor fill) {
		this.fill = fill;
		return this;
	}

}
