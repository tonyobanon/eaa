package com.kylantis.eaa.core.components.charts;

public enum ChartLegendPosition {

	SAME_ROW ("sameRow"),

	NEW_ROW ("newRow");

	private final String position;

	private ChartLegendPosition(String position) {
		this.position = position;
	}

	public String getPosition() {
		return position;
	}

	@Override
	public String toString() {
		return position;
	}
}
