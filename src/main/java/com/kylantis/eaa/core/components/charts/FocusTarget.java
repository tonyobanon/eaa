package com.kylantis.eaa.core.components.charts;

public enum FocusTarget {

	/** Focus on a single data point. Correlates to a cell in the data table. */
	DATUM("datum"),

	/**
	 * Focus on a grouping of all data points along the major axis. Correlates to a
	 * row in the data table. In focusTarget 'category' the tooltip displays all the
	 * category values. This may be useful for comparing values of different series.
	 */
	CATEGORY("category");

	private final String target;

	private FocusTarget(String target) {
		this.target = target;
	}

	@Override
	public String toString() {
		return target;
	}

}
