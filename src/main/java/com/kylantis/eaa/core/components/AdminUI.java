package com.kylantis.eaa.core.components;

import com.kylantis.eaa.core.config.Id;
import com.kylantis.eaa.core.config.Namespace;

public class AdminUI {
	
	public static final Id DASHBOARD_INDEX_PAGE = Namespace.forAdmin().getKey("dashboard_index");
}
