package com.kylantis.eaa.core.components.charts;

import com.kylantis.eaa.core.components.styling.MDColor;
import com.kylantis.eaa.core.utils.NumberUtil;

public class AnnotationBoxStyleGradient {

	private MDColor color1;
	private MDColor color2;

	private String x1;
	private String y1;
	private String x2;
	private String y2;

	private Boolean useObjectBoundingBoxUnits;

	public MDColor getColor1() {
		return color1;
	}

	/** Start color for gradient. */
	public void setColor1(MDColor color1) {
		this.color1 = color1;
	}

	public MDColor getColor2() {
		return color2;
	}

	/** Finish color for gradient. */
	public void setColor2(MDColor color2) {
		this.color2 = color2;
	}

	public String getX1() {
		return x1;
	}

	/**
	 * Where on the boundary to start and end the color1/color2 gradient, relative
	 * to the upper left corner of the boundary.
	 */
	public void setX1(Number x1) {
		this.x1 = NumberUtil.asPercentile(x1);
	}

	public String getY1() {
		return y1;
	}

	/**
	 * Where on the boundary to start and end the color1/color2 gradient, relative
	 * to the upper left corner of the boundary.
	 */
	public void setY1(Number y1) {
		this.y1 = NumberUtil.asPercentile(y1);
	}

	public String getX2() {
		return x2;
	}

	/**
	 * Where on the boundary to start and end the color1/color2 gradient, relative
	 * to the upper left corner of the boundary.
	 */
	public void setX2(Number x2) {
		this.x2 = NumberUtil.asPercentile(x2);
	}

	public String getY2() {
		return y2;
	}

	/**
	 * Where on the boundary to start and end the color1/color2 gradient, relative
	 * to the upper left corner of the boundary.
	 */
	public void setY2(Number y2) {
		this.y2 = NumberUtil.asPercentile(y2);
	}

	public Boolean getUseObjectBoundingBoxUnits() {
		return useObjectBoundingBoxUnits;
	}

	/**
	 * If true, the boundary for x1, y1, x2, and y2 is the box. If false, it's the
	 * entire chart.
	 */
	public void setUseObjectBoundingBoxUnits(Boolean useObjectBoundingBoxUnits) {
		this.useObjectBoundingBoxUnits = useObjectBoundingBoxUnits;
	}

}
