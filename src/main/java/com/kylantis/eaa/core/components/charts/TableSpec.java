package com.kylantis.eaa.core.components.charts;

public class TableSpec {

	private Boolean sortAscending;
	private Number sortColumn;

	public Boolean getSortAscending() {
		return sortAscending;
	}

	/**
	 * If set to <b>true</b>, reverses the order of the annotations table and
	 * displays them in ascending order. Default: false
	 */
	public TableSpec setSortAscending(Boolean sortAscending) {
		this.sortAscending = sortAscending;
		return this;
	}

	public Number getSortColumn() {
		return sortColumn;
	}

	/**
	 * The column index of the annotations table for which the annotations will be
	 * sorted. The index must be either 0, for the annotation label column, or 1,
	 * for the annotation text column. Default: 0
	 */
	public TableSpec setSortColumn(Number sortColumn) {
		this.sortColumn = sortColumn;
		return this;
	}

}
