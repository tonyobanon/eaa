package com.kylantis.eaa.core.components.charts;

import java.util.Calendar;

public class HAxisDateTick extends HAxisTickElement {

	private final Integer ms;
	private final Integer sec;
	private final Integer min;
	private final Integer hh;
	private final Integer dd;
	private final Integer mm;
	private final Integer yy;

	public HAxisDateTick(Calendar calendar) {
		this.yy = calendar.get(Calendar.YEAR);
		this.mm = calendar.get(Calendar.MONTH);
		this.dd = calendar.get(Calendar.DAY_OF_MONTH);
		this.hh = calendar.get(Calendar.HOUR);
		this.min = calendar.get(Calendar.MINUTE);
		this.sec = calendar.get(Calendar.SECOND);
		this.ms = calendar.get(Calendar.MILLISECOND);
	}

	public Integer getDay() {
		return dd;
	}

	public Integer getMonth() {
		return mm;
	}

	public Integer getYear() {
		return yy;
	}

	public Integer getMicroseconds() {
		return ms;
	}

	public Integer getSeconds() {
		return sec;
	}

	public Integer getMinutes() {
		return min;
	}

	public Integer getHour() {
		return hh;
	}

}
