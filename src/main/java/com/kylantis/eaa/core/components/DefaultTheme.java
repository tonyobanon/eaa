package com.kylantis.eaa.core.components;

import com.kylantis.eaa.core.components.styling.MDColor;

public class DefaultTheme implements Theme {

	@Override
	public MDColor getColor(BaseComponent type) {
		return MDColor.blue_300;
	}

	@Override
	public MDColor getBackgroundColor(BaseComponent type) {
		
		//For chart components, default to white
		if(type instanceof ChartComponent) {
			return MDColor.white_1000;
		}
		
		return MDColor.white_1000;
	}

	@Override
	public ValueTransformStrategy getValueTransformStrategy() {
		return new ValueTransformStrategy() {
			
			@Override
			public Object transform(ValueTransformContext ctx, Object o) {
				//@DEV
				return o;
			}
		};
	}

}
