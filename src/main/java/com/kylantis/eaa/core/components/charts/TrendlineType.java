package com.kylantis.eaa.core.components.charts;

public enum TrendlineType {

	LINEAR("linear"),

	EXPONENTIAL("exponential"),

	POLYNOMIAL("polynomial");

	private final Object value;

	private TrendlineType(Object value) {
		this.value = value;
	}

	public Object getValue() {
		return this.value;
	}

}
