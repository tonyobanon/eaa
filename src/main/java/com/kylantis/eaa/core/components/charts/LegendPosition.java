package com.kylantis.eaa.core.components.charts;

public enum LegendPosition {

	/** Below the chart. */
	BOTTOM("bottom"),

	/**
	 * To the left of the chart, provided the left axis has no series associated
	 * with it. So if you want the legend on the left, use the option
	 * <b>targetAxisIndex: 1</b>.
	 */
	LEFT("left"),

	/** Inside the chart, by the top left corner. */
	IN("in"),

	/** No legend is displayed. */
	NONE("none"),

	/** To the right of the chart. Incompatible with the <b>vAxes<b> option. */
	RIGHT("right"),

	/** Above the chart. */
	TOP("top");

	private final String position;

	private LegendPosition(String position) {
		this.position = position;
	}

	public String getPosition() {
		return position;
	}

	@Override
	public String toString() {
		return position;
	}
}
