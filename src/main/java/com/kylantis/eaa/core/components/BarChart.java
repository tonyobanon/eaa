package com.kylantis.eaa.core.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.kylantis.eaa.core.components.charts.Animation;
import com.kylantis.eaa.core.components.charts.Annotations;
import com.kylantis.eaa.core.components.charts.Axis;
import com.kylantis.eaa.core.components.charts.BarSpec;
import com.kylantis.eaa.core.components.charts.Bars;
import com.kylantis.eaa.core.components.charts.ChartArea;
import com.kylantis.eaa.core.components.charts.ChartSpec;
import com.kylantis.eaa.core.components.charts.ChartStackOptions;
import com.kylantis.eaa.core.components.charts.FocusTarget;
import com.kylantis.eaa.core.components.charts.Legend;
import com.kylantis.eaa.core.components.charts.Orientation;
import com.kylantis.eaa.core.components.charts.SeriesSpec;
import com.kylantis.eaa.core.components.charts.TextPosition;
import com.kylantis.eaa.core.components.charts.TextStyle;
import com.kylantis.eaa.core.components.charts.Tooltip;
import com.kylantis.eaa.core.components.charts.TrendlineElement;
import com.kylantis.eaa.core.components.styling.BackgroundColor;
import com.kylantis.eaa.core.components.styling.MDColor;
import com.kylantis.eaa.core.components.styling.Sizes;
import com.kylantis.eaa.core.config.Id;

/**
 * 
 * Note: <br>
 * 
 * Some config parameters were purposely not added to this class: because they
 * were in beta at the time. i.e <b>explorer</b>
 * 
 * Also, the <b>theme</b> property was not included because of lack of
 * configuration
 * 
 **/
public abstract class BarChart extends ChartComponent {

	private Animation animation;
	private Annotations annotations;
	private TextPosition axisTitlesPosition;
	private BackgroundColor backgroundColor = new BackgroundColor(this);
	private BarSpec bar;
	private Bars bars;
	private ChartArea chartArea = new ChartArea(this);
	private ChartSpec chart;
	private ArrayList<String> colors = new ArrayList<>();
	private Number dataOpacity;
	private Boolean enableInteractivity;
	private FocusTarget focusTarget;
	private Number fontSize;
	private String fontName;
	private Boolean forceIFrame;
	private ArrayList<Axis> hAxes;
	private Axis hAxis;

	private Number height;
	private ChartStackOptions isStacked;
	private Legend legend;
	private Orientation orientation;
	private Boolean reverseCategories;
	private ArrayList<SeriesSpec> series = new ArrayList<>();
	private String title;
	private TextPosition titlePosition;
	private TextStyle titleTextStyle;
	private Tooltip tooltip;
	private Map<Number, TrendlineElement> trendlines = new HashMap<>();
	private Axis vAxis;
	private Number width;

	public BarChart() {
	}

	public Animation getAnimation() {
		return animation;
	}

	public BarChart setAnimation(Animation animation) {
		this.animation = animation;
		return this;
	}

	public Annotations getAnnotations() {
		return annotations;
	}

	public BarChart setAnnotations(Annotations annotations) {
		this.annotations = annotations;
		return this;
	}

	public TextPosition getAxisTitlesPosition() {
		return axisTitlesPosition;
	}

	/**
	 * Where to place the axis titles, compared to the chart area. Default is "out".
	 */
	public BarChart setAxisTitlesPosition(TextPosition axisTitlesPosition) {
		this.axisTitlesPosition = axisTitlesPosition;
		return this;
	}

	public BackgroundColor getBackgroundColor() {
		return backgroundColor;
	}

	/** The background color for the main area of the chart. */
	public BarChart setBackgroundColor(BackgroundColor backgroundColor) {
		this.backgroundColor = backgroundColor;
		return this;
	}

	public BarSpec getBar() {
		return bar;
	}

	public BarChart setBar(BarSpec bar) {
		this.bar = bar;
		return this;
	}

	public Bars getBars() {
		return bars;
	}

	/**
	 * Whether the bars in a Material Bar Chart are vertical or horizontal. This
	 * option has no effect on Classic Bar Charts or Classic Column Charts.<br>
	 * Default: 'vertical'
	 */
	public BarChart setBars(Bars bars) {
		this.bars = bars;
		return this;
	}

	public ChartArea getChartArea() {
		return chartArea;
	}

	/**
	 * This configures the placement and size of the chart area (where the chart
	 * itself is drawn, excluding axis and legends).
	 */
	public BarChart setChartArea(ChartArea chartArea) {
		this.chartArea = chartArea;
		return this;
	}

	public ChartSpec getChart() {
		return chart;
	}

	public BarChart setChart(ChartSpec chart) {
		this.chart = chart;
		return this;
	}

	public ArrayList<String> getColors() {
		return colors;
	}

	/** Add a new color to use for the chart elements. */
	public BarChart addColor(MDColor color) {
		this.colors.add(color.asString());
		return this;
	}

	public BarChart setColors(ArrayList<String> colors) {
		this.colors = colors;
		return this;
	}

	public Number getDataOpacity() {
		return dataOpacity;
	}

	/**
	 * The transparency of data points, with 1.0 being completely opaque and 0.0
	 * fully transparent. In scatter, histogram, bar, and column charts, this refers
	 * to the visible data: dots in the scatter chart and rectangles in the others.
	 * In charts where selecting data creates a dot, such as the line and area
	 * charts, this refers to the circles that appear upon hover or selection. The
	 * combo chart exhibits both behaviors, and this option has no effect on other
	 * charts. Default: 1.0
	 */
	public BarChart setDataOpacity(Number dataOpacity) {
		this.dataOpacity = dataOpacity;
		return this;
	}

	public Boolean getEnableInteractivity() {
		return enableInteractivity;
	}

	/**
	 * Whether the chart throws user-based events or reacts to user interaction. If
	 * false, the chart will not throw 'select' or other interaction-based events
	 * (but will throw ready or error events), and will not display hovertext or
	 * otherwise change depending on user input. Default true
	 */
	public BarChart setEnableInteractivity(Boolean enableInteractivity) {
		this.enableInteractivity = enableInteractivity;
		return this;
	}

	public FocusTarget getFocusTarget() {
		return focusTarget;
	}

	/**
	 * 
	 * The type of the entity that receives focus on mouse hover. Also affects which
	 * entity is selected by mouse click, and which data table element is associated
	 * with events.<br>
	 * 
	 * <br>
	 * Default: "datum"<br>
	 * 
	 */
	public BarChart setFocusTarget(FocusTarget focusTarget) {
		this.focusTarget = focusTarget;
		return this;
	}

	public Number getFontSize() {
		return fontSize;
	}

	/**
	 * The default font size, in pixels, of all text in the chart. You can override
	 * this using properties for specific chart elements.
	 */
	public BarChart setFontSize(Number fontSize) {
		this.fontSize = fontSize;
		return this;
	}

	public String getFontName() {
		return fontName;
	}

	/**
	 * The default font face for all text in the chart. You can override this using
	 * properties for specific chart elements. Default: 'Arial'
	 */
	public BarChart setFont(FontFace font) {
		this.fontName = font.getFontName();
		return this;
	}

	public BarChart setFontName(String font) {
		this.fontName = font;
		return this;
	}

	public Boolean getForceIFrame() {
		return forceIFrame;
	}

	/** Draws the chart inside an inline frame. Default: false. */
	public BarChart setForceIFrame(Boolean forceIFrame) {
		this.forceIFrame = forceIFrame;
		return this;
	}

	public ArrayList<Axis> gethAxes() {
		return hAxes;
	}

	/**
	 * Specifies properties for individual horizontal axes, if the chart has
	 * multiple horizontal axes. Each child object is an <b>{@link Axis }</b>
	 * instance, and can contain all the properties supported by a horizontal axis.
	 * These property values override any global settings for the same property.
	 * <br>
	 * To specify a chart with multiple horizontal axes, first define a new axis
	 * using <b>series.targetAxisIndex</b>, then configure the axis using
	 * <b>Axis.</b>
	 */
	public BarChart sethAxes(ArrayList<Axis> hAxes) {
		this.hAxes = hAxes;
		return this;
	}

	public Axis gethAxis() {
		return hAxis;
	}

	/** This configures various horizontal axis elements. */
	public void sethAxis(Axis hAxis) {
		this.hAxis = hAxis;
	}

	public Number getHeight() {
		return height;
	}

	/** Height of the chart, in pixels. */
	public BarChart setHeight(Number height) {
		this.height = height;
		return this;
	}

	public ChartStackOptions getIsStacked() {
		return isStacked;
	}

	/**
	 * 
	 * If set to true, stacks the elements for all series at each domain value.
	 * Note: In Column, Area, and SteppedArea charts, the order of legend items is
	 * reversed, to better correspond with the stacking of the series elements (E.g.
	 * series 0 will be the bottom-most legend item). This does not apply to Bar
	 * Charts. <br>
	 * The <b>isStacked</b> option also supports 100% stacking, where the stacks of
	 * elements at each domain value are rescaled to add up to 100%. <br>
	 * For 100% stacking, the calculated value for each element will appear in the
	 * tooltip after its actual value. <br>
	 * The target axis will default to tick values based on the relative 0-1 scale
	 * as fractions of 1 for <b>'relative'</b>, and 0-100% for <b>'percent'</b>
	 * (Note: when using the <b>'percent'</b> option, the axis/tick values are
	 * displayed as percentages, however the actual values are the relative 0-1
	 * scale values. This is because the percentage axis ticks are the result of
	 * applying a format of "#.##%" to the relative 0-1 scale values. When using
	 * <b>isStacked: 'percent'</b>, be sure to specify any ticks/gridlines using the
	 * relative 0-1 scale values). You can customize the gridlines/tick values and
	 * formatting using the appropriate <b>hAxis/vAxis</b> options. <br>
	 * 100% stacking only supports data values of type number, and must have a
	 * baseline of zero. <br>
	 * Default: false
	 * 
	 */
	public BarChart setIsStacked(ChartStackOptions isStacked) {
		this.isStacked = isStacked;
		return this;
	}

	public Legend getLegend() {
		return legend;
	}

	/** This configures various aspects of the legend. */
	public BarChart setLegend(Legend legend) {
		this.legend = legend;
		return this;
	}

	public Boolean getReverseCategories() {
		return reverseCategories;
	}

	/**
	 * If set to true, will draw series from right to left. The default is to draw
	 * left-to-right. <br>
	 * This option is only supported for a discrete major axis. <br>
	 * Default: false
	 */
	public BarChart setReverseCategories(Boolean reverseCategories) {
		this.reverseCategories = reverseCategories;
		return this;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	/**
	 * The orientation of the chart. When set to <b>'vertical'</b>, rotates the axes
	 * of the chart so that (for instance) a column chart becomes a bar chart, and
	 * an area chart grows rightward instead of up: <br>
	 * Default: 'horizontal'
	 */
	public BarChart setOrientation(Orientation orientation) {
		this.orientation = orientation;
		return this;
	}

	public ArrayList<SeriesSpec> getSeries() {
		return series;
	}

	/**
	 * An array, each describing the format of the corresponding series in the
	 * chart.
	 */
	public BarChart setSeries(ArrayList<SeriesSpec> series) {
		this.series = series;
		return this;
	}

	/** Describe the format of the corresponding series in the chart. */
	public BarChart addSeries(SeriesSpec series) {
		this.series.add(series);
		return this;
	}

	public String getTitle() {
		return title;
	}

	/** Text to display above the chart. */
	public BarChart setTitle(String title) {
		this.title = title;
		return this;
	}

	public TextPosition getTitlePosition() {
		return titlePosition;
	}

	/** Where to place the chart title, compared to the chart area. */
	public BarChart setTitlePosition(TextPosition titlePosition) {
		this.titlePosition = titlePosition;
		return this;
	}

	public TextStyle getTitleTextStyle() {
		return titleTextStyle;
	}

	/** This specifies the title text style. */
	public BarChart setTitleTextStyle(TextStyle titleTextStyle) {
		this.titleTextStyle = titleTextStyle;
		return this;
	}

	public Tooltip getTooltip() {
		return tooltip;
	}

	/** This configures various tooltip elements. */
	public BarChart setTooltip(Tooltip tooltip) {
		this.tooltip = tooltip;
		return this;
	}

	public Map<Number, TrendlineElement> getTrendlines() {
		return trendlines;
	}

	/** Displays trendlines on the charts that support them. */
	public BarChart setTrendlines(Map<Number, TrendlineElement> trendlines) {
		this.trendlines = trendlines;
		return this;
	}

	public BarChart addTrendline(Number index, TrendlineElement trendline) {
		this.trendlines.put(index, trendline);
		return this;
	}

	public Axis getvAxis() {
		return vAxis;
	}

	/** This configures various vertical axis elements. */
	public BarChart setvAxis(Axis vAxis) {
		this.vAxis = vAxis;
		return this;
	}

	public Number getWidth() {
		return width;
	}

	/** Width of the chart, in pixels. */
	public BarChart setWidth(Number width) {
		this.width = width;
		return this;
	}

	@Override
	protected String title() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Id page() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void render() {
		
	}

	@Override
	protected void init() {
		
	}

	@Override
	protected ComponentType type() {
		return ComponentType.AREA_CHART;
	}

	@Override
	public final Iterable<MDColor> themeColor() {
		return null;
	}

	@Override
	public final Sizes minWidth() {
		return Sizes.forChart();
	}

}
