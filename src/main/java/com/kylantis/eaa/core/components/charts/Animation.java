package com.kylantis.eaa.core.components.charts;

public class Animation {

	private Number duration;
	private AnimationEasingFunction easing;
	private Boolean startup;

	public Number getDuration() {
		return duration;
	}

	/** The duration of the animation, in milliseconds. It defaults to 0 */
	public void setDuration(Number duration) {
		this.duration = duration;
	}

	public AnimationEasingFunction getEasingFunction() {
		return easing;
	}

	/**
	 * The easing function applied to the animation. <br>
	 * It defaults to "linear"
	 * 
	 */
	public void setEasingFunction(AnimationEasingFunction easingFunction) {
		this.easing = easingFunction;
	}

	public Boolean getStartup() {
		return startup;
	}

	/**
	 * Determines if the chart will animate on the initial draw. If true, the chart
	 * will start at the baseline and animate to its final state. Default: false
	 */
	public void setStartup(Boolean startup) {
		this.startup = startup;
	}

}
