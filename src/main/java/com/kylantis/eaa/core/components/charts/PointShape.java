package com.kylantis.eaa.core.components.charts;

public enum PointShape {

	CIRCLE("circle"), TRIANGLE("triangle"), SQUARE("square"), DIAMOND("diamond"), STAR("star"), POLYGON("polygon");

	private final String value;

	private PointShape(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
