package com.kylantis.eaa.core.components;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Map;

import com.kylantis.eaa.core.components.charts.ChartLegendPosition;
import com.kylantis.eaa.core.components.charts.DateFormat;
import com.kylantis.eaa.core.components.charts.ScaleType;
import com.kylantis.eaa.core.components.charts.TableSpec;
import com.kylantis.eaa.core.components.styling.MDColor;
import com.kylantis.eaa.core.components.styling.Sizes;
import com.kylantis.eaa.core.config.Id;

public abstract class AnnotationChart extends ChartComponent {

	private Boolean allowHtml;
	private String allValuesSuffix;
	private Number annotationsWidth;
	private MDColor[] colors;
	private DateFormat dateFormat;
	private Boolean displayAnnotations;
	private Boolean displayAnnotationsFilter;
	private Boolean displayDateBarSeparator;
	private Boolean displayExactValues;
	private Boolean displayLegendDots;
	private Boolean displayLegendValues;
	private Boolean displayRangeSelector;
	private Boolean displayZoomButtons;
	private Number fill;
	private ChartLegendPosition legendPosition;
	private Number max;
	private Number min;
	private Map<Integer, DecimalFormat> numberFormats;
	private Number[] scaleColumns;
	private String scaleFormat;
	private ScaleType scaleType;
	private TableSpec table;
	private Number thickness;
	private Date zoomEndTime;
	private Date zoomStartTime;

	@Override
	protected abstract String title();

	@Override
	protected abstract Id page();

	@Override
	protected void init() {

	}

	@Override
	protected void render() {

	}

	@Override
	protected ComponentType type() {
		return ComponentType.ANNOTATION_CHART;
	}

	@Override
	public final Iterable<MDColor> themeColor() {
		return null;
	}

	@Override
	public final Sizes minWidth() {
		return Sizes.forChart();
	}

	public Boolean getAllowHtml() {
		return allowHtml;
	}

	/**
	 * If set to true, any annotation text that includes HTML tags will be rendered
	 * as HTML. Defaults to false
	 **/

	public AnnotationChart setAllowHtml(Boolean allowHtml) {
		this.allowHtml = allowHtml;
		return this;
	}

	public String getAllValuesSuffix() {
		return allValuesSuffix;
	}

	/**
	 * A suffix to be added to all values in the legend and tick labels in the
	 * vertical axes. Defaults to 'none'
	 **/

	public AnnotationChart setAllValuesSuffix(String allValuesSuffix) {
		this.allValuesSuffix = allValuesSuffix;
		return this;
	}

	public Number getAnnotationsWidth() {
		return annotationsWidth;
	}

	/**
	 * The width (in percent) of the annotations area, out of the entire chart area.
	 * Must be a number in the range 5-80. Defaults to 25
	 **/

	public AnnotationChart setAnnotationsWidth(Number annotationsWidth) {
		this.annotationsWidth = annotationsWidth;
		return this;
	}

	public MDColor[] getColors() {
		return colors;
	}

	/**
	 * The colors to use for the chart lines and labels.
	 **/

	public AnnotationChart setColors(MDColor... colors) {
		this.colors = colors;
		return this;
	}

	public DateFormat getDateFormat() {
		return dateFormat;
	}

	/**
	 * The format used to display the date information in the top right corner. The
	 * format of this field is as specified by the java SimpleDateFormat class.
	 * Default to either 'MMMM dd, yyyy' or 'HH:mm MMMM dd, yyyy', depending on the
	 * type of the first column (date, or datetime, respectively)
	 **/

	public AnnotationChart setDateFormat(DateFormat dateFormat) {
		this.dateFormat = dateFormat;
		return this;
	}

	public Boolean getDisplayAnnotations() {
		return displayAnnotations;
	}

	/**
	 * If set to false, the chart will hide the annotations table, and the
	 * annotations and annotationText will not be visible (the annotations table
	 * will also not be displayed if there are no annotations in your data,
	 * regardless of this option). When this option is set to true, after every
	 * numeric column, two optional annotation string columns can be added, one for
	 * the annotation title and one for the annotation text. Defaults to true
	 **/

	public AnnotationChart setDisplayAnnotations(Boolean displayAnnotations) {
		this.displayAnnotations = displayAnnotations;
		return this;
	}

	public Boolean getDisplayAnnotationsFilter() {
		return displayAnnotationsFilter;
	}

	/**
	 * If set to true, the chart will display a filter control to filter
	 * annotations. Use this option when there are many annotations. Defaults to
	 * false
	 **/

	public AnnotationChart setDisplayAnnotationsFilter(Boolean displayAnnotationsFilter) {
		this.displayAnnotationsFilter = displayAnnotationsFilter;
		return this;
	}

	public Boolean getDisplayDateBarSeparator() {
		return displayDateBarSeparator;
	}

	/**
	 * Whether to display a small bar separator ( | ) between the series values and
	 * the date in the legend, where true means yes. Defaults to true
	 **/

	public AnnotationChart setDisplayDateBarSeparator(Boolean displayDateBarSeparator) {
		this.displayDateBarSeparator = displayDateBarSeparator;
		return this;
	}

	public Boolean getDisplayExactValues() {
		return displayExactValues;
	}

	/**
	 * Whether to display a shortened, rounded version of the values on the top of
	 * the graph, to save space; false indicates that it may. For example, if set to
	 * false, 56123.45 might be displayed as 56.12k. Defaults to false
	 **/

	public AnnotationChart setDisplayExactValues(Boolean displayExactValues) {
		this.displayExactValues = displayExactValues;
		return this;
	}

	public Boolean getDisplayLegendDots() {
		return displayLegendDots;
	}

	/**
	 * Whether to display dots next to the values in the legend text, where true
	 * means yes. Defaults to true
	 **/

	public AnnotationChart setDisplayLegendDots(Boolean displayLegendDots) {
		this.displayLegendDots = displayLegendDots;
		return this;
	}

	public Boolean getDisplayLegendValues() {
		return displayLegendValues;
	}

	/**
	 * Whether to display the highlighted values in the legend, where true means
	 * yes. Defaults to true
	 **/

	public AnnotationChart setDisplayLegendValues(Boolean displayLegendValues) {
		this.displayLegendValues = displayLegendValues;
		return this;
	}

	public Boolean getDisplayRangeSelector() {
		return displayRangeSelector;
	}

	/**
	 * Whether to show the zoom range selection area (the area at the bottom of the
	 * chart), where false means no.
	 * 
	 * The outline in the zoom selector is a log scale version of the first series
	 * in the chart, scaled to fit the height of the zoom selector. Defaults to true
	 **/
	public AnnotationChart setDisplayRangeSelector(Boolean displayRangeSelector) {
		this.displayRangeSelector = displayRangeSelector;
		return this;
	}

	public Boolean getDisplayZoomButtons() {
		return displayZoomButtons;
	}

	/**
	 * Whether to show the zoom buttons ("1d 5d 1m" and so on), where false means
	 * no. Defaults to true
	 **/

	public AnnotationChart setDisplayZoomButtons(Boolean displayZoomButtons) {
		this.displayZoomButtons = displayZoomButtons;
		return this;
	}

	public Number getFill() {
		return fill;
	}

	/**
	 * A number from 0—100 (inclusive) specifying the alpha of the fill below each
	 * line in the line graph. 100 means 100% opaque, and 0 means no fill at all.
	 * The fill color is the same color as the line above it. Defaults to 0
	 **/

	public AnnotationChart setFill(Number fill) {
		this.fill = fill;
		return this;
	}

	public ChartLegendPosition getLegendPosition() {
		return legendPosition;
	}

	/**
	 * Whether to put the colored legend on the same row with the zoom buttons and
	 * the date ('sameRow'), or on a new row ('newRow'). Defaults to "sameRow"
	 **/

	public AnnotationChart setLegendPosition(ChartLegendPosition legendPosition) {
		this.legendPosition = legendPosition;
		return this;
	}

	public Number getMax() {
		return max;
	}

	/**
	 * The maximum value to show on the Y axis. If the maximum data point exceeds
	 * this value, this setting will be ignored, and the chart will be adjusted to
	 * show the next major tick mark above the maximum data point. This will take
	 * precedence over the Y axis maximum determined by scaleType. This is similar
	 * to maxValue in core charts.
	 **/

	public AnnotationChart setMax(Number max) {
		this.max = max;
		return this;
	}

	public Number getMin() {
		return min;
	}

	/**
	 * The minimum value to show on the Y axis. If the minimum data point is less
	 * than this value, this setting will be ignored, and the chart will be adjusted
	 * to show the next major tick mark below the minimum data point. This will take
	 * precedence over the Y axis minimum determined by scaleType.
	 * 
	 * This is similar to minValue in core charts.
	 **/

	public AnnotationChart setMin(Number min) {
		this.min = min;
		return this;
	}

	public Map<Integer, DecimalFormat> getNumberFormats() {
		return numberFormats;
	}

	/**
	 * Specifies the number format patterns to be used to format the values at the
	 * top of the graph.
	 * 
	 * The patterns should be in the format as specified by the java DecimalFormat
	 * class.
	 * 
	 * If not specified, the default format pattern is used. If a map is specified,
	 * the keys are (zero-based) indexes of series, and the values are the patterns
	 * to be used to format the specified series.
	 * 
	 * You are not required to include a format for every series on the chart; any
	 * unspecified series will use the default format. If this option is specified,
	 * the <b>displayExactValues</b> option is ignored.
	 **/

	public AnnotationChart setNumberFormats(Map<Integer, DecimalFormat> numberFormats) {
		this.numberFormats = numberFormats;
		return this;
	}

	public Number[] getScaleColumns() {
		return scaleColumns;
	}

	/**
	 * Specifies which values to show on the Y axis tick marks in the graph. The
	 * default is to have a single scale on the right side, which applies to both
	 * series; but you can have different sides of the graph scaled to different
	 * series values.
	 * 
	 * This option takes an array of zero to three numbers specifying the
	 * (zero-based) index of the series to use as the scale value. Where these
	 * values are shown depends on how many values you include in your array:
	 * 
	 * If you specify an empty array, the chart will not show Y values next to the
	 * tick marks. If you specify one value, the scale of the indicated series will
	 * be displayed on the right side of the chart only. If you specify two values,
	 * a the scale for the second series will be added to the right of the chart. If
	 * you specify three values, a scale for the third series will be added to the
	 * middle of the chart. Any values after the third in the array will be ignored.
	 * When displaying more than one scale, it is advisable to set the scaleType
	 * option to either 'allfixed' or 'allmaximized'.
	 * 
	 **/
	public AnnotationChart setScaleColumns(Number[] scaleColumns) {
		this.scaleColumns = scaleColumns;
		return this;
	}

	public String getScaleFormat() {
		return scaleFormat;
	}

	/**
	 * Number format to be used for the axis tick labels. The default of '#'
	 * displays as an integer. Defaults to '#'
	 **/

	public AnnotationChart setScaleFormat(String scaleFormat) {
		this.scaleFormat = scaleFormat;
		return this;
	}

	public ScaleType getScaleType() {
		return scaleType;
	}

	/**
	 * Sets the maximum and minimum values shown on the Y axis. <br>
	 * If you specify the min and/or max options, they will take precedence over the
	 * minimum and maximum values determined by your scale type. Defaults to
	 * "fixed".
	 **/
	public AnnotationChart setScaleType(ScaleType scaleType) {
		this.scaleType = scaleType;
		return this;
	}

	public TableSpec getTable() {
		return table;
	}

	/** This contains options relating to the annotations table. */
	public AnnotationChart setTable(TableSpec table) {
		this.table = table;
		return this;
	}

	public Number getThickness() {
		return thickness;
	}

	/**
	 * A number from 0—10 (inclusive) specifying the thickness of the lines, where 0
	 * is the thinnest. Defaults to 0
	 **/

	public AnnotationChart setThickness(Number thickness) {
		this.thickness = thickness;
		return this;
	}

	public Date getZoomEndTime() {
		return zoomEndTime;
	}

	/** Sets the end date/time of the selected zoom range. **/
	public AnnotationChart setZoomEndTime(Date zoomEndTime) {
		this.zoomEndTime = zoomEndTime;
		return this;
	}

	public Date getZoomStartTime() {
		return zoomStartTime;
	}

	/** Sets the start date/time of the selected zoom range. **/
	public AnnotationChart setZoomStartTime(Date zoomStartTime) {
		this.zoomStartTime = zoomStartTime;
		return this;
	}

}
