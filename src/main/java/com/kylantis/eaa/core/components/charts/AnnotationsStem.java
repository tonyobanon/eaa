package com.kylantis.eaa.core.components.charts;

import com.kylantis.eaa.core.components.styling.MDColor;

public class AnnotationsStem {

	private MDColor color;
	private Number length;

	public MDColor getColor() {
		return color;
	}

	/** You can use this to control the color. Default is "black" */
	public void setColor(MDColor color) {
		this.color = color;
	}

	public Number getLength() {
		return length;
	}

	/**
	 * You can use this to control the stem length. Default is 5 for domain
	 * annotations and 12 for datum annotations.
	 */
	public void setLength(Number length) {
		this.length = length;
	}
}
