package com.kylantis.eaa.core.components.charts;

import com.kylantis.eaa.core.components.FontFace;
import com.kylantis.eaa.core.components.styling.MDColor;
import com.kylantis.eaa.core.utils.NumberUtil;

public class TextStyle {

	private FontFace fontName;
	private Number fontSize;
	private Boolean bold;
	private Boolean italic;
	private MDColor color;
	private MDColor auraColor;
	private Number opacity;

	public FontFace getFontName() {
		return fontName;
	}

	public void setFontName(FontFace fontName) {
		this.fontName = fontName;
	}

	public Number getFontSize() {
		return fontSize;
	}

	public void setFontSize(Number fontSize) {
		this.fontSize = fontSize;
	}

	public Boolean getBold() {
		return bold;
	}

	public void setBold(Boolean bold) {
		this.bold = bold;
	}

	public Boolean getItalic() {
		return italic;
	}

	public void setItalic(Boolean italic) {
		this.italic = italic;
	}

	public MDColor getColor() {
		return color;
	}

	/** The color of the text. */
	public void setColor(MDColor color) {
		this.color = color;
	}

	public MDColor getAuraColor() {
		return auraColor;
	}

	/** The color of the text outline. */
	public void setAuraColor(MDColor auraColor) {
		this.auraColor = auraColor;
	}

	public Number getOpacity() {
		return opacity;
	}

	/** The transparency of the text. */
	public void setOpacity(Number opacity) {
		this.opacity = NumberUtil.asReal(opacity);
	}

}
