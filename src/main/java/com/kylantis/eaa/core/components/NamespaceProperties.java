package com.kylantis.eaa.core.components;

import com.kylantis.eaa.core.config.Namespace;
import com.kylantis.eaa.core.users.RoleRealm;

public class NamespaceProperties {

	private String prefix;
	private RoleRealm roleRealm;
	private Boolean isInternal;

	public String getPrefix() {
		return prefix;
	}

	public NamespaceProperties setPrefix(String prefix) {
		this.prefix = prefix;
		return this;
	}

	public Boolean isInternal() {
		return isInternal;
	}

	public NamespaceProperties setIsInternal(Boolean isInternal) {
		this.isInternal = isInternal;
		return this;
	}

	public RoleRealm getRoleRealm() {
		return roleRealm;
	}

	public NamespaceProperties setRoleRealm(RoleRealm roleRealm) {
		this.roleRealm = roleRealm;
		return this;
	}

	public static NamespaceProperties from(Namespace namespace) {
		return new NamespaceProperties().setPrefix(namespace.getPrefix()).setRoleRealm(namespace.realm())
				.setIsInternal(namespace.isInternal());
	}
}
