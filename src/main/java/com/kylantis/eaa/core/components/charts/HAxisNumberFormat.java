package com.kylantis.eaa.core.components.charts;

/**
 * For number axis labels, this is a subset of the decimal formatting <a href=
 * "http://icu-project.org/apiref/icu4c/classDecimalFormat.html#_details">ICU
 * pattern set</a> . For instance, {format:'#,###%'} will display values
 * "1,000%", "750%", and "50%" for values 10, 7.5, and 0.5.
 *  <br>
 * The actual formatting applied to the label is derived from the locale the API
 * has been loaded with. 
 * <br>
 * This option is only supported for a continuous axis.
 * 
 */
public enum HAxisNumberFormat {

	/** displays numbers with no formatting (e.g., 8000000) */
	NONE("none"),

	/** displays numbers with thousands separators (e.g., 8,000,000) */
	DECIMAL("decimal"),

	/** displays numbers in scientific notation (e.g., 8e6) */
	SCIENTIFIC("scientific"),

	/** displays numbers in the local currency (e.g., $8,000,000.00) */
	CURRENCY("currency"),

	/** displays numbers as percentages (e.g., 800,000,000%) */
	PERCENT("percent"),

	/** displays abbreviated numbers (e.g., 8M) */
	SHORT("short"),

	/** displays numbers as full words (e.g., 8 million) */
	LONG("long");

	private final String format;

	private HAxisNumberFormat(String format) {
		this.format = format;
	}

	@Override
	public String toString() {
		return format;
	}

	public String getFormat() {
		return format;
	}

}
