package com.kylantis.eaa.core.components.charts;

import com.kylantis.eaa.core.components.styling.MDColor;

public class TrendlineElement {

	private MDColor color;
	private Number degree;
	private String labelInLegend;
	private Number lineWidth;
	private Number opacity;
	private Number pointSize;
	private Boolean pointsVisible;
	private Boolean showR2;
	private TrendlineType type;
	private Boolean visibleInLegend;

	public MDColor getColor() {
		return color;
	}

	/** The color of the trendline. Default: default series color */
	public TrendlineElement setColor(MDColor color) {
		this.color = color;
		return this;
	}

	public Number getDegree() {
		return degree;
	}

	/**
	 * For trendlines of <b>type: 'polynomial'</b>, the degree of the polynomial (2
	 * for quadratic, 3 for cubic, and so on). (The default degree may change from 3
	 * to 2 in an upcoming release.). Default: 3
	 */
	public TrendlineElement setDegree(Number degree) {
		this.degree = degree;
		return this;
	}

	public String getLabelInLegend() {
		return labelInLegend;
	}

	/** If set, the trendline will appear in the legend as this string. */
	public TrendlineElement setLabelInLegend(String labelInLegend) {
		this.labelInLegend = labelInLegend;
		return this;
	}

	public Number getLineWidth() {
		return lineWidth;
	}

	/** The line width of the trendline , in pixels. Default: 2 */
	public TrendlineElement setLineWidth(Number lineWidth) {
		this.lineWidth = lineWidth;
		return this;
	}

	public Number getOpacity() {
		return opacity;
	}

	/**
	 * The transparency of the trendline , from 0.0 (transparent) to 1.0 (opaque).
	 * Default: 1.0
	 */
	public TrendlineElement setOpacity(Number opacity) {
		this.opacity = opacity;
		return this;
	}

	public Number getPointSize() {
		return pointSize;
	}

	/**
	 * Trendlines are constucted by stamping a bunch of dots on the chart; this
	 * rarely-needed option lets you customize the size of the dots. The trendline's
	 * <b>lineWidth</b> option will usually be preferable. However, you'll need this
	 * option if you're using the global <b>pointSize</b> option and want a
	 * different point size for your trendlines. Default: 1
	 */
	public TrendlineElement setPointSize(Number pointSize) {
		this.pointSize = pointSize;
		return this;
	}

	public Boolean getPointsVisible() {
		return pointsVisible;
	}

	/**
	 * Trendlines are constucted by stamping a bunch of dots on the chart. The
	 * trendline's pointsVisible option determines whether the points for a
	 * particular trendline are visible. Trendlines are constucted by stamping a
	 * bunch of dots on the chart. The trendline's <b>pointsVisible</b> option
	 * determines whether the points for a particular trendline are visible.
	 * Default: true
	 */
	public TrendlineElement setPointsVisible(Boolean pointsVisible) {
		this.pointsVisible = pointsVisible;
		return this;
	}

	public Boolean getShowR2() {
		return showR2;
	}

	/**
	 * Whether to show the coefficient of determination in the legend or trendline
	 * tooltip. Default: false
	 */
	public TrendlineElement setShowR2(Boolean showR2) {
		this.showR2 = showR2;
		return this;
	}

	public TrendlineType getType() {
		return type;
	}

	/**
	 * Whether the trendlines is <b>'linear'</b> (the default),
	 * <b>'exponential'</b>, or <b>'polynomial'</b>. Default: linear
	 */
	public TrendlineElement setType(TrendlineType type) {
		this.type = type;
		return this;
	}

	public Boolean getVisibleInLegend() {
		return visibleInLegend;
	}

	/**
	 * Whether the trendline equation appears in the legend. (It will appear in the
	 * trendline tooltip. Default: false)
	 */
	public TrendlineElement setVisibleInLegend(Boolean visibleInLegend) {
		this.visibleInLegend = visibleInLegend;
		return this;
	}

}
