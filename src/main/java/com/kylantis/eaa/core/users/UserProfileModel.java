package com.kylantis.eaa.core.users;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.amazonaws.services.dynamodbv2.document.AttributeUpdate;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.google.common.collect.Lists;
import com.kylantis.eaa.core.BlockerTodo;
import com.kylantis.eaa.core.Dates;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.AddressesSpec;
import com.kylantis.eaa.core.attributes.BaseUserTableSpec;
import com.kylantis.eaa.core.base.PlatformInternal;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.components.Page;
import com.kylantis.eaa.core.config.Namespace;
import com.kylantis.eaa.core.dbutils.DBTooling;
import com.kylantis.eaa.core.indexing.GraphEntityType;
import com.kylantis.eaa.core.indexing.SearchGraphId;
import com.kylantis.eaa.core.keys.RegistryKeys;
import com.kylantis.eaa.core.metrics.MetricKeys;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IUserProfileModel;
import com.kylantis.eaa.core.ml.MLRepository;
import com.kylantis.eaa.core.setup.InstallOptions;

@ConcreteModel(dependencies = { MLRepository.CONFIG_MODEL, MLRepository.LOCATION_MODEL, MLRepository.SEARCH_GRAPH_MODEL, 
		MLRepository.METRICS_MODEL, MLRepository.ROLES_MODEL, MLRepository.BLOB_STORE_MODEL })
public class UserProfileModel extends IUserProfileModel {

	@Override
	public void preInstall() {

	}

	@Override
	public void install(InstallOptions options) {

		for (UserProfileSpec spec : options.getAdmins()) {
			registerUser(spec, getRolesModel().getDefaultRole(RoleRealm.ADMIN));
		}
	}

	@Override
	public void start() {
	}

	@BlockerTodo("Use a more efficient way to retrieve all registered users")
	@PlatformInternal
	public Iterator<Integer> getUsers() {
		List<Integer> result = new ArrayList<>();
		DBTooling.scanTable(BaseUserTableSpec.TABLE_NAME, "#id",
				new FluentHashMap<String, String>().with("#id", BaseUserTableSpec.ID)).forEach(i -> {
					result.add(i.getInt(BaseUserTableSpec.ID));
				});
		return result.iterator();
	}

	private Integer nextKey() {
		return getMetricsModel().getInt(MetricKeys.USERS_COUNT_ARCHIVE) + 1;
	}

	@PlatformInternal
	public void deleteUser(Integer userId) {
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);
		table.deleteItem(BaseUserTableSpec.ID, userId);
		getMetricsModel().decrementInt(MetricKeys.USERS_COUNT_CURRENT);
	}

	public Integer loginByEmail(String email, String password) {

		Table baseUserTable = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		QuerySpec spec = new QuerySpec().withKeyConditionExpression("#k = :v")
				.withNameMap(new NameMap().with("#k", BaseUserTableSpec.EMAIL))
				.withValueMap(new ValueMap().withString(":v", email));

		Iterator<Item> outcome = baseUserTable.getIndex(BaseUserTableSpec.EMAIL_INDEX).query(spec).iterator();

		if (!outcome.hasNext()) {
			// Incorrect email
			throw new ResourceException(ResourceException.ACCESS_NOT_ALLOWED);
		}

		Item item = outcome.next();
		if (item.getString(BaseUserTableSpec.PASS).equals(password)) {
			// Success
			return item.getInt(BaseUserTableSpec.ID);
		} else {
			// Wrong password
			throw new ResourceException(ResourceException.ACCESS_NOT_ALLOWED);
		}
	}

	public Integer loginByPhone(Long phone, String password) {

		Table baseUserTable = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		QuerySpec spec = new QuerySpec().withKeyConditionExpression("#k = :v")
				.withNameMap(new NameMap().with("#k", BaseUserTableSpec.PHONE))
				.withValueMap(new ValueMap().withLong(":v", phone));

		Iterator<Item> outcome = baseUserTable.getIndex(BaseUserTableSpec.PHONE_INDEX).query(spec).iterator();

		if (!outcome.hasNext()) {
			// Incorrect phone
			throw new ResourceException(ResourceException.ACCESS_NOT_ALLOWED);
		}

		Item item = outcome.next();
		if (item.getString(BaseUserTableSpec.PASS).equals(password)) {
			// Success
			return item.getInt(BaseUserTableSpec.ID);
		} else {
			// Wrong password
			throw new ResourceException(ResourceException.ACCESS_NOT_ALLOWED);
		}
	}

	public Integer registerUser(UserProfileSpec spec) {
		return registerUser(spec, null);
	}

	public Integer registerUser(UserProfileSpec spec, String role) {

		// @todo Create a role "admin" for this user
		// @todo Add all available functionalities to it
		// @todo Modify the metric : LAST_CREATED_ROLE_ID_$USER

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		// Check if Phone || email already exists
		if (doesEmailExist(spec.getEmail())) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS, BaseUserTableSpec.EMAIL);
		}

		if (doesPhoneExist(spec.getPhone())) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS, BaseUserTableSpec.PHONE);
		}

		// Verify roleName
		if (!getRolesModel().isRoleValid(role)) {
			throw new ResourceException(ResourceException.FAILED_VALIDATION);
		}

		// Create entry in Addresses
		String addressId = getLocationModel().newAddresss(null,
				new AddressSpec(spec.getAddress(), spec.getCity(), spec.getTerritory(), spec.getCountry()));

		Set<String> addressSet = new LinkedHashSet<>();
		addressSet.add(addressId);

		// Generate UserId
		Integer userId = nextKey();
		getMetricsModel().incrementInt(MetricKeys.USERS_COUNT_ARCHIVE);

		// Create entry in BaseUser Table

		Item item = new Item().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withString(BaseUserTableSpec.EMAIL, spec.getEmail())
				.withString(BaseUserTableSpec.PASS, spec.getPassword())
				.withStringSet(BaseUserTableSpec.ROLES, new String[] { role })
				.with(BaseUserTableSpec.DEFAULT_ROLE, role).withString(BaseUserTableSpec.FNAME, spec.getFname())
				.withString(BaseUserTableSpec.LNAME, spec.getLname()).withNull(BaseUserTableSpec.MNAME)

				.withLong(BaseUserTableSpec.PHONE, spec.getPhone()).withNull(BaseUserTableSpec.DOB)

				.withStringSet(BaseUserTableSpec.ADDRESSES, addressSet)
				.withString(BaseUserTableSpec.DATE_CREATED, Dates.currentDate());

		table.putItem(new PutItemSpec().withItem(item)
				.withConditionExpression("attribute_not_exists(" + BaseUserTableSpec.ID + ")"));

		// Update Addresses

		Table addressTable = StorageServiceFactory.getDocumentDatabase().getTable(AddressesSpec.TABLE_NAME);

		addressTable.updateItem(new UpdateItemSpec().withPrimaryKey(AddressesSpec.ID, addressId)
				.withUpdateExpression("SET #k = :v").withNameMap(new NameMap().with("#k", AddressesSpec.OWNER_ID))
				.withValueMap(new ValueMap().withString(":v", "u" + userId)));

		// Add name to Search Graph

		getSearchGraphModel().putEntry(new SearchGraphId(BaseUserTableSpec.TABLE_NAME, userId),
				GraphEntityType.PERSON_NAME,
				new String[] { spec.getFname() + " " + spec.getLname(), spec.getLname() + " " + spec.getFname() });

		// Add metrics

		getMetricsModel().putInt(MetricKeys.USER_NAME_CHANGE_COUNT_$USER_CURRENT.replace("$USER", userId.toString()),
				0);

		getMetricsModel().incrementInt(MetricKeys.USER_COUNT_TO_ROLE_$ROLE_CURRENT.replace("$ROLE", role));
		getMetricsModel().incrementInt(MetricKeys.USER_COUNT_TO_ROLE_$ROLE_TIMESERIES.replace("$ROLE", role));

		//

		getMetricsModel().incrementInt(MetricKeys.USERS_COUNT_CURRENT);
		getMetricsModel().incrementInt(MetricKeys.USERS_COUNT_TIMESERIES);

		return userId;

	}

	private static boolean doesEmailExist(String email) {

		Table baseUserTable = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		QuerySpec spec = new QuerySpec().withKeyConditionExpression("#k = :v")
				.withNameMap(new NameMap().with("#k", BaseUserTableSpec.EMAIL))
				.withValueMap(new ValueMap().withString(":v", email));

		return baseUserTable.getIndex(BaseUserTableSpec.EMAIL_INDEX).query(spec).firstPage().iterator().hasNext();
	}

	private static boolean doesPhoneExist(long phone) {

		Table baseUserTable = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		QuerySpec spec = new QuerySpec().withKeyConditionExpression("#k = :v")
				.withNameMap(new NameMap().with("#k", BaseUserTableSpec.PHONE))
				.withValueMap(new ValueMap().withLong(":v", phone));

		return baseUserTable.getIndex(BaseUserTableSpec.PHONE_INDEX).query(spec).firstPage().iterator().hasNext();
	}

	public UserProfile getProfile(Integer userId) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withProjectionExpression("#img,#em,#fn,#mn,#ln,#ph,#dob")
				.withNameMap(new NameMap().with("#img", BaseUserTableSpec.IMAGE).with("#em", BaseUserTableSpec.EMAIL)
						.with("#fn", BaseUserTableSpec.FNAME).with("#mn", BaseUserTableSpec.MNAME)
						.with("#ln", BaseUserTableSpec.LNAME).with("#ph", BaseUserTableSpec.PHONE)
						.with("#dob", BaseUserTableSpec.DOB)));

		if (item == null) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}

		UserProfile user = new UserProfile();
		user.setId(item.getInt(BaseUserTableSpec.ID));
		user.setEmail(item.getString(BaseUserTableSpec.EMAIL));
		user.setImage(item.getString(BaseUserTableSpec.IMAGE));
		user.setFname(item.getString(BaseUserTableSpec.FNAME));
		user.setMname(item.get(BaseUserTableSpec.MNAME));
		user.setLname(item.getString(BaseUserTableSpec.LNAME));
		user.setPhone(item.getLong(BaseUserTableSpec.PHONE));
		user.setDob(item.getString(BaseUserTableSpec.DOB));

		return user;
	}

	public UserContact getUserContact(Integer userId) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withProjectionExpression("#em,#fn,#ln,#ph")
				.withNameMap(new NameMap().with("#em", BaseUserTableSpec.EMAIL).with("#fn", BaseUserTableSpec.FNAME)
						.with("#ln", BaseUserTableSpec.LNAME).with("#ph", BaseUserTableSpec.PHONE)));

		if (item == null) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}

		UserContact user = new UserContact();
		user.setId(item.getInt(BaseUserTableSpec.ID));
		user.setEmail(item.getString(BaseUserTableSpec.EMAIL));
		user.setPhone(item.getLong(BaseUserTableSpec.PHONE));
		user.setFname(item.getString(BaseUserTableSpec.FNAME));
		user.setLname(item.getString(BaseUserTableSpec.LNAME));

		return user;
	}

	public List<String> getRoles(Integer userId) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withProjectionExpression("#roles").withNameMap(new NameMap().with("#roles", BaseUserTableSpec.ROLES)));

		if (item == null) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}

		return Lists.newArrayList(item.getStringSet(BaseUserTableSpec.ROLES));
	}

	public Map<String, AddressSpec> getAddresses(Integer userId) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);
		Item item = table.getItem(new GetItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withProjectionExpression("#adr").withNameMap(new NameMap().with("#adr", BaseUserTableSpec.ADDRESSES)));
		Set<String> addressIds = item.getStringSet(BaseUserTableSpec.ADDRESSES);
		return getLocationModel().getAddresses(addressIds);
	}

	public String getDefaultRole(Integer userId) {
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);
		Item item = table
				.getItem(new GetItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId).withProjectionExpression("#dr")
						.withNameMap(new NameMap().with("#dr", BaseUserTableSpec.DEFAULT_ROLE)));
		return item.getString(BaseUserTableSpec.DEFAULT_ROLE);
	}

	public String getDefaultAddress(Integer userId) {
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);
		Item item = table
				.getItem(new GetItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId).withProjectionExpression("#da")
						.withNameMap(new NameMap().with("#da", BaseUserTableSpec.DEFAULT_ADDRESS)));
		return item.getString(BaseUserTableSpec.DEFAULT_ADDRESS);
	}

	public String addAddress(Integer userId, AddressSpec address) {

		String addressId = getLocationModel().newAddresss("u" + userId, address);

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		table.updateItem(new UpdateItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withAttributeUpdate(new AttributeUpdate(BaseUserTableSpec.ADDRESSES).addElements(addressId)));

		return addressId;
	}

	public void removeAddress(Integer userId, String addressId) {

		if (!getLocationModel().getAddressOwner(addressId).equals("u" + userId)) {
			throw new ResourceException(ResourceException.UPDATE_NOT_ALLOWED);
		}

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		table.updateItem(new UpdateItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withAttributeUpdate(new AttributeUpdate(BaseUserTableSpec.ADDRESSES).removeElements(addressId)));

		getLocationModel().deleteAddress(addressId);

	}

	public Integer getNameChangeCount(Integer userId) {
		return getMetricsModel()
				.getInt(MetricKeys.USER_NAME_CHANGE_COUNT_$USER_CURRENT.replace("$USER", userId.toString()));
	}

	public void updateName(Integer userId, String lname) {

		if (!isNameChangeAllowed(userId)) {
			throw new ResourceException(ResourceException.UPDATE_NOT_ALLOWED);
		}

		Table baseUser = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		baseUser.updateItem(new UpdateItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withAttributeUpdate(new AttributeUpdate(BaseUserTableSpec.LNAME).put(lname)));

		incrementNameChangeCount(userId);
	}

	public boolean isNameChangeAllowed(Integer userId) {
		return (Integer) getConfigModel().get(
				Namespace.forAdmin().getKey(RegistryKeys.MAX_ALLOWED_LNAME_CHANGE_OPS)) < getNameChangeCount(userId);
	}

	private void incrementNameChangeCount(Integer userId) {
		getMetricsModel()
				.incrementInt(MetricKeys.USER_NAME_CHANGE_COUNT_$USER_CURRENT.replace("$USER", userId.toString()));
	}

	public void updateDefaultRole(Integer userId, String role) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		if (!getRolesModel().isRoleValid(role)) {
			throw new ResourceException(ResourceException.FAILED_VALIDATION);
		}

		table.updateItem(new UpdateItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withAttributeUpdate(new AttributeUpdate(BaseUserTableSpec.DEFAULT_ROLE).put(role)));
	}

	public void updateDefaultAddress(Integer userId, String addressId) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		// Verify address owner
		if (!getLocationModel().getAddressOwner(addressId).equals(userId)) {
			throw new ResourceException(ResourceException.FAILED_VALIDATION);
		}

		table.updateItem(new UpdateItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withAttributeUpdate(new AttributeUpdate(BaseUserTableSpec.DEFAULT_ADDRESS).put(addressId)));
	}

	public void updateEmail(Integer userId, String email) {
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		// Check if email already exists
		if (doesEmailExist(email)) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS, BaseUserTableSpec.EMAIL);
		}

		table.updateItem(new UpdateItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withAttributeUpdate(new AttributeUpdate(BaseUserTableSpec.EMAIL).put(email)));
	}

	public void updatePhone(Integer userId, long phone) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		// Check if Phone already exists
		if (doesPhoneExist(phone)) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS, BaseUserTableSpec.PHONE);
		}
		table.updateItem(new UpdateItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withAttributeUpdate(new AttributeUpdate(BaseUserTableSpec.PHONE).put(phone)));
	}

	public void updateImage(Integer userId, String blobId) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		String currentImage = table
				.getItem(new GetItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId).withProjectionExpression("#img")
						.withNameMap(new NameMap().with("#img", BaseUserTableSpec.IMAGE)))
				.getString(BaseUserTableSpec.IMAGE);

		if (currentImage != null) {
			getBlobStoreModel().delete(currentImage);
		}

		table.updateItem(new UpdateItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withAttributeUpdate(new AttributeUpdate(BaseUserTableSpec.IMAGE).put(blobId)));
	}

}
