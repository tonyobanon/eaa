package com.kylantis.eaa.core.users;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.AttributeUpdate;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.google.common.collect.Lists;
import com.kylantis.eaa.core.BlockerTodo;
import com.kylantis.eaa.core.FluentHashMap;
import com.kylantis.eaa.core.attributes.BaseUserTableSpec;
import com.kylantis.eaa.core.attributes.RoleDefinitionSpec;
import com.kylantis.eaa.core.base.Exceptions;
import com.kylantis.eaa.core.base.ObjectMarshaller;
import com.kylantis.eaa.core.base.ObjectUtils;
import com.kylantis.eaa.core.base.ResourceException;
import com.kylantis.eaa.core.base.StorageServiceFactory;
import com.kylantis.eaa.core.dbutils.DBTooling;
import com.kylantis.eaa.core.metrics.MetricKeys;
import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IRolesModel;

@ConcreteModel(dependencies = {})
public class RolesModel extends IRolesModel {

	@Override
	public void start() {

	}

	public void preInstall() {

		// Create Default Roles
		newRole("role.title.admin", RoleRealm.ADMIN, true);
		newRole("role.title.user", RoleRealm.CUSTOMER, true);

	};

	public void newRole(String name, RoleRealm realm) {
		newRole(name, realm, false);
	}

	/**
	 * The process of role creation is a two-step process. The first step involves
	 * calling this method. Then the second step is calling updateFunctionality(...)
	 */
	private void newRole(String name, RoleRealm realm, boolean isDefault) {

		Table rolesTable = StorageServiceFactory.getDocumentDatabase().getTable(RoleDefinitionSpec.TABLE_NAME);

		Item item = new Item().withPrimaryKey(RoleDefinitionSpec.NAME, name).withInt(RoleDefinitionSpec.REALM,
				realm.getRealm());

		try {

			rolesTable.putItem(new PutItemSpec().withItem(item)
					.withNameMap(new FluentHashMap<String, String>().with("#n", RoleDefinitionSpec.NAME))
					.withConditionExpression("attribute_not_exists(#n)"));

		} catch (ConditionalCheckFailedException ex) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS);
		}

		// Add Metrics
		getMetricsModel().putInt(MetricKeys.USER_COUNT_TO_ROLE_$ROLE_CURRENT.replace("$ROLE", name), 0);
	}

	public void deleteRole(String name) {

		if (isDefaultRole(name)) {
			// Not allowed
			throw new ResourceException(ResourceException.DELETE_NOT_ALLOWED);
		}

		if (getMetricsModel().getInt(MetricKeys.USER_COUNT_TO_ROLE_$ROLE_CURRENT.replace("$ROLE", name)) > 0) {
			// Accounts are still connected to this role
			throw new ResourceException(ResourceException.RESOURCE_STILL_IN_USE);
		}

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(RoleDefinitionSpec.TABLE_NAME);
		table.deleteItem(RoleDefinitionSpec.NAME, name);

		// Add Metric
		getMetricsModel().delete(MetricKeys.USER_COUNT_TO_ROLE_$ROLE_CURRENT.replace("$ROLE", name));
		getMetricsModel().delete(MetricKeys.USER_COUNT_TO_ROLE_$ROLE_TIMESERIES.replace("$ROLE", name));
	}

	public Map<String, RoleRealm> listRoles() {
		List<Item> result = DBTooling.scanTable(RoleDefinitionSpec.TABLE_NAME, "#n, #r",
				new NameMap().with("#n", RoleDefinitionSpec.NAME).with("#r", RoleDefinitionSpec.REALM));

		Map<String, RoleRealm> roles = new HashMap<>();

		result.forEach(i -> {
			roles.put(i.getString(RoleDefinitionSpec.NAME), RoleRealm.from(i.getInt(RoleDefinitionSpec.REALM)));
		});

		return roles;
	}

	public List<String> listRoles(RoleRealm realm) {

		String conditionExpr = "(#r = :rv)";

		Map<String, String> nameMap = new FluentHashMap<String, String>().with("#r", RoleDefinitionSpec.REALM)
				.with("#n", RoleDefinitionSpec.NAME);

		Map<String, Object> valueMap = new FluentHashMap<String, Object>().with(":rv", realm.getRealm());

		List<String> result = new ArrayList<>();

		DBTooling
				.queryIndex(StorageServiceFactory.getDocumentDatabase().getTable(RoleDefinitionSpec.TABLE_NAME)
						.getIndex(RoleDefinitionSpec.REALM_INDEX), "#n", nameMap, valueMap, conditionExpr)
				.forEach(i -> {
					result.add(i.getString(RoleDefinitionSpec.NAME));
				});

		return result;
	}

	@BlockerTodo("Use Bulk Updates instead, where necessary")
	public void updateFunctionality(String name, RoleUpdateAction action, String functionality) {

		if (isDefaultRole(name)) {
			// Not allowed
			throw new ResourceException(ResourceException.UPDATE_NOT_ALLOWED);
		}

		List<UpdateItemSpec> updates = new ArrayList<>();

		switch (action) {
		case ADD:
			updates.add(new UpdateItemSpec().withPrimaryKey(RoleDefinitionSpec.NAME, name).withAttributeUpdate(
					new AttributeUpdate(RoleDefinitionSpec.FUNCTIONALITIES).addElements(functionality)));
			break;
		case REMOVE:

			if (isDefaultRole(name)) {

				// Iterate all roles for this realm, and remove functionality
				listRoles(getRealm(name)).forEach(i -> {

					updates.add(new UpdateItemSpec().withPrimaryKey(RoleDefinitionSpec.NAME, i)
							.withAttributeUpdate(new AttributeUpdate(RoleDefinitionSpec.FUNCTIONALITIES)
									.removeElements(functionality)));
				});

			} else {

				updates.add(new UpdateItemSpec().withPrimaryKey(RoleDefinitionSpec.NAME, name).withAttributeUpdate(
						new AttributeUpdate(RoleDefinitionSpec.FUNCTIONALITIES).removeElements(functionality)));
			}

			break;
		}

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(RoleDefinitionSpec.TABLE_NAME);
		updates.forEach(spec -> {
			table.updateItem(spec);
		});
	}

	public List<String> getFunctionalities(String name) {
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(RoleDefinitionSpec.TABLE_NAME);

		Item item = table
				.getItem(new GetItemSpec().withPrimaryKey(RoleDefinitionSpec.NAME, name).withProjectionExpression("#f")
						.withNameMap(new NameMap().with("#f", RoleDefinitionSpec.FUNCTIONALITIES)));

		List<String> functionalities = Lists.newArrayList();

		item.getStringSet(RoleDefinitionSpec.FUNCTIONALITIES).forEach(o -> {
			functionalities.add(o);
		});

		return functionalities;
	}

	public RoleRealm getRealm(String name) {
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(RoleDefinitionSpec.TABLE_NAME);

		Item item = table.getItem(new GetItemSpec().withPrimaryKey(RoleDefinitionSpec.NAME, name)
				.withProjectionExpression("#r").withNameMap(new NameMap().with("#r", RoleDefinitionSpec.REALM)));
		return RoleRealm.from(item.getInt(RoleDefinitionSpec.REALM));
	}

	public List<String> getAvailableFunctionalities(RoleRealm realm) {
		return getFunctionalities(getDefaultRole(realm));
	}

	public String getDefaultRole(RoleRealm realm) {

		String conditionExpr = "(#r = :rv) and (#d = :dv)";

		Map<String, String> nameMap = new FluentHashMap<String, String>().with("#r", RoleDefinitionSpec.REALM)
				.with("#d", RoleDefinitionSpec.IS_DEFAULT).with("#n", RoleDefinitionSpec.NAME);

		Map<String, Object> valueMap = new FluentHashMap<String, Object>().with(":rv", realm.getRealm()).with(":dv",
				ObjectMarshaller.marshal(true));

		List<Item> result = DBTooling.queryIndex(StorageServiceFactory.getDocumentDatabase()
				.getTable(RoleDefinitionSpec.TABLE_NAME).getIndex(RoleDefinitionSpec.REALM_INDEX), "#n", nameMap,
				valueMap, conditionExpr);

		if (result.size() != 1) {
			Exceptions.throwRuntime(new RuntimeException(
					result.size() > 0 ? "Multiple" : "No" + " default roles was found for realm: " + realm.toString()));
		}

		return result.get(0).getString(RoleDefinitionSpec.NAME);
	}

	public boolean isDefaultRole(String name) {

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(RoleDefinitionSpec.TABLE_NAME);

		Item item = table.getItem(new GetItemSpec().withPrimaryKey(RoleDefinitionSpec.NAME, name)
				.withProjectionExpression("#d").withNameMap(new NameMap().with("#d", RoleDefinitionSpec.IS_DEFAULT)));

		if (item == null) {
			throw new ResourceException(ResourceException.RESOURCE_NOT_FOUND);
		}

		return item.getString(RoleDefinitionSpec.IS_DEFAULT).equals(ObjectMarshaller.marshal(true));
	}

	public boolean isRoleValid(String name) {

		if (ObjectUtils.isEmpty(name)) {
			return false;
		}

		Table table = StorageServiceFactory.getDocumentDatabase().getTable(RoleDefinitionSpec.TABLE_NAME);

		Item item = table.getItem(new GetItemSpec().withPrimaryKey(RoleDefinitionSpec.NAME, name));

		return item != null;
	}

	public void addRole(Integer userId, String role) {

		// Verify role
		if (!getRolesModel().isRoleValid(role)) {
			throw new ResourceException(ResourceException.FAILED_VALIDATION);
		}
		
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		try {

			table.updateItem(new UpdateItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
					.withAttributeUpdate(new AttributeUpdate(BaseUserTableSpec.ROLES).addElements(role))
					.withConditionExpression("attribute_not_exists (#roles.:role)")
					.withNameMap(new NameMap().with("#roles", BaseUserTableSpec.ROLES))
					.withValueMap(new ValueMap().withString(":role", role)));

		} catch (ConditionalCheckFailedException e) {
			throw new ResourceException(ResourceException.RESOURCE_ALREADY_EXISTS);
		}

		// Add to Metrics
		getMetricsModel().incrementInt(MetricKeys.USER_COUNT_TO_ROLE_$ROLE_CURRENT.replace("$ROLE", role));
		getMetricsModel()
				.incrementInt(MetricKeys.USER_COUNT_TO_ROLE_$ROLE_TIMESERIES.replace("$ROLE", role));
	}

	public void removeRole(Integer userId, String role) {

		// Verify role
		if (!getRolesModel().isRoleValid(role)) {
			throw new ResourceException(ResourceException.FAILED_VALIDATION);
		}
		Table table = StorageServiceFactory.getDocumentDatabase().getTable(BaseUserTableSpec.TABLE_NAME);

		table.updateItem(new UpdateItemSpec().withPrimaryKey(BaseUserTableSpec.ID, userId)
				.withAttributeUpdate(new AttributeUpdate(BaseUserTableSpec.ROLES).removeElements(role)));

		// Add to Metrics
		getMetricsModel().decrementInt(MetricKeys.USER_COUNT_TO_ROLE_$ROLE_CURRENT.replace("$ROLE", role));
	}

}
