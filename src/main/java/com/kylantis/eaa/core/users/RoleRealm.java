package com.kylantis.eaa.core.users;

public enum RoleRealm {

	CUSTOMER(1), ADMIN(0);

	private final Integer realm;

	private RoleRealm(int realm) {
		this.realm = realm;
	}

	public Integer getRealm() {
		return realm;
	}

	public static RoleRealm from(int realm) {
		switch (realm) {
		case 0:
			return ADMIN;
		case 1:
		default:
			return CUSTOMER;
		}
	}
	
	@Override
	public String toString() {
		switch (this) {
		case ADMIN:
			return "0";
		case CUSTOMER:
		default:
			return "1";
		}
	}
	
	public String prettyString() {
		switch (this) {
		case ADMIN:
			return "Admin";
		case CUSTOMER:
		default:
			return "Customer";
		}
	}

}
