package com.kylantis.eaa.core.users;

import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.ICustomerGroupModel;
import com.kylantis.eaa.core.ml.MLRepository;
import com.kylantis.eaa.core.setup.InstallOptions;

@ConcreteModel(dependencies = { MLRepository.CONFIG_MODEL, MLRepository.EXPRESSIONS_MODEL })
public class CustomerGroupModel extends ICustomerGroupModel {


	@Override
	public void install(InstallOptions options) {
	}

	@Override
	public void start() {
	}

}
