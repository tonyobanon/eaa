package com.kylantis.eaa.core.users;

import com.kylantis.eaa.core.base.Todo;

@Todo("Add field for Postal Code")
public class UserProfileSpec {

	public String email;
	public String password;
	
	public String fname;
	public String lname;
	
	public long phone;

	public String dob;

	public String address;
	
	public int city;
	public String territory;
	public String country;
	
	public UserProfileSpec() {
	}

	public UserProfileSpec(String email, String password, String fname, String lname, long phone, String dob,
			String address, int city, String territory, String country) {
		this.email = email;
		this.password = password;
		this.fname = fname;
		this.lname = lname;
		this.phone = phone;
		this.dob = dob;
		this.address = address;
		this.city = city;
		this.territory = territory;
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getCity() {
		return city;
	}

	public void setCity(int city) {
		this.city = city;
	}

	public String getTerritory() {
		return territory;
	}

	public void setTerritory(String territory) {
		this.territory = territory;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
}
