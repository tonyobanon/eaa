package com.kylantis.eaa.core.users;

public class UserProfile {

	private Integer id;

	private String email;
	private String image;
	
	private String fname;
	private Object mname;
	private String lname;
	
	private long phone;
	
	private String dob;

	public UserProfile() {
	}

	public UserProfile(Integer id, String email, String image, String fname, Object mname, String lname, long phone,
			String dob) {
		this.id = id;
		this.email = email;
		this.image = image;
		this.fname = fname;
		this.mname = mname;
		this.lname = lname;
		this.phone = phone;
		this.dob = dob;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public Object getMname() {
		return mname;
	}

	public void setMname(Object mname) {
		this.mname = mname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

}
