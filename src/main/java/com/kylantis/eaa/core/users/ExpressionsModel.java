package com.kylantis.eaa.core.users;

import com.kylantis.eaa.core.ml.ConcreteModel;
import com.kylantis.eaa.core.ml.IExpressionsModel;
import com.kylantis.eaa.core.ml.MLRepository;
import com.kylantis.eaa.core.setup.InstallOptions;

@ConcreteModel(dependencies = { MLRepository.CONFIG_MODEL })
public class ExpressionsModel extends IExpressionsModel {

	@Override
	public void install(InstallOptions options) {
		
	}
	
	@Override
	public void start() {
		
	}

	
}
